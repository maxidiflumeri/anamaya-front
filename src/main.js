import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import './bootstrap'
import './httpClient'
import store from './store/index'
import router from './routers.js'
import axios from 'axios'
import Meta from "vue-meta"
import JsonExcel from "vue-json-excel";
 
Vue.component("downloadExcel", JsonExcel);

Vue.use(Meta)

Vue.config.productionTip = false
axios.defaults.baseURL = process.env.VUE_APP_RUTA_API

new Vue({
  vuetify,
  store,
  router,
  render: h => h(App)
}).$mount('#app')
