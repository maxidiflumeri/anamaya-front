import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import '@fortawesome/fontawesome-free/css/all.css'
import es from 'vuetify/lib/locale/es'

Vue.use(Vuetify);


export default new Vuetify({
  lang: {
    locales: { es },
    current: 'es',
  },
  theme: {
    dark: false,
    themes: {      
      light: {
        primary: '#48A4AC',
        red: "#FF5c4E",
        white: "#FFFFFF",
        error: "#FF5252",
        info: "#2196F3",
        success: "#4CAF50",
        warning: "#FFC107"
      },
      dark: {
        primary: '#1E5F74',
        red: "#FF5c4E",
        white: "#FFFFFF",
        error: "#FF5252",
        info: "#2196F3",
        success: "#4CAF50",
        warning: "#FFC107",
        background: "#232323"
      }
    },
  },
});
