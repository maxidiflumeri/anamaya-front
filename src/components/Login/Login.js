import url from '../../urls/index.js'
// import urlUsuarios from '../../urls/usuarios.js'
import https from 'https';
import { mapActions } from 'vuex'
import download from 'downloadjs'

export default {
  name: 'login',
  components: {},
  props: [],
  data() {
    return {
      errorLogin: false,
      text: `Usuario o clave incorrecta`,
      usuarios: [],
      usuario: {
        password: '',
        email: ''
      },
      reveal: false,
      valid: true,
      // contrasena: '',
      contrasenaRules: [
        v => !!v || 'El campo contraseña es requerido',

      ],
      // correo: '',
      correo: '',
      password: '',
      correoRules: [
        v => !!v || 'El campo correo es requerido',
        v => /.+@.+\..+/.test(v) || 'El correo debe ser valido',
      ],
      select: null,
      // checkbox: false,
      formData: this.getDatosIniciales(),
      error: false,
      btnLoading: false

    }
  },

  computed: {

  },
  mounted() {

  },
  methods: {
    ...mapActions('auth', ['guardarToken', 'guardarRolUsuario']),

    async borrar() {
      try {
        let res = await this.axios.get('http://localhost:3000/pdf', {responseType: 'blob'})
        const content = res.headers['content-type'];
        download(res.data, 'pepe', content)
      }
      catch (error) {
        console.log(error)
      }
    },

    async validate() {
      this.btnLoading = true
      this.$refs.form.validate()
      try {
        let res = await this.axios.post(url.usuarios.urlLogin, this.usuario)
        this.guardarToken(res.data.tokenReturn)
        this.btnLoading = false
        this.$router.push({ path: '/PantallaPrincipal' })
      }
      catch (error) {
        console.log(error.response)
        if (error) {
          this.errorLogin = true
          this.btnLoading = false
        }
      }
    }
    ,

    getDatosIniciales() {
      return {
        email: '',
        password: ''
      }
    },

    getAxios() {
      const instance = this.axios.create({
        httpsAgent: new https.Agent({
          rejectUnauthorized: false
        })
      });
      return instance
    }
  }
}


