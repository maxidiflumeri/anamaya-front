import { mapActions, mapState } from 'vuex'

export default {
  name: 'logs-reportes',
  components: {},
  props: [],
  data () {
    return {
      cabeceras: [        
        { text: 'Fecha Inicio', value: 'fecha_inicio' },
        { text: 'Fecha Fin', value: 'fecha_fin' },
        { text: 'Tipo', value: 'id_tipo_transaccion' },
        { text: 'Reporte', value: 'reporte' },
        { text: 'Detalle', value: 'detalle' },
        { text: 'Usuario', value: 'nombre_completo' }
      ],
      dialog: false,
      detalleItem: '',
      search: '',
      transaccionesOk: [],
      loading: false,
    }
  },
  computed: {
    ...mapState('deudor', ['transacciones']),
    ...mapState('adminTipos', ['tiposTransaccion']),

  },
  mounted () {    
    this.get()

  },
  methods: {
    ...mapActions('deudor', ['obtenerTransacciones']),
    ...mapActions('adminTipos', ['obtenerTiposTransaccion']),

    armaDataTable() {      
      this.transaccionesOk = []
      for (let index = 0; index < this.transacciones.length; index++) {
        const element = this.transacciones[index];
        let nomReporte = this.tiposTransaccion.find(tipo => tipo.id_tipo_transaccion == element.id_tipo_transaccion).descripcion
        element['reporte'] = nomReporte
        this.transaccionesOk.push(element)
      }
      this.loading = false
    },

    async get() {
      this.loading = true
      await this.obtenerTransacciones('1000')
      await this.obtenerTiposTransaccion()
      this.armaDataTable()
    },


    getDate(datetime) {
      let date = datetime.substr(8, 2) + "/" + datetime.substr(5, 2) + "/" + datetime.substr(0, 4) + " " + datetime.substr(11, 8)      
      return date
    },
    mostrarDetalle(item){
      this.dialog = true
      this.detalleItem = item.detalle
    }
  }
}


