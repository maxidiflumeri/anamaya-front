import { mapState } from "vuex"
import { mapActions } from "vuex"
import url from '../../../urls/index.js'

export default {
  props: {
  },
  data: () => ({
    idllamada: 0,
    grabacion: '',
    formValid: false,
    switchDrop: false,
    btnLoading: false,
    itemsPorPagina: 5,
    dialog: false,
    dialogDelete: false,
    editedIndex: -1,
    modeloEditar: {
      calle: "",
      numero: "",
      piso: "",
      departamento: "",
      entrada: "",
      escalera: "",
      cod_postal: "",
      localidad: "",
      provincia: "",
      efectivo: "",
      id_tipo_direccion: ""
    },
    muestraMensaje: false,
    mensaje: "",
    colorMensaje: "",
    DireccionEliminado: {},
    reglas: [
      value => !!value || 'El campo es requerido!'
    ],
    reglasCodPos: [
      value => !!value || 'El campo es requerido!',
      value => (value && value.length >= 4) || 'Mínimo 4 caracteres',
      value => (value && value.length <= 8) || 'Maximo 8 caracteres permitidos'
    ],
    reglasNumero: [
      value => /^\d+$/.test(value) || 'Solo se permiten numeros'
    ],
    cabecera: [
      {
        text: "Calle",
        value: "calle"
      },
      {
        text: "Numero",
        value: "numero"
      },
      {
        text: "Piso",
        value: "piso"
      },
      {
        text: "Departamento",
        value: "departamento"
      },
      {
        text: "Entrada",
        value: "entrada"
      },
      {
        text: "Escalera",
        value: "escalera"
      },
      {
        text: "Codigo Postal",
        value: "cod_postal"
      },
      {
        text: "Localidad",
        value: "localidad"
      },
      {
        text: "Provincia",
        value: "provincia"
      },
      {
        text: "Principal",
        value: "efectivo"
      },
      {
        text: "Tipo Direccion",
        value: "id_tipo_direccion"
      },
      {
        text: 'Acciones',
        value: 'actions',
        sortable: false
      }
    ],

  }),

  computed: {
    formTitle() {
      return this.editedIndex === -1 ? 'Nueva dirección' : 'Edicion de dirección'
    },
    ...mapState('tiposDireccion', ['tiposDireccion']),
    ...mapState('auth', ['token']),
    ...mapState('deudor', ['direcciones', 'deudor']),
    ...mapState('tiposDireccion', ['tiposDireccion']),
    ...mapState('neotel', ['posicion']),

    validarAgregar() {
      if (this.modeloEditar.calle == '' || this.modeloEditar.localidad == '' || this.modeloEditar.provincia == '' || this.modeloEditar.cod_postal == '') {
        return false
      } else if ((!/^\d+$/.test(this.modeloEditar.numero) && this.modeloEditar.numero.length > 0) || this.modeloEditar.cod_postal.length < 4 || this.modeloEditar.cod_postal.length > 8) {
        return false
      } else {
        return true
      }
    }
  },

  watch: {
  },

  created() {
  },

  methods: {

    ...mapActions('deudor', ['obtenerDirecciones']),

    editItem(item) {
      this.editedIndex = this.direcciones.indexOf(item)
      this.modeloEditar.calle = item.calle
      this.modeloEditar.numero = item.numero
      this.modeloEditar.piso = item.piso
      this.modeloEditar.departamento = item.departamento
      this.modeloEditar.entrada = item.entrada
      this.modeloEditar.escalera = item.escalera
      this.modeloEditar.cod_postal = item.cod_postal
      this.modeloEditar.localidad = item.localidad
      this.modeloEditar.provincia = item.provincia
      this.modeloEditar.id_tipo_direccion = this.mostrarDescripcionTipoDireccion(item.id_tipo_direccion)
      if (item.efectivo == 1) {
        this.switchDrop = true
      } else {
        this.switchDrop = false
      }
      this.dialog = true
    },

    dialogEliminar(item) {
      this.DireccionEliminado = item
      this.dialogDelete = true
    },

    cerrarDialogEliminar() {
      this.dialogDelete = false,
        this.DireccionEliminado = {}
    },

    cerrarDialogGuardar() {
      this.modeloEditar = {
        calle: "",
        numero: "",
        piso: "",
        departamento: "",
        entrada: "",
        escalera: "",
        cod_postal: "",
        localidad: "",
        provincia: "",
        efectivo: "",
        id_tipo_direccion: ""
      }
      this.dialog = false
      this.editedIndex = -1
      this.$refs.form.resetValidation()
      this.switchDrop = false
    },

    async guardar() {
      if (this.editedIndex > -1) {
        await this.editarDireccion(this.direcciones[this.editedIndex].id_direccion)
      } else {
        await this.agregarDireccion()
      }
    },

    mostrarMensaje(mensaje, color) {
      this.muestraMensaje = true
      this.mensaje = mensaje
      this.colorMensaje = color
    },

    async editarDireccion(direccionViejo) {
      var tipoDireccion = ''
      var efectivo = ''
      if (this.modeloEditar.id_tipo_direccion.id_tipo_direccion) {
        tipoDireccion = this.modeloEditar.id_tipo_direccion.id_tipo_direccion
      } else {
        tipoDireccion = this.modeloEditar.id_tipo_direccion
      }
      if (this.switchDrop == true) {
        efectivo = 1
      } else if (this.switchDrop == false) {
        efectivo = 0
      }

      this.verificarDatosLlamada()

      const direccionPut = {
        calle: this.modeloEditar.calle,
        numero: this.modeloEditar.numero,
        piso: this.modeloEditar.piso,
        departamento: this.modeloEditar.departamento,
        entrada: this.modeloEditar.entrada,
        escalera: this.modeloEditar.escalera,
        cod_postal: this.modeloEditar.cod_postal,
        localidad: this.modeloEditar.localidad,
        provincia: this.modeloEditar.provincia,
        efectivo: efectivo,
        id_tipo_direccion: tipoDireccion,
        id_llamada: this.idllamada,
        grabacion: this.grabacion
      }
      try {
        this.btnLoading = true
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        await this.axios.put(url.direcciones.urlModificar + '/' + direccionViejo, direccionPut, configuracion)
        this.mostrarMensaje("Direccion modificada exitosamente.", "success")
        this.switchDrop = false
        this.cerrarDialogGuardar()
        await this.obtenerDirecciones(this.deudor.id_deudor)
      } catch (error) {
        console.log("Error POST: " + error)
        this.mostrarMensaje("Error al modificar la direccion.", "error")
      }
      this.btnLoading = false
    },

    async agregarDireccion() {
      var efectivo = ''
      if (this.switchDrop == true) {
        efectivo = 1
      } else if (this.switchDrop == false) {
        efectivo = 0
      }

      this.verificarDatosLlamada()

      const direccionPost = {
        id_deudor: this.deudor.id_deudor,
        calle: this.modeloEditar.calle,
        numero: this.modeloEditar.numero,
        piso: this.modeloEditar.piso,
        departamento: this.modeloEditar.departamento,
        entrada: this.modeloEditar.entrada,
        escalera: this.modeloEditar.escalera,
        cod_postal: this.modeloEditar.cod_postal,
        localidad: this.modeloEditar.localidad,
        provincia: this.modeloEditar.provincia,
        efectivo: efectivo,
        id_tipo_direccion: this.modeloEditar.id_tipo_direccion,
        id_llamada: this.idllamada,
        grabacion: this.grabacion
      }
      try {
        this.btnLoading = true
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        await this.axios.post(url.direcciones.urlAgregar, direccionPost, configuracion)
        this.mostrarMensaje("Direccion agregada exitosamente.", "success")
        this.switchDrop = false
        this.cerrarDialogGuardar()
        await this.obtenerDirecciones(this.deudor.id_deudor)
      } catch (error) {
        console.log("Error POST: " + error)
        this.mostrarMensaje("Error al agregar la direccion.", "error")
      }
      this.btnLoading = false
    },

    async eliminarDireccion() {
      this.verificarDatosLlamada()
      try {
        this.btnLoading = true
        let header = { 'token': this.token }
        let configuracion = {
          headers: header,
          data: {
            id_llamada: this.idllamada,
            grabacion: this.grabacion
          }
        }
        await this.axios.delete(url.direcciones.urlEliminar + '/' + this.DireccionEliminado.id_direccion, configuracion)
        this.cerrarDialogEliminar()
        this.mostrarMensaje("Direccion eliminada exitosamente.", "success")
        await this.obtenerDirecciones(this.deudor.id_deudor)
      } catch (error) {
        console.log("Error POST: " + error)
        this.mostrarMensaje("Error al eliminar la direccion.", "error")
      }
      this.btnLoading = false
    },

    verificarDatosLlamada() {
      if (this.posicion.data) {
        if (this.posicion.data.data != this.deudor.id_deudor) {
          this.idllamada = 0
          this.grabacion = ''
        } else {
          this.idllamada = this.posicion.data.idllamada
          this.grabacion = this.posicion.data.grabacion
        }
      } else {
        this.idllamada = 0
        this.grabacion = ''
      }
    },

    mostrarDescripcionTipoDireccion(id_tipo_direccion) {
      let td = this.tiposDireccion.find(dir => dir.id_tipo_direccion == id_tipo_direccion)
      return td
    },

    mostrarNombre(valor) {
      if (valor == "" || !valor) {
        return '-'
      } else {
        return valor
      }
    }
  },
}