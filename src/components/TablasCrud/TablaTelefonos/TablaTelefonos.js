import { mapState } from "vuex"
import { mapActions } from "vuex"
import telefonoApi from '../../../services/Apis/BackEnd/telefono'

export default {
  props: {
    cabecera: Array,
    datos: Array
  },
  data: () => ({
    idllamada: 0,
    grabacion: '',
    formValid: false,
    switchDrop: false,
    btnLoading: false,
    itemsPorPagina: 5,
    dialog: false,
    dialogDelete: false,
    editedIndex: -1,
    modeloEditar: {
      telefono: "",
      tipo_telefono: "",
      efectivo: ""
    },
    modeloDefault: {
      telefono: "",
      tipo_telefono: "",
      efectivo: ""
    },
    muestraMensaje: false,
    mensaje: "",
    colorMensaje: "",
    TelefonoEliminado: {},
    reglasTelefono: [
      value => !!value || 'El campo teléfono es requerido!',
      value => (value && value.length >= 8) || 'Mínimo 8 caracteres',
      value => (value && value.length <= 11) || 'Maximo 13 caracteres permitidos',
      value => /^\d+$/.test(value) || 'Solo se permiten numeros',
    ],
    reglas: [
      value => !!value || 'El campo es requerido!'
    ]

  }),

  computed: {
    formTitle() {
      return this.editedIndex === -1 ? 'Nuevo teléfono' : 'Edicion de teléfono'
    },
    ...mapState('tiposTelefono', ['tiposTelefono']),
    ...mapState('deudor', ['telefonos']),
    ...mapState('deudor', ['deudor']),
    ...mapState('auth', ['token']),
    ...mapState('neotel', ['posicion']),

    validarAgregar() {
      if (this.modeloEditar.telefono == '' || this.modeloEditar.tipo_telefono == '') {
        return false
      } else if (!/^\d+$/.test(this.modeloEditar.telefono) || this.modeloEditar.telefono.length <= 7 || this.modeloEditar.telefono.length >= 14) {
        return false
      } else {
        return true
      }
    }
  },

  watch: {
  },

  created() {
  },

  methods: {

    ...mapActions('deudor', ['obtenerTelefonos']),

    editItem(item) {
      this.editedIndex = this.datos.indexOf(item)
      this.modeloEditar.telefono = item.telefono
      this.modeloEditar.tipo_telefono = item.tipo_telefono
      if (item.efectivo == 1) {
        this.switchDrop = true
      } else {
        this.switchDrop = false
      }
      this.dialog = true
    },

    dialogEliminar(item) {
      this.TelefonoEliminado = item
      this.dialogDelete = true
    },

    cerrarDialogEliminar() {
      this.dialogDelete = false,
        this.TelefonoEliminado = {}
    },

    cerrarDialogGuardar() {
      this.modeloEditar = {
        telefono: "",
        tipo_telefono: "",
        efectivo: ""
      }
      this.dialog = false
      this.editedIndex = -1
      this.$refs.form.resetValidation()
      this.switchDrop = false
    },

    async guardar() {
      if (this.editedIndex > -1) {
        await this.editarTelefono(this.datos[this.editedIndex].telefono)
      } else {
        await this.agregarTelefono()
      }
    },

    mostrarMensaje(mensaje, color) {
      this.muestraMensaje = true
      this.mensaje = mensaje
      this.colorMensaje = color
    },

    async editarTelefono(telefonoViejo) {
      var tipoTelefono = ''
      var efectivo = ''
      if (this.modeloEditar.tipo_telefono.id_tipo_telefono) {
        tipoTelefono = this.modeloEditar.tipo_telefono.id_tipo_telefono
      } else {
        tipoTelefono = this.modeloEditar.tipo_telefono
      }
      if (this.switchDrop == true) {
        efectivo = 1
      } else if (this.switchDrop == false) {
        efectivo = 0
      }

      this.verificarDatosLlamada()

      const telefonoPut = {
        telefono: this.modeloEditar.telefono,
        efectivo: efectivo,
        id_tipo_telefono: tipoTelefono,
        id_llamada: this.idllamada,
        grabacion: this.grabacion
      }
      this.btnLoading = true
      var response = await telefonoApi.modificarTelefono(telefonoPut, telefonoViejo, this.deudor.id_deudor)
      if (response.data) {
        this.mostrarMensaje("Telefono modificado exitosamente.", "success")
        this.switchDrop = false
        this.cerrarDialogGuardar()
        await this.obtenerTelefonos(this.deudor.id_deudor)
        this.datos = this.telefonos
      } else if (response.error) {
        this.mostrarMensaje(response.error.response.data.mensaje, "error")
      }
      this.btnLoading = false
    },

    async agregarTelefono() {
      var efectivo = ''
      if (this.switchDrop == true) {
        efectivo = 1
      } else if (this.switchDrop == false) {
        efectivo = 0
      }

      this.verificarDatosLlamada()

      const telefonoPost = {
        id_deudor: this.deudor.id_deudor,
        telefono: this.modeloEditar.telefono,
        efectivo: efectivo,
        id_tipo_telefono: this.modeloEditar.tipo_telefono,
        id_llamada: this.idllamada,
        grabacion: this.grabacion
      }
      this.btnLoading = true
      var response = await telefonoApi.agregarTelefono(telefonoPost)
      if (response.data) {
        this.mostrarMensaje("Telefono agregado exitosamente.", "success")
        this.switchDrop = false
        this.cerrarDialogGuardar()
        await this.obtenerTelefonos(this.deudor.id_deudor)
        this.datos = this.telefonos
      } else if (response.error) {
        this.mostrarMensaje(response.error.response.data.mensaje, "error")
      }
      this.btnLoading = false
    },

    async eliminarTelefono() {
      this.verificarDatosLlamada()
      this.btnLoading = true
      var response = await telefonoApi.eliminarTelefono(this.TelefonoEliminado.telefono, this.deudor.id_deudor, this.idllamada, this.grabacion)
      if (response.data) {
        this.cerrarDialogEliminar()
        this.mostrarMensaje("Telefono eliminado exitosamente.", "success")
        await this.obtenerTelefonos(this.deudor.id_deudor)
        this.datos = this.telefonos
      } else if (response.error) {
        this.mostrarMensaje(response.error.response.data.mensaje, "error")
      }
      this.btnLoading = false
    },

    verificarDatosLlamada() {
      if (this.posicion.data) {
        if (this.posicion.data.data != this.deudor.id_deudor) {
          this.idllamada = 0
          this.grabacion = ''
        } else {
          this.idllamada = this.posicion.data.idllamada
          this.grabacion = this.posicion.data.grabacion
        }
      } else {
        this.idllamada = 0
        this.grabacion = ''
      }
    },
  },
}