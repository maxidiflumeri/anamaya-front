import { mapState } from "vuex"
import { mapActions } from "vuex"
import url from '../../../urls/index.js'

export default {
  props: {
  },
  data: () => ({
    idllamada: 0,
    grabacion: '',
    btnLoading: false,
    itemsPorPagina: 5,
    dialog: false,
    dialogDelete: false,
    editedIndex: -1,
    modeloEditar: {
      nombre: "",
      id_tipo_parentesco: "",
      telefono: "",
      correo: ""
    },
    muestraMensaje: false,
    mensaje: "",
    colorMensaje: "",
    VinculoEliminado: {},
    cabecera: [
      {
        text: "Nombre",
        value: "nombre"
      },
      {
        text: "Tipo Vinculo",
        value: "id_tipo_parentesco"
      },
      {
        text: "Telefono",
        value: "telefono"
      },
      {
        text: "Correo",
        value: "correo"
      },
      {
        text: 'Acciones',
        value: 'actions',
        sortable: false
      }
    ],
    reglasTelefono: [
      value => !!value || 'El campo teléfono o correo es requerido!',
      value => (value && value.length >= 8) || 'Mínimo 8 caracteres',
      value => (value && value.length <= 11) || 'Maximo 13 caracteres permitidos',
      value => /^\d+$/.test(value) || 'Solo se permiten numeros',
    ],
    reglasCorreo: [
      value => !!value || 'El campo teléfono o correo es requerido!',
      value => /^(([^<>()[\]\\.,;:\s@']+(\.[^<>()\\[\]\\.,;:\s@']+)*)|('.+'))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value) || 'Solo se permiten correos electronicos validos'
    ],
    reglas: [
      value => !!value || 'El campo es requerido!'
    ],

  }),

  computed: {
    formTitle() {
      return this.editedIndex === -1 ? 'Nuevo vinculo' : 'Edicion de vinculo'
    },
    ...mapState('tiposVinculo', ['tiposVinculo']),
    ...mapState('deudor', ['vinculos']),
    ...mapState('deudor', ['deudor']),
    ...mapState('auth', ['token']),
    ...mapState('neotel', ['posicion']),

    validarAgregar() {
      if (this.modeloEditar.nombre == '' || this.modeloEditar.id_tipo_parentesco == '' || (this.modeloEditar.telefono == '' && this.modeloEditar.correo == '')) {
        return false
      } else if ((!/^\d+$/.test(this.modeloEditar.telefono) || this.modeloEditar.telefono.length <= 7 || this.modeloEditar.telefono.length >= 14) && this.modeloEditar.telefono != '') {
        return false
      } else if (!/^(([^<>()[\]\\.,;:\s@']+(\.[^<>()\\[\]\\.,;:\s@']+)*)|('.+'))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(this.modeloEditar.correo) && this.modeloEditar.correo != '') {
        return false
      } else {
        return true
      }
    }
  },

  watch: {
  },

  created() {
  },

  methods: {

    ...mapActions('deudor', ['obtenerVinculos']),

    editItem(item) {
      this.editedIndex = this.vinculos.indexOf(item)
      this.modeloEditar.nombre = item.nombre
      this.modeloEditar.telefono = item.telefono
      this.modeloEditar.correo = item.correo
      this.modeloEditar.id_tipo_parentesco = this.mostrarDescripcionTipoVinculo(item.id_tipo_parentesco)
      this.dialog = true
    },

    dialogEliminar(item) {
      this.VinculoEliminado = item
      this.dialogDelete = true
    },

    cerrarDialogEliminar() {
      this.dialogDelete = false,
        this.VinculoEliminado = {}
    },

    cerrarDialogGuardar() {
      this.modeloEditar = {
        nombre: "",
        id_tipo_parentesco: "",
        telefono: "",
        correo: ""
      }
      this.dialog = false
      this.editedIndex = -1
    },

    async guardar() {
      if (this.editedIndex > -1) {
        await this.editarVinculo(this.vinculos[this.editedIndex].id_vinculo)
      } else {
        await this.agregarVinculo()
      }
    },

    mostrarMensaje(mensaje, color) {
      this.muestraMensaje = true
      this.mensaje = mensaje
      this.colorMensaje = color
    },

    async editarVinculo(id_vinculo) {
      var tipoVinculo = ''
      if (this.modeloEditar.id_tipo_parentesco.id_tipo_parentesco) {
        tipoVinculo = this.modeloEditar.id_tipo_parentesco.id_tipo_parentesco
      } else {
        tipoVinculo = this.modeloEditar.id_tipo_parentesco
      }

      this.verificarDatosLlamada()

      const vinculoPut = {
        nombre: this.modeloEditar.nombre,
        telefono: this.modeloEditar.telefono,
        correo: this.modeloEditar.correo,
        id_tipo_parentesco: tipoVinculo,
        id_llamada: this.idllamada,
        grabacion: this.grabacion
      }
      try {
        this.btnLoading = true
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        await this.axios.put(url.vinculos.urlModificar + '/' + id_vinculo, vinculoPut, configuracion)
        this.mostrarMensaje("Vinculo modificado exitosamente.", "success")
        this.cerrarDialogGuardar()
        await this.obtenerVinculos(this.deudor.id_deudor)
      } catch (error) {
        console.log("Error POST: " + error)
        this.mostrarMensaje("Error al modificar el telefono.", "error")
      }
      this.btnLoading = false
    },

    async agregarVinculo() {
      this.verificarDatosLlamada()
      const vinculoPost = {
        id_deudor: this.deudor.id_deudor,
        nombre: this.modeloEditar.nombre,
        telefono: this.modeloEditar.telefono,
        correo: this.modeloEditar.correo,
        id_tipo_parentesco: this.modeloEditar.id_tipo_parentesco,
        id_llamada: this.idllamada,
        grabacion: this.grabacion
      }
      try {
        this.btnLoading = true
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        await this.axios.post(url.vinculos.urlAgregar, vinculoPost, configuracion)
        this.mostrarMensaje("Vinculo agregado exitosamente.", "success")
        this.cerrarDialogGuardar()
        await this.obtenerVinculos(this.deudor.id_deudor)
      } catch (error) {
        console.log("Error POST: " + error)
        this.mostrarMensaje("Error al agregar el telefono.", "error")
      }
      this.btnLoading = false
    },

    async eliminarVinculo() {
      this.verificarDatosLlamada()
      try {
        this.btnLoading = true
        let header = { 'token': this.token }
        let configuracion = {
          headers: header,
          data: {
            id_llamada: this.idllamada,
            grabacion: this.grabacion
          }
        }
        await this.axios.delete(url.vinculos.urlEliminar + '/' + this.VinculoEliminado.id_vinculo, configuracion)
        this.cerrarDialogEliminar()
        this.mostrarMensaje("Vinculo eliminado exitosamente.", "success")
        await this.obtenerVinculos(this.deudor.id_deudor)
      } catch (error) {
        console.log("Error POST: " + error)
        this.mostrarMensaje("Error al eliminar el telefono.", "error")
      }
      this.btnLoading = false
    },

    verificarDatosLlamada() {
      if (this.posicion.data) {
        if (this.posicion.data.data != this.deudor.id_deudor) {
          this.idllamada = 0
          this.grabacion = ''
        } else {
          this.idllamada = this.posicion.data.idllamada
          this.grabacion = this.posicion.data.grabacion
        }
      } else {
        this.idllamada = 0
        this.grabacion = ''
      }
    },

    mostrarDescripcionTipoVinculo(id_tipo_parentesco) {
      let tv = this.tiposVinculo.find(vin => vin.id_tipo_parentesco == id_tipo_parentesco)
      return tv
    },
  },
}