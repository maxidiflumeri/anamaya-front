import { mapState } from "vuex"
import { mapActions } from "vuex"
import url from '../../../urls/index.js'

export default {
  props: {
    cabecera: Array,
    datos: Array
  },
  data: () => ({
    idllamada: 0,
    grabacion: '',
    formValid: false,
    switchDrop: false,
    btnLoading: false,
    itemsPorPagina: 5,
    dialog: false,
    dialogDelete: false,
    editedIndex: -1,
    modeloEditar: {
      correo: "",
      tipo_correo: "",
      efectivo: ""
    },
    modeloDefault: {
      correo: "",
      tipo_correo: "",
      efectivo: ""
    },
    muestraMensaje: false,
    mensaje: "",
    colorMensaje: "",
    CorreoEliminado: {},
    reglasCorreo: [
      value => !!value || 'El campo Correo electrónico es requerido!',
      value => /^(([^<>()[\]\\.,;:\s@']+(\.[^<>()\\[\]\\.,;:\s@']+)*)|('.+'))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value) || 'Solo se permiten correos electronicos validos',
    ],
    reglas: [
      value => !!value || 'El campo es requerido!'
    ]
  }),

  computed: {
    formTitle() {
      return this.editedIndex === -1 ? 'Nuevo correo' : 'Edicion de correo'
    },
    ...mapState('tiposCorreo', ['tiposCorreo']),
    ...mapState('deudor', ['correos']),
    ...mapState('deudor', ['deudor']),
    ...mapState('auth', ['token']),
    ...mapState('neotel', ['posicion']),

    validarAgregar() {
      if (this.modeloEditar.correo == '' || this.modeloEditar.tipo_correo == '') {
        return false
      } else if (!/^(([^<>()[\]\\.,;:\s@']+(\.[^<>()\\[\]\\.,;:\s@']+)*)|('.+'))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(this.modeloEditar.correo)) {
        return false
      } else {
        return true
      }
    }
  },

  watch: {
  },

  created() {
  },

  methods: {

    ...mapActions('deudor', ['obtenerCorreos']),

    editItem(item) {
      this.editedIndex = this.datos.indexOf(item)
      this.modeloEditar.correo = item.correo
      this.modeloEditar.tipo_correo = item.tipo_correo
      if (item.efectivo == 1) {
        this.switchDrop = true
      } else {
        this.switchDrop = false
      }
      this.dialog = true
    },

    dialogEliminar(item) {
      this.CorreoEliminado = item
      this.dialogDelete = true
    },

    cerrarDialogEliminar() {
      this.dialogDelete = false,
        this.CorreoEliminado = {}
    },

    cerrarDialogGuardar() {
      this.modeloEditar = {
        correo: "",
        tipo_correo: "",
        efectivo: ""
      }
      this.dialog = false
      this.editedIndex = -1
      this.$refs.form.resetValidation()
      this.switchDrop = false
    },

    async guardar() {
      if (this.editedIndex > -1) {
        await this.editarCorreo(this.datos[this.editedIndex].correo)
      } else {
        await this.agregarCorreo()
      }
    },

    mostrarMensaje(mensaje, color) {
      this.muestraMensaje = true
      this.mensaje = mensaje
      this.colorMensaje = color
    },

    async editarCorreo(correoViejo) {
      var tipoCorreo = ''
      var efectivo = ''
      if (this.modeloEditar.tipo_correo.id_tipo_correo) {
        tipoCorreo = this.modeloEditar.tipo_correo.id_tipo_correo
      } else {
        tipoCorreo = this.modeloEditar.tipo_correo
      }
      if (this.switchDrop == true) {
        efectivo = 1
      } else if (this.switchDrop == false) {
        efectivo = 0
      }

      this.verificarDatosLlamada()

      const correoPut = {
        correo: this.modeloEditar.correo,
        efectivo: efectivo,
        id_tipo_correo: tipoCorreo,
        id_llamada: this.idllamada,
        grabacion: this.grabacion
      }
      try {
        this.btnLoading = true
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        await this.axios.put(url.correos.urlModificar + '/' + this.deudor.id_deudor + '/' + correoViejo, correoPut, configuracion)
        this.mostrarMensaje("Correo electrónico modificado exitosamente.", "success")
        this.switchDrop = false
        this.cerrarDialogGuardar()
        await this.obtenerCorreos(this.deudor.id_deudor)
        this.datos = this.correos
      } catch (error) {
        console.log("Error POST: " + error)
        this.mostrarMensaje("Error al modificar Correo electrónico.", "error")
      }
      this.btnLoading = false
    },

    async agregarCorreo() {
      var efectivo = ''
      if (this.switchDrop == true) {
        efectivo = 1
      } else if (this.switchDrop == false) {
        efectivo = 0
      }

      this.verificarDatosLlamada()

      const correoPost = {
        id_deudor: this.deudor.id_deudor,
        correo: this.modeloEditar.correo,
        efectivo: efectivo,
        id_tipo_correo: this.modeloEditar.tipo_correo,
        id_llamada: this.idllamada,
        grabacion: this.grabacion
      }
      try {
        this.btnLoading = true
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        await this.axios.post(url.correos.urlAgregar, correoPost, configuracion)
        this.mostrarMensaje("Correo electrónico agregado exitosamente.", "success")
        this.switchDrop = false
        this.cerrarDialogGuardar()
        await this.obtenerCorreos(this.deudor.id_deudor)
        this.datos = this.correos
      } catch (error) {
        console.log("Error POST: " + error)
        this.mostrarMensaje("Error al agregar Correo electrónico.", "error")
      }
      this.btnLoading = false
    },

    verificarDatosLlamada() {
      if (this.posicion.data) {
        if (this.posicion.data.data != this.deudor.id_deudor) {
          this.idllamada = 0
          this.grabacion = ''
        } else {
          this.idllamada = this.posicion.data.idllamada
          this.grabacion = this.posicion.data.grabacion
        }
      } else {
        this.idllamada = 0
        this.grabacion = ''
      }
    },

    async eliminarCorreo() {
      this.verificarDatosLlamada()
      try {
        this.btnLoading = true
        let header = { 'token': this.token }
        let configuracion = {
          headers: header,
          data: {
            id_llamada: this.idllamada,
            grabacion: this.grabacion
          }
        }
        await this.axios.delete(url.correos.urlEliminar + '/' + this.deudor.id_deudor + '/' + this.CorreoEliminado.correo, configuracion)
        this.cerrarDialogEliminar()
        this.mostrarMensaje("Correo electrónico eliminado exitosamente.", "success")
        await this.obtenerCorreos(this.deudor.id_deudor)
        this.datos = this.correos
      } catch (error) {
        console.log("Error POST: " + error)
        this.mostrarMensaje("Error al eliminar Correo electrónico.", "error")
      }
      this.btnLoading = false
    }
  },
}