import { mapState } from "vuex"
import { mapActions } from "vuex"
import url from '../../../urls/index.js'

export default {
  props: {
    cabecera: Array,
    datos: Array
  },
  data: () => ({
    idllamada: 0,
    grabacion: '',
    formValid: false,
    btnLoading: false,
    itemsPorPagina: 5,
    dialog: false,
    dialogDelete: false,
    editedIndex: -1,
    modeloEditar: {
      usuario: "",
      id_red_social: ""
    },
    modeloDefault: {
      usuario: "",
      id_red_social: ""
    },
    muestraMensaje: false,
    mensaje: "",
    colorMensaje: "",
    RedEliminado: {},
    reglas: [
      value => !!value || 'El campo es requerido!'
    ],

  }),

  computed: {
    formTitle() {
      return this.editedIndex === -1 ? 'Nuevo usuario de red social' : 'Edicion de usuario de red social'
    },
    ...mapState('tiposRed', ['tiposRed']),
    ...mapState('deudor', ['redes']),
    ...mapState('deudor', ['deudor']),
    ...mapState('auth', ['token']),
    ...mapState('neotel', ['posicion']),

    validarAgregar() {
      if (this.modeloEditar.usuario == '' || this.modeloEditar.id_red_social == '') {
        return false
      } else {
        return true
      }
    }
  },

  watch: {
  },

  created() {
  },

  methods: {

    ...mapActions('deudor', ['obtenerRedes']),

    editItem(item) {
      this.editedIndex = this.datos.indexOf(item)
      this.modeloEditar.usuario = item.usuario
      this.modeloEditar.id_red_social = item.id_red_social
      this.dialog = true
    },

    dialogEliminar(item) {
      this.RedEliminado = item
      this.dialogDelete = true
    },

    cerrarDialogEliminar() {
      this.dialogDelete = false,
        this.RedEliminado = {}
    },

    cerrarDialogGuardar() {
      this.modeloEditar = {
        usuario: "",
        id_red_social: ""
      }
      this.dialog = false
      this.editedIndex = -1
      this.$refs.form.resetValidation()
    },

    async guardar() {
      if (this.editedIndex > -1) {
        await this.editarUsuarioRed(this.datos[this.editedIndex].usuario, this.datos[this.editedIndex].id_red_social)
      } else {
        await this.agregarUsuarioRed()
      }
    },

    mostrarMensaje(mensaje, color) {
      this.muestraMensaje = true
      this.mensaje = mensaje
      this.colorMensaje = color
    },

    async editarUsuarioRed(usuarioViejo, tipoRedViejo) {
      var tipoRed = ''
      if (this.modeloEditar.id_red_social.id_red_social) {
        tipoRed = this.modeloEditar.id_red_social.id_red_social
      } else {
        tipoRed = this.modeloEditar.id_red_social
      }
      this.verificarDatosLlamada()
      const usuarioPut = {
        usuario: this.modeloEditar.usuario,
        id_red_social: tipoRed,
        id_llamada: this.idllamada,
        grabacion: this.grabacion
      }
      try {
        this.btnLoading = true
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        await this.axios.put(url.redesSociales.urlModificar + '/' + this.deudor.id_deudor + '/' + usuarioViejo + '/' + tipoRedViejo.id_red_social, usuarioPut, configuracion)
        this.mostrarMensaje("Usuario modificado exitosamente.", "success")
        this.cerrarDialogGuardar()
        await this.obtenerRedes(this.deudor.id_deudor)
        this.datos = this.redes
      } catch (error) {
        console.log("Error POST: " + error)
        this.mostrarMensaje("Error al modificar el usuario.", "error")
      }
      this.btnLoading = false
    },

    async agregarUsuarioRed() {
      this.verificarDatosLlamada()
      const usuarioPost = {
        id_deudor: this.deudor.id_deudor,
        usuario: this.modeloEditar.usuario,
        id_red_social: this.modeloEditar.id_red_social,
        id_llamada: this.idllamada,
        grabacion: this.grabacion
      }
      try {
        this.btnLoading = true
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        await this.axios.post(url.redesSociales.urlAgregar, usuarioPost, configuracion)
        this.mostrarMensaje("Usuario agregado exitosamente.", "success")
        this.cerrarDialogGuardar()
        await this.obtenerRedes(this.deudor.id_deudor)
        this.datos = this.redes
      } catch (error) {
        console.log("Error POST: " + error)
        this.mostrarMensaje("Error al agregar el usuario.", "error")
      }
      this.btnLoading = false
    },

    async eliminarUsuarioRed() {
      this.verificarDatosLlamada()
      try {
        this.btnLoading = true
        let header = { 'token': this.token }
        let configuracion = {
          headers: header,
          data: {
            id_llamada: this.idllamada,
            grabacion: this.grabacion
          }
        }
        await this.axios.delete(url.redesSociales.urlEliminar + '/' + this.deudor.id_deudor + '/' + this.RedEliminado.usuario + '/' + this.RedEliminado.id_red_social.id_red_social, configuracion)
        this.cerrarDialogEliminar()
        this.mostrarMensaje("Usuario eliminado exitosamente.", "success")
        await this.obtenerRedes(this.deudor.id_deudor)
        this.datos = this.redes
      } catch (error) {
        console.log("Error POST: " + error)
        this.mostrarMensaje("Error al eliminar el usuario.", "error")
      }
      this.btnLoading = false
    },

    verificarDatosLlamada() {
      if (this.posicion.data) {
        if (this.posicion.data.data != this.deudor.id_deudor) {
          this.idllamada = 0
          this.grabacion = ''
        } else {
          this.idllamada = this.posicion.data.idllamada
          this.grabacion = this.posicion.data.grabacion
        }
      } else {
        this.idllamada = 0
        this.grabacion = ''
      }
    },
  },
}