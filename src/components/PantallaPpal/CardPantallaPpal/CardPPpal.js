import { mapState } from 'vuex'

export default {

    name: 'CardPPal',

    props: {
        producto: Object,
        idGrupo: String
    },
    data() {
        return {            
        }
    },

    methods: {
        getRuta() {
            let ruta = `/CrmMenu/${this.primerDeudor}/CrmGestion`
            if (this.producto.idGrupo !== 1) {
                ruta = { name: this.producto.path, params: { idGrupo: this.producto.idGrupo } }
            }
            return ruta
        },
    },

    computed: {
        ...mapState('deudores', ['primerDeudor'])
    },

}