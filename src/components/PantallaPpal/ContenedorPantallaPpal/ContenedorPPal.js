import CardPPal from "../CardPantallaPpal/index"
import Loader from "../loaderPPal.vue"
import { mapState, mapActions, mapGetters } from "vuex"

export default {

    name: 'ContenedorPPal',

    components: {
        CardPPal,
        Loader
    },

    data() {
        return {
            opcionBuscada: '',
            page: 1,  // valor inicial paginacion items
            nroItems: null  // cantidad ded items que puede ver el usuario activo 
        }
    },

    mounted() {
        this.obtenerGrupos()
        this.obtenerPrimerDeudor()
    },

    methods: {

        ...mapActions('operaciones', ['obtenerGrupos', 'cerrarError']),
        ...mapActions('deudores', ['obtenerPrimerDeudor']),
        ...mapGetters('operaciones', ['getImg']),

        opcionesPorIdRol() {
            if (this.operaciones.grupos) {
                this.nroItems = this.operaciones.grupos.filter(grupo => grupo.idRol <= this.auth.usuario.rol).length
                return this.operaciones.grupos.filter(grupo => grupo.idRol <= this.auth.usuario.rol)
            }

        },

        rmAcentos(str) {
            return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
        }

    },

    computed: {

        ...mapState(['operaciones', 'auth']),

        theme() {
            return (this.$vuetify.theme.dark) ? 'dark' : 'light'
        },

        opcionesFiltradas() {
            let opciones = this.opcionesPorIdRol()
            if (opciones) {
                let accion1 = opciones.slice((this.page - 1) * 6, (this.page) * 6)
                let accion2 = opciones.filter(grupo => this.rmAcentos(grupo.nombreGrupo).toLowerCase().match(this.rmAcentos(this.opcionBuscada).toLowerCase())).slice((this.page - 1) * 6, (this.page) * 6)
                return (this.opcionBuscada === '') ? accion1 : accion2
            }
        },

        nroPaginas() {
            return Math.round((this.nroItems + 2) / 6)
        }

    }

}