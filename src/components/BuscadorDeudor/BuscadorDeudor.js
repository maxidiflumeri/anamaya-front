//import url from '../../urls/index.js'
//import https from 'https';
import { mapActions } from 'vuex'

export default {
  name: 'buscador-deudor',
  components: {},
  props: [],
  data() {
    return {
      cargando: false,
      dialog: false,
      noResult: true,
      listaDeudores: [],
      mostrarTabla: false,
      empresas: [],
      empresa: {},
      deudor: {},
      switchDrop: false,
      radioGroup: 2,
      input: "",
      listaRadio: [
        { title: 'Número de deudor' },
        { title: 'Número de cliente' },
        { title: 'Número de documento' },
        { title: 'Nombre y apellido' },
        { title: 'Número de teléfono' },
        { title: 'Correo electrónico' },
        { title: 'Usuario red social' },
      ],
      rules: [],
      listaRules: [
        [
          value => !!value || 'El campo Número de deudor es requerido!',
          //value => (value && value.length <= 10) || 'Maximo 10 caracteres permitidos',
          value => /^\d+$/.test(value) || 'Solo se permiten numeros',
        ],
        [
          value => !!value || 'El campo Número de cliente es requerido!'
        ],
        [
          value => !!value || 'El campo Número de documento es requerido!',
          value => (value && value.length >= 8) || 'Mínimo 8 caracteres',
          value => (value && value.length <= 11) || 'Maximo 11 caracteres permitidos',
          value => /^\d+$/.test(value) || 'Solo se permiten numeros',
        ],
        [
          value => !!value || 'El campo Nombre y apellido es requerido!',
          value => (value && value.length <= 50) || 'Maximo 50 caracteres permitidos',
          value => /^[a-zA-ZÀ-ž\s]*$/.test(value) || 'No se permiten nombres con numeros',
        ],
        [
          value => !!value || 'El campo Número de teléfono es requerido!',
          value => (value && value.length <= 20) || 'Maximo 20 caracteres permitidos',
          value => /^\d+$/.test(value) || 'Solo se permiten numeros',
        ],
        [
          value => !!value || 'El campo Correo electrónico es requerido!',
          value => /^(([^<>()[\]\\.,;:\s@']+(\.[^<>()\\[\]\\.,;:\s@']+)*)|('.+'))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value) || 'Solo se permiten correos electronicos validos',
        ],
        [
          value => !!value || 'El campo Usuario red social es requerido!',
          value => (value && value.length <= 30) || 'Maximo 30 caracteres permitidos',
        ],
      ],
      inputLabel: "",

      headers: [
        {
          text: 'Nro deudor',
          align: 'start',
          value: 'nroDeudor'
        },
        { text: 'Empresa', value: 'empresa' },
        { text: 'Nombre y apellido', value: 'nyp' },
        { text: 'Nro Cliente', value: 'nroCliente' },
        { text: 'DNI', value: 'dni' },
        { text: 'Nro Remesa', value: 'nroRemesa' },
      ],
      resultadoBusqueda: [],
      tam: 1200,
    }
  },
  computed: {
    descripcionEmp() {
      let aux = this.empresas.map(emp => {
        return emp.descripcion
      })
      return aux.sort()
    },

    validarBusqueda() {
      if (this.inputLabel == "Número de deudor") {
        if (!/^\d+$/.test(this.input) || this.input == '') {
          return false
        } else {
          return true
        }
      }
      if (this.inputLabel == "Número de cliente") {
        if (this.input == '') {
          return false
        } else {
          return true
        }
      }
      else if (this.inputLabel == "Número de documento") {
        if (!/^\d+$/.test(this.input) || this.input == '' || this.input.length <= 7 || this.input.length >= 12) {
          return false
        } else {
          return true
        }
      }
      else if (this.inputLabel == "Nombre y apellido") {
        if (!/^[a-zA-ZÀ-ž\s]*$/.test(this.input) || this.input == '') {
          return false
        } else {
          return true
        }
      }
      else if (this.inputLabel == "Número de teléfono") {
        if (!/^\d+$/.test(this.input) || this.input == '' || this.input.length >= 15) {
          return false
        } else {
          return true
        }
      }
      else if (this.inputLabel == "Correo electrónico") {
        if (!/^(([^<>()[\]\\.,;:\s@']+(\.[^<>()\\[\]\\.,;:\s@']+)*)|('.+'))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(this.input) || this.input == '') {
          return false
        } else {
          return true
        }
      }
      else if (this.inputLabel == "Usuario red social") {
        if (this.input == '' || this.input.length >= 31) {
          return false
        } else {
          return true
        }
      }
    },

    mostrarPorTam() {
      if (this.tam >= 768) {
        return false
      }
      else {
        return true
      }
    }

  },
  mounted() {
    this.getEmpresas();

    window.addEventListener('resize', this.cambiarTam)

  },
  methods: {
    ...mapActions('deudores', ['buscarDeudorPorId']),
    ...mapActions('deudores', ['buscarDeudorPorNroCliente']),
    ...mapActions('deudores', ['buscarDeudorPorNroDocumento']),
    ...mapActions('deudores', ['buscarDeudorPorNombre']),
    ...mapActions('empresas', ['obtenerEmpresas']),
    ...mapActions('telefonos', ['buscarTelefonosPorNro']),
    ...mapActions('redesSociales', ['buscarRedesPorUsuario']),
    ...mapActions('correos', ['buscarCorreosPorCorreo']),

    onChange(index) {
      this.input = ""
      this.inputLabel = this.listaRadio[index].title
      this.rules = this.listaRules[index]
    },

    async getEmpresas() {
      this.empresas = await this.obtenerEmpresas()
    },

    getDescEmpresaPorId(idEmpresa) {
      let desc = ""
      this.empresas.forEach(element => {
        if (element.idEmp == idEmpresa) {
          desc = element.descripcion
        }
      });
      return desc
    },

    getIdEmpresaPorDesc(desc) {
      let id = ""
      this.empresas.forEach(element => {
        if (element.descripcion == desc) {
          id = element.idEmp
        }
      });
      return id
    },

    async buscarInput() {
      this.cargando = true
      this.dialog = true
      this.noResult = false
      this.listaDeudores = []
      if (this.input) {
        this.resultadoBusqueda = []
        let subBusqueda = []
        let empresaAux = null

        if (this.switchDrop) {
          empresaAux = this.getIdEmpresaPorDesc(this.empresa)
        }

        if (this.inputLabel == this.listaRadio[0].title) {
          let deudorEncontrado = await this.buscarDeudorPorId({ empresa: empresaAux, input: this.input })
          this.listaDeudores = []
          this.listaDeudores.push(deudorEncontrado)
          this.renderizarTabla()
        }
        else if (this.inputLabel == this.listaRadio[1].title) {
          this.listaDeudores = await this.buscarDeudorPorNroCliente({ empresa: empresaAux, input: this.input })
          this.renderizarTabla()
        }
        else if (this.inputLabel == this.listaRadio[2].title) {
          this.listaDeudores = await this.buscarDeudorPorNroDocumento({ empresa: empresaAux, input: this.input })
          this.renderizarTabla()
        }
        else if (this.inputLabel == this.listaRadio[3].title) {
          this.listaDeudores = await this.buscarDeudorPorNombre({ empresa: empresaAux, input: this.input })
          this.renderizarTabla()
        }
        else if (this.inputLabel == this.listaRadio[4].title) {
          subBusqueda = await this.buscarTelefonosPorNro(this.input)
          await this.getDeudorPorTelefono(subBusqueda, empresaAux)
          this.renderizarTabla()
        }
        else if (this.inputLabel == this.listaRadio[5].title) {
          subBusqueda = await this.buscarCorreosPorCorreo(this.input)
          await this.getDeudorPorCorreo(subBusqueda, empresaAux)
          this.renderizarTabla()
        }

        else if (this.inputLabel == this.listaRadio[6].title) {
          subBusqueda = await this.buscarRedesPorUsuario(this.input)
          await this.getDeudorPorUsuarioRed(subBusqueda, empresaAux)
          this.renderizarTabla()
        }
      }
      this.cargando = false
      this.dialog = false
    },

    async getDeudorPorTelefono(lista, empresaAux) {
      this.listaDeudores = []
      for (let index = 0; index < lista.length; index++) {
        const element = lista[index];
        try {
          let deu = await this.buscarDeudorPorId({ empresa: empresaAux, input: element.id_deudor })
          if (deu[0] && empresaAux) {
            this.listaDeudores.push(deu[0])
          }
          if (empresaAux == null) {
            this.listaDeudores.push(deu)
          }
        } catch (error) {
          console.log(error)
        }
      }
    },

    async getDeudorPorCorreo(lista, empresaAux) {
      this.listaDeudores = []
      for (let index = 0; index < lista.length; index++) {
        const element = lista[index];
        try {
          let deu = await this.buscarDeudorPorId({ empresa: empresaAux, input: element.id_deudor })
          if (deu[0] && empresaAux) {
            this.listaDeudores.push(deu[0])
          }
          if (empresaAux == null) {
            this.listaDeudores.push(deu)
          }
        } catch (error) {
          console.log(error)
        }
      }
    },

    async getDeudorPorUsuarioRed(lista, empresaAux) {
      this.listaDeudores = []
      for (let index = 0; index < lista.length; index++) {
        const element = lista[index];
        try {
          let deu = await this.buscarDeudorPorId({ empresa: empresaAux, input: element.id_deudor })
          if (deu[0] && empresaAux) {
            this.listaDeudores.push(deu[0])
          }
          if (empresaAux == null) {
            this.listaDeudores.push(deu)
          }
        } catch (error) {
          console.log(error)
        }
      }
    },

    renderizarTabla() {
      this.listaDeudores.forEach(element => {
        let descEmpresa = this.getDescEmpresaPorId(element.id_empresa)
        //Los atributos verdes son los que trae la tabla deudores.
        let deudorAux = {
          nroDeudor: element.id_deudor,
          empresa: descEmpresa,
          nyp: element.nombre_apellido,
          nroCliente: element.nro_cliente1 || element.nro_cliente2 || element.nro_cliente3,
          dni: element.nro_doc,
          nroRemesa: element.remesa
        }

        this.resultadoBusqueda.push(deudorAux)
      });

      if (this.resultadoBusqueda.length > 0) {
        this.mostraTabla = false
      }
      else {
        this.mostraTabla = true
      }
    },

    cambiarTam() {
      this.tam = window.innerWidth
    },

  }
}