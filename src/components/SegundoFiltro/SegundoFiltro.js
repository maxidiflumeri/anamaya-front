import Loader from "../PantallaPpal/loaderPPal.vue"
import { mapState, mapActions } from "vuex"

export default {

    name: 'SegundoFiltro',

    components: {
        Loader
    },

    data() {

        return {
            page: 1,  // valor inicial paginacion items
            sistemaBuscado: '',  // valor ingresado en barra busqueda 
            nroItems: null  // cantidad ded items que puede ver el usuario activo 
        }

    },

    mounted() {

        if (this.operaciones.itemsGrupos.length === 0) {
            this.obtenerItemsGrupos()
        }

    },

    methods: {

        ...mapActions('operaciones', ['obtenerGrupos', 'obtenerItemsGrupos','cerrarError']),

        //paginaActual() {
        //    console.log(this.page)
        //},

        getNombreGrupo() {

            if (this.operaciones.grupos.length === 0) {
                this.obtenerGrupos()
            }

            return this.operaciones.grupos[this.$route.params.idGrupo-1] ? this.operaciones.grupos[this.$route.params.idGrupo-1].nombreGrupo : ''
        },

        itemsPorIdRol() {
            if (this.operaciones.itemsGrupos.length !== 0) {
                let itemsPorRol = this.operaciones.itemsGrupos.filter(item => item.idGrupo == this.$route.params.idGrupo && item.idRol <= this.auth.usuario.rol)
                this.nroItems = itemsPorRol.length
                return itemsPorRol
            }
        },

        rmAcentos(str) {
            return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
        }

    },

    computed: {

        ...mapState(['operaciones', 'auth']),

        getItems() {
            let itemsPorRol = this.itemsPorIdRol()
            if (itemsPorRol) {
                let accion1 = itemsPorRol.slice((this.page - 1) * 4, (this.page) * 4)
                let accion2 = itemsPorRol.filter(item => this.rmAcentos(item.nombreItem).toLowerCase().match(this.rmAcentos(this.sistemaBuscado).toLowerCase())).slice((this.page - 1) * 4, (this.page) * 4)                
                return (this.sistemaBuscado === '') ? accion1 : accion2
            }
        },

        nroPaginas() {
            return Math.round((this.nroItems + 1) / 4)
        }

    }

}