import { mapState } from "vuex"
import { mapActions } from "vuex"
import urls from '../../urls/index'

export default {
  name: 'pagos-con-tarjeta',
  components: {},
  props: {},
  data() {
    return {
      loading: true,
      cabeceras: [
        { text: 'Id Agenda', value: 'id_agenda' },
        { text: 'Id Deudor', value: 'id_deudor' },
        { text: 'Telefono', value: 'telefono' },
        { text: 'Fecha', value: 'fecha' },
        { text: 'Hora', value: 'hora' },
        { text: 'Estado', value: 'estado' },
        { text: 'Acciones', value: 'acciones' }
      ],
      agendas: [],
      countDown: 30,
      muestraMensaje: false,
      mensaje: "",
      colorMensaje: ""
    }
  },
  computed: {
    ...mapState('agendasTc', ['agendasTc']),
    ...mapState('auth', ['token']),
  },
  mounted() {
    this.get()
    this.actualizarAgendas()
    this.countDownTimer()
  },

  methods: {
    ...mapActions('agendasTc', ['obtenerAgendasTc']),

    actualizarAgendas() {
      setInterval(async () => {
        await this.get()
        this.countDown = 30
      }, 30000)
    },

    countDownTimer() {
      if (this.countDown >= 0) {
        setTimeout(() => {
          this.countDown -= 1
          this.countDownTimer()
        }, 1000)
      }
    },

    mostrarMensaje(mensaje, color) {
      this.muestraMensaje = true
      this.mensaje = mensaje
      this.colorMensaje = color
    },

    async get() {
      await this.obtenerAgendasTc()
      this.agendas = this.agendasTc
      this.loading = false
    },

    async modificarAgenda(agenda) {
      let agendaAux = null
      if (agenda.estado == 'PENDIENTE') {
        agendaAux = {
          estado: 'EN CURSO'
        }
      } else {
        agendaAux = {
          estado: 'PENDIENTE'
        }
      }

      try {
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        await this.axios.put(urls.agendasTc.urlModificar + '/' + agenda.id_agenda, agendaAux, configuracion)
        await this.get()
        this.mostrarMensaje('Agenda modificada exitosamente.', 'success')
      } catch (error) {
        this.mostrarMensaje('Error al modificar la agenda.', 'error')
      }
    },

    async eliminarAgenda(agenda) {
      try {
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        await this.axios.delete(urls.agendasTc.urlEliminar + '/' + agenda.id_agenda, configuracion)
        await this.get()
        this.mostrarMensaje('Agenda eliminada exitosamente.', 'success')
      } catch (error) {
        this.mostrarMensaje('Error al eliminar la agenda.', 'error')
      }
    }

  }
}





