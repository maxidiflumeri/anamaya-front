import { mapState, mapActions } from 'vuex'
import axios from 'axios'
import url from '../../../urls/index.js'

export default {
  name: 'tipos-parentesco',
  components: {},
  props: [],
  data() {
    return {
      search: '',
      snackbarOn: false,
      opExitosa: false,
      isFormValid: false,
      reglasID: [
        value => (/^\d+$/.test(value) && value > 0 && value <= 9999) || 'Id Tipos de parentesco debe ser un número entre 1 y 9999',
        value => !this.tiposParentesco.find(tipo => tipo.id_tipo_parentesco == value) || `El ID ${value} ya fue utilizado`,
      ],
      reglasDescripcion: [
        value => !!value || 'El campo Descripción es requerido!'],
      headers: [
        {
          text: "Id tipo parentesco",
          value: "id_tipo_parentesco"
        },
        {
          text: "Descripcion",
          value: "descripcion"
        },
        {
          text: "Aux1",
          value: "aux1"
        },
        {
          text: "Aux2",
          value: "aux2"
        },
        {
          text: 'Acciones',
          value: 'actions',
          sortable: false
        }
      ],
      itemsPorPagina: 10,
      dialog: false,
      editedIndex: -1,
      editedItem: {
        id_tipo_parentesco: '',
        descripcion: '',
        aux1: '',
        aux2: '',
      },
      defaultItem: {
        id_tipo_parentesco: '',
        descripcion: '',
        aux1: '',
        aux2: '',
      }
    }
  },

  computed: {

    colorSnackbar() {
      return this.opExitosa ? 'success' : 'error'
    },

    ...mapState('auth', ['token']),
    ...mapState('adminTipos', ['tiposParentesco']),

    formTitle() {
      return this.editedIndex === -1 ? 'Agregar Tipo de Parentesco' : 'Editar Tipo de Parentesco'
    },

  },


  watch: {
    dialog(val) {
      val || this.close()
    },
  },

  mounted() {
    this.obtenerTiposParentesco()
  },

  methods: {
    ...mapActions('adminTipos', ['obtenerTiposParentesco']),

    cerrarSnackbar() {
      this.snackbarOn = false,
      this.opExitosa = false
    },

    editItem(item) {
      this.editedIndex = this.tiposParentesco.indexOf(item)
      this.editedItem = Object.assign({}, item)
      this.dialog = true
    },

    close() {
      this.dialog = false
      this.$nextTick(() => {
        this.editedItem = Object.assign({}, this.defaultItem)
        this.editedIndex = -1
      })
      this.$refs.form.resetValidation()
    },

    async save() {
      if (this.editedIndex > -1) {
        await this.actualizarTiposParentesco(this.editedItem)
      } else {
        await this.agregarTiposParentesco(this.editedItem)
      }
      this.snackbarOn = true
      this.close()
    },

    async actualizarTiposParentesco(aux) {
      try {
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        await axios.put(url.tiposParentesco.urlModificar + '/' + aux.id_tipo_parentesco, aux, configuracion)
        await this.obtenerTiposParentesco()
        this.opExitosa = true
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },

    async agregarTiposParentesco(aux) {
      try {
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        await axios.post(url.tiposParentesco.urlAgregar, aux, configuracion)
        await this.obtenerTiposParentesco()
        this.opExitosa = true
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },

  }

}
