import { mapState, mapActions } from 'vuex'
import axios from 'axios'
import url from '../../../urls/index.js'

export default {
  name: 'tasas-interes',
  components: {},
  props: [],
  data() {
    return {
      empresas: [],
      search: '',
      snackbarOn: false,
      opExitosa: false,
      isFormValid: false,
      reglasID: [
        value => (/^\d+$/.test(value) && value > 0 && value <= 9999) || 'Id Tasas de Interes debe ser un número entre 1 y 9999',
        value => !this.tasasInteres.find(tasa => tasa.id_tasa == value) || `El ID ${value} ya fue utilizado`,
      ],
      //El id empresa debe ser checkeado contra la tabla empresa
      reglasEmpresa: [
        value => (/^\d+$/.test(value) && value > 0 && value <= 9999) || 'Id Empresa debe ser un número entre 1 y 9999'        
      ],
      reglasValor: [
        value => /^\d+$/.test(value) || 'Id Tasas de Interes debe ser un número',
      ],
      reglasFecha: [
        value => /^\d{4}[-](0?[1-9]|1[012])[-](0?[1-9]|[12][0-9]|3[01])$/.test(value) || 'La fecha debe tener el formato AAAA-MM-DD',
      ],
      headers: [
        {
          text: "Id-tasa",
          value: "id_tasa"
        },
        {
          text: "Id-empresa",
          value: "id_empresa"
        },
        {
          text: "Empresa",
          value: "empresa"
        },
        {
          text: "Valor",
          value: "valor"
        },
        {
          text: "Fecha",
          value: "fecha"
        },
        {
          text: "Aux1",
          value: "aux1"
        },
        {
          text: "Aux2",
          value: "aux2"
        },
        {
          text: 'Acciones',
          value: 'actions',
          sortable: false
        }
      ],
      itemsPorPagina: 10,
      dialog: false,
      editedIndex: -1,
      editedItem: {
        id_tasa: '',
        id_empresa: '',
        valor: '',
        fecha: '',
        aux1: '',
        aux2: '',
      },
      defaultItem: {
        id_tasa: '',
        id_empresa: '',
        valor: '',
        fecha: '',
        aux1: '',
        aux2: '',
      },
      tasasInteresOk: []
    }
  },

  computed: {

    colorSnackbar() {
      return this.opExitosa ? 'success' : 'error'
    },

    ...mapState('auth', ['token']),
    ...mapState('adminTipos', ['tasasInteres']),
    // ...mapState('empresas', ['empresas']),

    formTitle() {
      return this.editedIndex === -1 ? 'Agregar Tasa de Interes' : 'Editar Tasa de Interes'
    },

  },


  watch: {
    dialog(val) {
      val || this.close()
    },
  },

  mounted() {
    this.obtenerTiposTasasInteres()
    this.getEmpresas()
  },

  methods: {
    ...mapActions('adminTipos', ['obtenerTiposTasasInteres']),
    ...mapActions('empresas', ['obtenerEmpresas']),

    armaDataTable() {
      this.tasasInteresOk = []
      for (let index = 0; index < this.tasasInteres.length; index++) {
        const element = this.tasasInteres[index];        
        let nomEmpresa = this.empresas.find(empresa => empresa.idEmp == element.id_empresa).descripcion                
        element['empresa'] = nomEmpresa        
        this.tasasInteresOk.push(element)
      }            
    },

    async getEmpresas() {
      this.empresas = await this.obtenerEmpresas()
      this.empresas.sort(function (a, b) {
        if (a.descripcion > b.descripcion) {
          return 1
        }
        if (a.descripcion < b.descripcion) {
          return -1
        }
        return 0;
      })
      this.armaDataTable()
    },

    cerrarSnackbar() {
      this.snackbarOn = false,
      this.opExitosa = false
    },

    editItem(item) {
      this.editedIndex = this.tasasInteres.indexOf(item)
      this.editedItem = Object.assign({}, item)
      this.dialog = true
    },

    close() {
      this.dialog = false
      this.$nextTick(() => {
        this.editedItem = Object.assign({}, this.defaultItem)
        this.editedIndex = -1
      })
      this.$refs.form.resetValidation()
    },

    async save() {
      if (this.editedIndex > -1) {
        await this.actualizarTasasInteres(this.editedItem)
      } else {
        await this.agregarTasasInteres(this.editedItem)
      }
      this.snackbarOn = true
      this.close()
    },

    async actualizarTasasInteres(aux) {
      try {
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        await axios.put(url.tasasInteres.urlModificar + '/' + aux.id_tasa, aux, configuracion)
        await this.obtenerTiposTasasInteres()
        this.opExitosa = true
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },

    async agregarTasasInteres(aux) {
      try {
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        await axios.post(url.tasasInteres.urlAgregar, aux, configuracion)
        await this.obtenerTiposTasasInteres()
        this.armaDataTable()
        this.opExitosa = true
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },

  }

}

