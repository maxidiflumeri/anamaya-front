import { mapActions, mapState } from "vuex"
import url from '../../../urls/index.js'
import { VueEditor } from "vue2-editor";

export default {
  name: 'src-components-administracion-template-correos',
  components: { VueEditor },
  props: [],
  data() {
    return {
      empresas: [],
      search: '',
      formulaCheck: false,
      campoSeleccionado: '',
      operacion: '',
      valorFormula: '',
      operaciones: [
        'Suma',
        'Resta',
        'Division',
        'Multiplicacion',
        'Porcentaje Quita',
        'Porcentaje Recargo'
      ],
      campos: [
        '@nombre_apellido@',
        '@nombre_contacto@',
        '@nro_doc@',
        '@nro_cliente1@',
        '@nro_cliente2@',
        '@nro_cliente3@',
        '@deuda_historica@',
        '@deuda_actualizada@'
      ],
      headers: [
        {
          text: "Id Template",
          value: "id_template"
        },
        {
          text: "id Empresa",
          value: "id_empresa"
        },
        {
          text: "Nombre",
          value: "nombre"
        },
        {
          text: 'Acciones',
          value: 'actions',
          sortable: false
        }
      ],
      btnLoading: false,
      dialog: false,
      dialogDelete: false,
      itemsPorPagina: 10,
      editedIndex: -1,
      modeloEditar: {
        id_template: "",
        id_empresa: "",
        nombre: "",
        texto: "",
        asunto: ""
      },
      muestraMensaje: false,
      mensaje: "",
      colorMensaje: "",
      templateEliminado: {},
      reglas: [
        value => !!value || 'El campo es requerido!'
      ]
    }
  },
  computed: {
    ...mapState('adminTipos', ['templateCorreos']),    
    ...mapState('auth', ['token']),

    habilitaBotonInsertar() {
      if (this.campoSeleccionado.length > 0 && !this.formulaCheck) {
        return false
      } else if (this.campoSeleccionado.length > 0 && this.formulaCheck && this.operacion.length > 0 && this.valorFormula > 0) {
        return false
      } else {
        return true
      }
    },

    checkDisabled() {
      if (this.campoSeleccionado == '@deuda_historica@' || this.campoSeleccionado == '@deuda_actualizada@') {
        return false
      } else {
        return true
      }
    },

    formTitle() {
      return this.editedIndex === -1 ? 'Nuevo Template' : 'Edicion de template'
    },

    validarAgregar() {
      if (this.modeloEditar.id_empresa == '' || this.modeloEditar.nombre == '' || this.modeloEditar.texto == '') {
        return false
      } else {
        return true
      }
    }
  },
  mounted() {
    this.obtenerTemplateCorreos()
    this.getEmpresas()
  },

  watch: {
    formulaCheck() {
      this.operacion = ''
    }

  },

  methods: {

    ...mapActions('adminTipos', ['obtenerTemplateCorreos']),
    ...mapActions('empresas', ['obtenerEmpresas']),

    async getEmpresas() {
      this.empresas = await this.obtenerEmpresas()
      this.empresas.sort(function (a, b) {
        if (a.descripcion > b.descripcion) {
          return 1
        }
        if (a.descripcion < b.descripcion) {
          return -1
        }
        return 0;
      })
    },

    agregarCampoTexto() {
      if (this.formulaCheck) {
        switch (this.operacion) {
          case 'Suma':
            this.campoSeleccionado == '@deuda_historica@'? this.modeloEditar.texto = this.modeloEditar.texto + ' ' + this.campoSeleccionado + `{[su${this.valorFormula}-hist]}` : 
            this.modeloEditar.texto = this.modeloEditar.texto + ' ' + this.campoSeleccionado + `{[su${this.valorFormula}-actu]}` 
            break
          case 'Resta':
            this.campoSeleccionado == '@deuda_historica@'? this.modeloEditar.texto = this.modeloEditar.texto + ' ' + this.campoSeleccionado + `{[re${this.valorFormula}-hist]}` :
            this.modeloEditar.texto = this.modeloEditar.texto + ' ' + this.campoSeleccionado + `{[re${this.valorFormula}-actu]}`
            break
          case 'Multiplicacion':
            this.campoSeleccionado == '@deuda_historica@'? this.modeloEditar.texto = this.modeloEditar.texto + ' ' + this.campoSeleccionado + `{[mu${this.valorFormula}-hist]}` :
            this.modeloEditar.texto = this.modeloEditar.texto + ' ' + this.campoSeleccionado + `{[mu${this.valorFormula}-actu]}` 
            break
          case 'Division':
            this.campoSeleccionado == '@deuda_historica@'? this.modeloEditar.texto = this.modeloEditar.texto + ' ' + this.campoSeleccionado + `{[di${this.valorFormula}-hist]}` :
            this.modeloEditar.texto = this.modeloEditar.texto + ' ' + this.campoSeleccionado + `{[di${this.valorFormula}-actu]}` 
            break
          case 'Porcentaje Quita':
            this.campoSeleccionado == '@deuda_historica@'? this.modeloEditar.texto = this.modeloEditar.texto + ' ' + this.campoSeleccionado + `{[pq${this.valorFormula}-hist]}` :
            this.modeloEditar.texto = this.modeloEditar.texto + ' ' + this.campoSeleccionado + `{[pq${this.valorFormula}-actu]}` 
            break
          case 'Porcentaje Recargo':
            this.campoSeleccionado == '@deuda_historica@'? this.modeloEditar.texto = this.modeloEditar.texto + ' ' + this.campoSeleccionado + `{[pr${this.valorFormula}-hist]}` :
            this.modeloEditar.texto = this.modeloEditar.texto + ' ' + this.campoSeleccionado + `{[pr${this.valorFormula}-actu]}` 
            break
        }
      } else {
        this.modeloEditar.texto = this.modeloEditar.texto + ' ' + this.campoSeleccionado
      }
      this.campoSeleccionado = ''
      this.operacion = ''
      this.formulaCheck = false
      this.valorFormula = ''
    },

    agregarCampoAsunto() {
      if (this.formulaCheck) {
        switch (this.operacion) {
          case 'Suma':
            this.campoSeleccionado == '@deuda_historica@'? this.modeloEditar.asunto = this.modeloEditar.asunto + ' ' + this.campoSeleccionado + `{[su${this.valorFormula}-hist]}` : 
            this.modeloEditar.asunto = this.modeloEditar.asunto + ' ' + this.campoSeleccionado + `{[su${this.valorFormula}-actu]}` 
            break
          case 'Resta':
            this.campoSeleccionado == '@deuda_historica@'? this.modeloEditar.asunto = this.modeloEditar.asunto + ' ' + this.campoSeleccionado + `{[re${this.valorFormula}-hist]}` :
            this.modeloEditar.asunto = this.modeloEditar.asunto + ' ' + this.campoSeleccionado + `{[re${this.valorFormula}-actu]}`
            break
          case 'Multiplicacion':
            this.campoSeleccionado == '@deuda_historica@'? this.modeloEditar.asunto = this.modeloEditar.asunto + ' ' + this.campoSeleccionado + `{[mu${this.valorFormula}-hist]}` :
            this.modeloEditar.asunto = this.modeloEditar.asunto + ' ' + this.campoSeleccionado + `{[mu${this.valorFormula}-actu]}` 
            break
          case 'Division':
            this.campoSeleccionado == '@deuda_historica@'? this.modeloEditar.asunto = this.modeloEditar.asunto + ' ' + this.campoSeleccionado + `{[di${this.valorFormula}-hist]}` :
            this.modeloEditar.asunto = this.modeloEditar.asunto + ' ' + this.campoSeleccionado + `{[di${this.valorFormula}-actu]}` 
            break
          case 'Porcentaje Quita':
            this.campoSeleccionado == '@deuda_historica@'? this.modeloEditar.asunto = this.modeloEditar.asunto + ' ' + this.campoSeleccionado + `{[pq${this.valorFormula}-hist]}` :
            this.modeloEditar.asunto = this.modeloEditar.asunto + ' ' + this.campoSeleccionado + `{[pq${this.valorFormula}-actu]}` 
            break
          case 'Porcentaje Recargo':
            this.campoSeleccionado == '@deuda_historica@'? this.modeloEditar.asunto = this.modeloEditar.asunto + ' ' + this.campoSeleccionado + `{[pr${this.valorFormula}-hist]}` :
            this.modeloEditar.asunto = this.modeloEditar.asunto + ' ' + this.campoSeleccionado + `{[pr${this.valorFormula}-actu]}` 
            break
        }
      } else {
        this.modeloEditar.asunto = this.modeloEditar.asunto + ' ' + this.campoSeleccionado
      }
      this.campoSeleccionado = ''
      this.operacion = ''
      this.formulaCheck = false
      this.valorFormula = ''
    },

    editItem(item) {
      this.editedIndex = this.templateCorreos.indexOf(item)
      this.modeloEditar.id_template = item.id_template
      this.modeloEditar.id_empresa = item.id_empresa
      this.modeloEditar.nombre = item.nombre
      this.modeloEditar.texto = item.texto
      this.modeloEditar.asunto = item.asunto
      this.dialog = true
    },

    dialogEliminar(item) {
      this.templateEliminado = item
      this.dialogDelete = true
    },

    cerrarDialogEliminar() {
      this.dialogDelete = false,
        this.templateEliminado = {}
    },

    cerrarDialogGuardar() {
      this.modeloEditar = {
        id_template: "",
        id_empresa: "",
        nombre: "",
        texto: ""
      },
        this.campoSeleccionado = ""
      this.dialog = false
      this.editedIndex = -1
      this.$refs.form.resetValidation()
      this.operacion = ''
      this.formulaCheck = false
      this.valorFormula = ''
    },

    async guardar() {
      if (this.editedIndex > -1) {
        await this.editarTemplate(this.templateCorreos[this.editedIndex].id_template)
      } else {
        await this.agregarTemplate()
      }
    },

    mostrarMensaje(mensaje, color) {
      this.muestraMensaje = true
      this.mensaje = mensaje
      this.colorMensaje = color
    },

    async editarTemplate(id_template) {
      const templatePut = {
        id_empresa: this.modeloEditar.id_empresa,
        nombre: this.modeloEditar.nombre,
        texto: this.modeloEditar.texto,
        asunto: this.modeloEditar.asunto
      }
      try {
        this.btnLoading = true
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        await this.axios.put(url.templateCorreos.urlModificar + '/' + id_template, templatePut, configuracion)
        this.mostrarMensaje("Template modificado exitosamente.", "success")
        this.cerrarDialogGuardar()
        await this.obtenerTemplateCorreos()
      } catch (error) {
        console.log("Error POST: " + error)
        this.mostrarMensaje("Error al modificar Template.", "error")
      }
      this.btnLoading = false
    },

    async agregarTemplate() {
      const templatePost = {
        id_empresa: this.modeloEditar.id_empresa,
        nombre: this.modeloEditar.nombre,
        texto: this.modeloEditar.texto,
        asunto: this.modeloEditar.asunto
      }
      try {
        this.btnLoading = true
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        await this.axios.post(url.templateCorreos.urlAgregar, templatePost, configuracion)
        this.mostrarMensaje("template agregado exitosamente.", "success")
        this.cerrarDialogGuardar()
        await this.obtenerTemplateCorreos()
      } catch (error) {
        console.log(error.response)
        this.mostrarMensaje("Error al agregar template.", "error")
      }
      this.btnLoading = false
    },

    async eliminarTemplate() {
      try {
        this.btnLoading = true
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        await this.axios.delete(url.templateCorreos.urlEliminar + '/' + this.templateEliminado.id_template, configuracion)
        this.cerrarDialogEliminar()
        this.mostrarMensaje("Template eliminado exitosamente.", "success")
        await this.obtenerTemplateCorreos()
      } catch (error) {
        console.log(error.response)
        this.mostrarMensaje("Error al eliminar Template.", "error")
      }
      this.btnLoading = false
    }

  }
}


