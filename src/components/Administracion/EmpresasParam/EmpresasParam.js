import { mapState, mapActions } from 'vuex'
import axios from 'axios'
import url from '../../../urls/index.js'

export default {
  name: 'empresas-param',
  components: {},
  props: [],
  data() {
    return {
      loadingTable: false,
      muestraMensaje: false,
      mensaje: '',
      colorMensaje: '',
      switch1: false,
      switch2: false,
      // snackbarOn: false,
      // snackbarAgregar: false,
      // opExitosa: false,
      // opAgregarExitosa: false,
      isFormValid: false,
      //El id empresa debe ser checkeado contra la tabla empresa
      reglasEmpresa: [
        value => (/^\d+$/.test(value) && value > 0 && value <= 9999) || 'Campo Id Empresa es obligatorio!'
      ],
      //El id tabla debe ser checkeado contra la tabla p_codigos_tabla
      reglasTabla: [
        value => (/^\d+$/.test(value) && value > 0 && value <= 9999) || 'Campo Id Tabla es obligatorio!'
      ],
      reglasIdOpcion: [
        value => (/^\d+$/.test(value) && value > 0 && value <= 9999) || 'Campo Id Código es obligatorio!',
      ],
      reglasVisible: [
        value => !!value || 'El campo Visible es requerido!',
        value => (/^\d+$/.test(value) && (value == 0 || value == 1)) || 'El cambo Visible debe ser 0 o 1'
      ],
      reglasBloqueante: [
        value => !!value || 'El campo Bloqueabte es requerido!',
        value => (/^\d+$/.test(value) && (value == 0 || value == 1)) || 'El cambo Bloqueante debe ser 0 o 1'
      ],
      headers: [
        {
          text: "Id Empresa",
          value: "id_empresa"
        },
        {
          text: "Empresa",
          value: "empresa"
        },
        {
          text: "Id Tabla",
          value: "id_tabla"
        },
        {
          text: 'Descripcion Tabla',
          value: 'codTabla'
        },
        {
          text: "Id Código",
          value: "id_opcion"
        },
        {
          text: "Descripcion Código",
          value: "codigoDesc"
        },
        {
          text: "Visible",
          value: "visible"
        },
        {
          text: "bloqueante",
          value: "bloqueante"
        },
        {
          text: 'Acciones',
          value: 'actions',
          sortable: false
        }
      ],
      itemsPorPagina: 10,
      dialog: false,
      editedIndex: -1,
      editedItem: {
        id_empresa: '',
        id_tabla: '',
        id_opcion: '',
        visible: 1,
        bloqueante: 0,
      },
      defaultItem: {
        id_empresa: '',
        id_tabla: '',
        id_opcion: '',
        visible: 1,
        bloqueante: 0,
      },
      empresas: [],
      codigosTablaOk: [],
      codigos: [],
      search: '',
      empresasParamOk: [],
      dialogDeleteItem: false,
      itemDeleted: {}
    }
  },

  computed: {

    ...mapState('auth', ['token']),
    ...mapState('adminTipos', ['tiposEmpresasParam', 'codigosTabla', 'codigosGestion', 'codigosSituacion', 'motivosNoPago']),

    formTitle() {
      return this.editedIndex === -1 ? 'Asignar Nuevo Código' : 'Editar Asignación de Código'
    },

  },


  watch: {
    dialog(val) {
      val || this.close()
    },
  },

  mounted() {
    this.getCodigosTabla()
    this.getEmpresas()
    this.get()
  },

  methods: {
    ...mapActions('adminTipos', ['obtenerTiposEmpresasParam', 'obtenerCodigosTabla', 'obtenerCodigosSituacion', 'obtenerCodigosGestion', 'obtenerMotivosNoPago']),
    ...mapActions('empresas', ['obtenerEmpresas']),

    async get() {
      this.loadingTable = true
      await this.obtenerTiposEmpresasParam()
      await this.obtenerCodigosSituacion()
      await this.obtenerCodigosGestion()
      await this.obtenerMotivosNoPago()
      this.armaDataTable()
    },

    async getEmpresas() {
      this.empresas = await this.obtenerEmpresas()
      this.empresas.sort(function (a, b) {
        if (a.descripcion > b.descripcion) {
          return 1
        }
        if (a.descripcion < b.descripcion) {
          return -1
        }
        return 0;
      })
    },

    mostrarMensaje(mensaje, color) {
      this.muestraMensaje = true
      this.mensaje = mensaje
      this.colorMensaje = color
    },


    armaDataTable() {
      this.empresasParamOk = []
      for (let index = 0; index < this.tiposEmpresasParam.length; index++) {
        const element = this.tiposEmpresasParam[index];
        let codigoDesc = ''
        let nomEmpresa = this.empresas.find(empresa => empresa.idEmp == element.id_empresa).descripcion
        let codTablaDesc = this.codigosTablaOk.find(codigo => codigo.id_tabla == element.id_tabla).descripcion

        if (element.id_tabla == 1) {
          codigoDesc = this.codigosGestion.find(codigo => codigo.id_gestion == element.id_opcion).descripcion
          element['codigoDesc'] = codigoDesc
        }
        if (element.id_tabla == 2) {
          codigoDesc = this.codigosSituacion.find(codigo => codigo.id_situacion == element.id_opcion).descripcion
          element['codigoDesc'] = codigoDesc
        }
        if (element.id_tabla == 3) {
          codigoDesc = this.motivosNoPago.find(codigo => codigo.id_motivo == element.id_opcion).descripcion
          element['codigoDesc'] = codigoDesc
        }
        element['empresa'] = nomEmpresa
        element['codTabla'] = codTablaDesc
        this.empresasParamOk.push(element)
      }
      this.loadingTable = false
    },

    async getCodigosTabla() {
      await this.obtenerCodigosTabla()
      this.codigosTablaOk = this.codigosTabla
      this.codigosTablaOk.sort(function (a, b) {
        if (a.descripcion > b.descripcion) {
          return 1
        }
        if (a.descripcion < b.descripcion) {
          return -1
        }
        return 0;
      })
    },

    devuelveNombreEmpresa(item) {
      let emp = this.empresas.find(empresa => empresa.idEmp == item.id_empresa)
      if (emp) {
        return emp.descripcion
      } else {
        return ''
      }
    },

    actualizaSelectCodigos() {
      this.codigos = []
      if (this.editedItem.id_tabla == 1) {
        for (let index = 0; index < this.codigosGestion.length; index++) {
          const element = this.codigosGestion[index];
          this.codigos.push({ id: element.id_gestion, descripcion: element.descripcion })
        }
      } else if (this.editedItem.id_tabla == 2) {
        for (let index = 0; index < this.codigosSituacion.length; index++) {
          const element = this.codigosSituacion[index];
          this.codigos.push({ id: element.id_situacion, descripcion: element.descripcion })
        }
      } else if (this.editedItem.id_tabla == 3) {
        for (let index = 0; index < this.motivosNoPago.length; index++) {
          const element = this.motivosNoPago[index];
          this.codigos.push({ id: element.id_motivo, descripcion: element.descripcion })
        }
      }

      this.codigos.sort(function (a, b) {
        if (a.descripcion > b.descripcion) {
          return 1
        }
        if (a.descripcion < b.descripcion) {
          return -1
        }
        return 0;
      })

    },

    switchClickVisible() {
      if (this.switch1) {
        this.editedItem.visible = 1
      } else {
        this.editedItem.visible = 0
      }
    },

    switchClickBloqueante() {
      if (this.switch2) {
        this.editedItem.bloqueante = 1
      } else {
        this.editedItem.bloqueante = 0
      }
    },

    editItem(item) {
      this.editedIndex = this.tiposEmpresasParam.indexOf(item)
      this.editedItem = Object.assign({}, item)
      if (this.editedItem.visible == 0) {
        this.switch1 = false
      } else {
        this.switch1 = true
      }
      if (this.editedItem.bloqueante == 0) {
        this.switch2 = false
      } else {
        this.switch2 = true
      }
      this.actualizaSelectCodigos()
      this.dialog = true
    },

    close() {
      this.dialog = false
      this.$nextTick(() => {
        this.editedItem = Object.assign({}, this.defaultItem)
        this.editedIndex = -1
      })
      this.$refs.form.resetValidation()
    },

    async abrirDialogDeleteItem(item) {
      this.dialogDeleteItem = true
      this.itemDeleted = item
    },

    async save() {
      if (this.switch1) {
        this.editedItem.visible = 1
      } else {
        this.editedItem.visible = 0
      }
      if (this.switch2) {
        this.editedItem.bloqueante = 1
      } else {
        this.editedItem.bloqueante = 0
      }
      if (this.editedIndex > -1) {
        await this.actualizarEmpresasParam(this.editedItem)
      } else if (this.tiposEmpresasParam.find(emp => emp.id_empresa == this.editedItem.id_empresa && emp.id_tabla == this.editedItem.id_tabla && emp.id_opcion == this.editedItem.id_opcion) == null) {
        await this.agregarEmpresasParam(this.editedItem)
      }
      this.close()
    },

    async desasociarCodigo() {
      try {
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        await axios.delete(url.empresasParam.urlEliminar + '/' + this.itemDeleted.id_empresa + '/' + this.itemDeleted.id_tabla + '/' + this.itemDeleted.id_opcion, configuracion)
        await this.get()
        this.dialogDeleteItem = false
        this.itemDeleted = {}
        this.mostrarMensaje('Operación realizada con éxito', 'success')
      } catch (error) {
        this.mostrarMensaje('Error al realizar la operación', 'error')
        console.log("Error GET: " + error)
      }
    },

    async actualizarEmpresasParam(aux) {
      try {
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        await axios.put(url.empresasParam.urlModificar + '/' + aux.id_empresa + '/' + aux.id_tabla + '/' + aux.id_opcion, aux, configuracion)
        await this.get()
        this.mostrarMensaje('Operación realizada con éxito', 'success')
      } catch (error) {
        this.mostrarMensaje('Error al realizar la operación', 'error')
        console.log("Error GET: " + error)
      }
    },

    async agregarEmpresasParam(aux) {
      try {
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        await axios.post(url.empresasParam.urlAgregar, aux, configuracion)
        await this.get()
        this.mostrarMensaje('Operación realizada con éxito', 'success')
      } catch (error) {
        this.mostrarMensaje('Error al realizar la operación', 'error')
        console.log("Error GET: " + error)
      }
    },

  }

}