import { mapState, mapActions } from 'vuex'
import axios from 'axios'
import url from '../../../urls/index.js'

export default {
  name: 'codigo-de-gestion',
  components: {},
  props: [],
  data () {
    return {
      valid: false,
      snackbarOn: false,
      opExitosa: false,
      reglasID: [
        value => (!isNaN(parseFloat(value)) && value > 0 && value <= 9999) || 'Id Codigos Gestion debe ser un número entre 1 y 9999',
        value => !this.codigosGestion.find(codigo_Gestion => codigo_Gestion.id_gestion == value) || `El ID ${value} ya fue utilizado`,
      ],
      reglasDescripcion: [
        value => !!value || 'El campo Descripción es requerido!'],
      headers: [
        {
          text: "Id-Gestion",
          value: "id_gestion"
        },
        {
          text: "Descripcion",
          value: "descripcion"
        },
        {
          text: "Aux1",
          value: "aux1"
        },
        {
          text: "Aux2",
          value: "aux2"
        },
        {
          text: 'Acciones',
          value: 'actions',
          sortable: false
        }
      ],
      itemsPorPagina: 10,
      dialog: false,
      editedIndex: -1,
      editedItem: {
        id_gestion: '',
        descripcion: '',
        aux1: '',
        aux2: '',
      },
      defaultItem: {
        id_gestion: '',
        descripcion: '',
        aux1: '',
        aux2: '',
      },
      loadingTable: false,
      search: ''
    }
  },

  computed: {

    colorSnackbar() {
      return this.opExitosa ? 'success' : 'error'
    },

    ...mapState('auth', ['token']),
    ...mapState('adminTipos', ['codigosGestion']),

    formTitle() {
      return this.editedIndex === -1 ? 'Agregar Codigos gestion' : 'Editar Codigos gestion'
    },

  },


  watch: {
    dialog(val) {
      val || this.close()
    },
  },

  mounted() {
    this.get()  

  },

  methods: {
    ...mapActions('adminTipos', ['obtenerCodigosGestion']),


    cerrarSnackbar() {
      this.snackbarOn = false,
      this.opExitosa = false
    },

    async get(){
      this.loadingTable = true
      this.obtenerCodigosGestion()
      this.loadingTable = false
    },

    editItem(item) {
      this.editedIndex = this.codigosGestion.indexOf(item)
      this.editedItem = Object.assign({}, item)
      this.dialog = true
    },

    close() {
      this.dialog = false
      this.$nextTick(() => {
        this.editedItem = Object.assign({}, this.defaultItem)
        this.editedIndex = -1
      })
      this.$refs.form.resetValidation()

    },

    async save() {
      if (this.editedIndex > -1) {
        await this.actualizarCodigosGestion(this.editedItem)
      } else {
        await this.agregarCodigosGestion(this.editedItem)
      }
      this.snackbarOn = true
      this.close()
    },

    async actualizarCodigosGestion(aux) {
      try {
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        await axios.put(url.codigosGestion.urlModificar + '/' + aux.id_gestion, aux, configuracion)
        await this.get()
        this.opExitosa = true
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },

    async agregarCodigosGestion(aux) {
      try {
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        await axios.post(url.codigosGestion.urlAgregar, aux, configuracion)
        await this.get()
        this.opExitosa = true
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },

  }

}
