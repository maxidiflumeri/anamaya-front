import { mapState, mapActions } from 'vuex'
import axios from 'axios'
import url from '../../../urls/index.js'

export default {
  name: 'tiposIva',
  components: {},
  props: [],
  data() {
    return {
      search: '',
      valid: false,
      snackbarOn: false,
      opExitosa: false,
      reglasDescripcion: [
        value => !!value || 'El campo Descripción es requerido!'],
      reglasID: [
        value => !!value || 'El campo Id Tipo de IVA es requerido!',
        value => (!isNaN(parseFloat(value)) && value > 0 && value <= 9999) || 'Id Tipo de IVA debe ser un número entre 1 y 9999',
        value => !this.tiposDeIva.find(tipo => tipo.id_tipo_iva == value) || `El ID ${value} ya fue utilizado`,
      ],
      reglaValor: [
        value => !!value || 'El campo Valor 1 es requerido!',
        value => (!isNaN(parseFloat(value)) && value > 0) || 'Valor 1 deber ser numérico',
      ],
      headers: [
        {
          text: "Id-Tipo de IVA",
          value: "id_tipo_iva"
        },
        {
          text: "Valor 1",
          value: "valor1"
        },
        {
          text: "Valor 2",
          value: "valor2"
        },
        {
          text: "Valor 3",
          value: "valor3"
        },
        {
          text: "Valor 4",
          value: "valor4"
        },
        {
          text: "Valor 5",
          value: "valor5"
        },
        {
          text: "Descripción",
          value: "descripcion"
        },
        {
          text: 'Aux 1',
          value: 'aux1',
          sortable: false
        },
        {
          text: 'Aux 2',
          value: 'aux2',
          sortable: false
        },
        {
          text: 'Acciones',
          value: 'actions',
          sortable: false
        }
      ],
      itemsPorPagina: 10,
      dialog: false,
      editedIndex: -1,
      editedItem: {
        id_tipo_iva: '',
        valor1: null,
        valor2: null,
        valor3: null,
        valor4: null,
        valor5: null,
        descripcion: '',
        aux1: '',
        aux2: '',
      },
      defaultItem: {
        id_tipo_iva: '',
        valor1: null,
        valor2: null,
        valor3: null,
        valor4: null,
        valor5: null,
        descripcion: '',
        aux1: '',
        aux2: '',
      }
    }
  },

  computed: {

    colorSnackbar() {
      return this.opExitosa ? 'success' : 'error'
    },

    ...mapState('auth', ['token']),
    ...mapState('adminTipos', ['tiposDeIva']),

    formTitle() {
      return this.editedIndex === -1 ? 'Agregar Tipo de IVA' : 'Editar Tipo de IVA'
    },

  },


  watch: {
    dialog(val) {
      val || this.close()
    },
  },

  mounted() {
    this.obtenerTiposIva()
  },

  methods: {
    ...mapActions('adminTipos', ['obtenerTiposIva']),

    cerrarSnackbar() {
      this.snackbarOn = false,
        this.opExitosa = false
    },

    editItem(item) {
      this.editedIndex = this.tiposDeIva.indexOf(item)
      this.editedItem = Object.assign({}, item)
      this.dialog = true
    },

    close() {
      this.dialog = false
      this.$nextTick(() => {
        this.editedItem = Object.assign({}, this.defaultItem)
        this.editedIndex = -1
      })
      this.$refs.form.resetValidation()
    },

    async save() {
      if (this.editedIndex > -1) {
        await this.actualizarTipoIva(this.editedItem)
      } else {
        await this.agregarTipoIva(this.editedItem)
      }
      this.snackbarOn = true
      this.close()
    },

    async actualizarTipoIva(aux) {
      try {
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        await axios.put(url.tiposIva.urlModificar + '/' + aux.id_tipo_iva, aux, configuracion)
        await this.obtenerTiposIva()
        this.opExitosa = true
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },

    async agregarTipoIva(aux) {
      try {
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        await axios.post(url.tiposIva.urlAgregar, aux, configuracion)
        await this.obtenerTiposIva()
        this.opExitosa = true
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },

  }

}








