import { mapState, mapActions } from 'vuex'
import axios from 'axios'
import url from '../../../urls/index.js'

export default {
  name: 'tipos-actualizacion',
  components: {},
  props: [],
  data() {
    return {
      search: '',
      snackbarOn: false,
      opExitosa: false,
      isFormValid: false,
      reglasID: [
        value => (/^\d+$/.test(value) && value > 0 && value <= 9999) || 'Id Tipos de Actualizacion debe ser un número entre 1 y 9999',
        value => !this.tiposActualizacion.find(tipo => tipo.id_tipo_actualizacion == value) || `El ID ${value} ya fue utilizado`,
      ],
      reglasDescripcion: [
        value => !!value || 'El campo Descripción es requerido!'],
      headers: [
        {
          text: "Id tipo actualizacion",
          value: "id_tipo_actualizacion"
        },
        {
          text: "Descripcion",
          value: "descripcion"
        },
        {
          text: "Aux1",
          value: "aux1"
        },
        {
          text: "Aux2",
          value: "aux2"
        },
        {
          text: 'Acciones',
          value: 'actions',
          sortable: false
        }
      ],
      itemsPorPagina: 10,
      dialog: false,
      editedIndex: -1,
      editedItem: {
        id_tipo_actualizacion: '',
        descripcion: '',
        aux1: '',
        aux2: '',
      },
      defaultItem: {
        id_tipo_actualizacion: '',
        descripcion: '',
        aux1: '',
        aux2: '',
      }
    }
  },

  computed: {

    colorSnackbar() {
      return this.opExitosa ? 'success' : 'error'
    },

    ...mapState('auth', ['token']),
    ...mapState('adminTipos', ['tiposActualizacion']),

    formTitle() {
      return this.editedIndex === -1 ? 'Agregar Tipo de Actualizacion' : 'Editar Tipo de Actualizacion'
    },

  },


  watch: {
    dialog(val) {
      val || this.close()
    },
  },

  mounted() {
    this.obtenerTiposActualizacion()
  },

  methods: {
    ...mapActions('adminTipos', ['obtenerTiposActualizacion']),

    cerrarSnackbar() {
      this.snackbarOn = false,
      this.opExitosa = false
    },

    editItem(item) {
      this.editedIndex = this.tiposActualizacion.indexOf(item)
      this.editedItem = Object.assign({}, item)
      this.dialog = true
    },

    close() {
      this.dialog = false
      this.$nextTick(() => {
        this.editedItem = Object.assign({}, this.defaultItem)
        this.editedIndex = -1
      })
      this.$refs.form.resetValidation()
    },

    async save() {
      if (this.editedIndex > -1) {
        await this.actualizarTiposActualizacion(this.editedItem)
      } else {
        await this.agregarTiposActualizacion(this.editedItem)
      }
      this.snackbarOn = true
      this.close()
    },

    async actualizarTiposActualizacion(aux) {
      try {
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        await axios.put(url.tiposActualizacion.urlModificar + '/' + aux.id_tipo_actualizacion, aux, configuracion)
        await this.obtenerTiposActualizacion()
        this.opExitosa = true
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },

    async agregarTiposActualizacion(aux) {
      try {
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        await axios.post(url.tiposActualizacion.urlAgregar, aux, configuracion)
        await this.obtenerTiposActualizacion()
        this.opExitosa = true
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },

  }

}