import { mapState, mapActions } from 'vuex'
import axios from 'axios'
import url from '../../../urls/index.js'

export default {
  name: 'codigos-correo',
  components: {},
  props: [],
  data () {
    return {
      valid: false,
      snackbarOn: false,
      opExitosa: false,
      reglasID: [
        value => (!isNaN(parseFloat(value)) && value > 0 && value <= 9999) || 'Id Tipos correo debe ser un número entre 1 y 9999',
        value => !this.codigosCorreo.find(codigo_Correo => codigo_Correo.id_correo == value) || `El ID ${value} ya fue utilizado`,
      ],
      reglasDescripcion: [
        value => !!value || 'El campo Descripción es requerido!'],
      headers: [
        {
          text: "Id-Correo",
          value: "id_correo"
        },
        {
          text: "Descripcion",
          value: "descripcion"
        },
        {
          text: "Aux1",
          value: "aux1"
        },
        {
          text: "Aux2",
          value: "aux2"
        },
        {
          text: 'Acciones',
          value: 'actions',
          sortable: false
        }
      ],
      itemsPorPagina: 10,
      dialog: false,
      editedIndex: -1,
      editedItem: {
        id_correo: '',
        descripcion: '',
        aux1: '',
        aux2: '',
      },
      defaultItem: {
        id_correo: '',
        descripcion: '',
        aux1: '',
        aux2: '',
      }
    }
  },

  computed: {

    colorSnackbar() {
      return this.opExitosa ? 'success' : 'error'
    },

    ...mapState('auth', ['token']),
    ...mapState('adminTipos', ['codigosCorreo']),

    formTitle() {
      return this.editedIndex === -1 ? 'Agregar Codigos correo' : 'Editar Codigos correo'
    },

  },


  watch: {
    dialog(val) {
      val || this.close()
    },
  },

  mounted() {
    this.obtenerCodigosCorreo()

  },

  methods: {
    ...mapActions('adminTipos', ['obtenerCodigosCorreo']),


    cerrarSnackbar() {
      this.snackbarOn = false,
      this.opExitosa = false
    },

    editItem(item) {
      this.editedIndex = this.codigosCorreo.indexOf(item)
      this.editedItem = Object.assign({}, item)
      this.dialog = true
    },

    close() {
      this.dialog = false
      this.$nextTick(() => {
        this.editedItem = Object.assign({}, this.defaultItem)
        this.editedIndex = -1
      })
      this.$refs.form.resetValidation()
    },

    async save() {
      if (this.editedIndex > -1) {
        await this.actualizarCodigosCorreo(this.editedItem)
      } else {
        await this.agregarCodigosCorreo(this.editedItem)
      }
      this.snackbarOn = true
      this.close()
    },

    async actualizarCodigosCorreo(aux) {
      try {
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        await axios.put(url.codigosCorreo.urlModificar + '/' + aux.id_correo, aux, configuracion)
        await this.obtenerCodigosCorreo()
        this.opExitosa = true
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },

    async agregarCodigosCorreo(aux) {
      try {
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        await axios.post(url.codigosCorreo.urlAgregar, aux, configuracion)
        await this.obtenerCodigosCorreo()
        this.opExitosa = true
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },

  }

}

