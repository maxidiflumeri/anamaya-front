import { mapState, mapActions } from 'vuex'
import axios from 'axios'
import url from '../../../urls/index.js'

export default {
  name: 'motivos_no_pago',
  components: {},
  props: [],
  data() {
    return {
      search: '',
      valid: false,
      snackbarOn: false,
      opExitosa: false,
      reglasDescripcion: [
        value => !!value || 'El campo Descripción es requerido!'],
      reglasID: [
        value => (!isNaN(parseFloat(value)) && value > 0 && value <= 9999) || 'Id Motivo debe ser un número entre 1 y 9999',
        value => !this.motivosNoPago.find(motivo => motivo.id_motivo == value) || `El ID ${value} ya fue utilizado`,
      ],
      headers: [
        {
          text: "Id-Motivo de No Pago",
          value: "id_motivo"
        },
        {
          text: "Descripción",
          value: "descripcion"
        },
        {
          text: 'Aux 1',
          value: 'aux1',
          sortable: false
        },
        {
          text: 'Aux 2',
          value: 'aux2',
          sortable: false
        },
        {
          text: 'Acciones',
          value: 'actions',
          sortable: false
        }
      ],
      itemsPorPagina: 10,
      dialog: false,
      editedIndex: -1,
      editedItem: {
        id_motivo: '',
        descripcion: '',
      },
      defaultItem: {
        id_motivo: '',
        descripcion: '',
      }
    }
  },

  computed: {

    colorSnackbar() {
      return this.opExitosa ? 'success' : 'error'
    },

    ...mapState('auth', ['token']),
    ...mapState('adminTipos', ['motivosNoPago']),

    formTitle() {
      return this.editedIndex === -1 ? 'Agregar Motivo' : 'Editar Motivo'
    },

  },


  watch: {
    dialog(val) {
      val || this.close()
    },
  },

  mounted() {
    this.obtenerMotivosNoPago()
  },

  methods: {
    ...mapActions('adminTipos', ['obtenerMotivosNoPago']),

    cerrarSnackbar() {
      this.snackbarOn = false,
        this.opExitosa = false
    },

    editItem(item) {
      this.editedIndex = this.motivosNoPago.indexOf(item)
      this.editedItem = Object.assign({}, item)
      this.dialog = true
    },

    close() {
      this.dialog = false
      this.$nextTick(() => {
        this.editedItem = Object.assign({}, this.defaultItem)
        this.editedIndex = -1
      })
      this.$refs.form.resetValidation()
    },

    async save() {
      if (this.editedIndex > -1) {
        await this.actualizarMotivosNoPago(this.editedItem)
      } else {
        await this.agregarMotivoNoPago(this.editedItem)
      }
      this.snackbarOn = true
      this.close()
    },

    async actualizarMotivoNoPago(aux) {
      try {
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        await axios.put(url.motivosNoPago.urlModificar + '/' + aux.id_motivo, aux, configuracion)
        await this.obtenerMotivosNoPago()
        this.opExitosa = true
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },

    async agregarMotivoNoPago(aux) {
      try {
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        await axios.post(url.motivosNoPago.urlAgregar, aux, configuracion)
        await this.obtenerMotivosNoPago()
        this.opExitosa = true
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },

  }

}




