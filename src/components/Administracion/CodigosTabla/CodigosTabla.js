import { mapState, mapActions } from 'vuex'
import axios from 'axios'
import url from '../../../urls/index.js'

export default {
  name: 'codigos-tabla',
  components: {},
  props: [],
  data () {
    return {
      valid: false,
      snackbarOn: false,
      opExitosa: false,
      reglasID: [
        value => (!isNaN(parseFloat(value)) && value > 0 && value <= 9999) || 'Id Codigos Tabla debe ser un número entre 1 y 9999',
        value => !this.codigosTabla.find(codigo_Tabla => codigo_Tabla.id_tabla == value) || `El ID ${value} ya fue utilizado`,
      ],
      reglasDescripcion: [
        value => !!value || 'El campo Descripción es requerido!'],
      headers: [
        {
          text: "Id-Tabla",
          value: "id_tabla"
        },
        {
          text: "Descripcion",
          value: "descripcion"
        },
        {
          text: 'Acciones',
          value: 'actions',
          sortable: false
        }
      ],
      itemsPorPagina: 10,
      dialog: false,
      editedIndex: -1,
      editedItem: {
        id_tabla: '',
        descripcion: '',
      },
      defaultItem: {
        id_tabla: '',
        descripcion: '',
      }
    }
  },

  computed: {

    colorSnackbar() {
      return this.opExitosa ? 'success' : 'error'
    },

    ...mapState('auth', ['token']),
    ...mapState('adminTipos', ['codigosTabla']),

    formTitle() {
      return this.editedIndex === -1 ? 'Agregar Codigos tabla' : 'Editar Codigos tabla'
    },

  },


  watch: {
    dialog(val) {
      val || this.close()
    },
  },

  mounted() {
    this.obtenerCodigosTabla()

  },

  methods: {
    ...mapActions('adminTipos', ['obtenerCodigosTabla']),


    cerrarSnackbar() {
      this.snackbarOn = false,
      this.opExitosa = false
    },

    editItem(item) {
      this.editedIndex = this.codigosTabla.indexOf(item)
      this.editedItem = Object.assign({}, item)
      this.dialog = true
    },

    close() {
      this.dialog = false
      this.$nextTick(() => {
        this.editedItem = Object.assign({}, this.defaultItem)
        this.editedIndex = -1
      })
      this.$refs.form.resetValidation()
    },

    async save() {
      if (this.editedIndex > -1) {
        await this.actualizarCodigosTabla(this.editedItem)
      } else {
        await this.agregarCodigosTabla(this.editedItem)
      }
      this.snackbarOn = true
      this.close()
    },

    async actualizarCodigosTabla(aux) {
      try {
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        await axios.put(url.codigosTabla.urlModificar + '/' + aux.id_tabla, aux, configuracion)
        await this.obtenerCodigosTabla()
        this.opExitosa = true
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },

    async agregarCodigosTabla(aux) {
      try {
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        await axios.post(url.codigosTabla.urlAgregar, aux, configuracion)
        await this.obtenerCodigosTabla()
        this.opExitosa = true
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },

  }

}


