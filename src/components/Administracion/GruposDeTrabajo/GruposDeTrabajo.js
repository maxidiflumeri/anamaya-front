import { mapState, mapActions } from 'vuex'
import axios from 'axios'
import url from '../../../urls/index.js'

export default {
  name: 'gruposTrabajo',
  components: {},
  props: [],
  data() {
    return {
      search: '',
      valid: false,
      snackbarOn: false,
      opExitosa: false,
      reglasDescripcion: [
        value => !!value || 'El campo Descripción es requerido!'],
      reglasID: [
        value => !!value || 'El campo Id Grupo es requerido!',
        value => (!isNaN(parseFloat(value)) && value > 0 && value <= 9999) || 'Id Grupo debe ser un número entre 1 y 9999',
        value => !this.gruposDeTrabajo.find(grupo => grupo.id_grupo == value) || `El ID ${value} ya fue utilizado`,
      ],
      headers: [
        {
          text: "Id-Grupo de Trabajo",
          value: "id_grupo"
        },
        {
          text: "Descripción",
          value: "descripcion"
        },
        {
          text: 'Aux 1',
          value: 'aux1',
          sortable: false
        },
        {
          text: 'Aux 2',
          value: 'aux2',
          sortable: false
        },
        {
          text: 'Acciones',
          value: 'actions',
          sortable: false
        }
      ],
      itemsPorPagina: 10,
      dialog: false,
      editedIndex: -1,
      editedItem: {
        id_grupo: '',
        descripcion: '',
        aux1: '',
        aux2:'',
      },
      defaultItem: {
        id_grupo: '',
        descripcion: '',
        aux1: '',
        aux2:'',
      }
    }
  },

  computed: {

    colorSnackbar() {
      return this.opExitosa ? 'success' : 'error'
    },

    ...mapState('auth', ['token']),
    ...mapState('adminTipos', ['gruposDeTrabajo']),

    formTitle() {
      return this.editedIndex === -1 ? 'Agregar Grupo' : 'Editar Grupo'
    },

  },


  watch: {
    dialog(val) {
      val || this.close()
    },
  },

  mounted() {
    this.obtenerGruposTrabajo()
  },

  methods: {
    ...mapActions('adminTipos', ['obtenerGruposTrabajo']),

    cerrarSnackbar() {
      this.snackbarOn = false,
        this.opExitosa = false
    },

    editItem(item) {
      this.editedIndex = this.gruposDeTrabajo.indexOf(item)
      this.editedItem = Object.assign({}, item)
      this.dialog = true
    },

    close() {
      this.dialog = false
      this.$nextTick(() => {
        this.editedItem = Object.assign({}, this.defaultItem)
        this.editedIndex = -1
      })
      this.$refs.form.resetValidation()
    },

    async save() {
      if (this.editedIndex > -1) {
        await this.actualizarGrupoTrabajo(this.editedItem)
      } else {
        await this.agregarGrupoTrabajo(this.editedItem)
      }
      this.snackbarOn = true
      this.close()
    },

    async actualizarGrupoTrabajo(aux) {
      try {
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        await axios.put(url.gruposTrabajo.urlModificar + '/' + aux.id_grupo, aux, configuracion)
        await this.obtenerGruposTrabajo()
        this.opExitosa = true
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },

    async agregarGrupoTrabajo(aux) {
      try {
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        await axios.post(url.gruposTrabajo.urlAgregar, aux, configuracion)
        await this.obtenerGruposTrabajo()
        this.opExitosa = true
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },

  }

}






