import { mapState, mapActions } from 'vuex'
import axios from 'axios'
import url from '../../../urls/index.js'

export default {
  name: 'tipos-pago',
  components: {},
  props: [],
  data() {
    return {
      switchVisible: false,
      search: '',
      valid: false,
      snackbarOn: false,
      opExitosa: false,
      reglasID: [
        value => (!isNaN(parseFloat(value)) && value > 0 && value <= 9999) || 'Id Tipo Pago debe ser un número entre 1 y 9999',
        value => !this.tiposPago.find(tipos_pago => tipos_pago.id_tipo_pago == value) || `El ID ${value} ya fue utilizado`,
      ],
      reglasVisible: [
        value => (!isNaN(parseFloat(value)) && value >= 0 && value <= 1) || 'El campo Visible debe ser un número entre 0 y 1',
        value => !!value || 'El campo Visible es requerido!'
      ],
      reglasDescripcion: [
        value => !!value || 'El campo Descripción es requerido!'],
      headers: [
        {
          text: "Id-Tipo-Pago",
          value: "id_tipo_pago"
        },
        {
          text: "Descripcion",
          value: "descripcion"
        },
        {
          text: "Visible",
          value: "visible"
        },
        {
          text: "Aux1",
          value: "aux1"
        },
        {
          text: "Aux2",
          value: "aux2"
        },
        {
          text: 'Acciones',
          value: 'actions',
          sortable: false
        }
      ],
      itemsPorPagina: 10,
      dialog: false,
      editedIndex: -1,
      editedItem: {
        id_tipo_pago: '',
        descripcion: '',
        visible: '',
        aux1: '',
        aux2: '',
      },
      defaultItem: {
        id_tipo_pago: '',
        descripcion: '',
        visible: '',
        aux1: '',
        aux2: '',
      }
    }
  },

  computed: {

    colorSnackbar() {
      return this.opExitosa ? 'success' : 'error'
    },

    ...mapState('auth', ['token']),
    ...mapState('adminTipos', ['tiposPago']),

    formTitle() {
      return this.editedIndex === -1 ? 'Agregar Tipos pago' : 'Editar Tipos pago'
    },

  },


  watch: {
    dialog(val) {
      val || this.close()
    },
  },

  mounted() {
    this.obtenerTiposPago()

  },

  methods: {
    ...mapActions('adminTipos', ['obtenerTiposPago']),

    switchClickVisible() {
      if (this.switchVisible) {
        this.editedItem.visible = 1
      } else {
        this.editedItem.visible = 0
      }
    },


    cerrarSnackbar() {
      this.snackbarOn = false,
        this.opExitosa = false
    },

    editItem(item) {
      this.editedIndex = this.tiposPago.indexOf(item)
      this.editedItem = Object.assign({}, item)
      if (this.editedItem.visible == 0) {
        this.switchVisible = false
      } else {
        this.switchVisible = true
      }
      this.dialog = true
    },

    close() {
      this.dialog = false
      this.$nextTick(() => {
        this.editedItem = Object.assign({}, this.defaultItem)
        this.editedIndex = -1
      })
      this.$refs.form.resetValidation()
    },

    async save() {
      this.switchClickVisible()
      if (this.editedIndex > -1) {
        await this.actualizarTiposPago(this.editedItem)
      } else {
        await this.agregarTiposPago(this.editedItem)
      }
      this.snackbarOn = true
      this.close()
    },

    async actualizarTiposPago(aux) {
      try {
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        await axios.put(url.tiposPago.urlModificar + '/' + aux.id_tipo_pago, aux, configuracion)
        await this.obtenerTiposPago()
        this.opExitosa = true
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },

    async agregarTiposPago(aux) {
      try {
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        await axios.post(url.tiposPago.urlAgregar, aux, configuracion)
        await this.obtenerTiposPago()
        this.opExitosa = true
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },

  }

}