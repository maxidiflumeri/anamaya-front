import { mapState, mapActions } from 'vuex'
import axios from 'axios'
import url from '../../../urls/index.js'

export default {
  name: 'tipos-correo',
  components: {},
  props: [],
  data() {
    return {
      empresas: [],
      search: '',
      valid: false,
      snackbarOn: false,
      opExitosa: false,
      reglasID: [
        value => !!value || 'El campo ID_Tipo_Correo es requerido!',
        value => (!isNaN(parseFloat(value)) && value > 0 && value <= 9999) || 'Id Tipos correo debe ser un número entre 1 y 9999',
        value => !this.tiposCorreo.find(tipo_Correo => tipo_Correo.id_tipo_correo == value) || `El ID ${value} ya fue utilizado`,
      ],
      reglaDescripcion: [
        value => !!value || 'El campo Descripcion es requerido!']
      ,
      reglasEmpresa: [
        value => !!value || 'El campo Empresa es requerido!'
      ]
      ,
      headers: [
        {
          text: "Id-tipo-correo",
          value: "id_tipo_correo"
        },
        {
          text: "Id-empresa",
          value: "id_empresa"
        },
        {
          text: "Descripcion",
          value: "descripcion"
        },
        {
          text: "Aux1",
          value: "aux1"
        },
        {
          text: "Aux2",
          value: "aux2"
        },
        {
          text: 'Acciones',
          value: 'actions',
          sortable: false
        }
      ],
      itemsPorPagina: 10,
      dialog: false,
      editedIndex: -1,
      editedItem: {
        id_tipo_correo: '',
        id_empresa: '',
        descripcion: '',
        aux1: '',
        aux2: '',
      },
      defaultItem: {
        id_tipo_correo: '',
        id_empresa: '',
        descripcion: '',
        aux1: '',
        aux2: '',
      }
    }
  },

  computed: {

    colorSnackbar() {
      return this.opExitosa ? 'success' : 'error'
    },

    ...mapState('auth', ['token']),
    ...mapState('adminTipos', ['tiposCorreo']),

    formTitle() {
      return this.editedIndex === -1 ? 'Agregar Tipos correo' : 'Editar Tipos correo'
    },

  },


  watch: {
    dialog(val) {
      val || this.close()
    },
  },

  mounted() {
    this.obtenerTiposCorreo()
    this.getEmpresas()
  },

  methods: {
    ...mapActions('adminTipos', ['obtenerTiposCorreo']),
    ...mapActions('empresas', ['obtenerEmpresas']),

    async getEmpresas() {
      this.empresas = await this.obtenerEmpresas()
      this.empresas.sort(function (a, b) {
        if (a.descripcion > b.descripcion) {
          return 1
        }
        if (a.descripcion < b.descripcion) {
          return -1
        }
        return 0;
      })
    },

    cerrarSnackbar() {
      this.snackbarOn = false,
        this.opExitosa = false
    },

    editItem(item) {
      this.editedIndex = this.tiposCorreo.indexOf(item)
      this.editedItem = Object.assign({}, item)
      this.dialog = true
    },

    close() {
      this.dialog = false
      this.$nextTick(() => {
        this.editedItem = Object.assign({}, this.defaultItem)
        this.editedIndex = -1
      })
      this.$refs.form.resetValidation()
    },

    async save() {
      if (this.editedIndex > -1) {
        await this.actualizarTiposCorreo(this.editedItem)
      } else {
        await this.agregarTiposCorreo(this.editedItem)
      }
      this.snackbarOn = true
      this.close()
    },

    async actualizarTiposCorreo(aux) {
      try {
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        await axios.put(url.tiposCorreo.urlModificar + '/' + aux.id_tipo_correo, aux, configuracion)
        await this.obtenerTiposCorreo()
        this.opExitosa = true
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },

    async agregarTiposCorreo(aux) {
      try {
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        await axios.post(url.tiposCorreo.urlAgregar, aux, configuracion)
        await this.obtenerTiposCorreo()
        this.opExitosa = true
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },

  }

}


