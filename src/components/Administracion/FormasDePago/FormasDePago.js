import { mapState, mapActions } from 'vuex'
import axios from 'axios'
import url from '../../../urls/index.js'

export default {
  name: 'formasDePago',
  components: {},
  props: [],
  data() {
    return {
      search: '',
      valid: false,
      snackbarOn: false,
      opExitosa: false,
      reglasDescripcion: [
        value => !!value || 'El campo Descripción es requerido!'],
      reglasID: [
        value => !!value || 'El campo Id Forma de Pago es requerido!',
        value => (!isNaN(parseFloat(value)) && value > 0 && value <= 9999) || 'Id Forma de Pago debe ser un número entre 1 y 9999',
        value => !this.formasDePago.find(forma => forma.id_forma == value) || `El ID ${value} ya fue utilizado`,
      ],
      headers: [
        {
          text: "Id-Forma de Pago",
          value: "id_forma"
        },
        {
          text: "Descripción",
          value: "descripcion"
        },
        {
          text: 'Aux 1',
          value: 'aux1',
          sortable: false
        },
        {
          text: 'Aux 2',
          value: 'aux2',
          sortable: false
        },
        {
          text: 'Acciones',
          value: 'actions',
          sortable: false
        }
      ],
      itemsPorPagina: 10,
      dialog: false,
      editedIndex: -1,
      editedItem: {
        id_forma: '',
        descripcion: '',
        aux1: '',
        aux2: '',
      },
      defaultItem: {
        id_forma: '',
        descripcion: '',
        aux1: '',
        aux2: '',
      }
    }
  },

  computed: {

    colorSnackbar() {
      return this.opExitosa ? 'success' : 'error'
    },

    ...mapState('auth', ['token']),
    ...mapState('adminTipos', ['formasDePago']),

    formTitle() {
      return this.editedIndex === -1 ? 'Agregar Forma de Pago' : 'Editar Forma de Pago'
    },

  },


  watch: {
    dialog(val) {
      val || this.close()
    },
  },

  mounted() {
    this.obtenerFormasPago()
  },

  methods: {
    ...mapActions('adminTipos', ['obtenerFormasPago']),

    cerrarSnackbar() {
      this.snackbarOn = false,
        this.opExitosa = false
    },

    editItem(item) {
      this.editedIndex = this.formasDePago.indexOf(item)
      this.editedItem = Object.assign({}, item)
      this.dialog = true
    },

    close() {
      this.dialog = false
      this.$nextTick(() => {
        this.editedItem = Object.assign({}, this.defaultItem)
        this.editedIndex = -1
      })
      this.$refs.form.resetValidation()
    },

    async save() {
      if (this.editedIndex > -1) {
        await this.actualizarFormaPago(this.editedItem)
      } else {
        await this.agregarFormaPago(this.editedItem)
      }
      this.snackbarOn = true
      this.close()
    },

    async actualizarFormaPago(aux) {
      try {
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        await axios.put(url.formasPago.urlModificar + '/' + aux.id_forma, aux, configuracion)
        await this.obtenerFormasPago()
        this.opExitosa = true
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },

    async agregarFormaPago(aux) {
      try {
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        await axios.post(url.formasPago.urlAgregar, aux, configuracion)
        await this.obtenerFormasPago()
        this.opExitosa = true
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },

  }

}






