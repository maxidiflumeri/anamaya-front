import { mapState, mapActions } from 'vuex'
import axios from 'axios'
import url from '../../../urls/index.js'

export default {
  name: 'tipos-convenio',
  components: {},
  props: [],
  data() {
    return {
      switchRef: false,
      switchHist: false,
      switchAnticipo: false,
      switchLibre: false,      
      search: '',
      valid: false,
      snackbarOn: false,
      opExitosa: false,
      reglasID: [
        value => (!isNaN(parseFloat(value)) && value > 0 && value <= 9999) || 'Id Tipos convenio debe ser un número entre 1 y 9999',
        value => !this.tiposConvenio.find(tipo_Convenio => tipo_Convenio.id_tipo_convenio == value) || `El ID ${value} ya fue utilizado`,
        value => !!value || 'El campo ID_Tipo_Convenio es requerido!']
      
      ,
      reglaDescripcion: [
        value => !!value || 'El campo Descripcion es requerido!'
      ]
      ,
      reglasEmpresa: [
        value => !!value || 'El campo Empresa es requerido!'
      ]
      ,
      reglaMaxCuotas: [
        value => (/^\d+$/.test(value)) || 'El cambo MaxCuotas debe ser numerico',
        value => !!value || 'El campo MaxCuotas es requerido!',
        value => value < 13 || 'Maximo de cuotas permitido 12!',
        value => value != 0 || 'No puede ser cuota 0!'
      ],   
      
      headers: [
        { text: "id_tipo_convenio", value: "id_tipo_convenio" },
        { text: "id_empresa", value: "id_empresa" },
        { text: "Empresa", value: "empresa" },
        { text: "descripcion", value: "descripcion" },
        { text: "max_cuotas", value: "max_cuotas" },
        { text: "refinanciacion", value: "refinanciacion" },
        { text: "sobre_historico", value: "sobre_historico" },
        { text: "anticipo_sobre_historico", value: "anticipo_sobre_historico" },
        { text: "anticipo0", value: "anticipo0" },
        { text: "anticipo1", value: "anticipo1" },
        { text: "anticipo2", value: "anticipo2" },
        { text: "anticipo3", value: "anticipo3" },
        { text: "anticipo4", value: "anticipo4" },
        { text: "anticipo5", value: "anticipo5" },
        { text: "anticipo6", value: "anticipo6" },
        { text: "anticipo7", value: "anticipo7" },
        { text: "anticipo8", value: "anticipo8" },
        { text: "anticipo9", value: "anticipo9" },
        { text: "anticipo10", value: "anticipo10" },
        { text: "anticipo11", value: "anticipo11" },
        { text: "anticipo12", value: "anticipo12" },
        { text: "porc_anticipo_0", value: "porc_anticipo_0" },
        { text: "porc_anticipo_1", value: "porc_anticipo_1" },
        { text: "porc_anticipo_2", value: "porc_anticipo_2" },
        { text: "porc_anticipo_3", value: "porc_anticipo_3" },
        { text: "porc_anticipo_4", value: "porc_anticipo_4" },
        { text: "porc_anticipo_5", value: "porc_anticipo_5" },
        { text: "porc_anticipo_6", value: "porc_anticipo_6" },
        { text: "porc_anticipo_7", value: "porc_anticipo_7" },
        { text: "porc_anticipo_8", value: "porc_anticipo_8" },
        { text: "porc_anticipo_9", value: "porc_anticipo_9" },
        { text: "porc_anticipo_10", value: "porc_anticipo_10" },
        { text: "porc_anticipo_11", value: "porc_anticipo_11" },
        { text: "porc_anticipo_12", value: "porc_anticipo_12" },
        { text: "monto_min_anticipo_0", value: "monto_min_anticipo_0" },
        { text: "monto_min_anticipo_1", value: "monto_min_anticipo_1" },
        { text: "monto_min_anticipo_2", value: "monto_min_anticipo_2" },
        { text: "monto_min_anticipo_3", value: "monto_min_anticipo_3" },
        { text: "monto_min_anticipo_4", value: "monto_min_anticipo_4" },
        { text: "monto_min_anticipo_5", value: "monto_min_anticipo_5" },
        { text: "monto_min_anticipo_6", value: "monto_min_anticipo_6" },
        { text: "monto_min_anticipo_7", value: "monto_min_anticipo_7" },
        { text: "monto_min_anticipo_8", value: "monto_min_anticipo_8" },
        { text: "monto_min_anticipo_9", value: "monto_min_anticipo_9" },
        { text: "monto_min_anticipo_10", value: "monto_min_anticipo_10" },
        { text: "monto_min_anticipo_11", value: "monto_min_anticipo_11" },
        { text: "monto_min_anticipo_12", value: "monto_min_anticipo_12" },
        { text: "limite_min_cuota_0", value: "limite_min_cuota_0" },
        { text: "limite_min_cuota_1", value: "limite_min_cuota_1" },
        { text: "limite_min_cuota_2", value: "limite_min_cuota_2" },
        { text: "limite_min_cuota_3", value: "limite_min_cuota_3" },
        { text: "limite_min_cuota_4", value: "limite_min_cuota_4" },
        { text: "limite_min_cuota_5", value: "limite_min_cuota_5" },
        { text: "limite_min_cuota_6", value: "limite_min_cuota_6" },
        { text: "limite_min_cuota_7", value: "limite_min_cuota_7" },
        { text: "limite_min_cuota_8", value: "limite_min_cuota_8" },
        { text: "limite_min_cuota_9", value: "limite_min_cuota_9" },
        { text: "limite_min_cuota_10", value: "limite_min_cuota_10" },
        { text: "limite_min_cuota_11", value: "limite_min_cuota_11" },
        { text: "limite_min_cuota_12", value: "limite_min_cuota_12" },
        { text: "limite_quita_cuota_0", value: "limite_quita_cuota_0" },
        { text: "limite_quita_cuota_1", value: "limite_quita_cuota_1" },
        { text: "limite_quita_cuota_2", value: "limite_quita_cuota_2" },
        { text: "limite_quita_cuota_3", value: "limite_quita_cuota_3" },
        { text: "limite_quita_cuota_4", value: "limite_quita_cuota_4" },
        { text: "limite_quita_cuota_5", value: "limite_quita_cuota_5" },
        { text: "limite_quita_cuota_6", value: "limite_quita_cuota_6" },
        { text: "limite_quita_cuota_7", value: "limite_quita_cuota_7" },
        { text: "limite_quita_cuota_8", value: "limite_quita_cuota_8" },
        { text: "limite_quita_cuota_9", value: "limite_quita_cuota_9" },
        { text: "limite_quita_cuota_10", value: "limite_quita_cuota_10" },
        { text: "limite_quita_cuota_11", value: "limite_quita_cuota_11" },
        { text: "limite_quita_cuota_12", value: "limite_quita_cuota_12" },
        { text: "maximo_quita_cuota_0", value: "maximo_quita_cuota_0" },
        { text: "maximo_quita_cuota_1", value: "maximo_quita_cuota_1" },
        { text: "maximo_quita_cuota_2", value: "maximo_quita_cuota_2" },
        { text: "maximo_quita_cuota_3", value: "maximo_quita_cuota_3" },
        { text: "maximo_quita_cuota_4", value: "maximo_quita_cuota_4" },
        { text: "maximo_quita_cuota_5", value: "maximo_quita_cuota_5" },
        { text: "maximo_quita_cuota_6", value: "maximo_quita_cuota_6" },
        { text: "maximo_quita_cuota_7", value: "maximo_quita_cuota_7" },
        { text: "maximo_quita_cuota_8", value: "maximo_quita_cuota_8" },
        { text: "maximo_quita_cuota_9", value: "maximo_quita_cuota_9" },
        { text: "maximo_quita_cuota_10", value: "maximo_quita_cuota_10" },
        { text: "maximo_quita_cuota_11", value: "maximo_quita_cuota_11" },
        { text: "maximo_quita_cuota_12", value: "maximo_quita_cuota_12" },
        { text: "aux1", value: "aux1" },
        { text: "aux2", value: "aux2" },
        { text: "aux3", value: "aux3" },
        { text: "aux4", value: "aux4" },
        { text: "aux5", value: "aux5" },
        {
          text: 'Acciones',
          value: 'actions',
          sortable: false
        }
      ],
      itemsPorPagina: 10,
      dialog: false,
      editedIndex: -1,
      editedItem: {
        id_tipo_convenio: '',
        id_empresa: '',
        descripcion: '',
        max_cuotas: 1,
        refinanciacion: 0,
        sobre_historico: 0,
        anticipo_sobre_historico: 0,
        anticipo0: null,
        anticipo1: null,
        anticipo2: null,
        anticipo3: null,
        anticipo4: null,
        anticipo5: null,
        anticipo6: null,
        anticipo7: null,
        anticipo8: null,
        anticipo9: null,
        anticipo1null: null,
        anticipo11: null,
        anticipo12: null,
        porc_anticipo_0: null,
        porc_anticipo_1: null,
        porc_anticipo_2: null,
        porc_anticipo_3: null,
        porc_anticipo_4: null,
        porc_anticipo_5: null,
        porc_anticipo_6: null,
        porc_anticipo_7: null,
        porc_anticipo_8: null,
        porc_anticipo_9: null,
        porc_anticipo_10: null,
        porc_anticipo_11: null,
        porc_anticipo_12: null,
        monto_min_anticipo_0: null,
        monto_min_anticipo_1: null,
        monto_min_anticipo_2: null,
        monto_min_anticipo_3: null,
        monto_min_anticipo_4: null,
        monto_min_anticipo_5: null,
        monto_min_anticipo_6: null,
        monto_min_anticipo_7: null,
        monto_min_anticipo_8: null,
        monto_min_anticipo_9: null,
        monto_min_anticipo_10: null,
        monto_min_anticipo_11: null,
        monto_min_anticipo_12: null,
        limite_min_cuota_0: null,
        limite_min_cuota_1: null,
        limite_min_cuota_2: null,
        limite_min_cuota_3: null,
        limite_min_cuota_4: null,
        limite_min_cuota_5: null,
        limite_min_cuota_6: null,
        limite_min_cuota_7: null,
        limite_min_cuota_8: null,
        limite_min_cuota_9: null,
        limite_min_cuota_10: null,
        limite_min_cuota_11: null,
        limite_min_cuota_12: null,
        limite_quita_cuota_0: null,
        limite_quita_cuota_1: null,
        limite_quita_cuota_2: null,
        limite_quita_cuota_3: null,
        limite_quita_cuota_4: null,
        limite_quita_cuota_5: null,
        limite_quita_cuota_6: null,
        limite_quita_cuota_7: null,
        limite_quita_cuota_8: null,
        limite_quita_cuota_9: null,
        limite_quita_cuota_10: null,
        limite_quita_cuota_11: null,
        limite_quita_cuota_12: null,
        maximo_quita_cuota_0: null,
        maximo_quita_cuota_1: null,
        maximo_quita_cuota_2: null,
        maximo_quita_cuota_3: null,
        maximo_quita_cuota_4: null,
        maximo_quita_cuota_5: null,
        maximo_quita_cuota_6: null,
        maximo_quita_cuota_7: null,
        maximo_quita_cuota_8: null,
        maximo_quita_cuota_9: null,
        maximo_quita_cuota_10: null,
        maximo_quita_cuota_11: null,
        maximo_quita_cuota_12: null,
        es_libre: 0,
        aux1: '',
        aux2: '',
        aux3: '',
        aux4: '',
        aux5: ''
      },
      defaultItem: {
        id_tipo_convenio: '',
        id_empresa: '',
        descripcion: '',
        max_cuotas: 1,
        refinanciacion: 0,
        sobre_historico: 0,
        anticipo_sobre_historico: 0,
        anticipo0: null,
        anticipo1: null,
        anticipo2: null,
        anticipo3: null,
        anticipo4: null,
        anticipo5: null,
        anticipo6: null,
        anticipo7: null,
        anticipo8: null,
        anticipo9: null,
        anticipo1null: null,
        anticipo11: null,
        anticipo12: null,
        porc_anticipo_0: null,
        porc_anticipo_1: null,
        porc_anticipo_2: null,
        porc_anticipo_3: null,
        porc_anticipo_4: null,
        porc_anticipo_5: null,
        porc_anticipo_6: null,
        porc_anticipo_7: null,
        porc_anticipo_8: null,
        porc_anticipo_9: null,
        porc_anticipo_10: null,
        porc_anticipo_11: null,
        porc_anticipo_12: null,
        monto_min_anticipo_0: null,
        monto_min_anticipo_1: null,
        monto_min_anticipo_2: null,
        monto_min_anticipo_3: null,
        monto_min_anticipo_4: null,
        monto_min_anticipo_5: null,
        monto_min_anticipo_6: null,
        monto_min_anticipo_7: null,
        monto_min_anticipo_8: null,
        monto_min_anticipo_9: null,
        monto_min_anticipo_10: null,
        monto_min_anticipo_11: null,
        monto_min_anticipo_12: null,
        limite_min_cuota_0: null,
        limite_min_cuota_1: null,
        limite_min_cuota_2: null,
        limite_min_cuota_3: null,
        limite_min_cuota_4: null,
        limite_min_cuota_5: null,
        limite_min_cuota_6: null,
        limite_min_cuota_7: null,
        limite_min_cuota_8: null,
        limite_min_cuota_9: null,
        limite_min_cuota_10: null,
        limite_min_cuota_11: null,
        limite_min_cuota_12: null,
        limite_quita_cuota_0: null,
        limite_quita_cuota_1: null,
        limite_quita_cuota_2: null,
        limite_quita_cuota_3: null,
        limite_quita_cuota_4: null,
        limite_quita_cuota_5: null,
        limite_quita_cuota_6: null,
        limite_quita_cuota_7: null,
        limite_quita_cuota_8: null,
        limite_quita_cuota_9: null,
        limite_quita_cuota_10: null,
        limite_quita_cuota_11: null,
        limite_quita_cuota_12: null,
        maximo_quita_cuota_0: null,
        maximo_quita_cuota_1: null,
        maximo_quita_cuota_2: null,
        maximo_quita_cuota_3: null,
        maximo_quita_cuota_4: null,
        maximo_quita_cuota_5: null,
        maximo_quita_cuota_6: null,
        maximo_quita_cuota_7: null,
        maximo_quita_cuota_8: null,
        maximo_quita_cuota_9: null,
        maximo_quita_cuota_10: null,
        maximo_quita_cuota_11: null,
        maximo_quita_cuota_12: null,
        es_libre: 0,
        aux1: '',
        aux2: '',
        aux3: '',
        aux4: '',
        aux5: ''
      },
      tiposConveniosOk: [],
      empresas: []
    }
  },

  computed: {

    colorSnackbar() {
      return this.opExitosa ? 'success' : 'error'
    },

    ...mapState('auth', ['token']),
    ...mapState('adminTipos', ['tiposConvenio']),   

    formTitle() {
      return this.editedIndex === -1 ? 'Agregar Tipos convenio' : 'Editar Tipos convenio'
    },

  },


  watch: {
    dialog(val) {
      val || this.close()
    },
  },

  mounted() {
    this.obtenerTiposConvenio()
    this.getEmpresas()
  },

  methods: {
    ...mapActions('adminTipos', ['obtenerTiposConvenio']),
    ...mapActions('empresas', ['obtenerEmpresas']),

    armaDataTable() {
      this.tiposConveniosOk = []
      for (let index = 0; index < this.tiposConvenio.length; index++) {
        const element = this.tiposConvenio[index];
        let nomEmpresa = this.empresas.find(empresa => empresa.idEmp == element.id_empresa).descripcion
        element['empresa'] = nomEmpresa
        this.tiposConveniosOk.push(element)
      }
    },

    async getEmpresas() {
      this.empresas = await this.obtenerEmpresas()
      this.empresas.sort(function (a, b) {
        if (a.descripcion > b.descripcion) {
          return 1
        }
        if (a.descripcion < b.descripcion) {
          return -1
        }
        return 0;
      })
      this.armaDataTable()
    },

    switchClickRefinanciacion() {
      if (this.switchRef) {
        this.editedItem.refinanciacion = 1
      } else {
        this.editedItem.refinanciacion = 0
      }
    },

    switchClickSobreHistorico() {
      if (this.switchHist) {
        this.editedItem.sobre_historico = 1
      } else {
        this.editedItem.sobre_historico = 0
      }
    },

    switchClickAnticipoHist() {
      if (this.switchAnticipo) {
        this.editedItem.anticipo_sobre_historico = 1
      } else {
        this.editedItem.anticipo_sobre_historico = 0
      }
    },

    switchClickLibre() {
      if (this.switchLibre) {
        this.editedItem.es_libre = 1
      } else {
        this.editedItem.es_libre = 0
      }
    },

    cerrarSnackbar() {
      this.snackbarOn = false,
        this.opExitosa = false
    },

    editItem(item) {
      this.editedIndex = this.tiposConvenio.indexOf(item)
      this.editedItem = Object.assign({}, item)
      if (this.editedItem.refinanciacion == 0) {
        this.switchRef = false
      } else {
        this.switchRef = true
      }
      if (this.editedItem.sobre_historico == 0) {
        this.switchHist = false
      } else {
        this.switchHist = true
      }
      if (this.editedItem.anticipo_sobre_historico == 0) {
        this.switchAnticipo = false
      } else {
        this.switchAnticipo = true
      }
      this.dialog = true
    },

    close() {
      this.dialog = false
      this.$nextTick(() => {
        this.editedItem = Object.assign({}, this.defaultItem)
        this.editedIndex = -1
      })
      this.switchHist = false
      this.switchAnticipo = false
      this.switchLibre = false
      this.switchRef = false
      this.$refs.form.resetValidation()
    },

    async save() {
      this.switchClickAnticipoHist()
      this.switchClickRefinanciacion()
      this.switchClickSobreHistorico()
      if (this.editedIndex > -1) {
        await this.actualizarTiposConvenio(this.editedItem)
      } else {
        await this.agregarTiposConvenio(this.editedItem)
      }
      this.snackbarOn = true
      this.close()
      this.valid = false
    },

    async actualizarTiposConvenio(aux) {
      try {
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        await axios.put(url.tiposConvenio.urlModificar + '/' + aux.id_tipo_convenio, aux, configuracion)
        await this.obtenerTiposConvenio()
        this.opExitosa = true
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },

    async agregarTiposConvenio(aux) {
      try {
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        console.log('lo que se manda en agregar es:' + aux.data)
        await axios.post(url.tiposConvenio.urlAgregar, aux, configuracion)
        await this.obtenerTiposConvenio()
        this.opExitosa = true
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },

  }

}


