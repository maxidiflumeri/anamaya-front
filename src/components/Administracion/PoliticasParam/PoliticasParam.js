import { mapState, mapActions } from 'vuex'
import axios from 'axios'
import url from '../../../urls/index.js'
import { VueEditor } from "vue2-editor";

export default {
  name: 'politicasParam',
  components: {VueEditor},
  props: [],
  data() {
    return {
      search: '',
      valid: false,
      snackbarOn: false,
      opExitosa: false,
      reglasDescripcion: [
        value => !!value || 'El campo Descripción es requerido!'],
        reglasEmpresa: [
          value => !!value || 'El campo Descripción es requerido!'],
      reglasID: [
        value => !!value || 'El campo Id Politica es requerido!',
        value => (!isNaN(parseFloat(value)) && value > 0 && value <= 9999) || 'Id Politica debe ser un número entre 1 y 9999',
        value => !this.politicas.find(politica => politica.id_politica == value) || `El ID ${value} ya fue utilizado`,
      ],
      reglasCorreo: [        
        value => /^(([^<>()[\]\\.,;:\s@']+(\.[^<>()\\[\]\\.,;:\s@']+)*)|('.+'))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value) || 'Solo se permiten correos electronicos validos',
      ],
      headers: [
        {
          text: "Id Politica",
          value: "id_politica"
        },
        {
          text: "Id Empresa",
          value: "id_empresa"
        },
        {
          text: 'Descripcion',
          value: 'descripcion',
          sortable: false
        },
        {
          text: 'Fecha de Creación',
          value: 'fecha_creacion',
          sortable: false
        },
        {
          text: 'Correo',
          value: 'correo',
          sortable: false
        },
        {
          text: 'Password',
          value: 'password',
          sortable: false
        },
        {
          text: 'Acciones',
          value: 'actions',
          sortable: false
        }
      ],
      itemsPorPagina: 10,
      dialog: false,
      editedIndex: -1,
      editedItem: {
        id_politica: '',
        id_empresa: '',
        descripcion: '',
        id_usuario: '',        
        txt_politica1: '',
        txt_politica2: '',
        txt_politica3: '',
        txt_politica4: '',
        txt_politica5: '',
        txt_politica6: '',
        txt_politica7: '',
        txt_politica8: '',
        txt_speech: '',
        txt_backoffice: '',
        objetivo: '',
        envio_mail: '',
        envio_sms: '',
        correo: '',
        password: '',
        aux1: '',
        aux2: '',
        aux3: '',
        aux4: '',
        aux5: ''
      },
      defaultItem: {
        id_politica: '',
        id_empresa: '',
        descripcion: '',
        id_usuario: '',        
        txt_politica1: '',
        txt_politica2: '',
        txt_politica3: '',
        txt_politica4: '',
        txt_politica5: '',
        txt_politica6: '',
        txt_politica7: '',
        txt_politica8: '',
        txt_speech: '',
        txt_backoffice: '',
        objetivo: '',
        envio_mail: '',
        envio_sms: '',
        correo: '',
        password: '',
        aux1: '',
        aux2: '',
        aux3: '',
        aux4: '',
      },
      politicasOk: [],
      empresas: [],
      show1: false,
    }
  },

  computed: {

    colorSnackbar() {
      return this.opExitosa ? 'success' : 'error'
    },

    ...mapState('auth', ['token', 'usuario']),
    ...mapState('politicas', ['politicas']),

    formTitle() {
      return this.editedIndex === -1 ? 'Agregar Política' : 'Editar Política'
    },

  },


  watch: {
    dialog(val) {
      val || this.close()
    },
  },

  mounted() {
    this.get()
  },

  methods: {
    ...mapActions('politicas', ['obtenerPoliticas']),
    ...mapActions('empresas', ['obtenerEmpresas']),

    async get(){
      await this.obtenerPoliticas()  
      await this.getEmpresas() 
      this.politicasOk = this.politicas         
    },

    async getEmpresas() {
      this.empresas = await this.obtenerEmpresas()
      this.empresas.sort(function (a, b) {
        if (a.descripcion > b.descripcion) {
          return 1
        }
        if (a.descripcion < b.descripcion) {
          return -1
        }
        return 0;
      })
    },

    cerrarSnackbar() {
      this.snackbarOn = false,
        this.opExitosa = false
    },

    editItem(item) {
      this.editedIndex = this.politicas.indexOf(item)
      this.editedItem = Object.assign({}, item)
      this.dialog = true
    },

    close() {
      this.dialog = false
      this.$nextTick(() => {
        this.editedItem = Object.assign({}, this.defaultItem)
        this.editedIndex = -1
      })
      this.$refs.form.resetValidation()
    },

    async save() {
      if (this.editedIndex > -1) {
        await this.actualizarPolitica(this.editedItem)
      } else {
        await this.agregarPolitica(this.editedItem)
      }
      this.snackbarOn = true
      this.close()
    },

    async actualizarPolitica(aux) {
      try {
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        await axios.put(url.politicas.urlModificar + '/' + aux.id_politica, aux, configuracion)
        await this.get()
        this.opExitosa = true
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },

    async agregarPolitica(aux) {
      var idPolitica = parseInt(aux.id_politica)
      aux.id_politica = idPolitica
      aux.id_usuario = this.usuario.id_usuario
      try {
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        await axios.post(url.politicas.urlAgregar, aux, configuracion)
        await this.get()
        this.opExitosa = true
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },

  }

}






