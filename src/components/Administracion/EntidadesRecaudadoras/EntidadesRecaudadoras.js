import { mapState, mapActions } from 'vuex'
import axios from 'axios'
import url from '../../../urls/index.js'

export default {
  name: 'entidadesRecaudadoras',
  components: {},
  props: [],
  data() {
    return {      
      search: '',
      valid: false,
      snackbarOn: false,
      opExitosa: false,
      reglasDescripcion: [
        value => !!value || 'El campo Descripción es requerido!'],
      reglasID: [
        value => !!value || 'El campo Id Banco es requerido!',
        value => (!isNaN(parseFloat(value)) && value > 0 && value <= 9999) || 'Id Banco debe ser un número entre 1 y 9999',
        value => !this.entidadesRecaudadoras.find(entidad => entidad.id_banco == value) || `El ID ${value} ya fue utilizado`,
      ],
      headers: [
        {
          text: "Id-Banco",
          value: "id_banco"
        },
        {
          text: "Descripción",
          value: "descripcion"
        },
        {
          text: 'Aux 1',
          value: 'aux1',
          sortable: false
        },
        {
          text: 'Aux 2',
          value: 'aux2',
          sortable: false
        },
        {
          text: 'Acciones',
          value: 'actions',
          sortable: false
        }
      ],
      itemsPorPagina: 10,
      dialog: false,
      editedIndex: -1,
      editedItem: {
        id_banco: '',
        descripcion: '',
        aux1: '',
        aux2: '',
      },
      defaultItem: {
        id_banco: '',
        descripcion: '',
        aux1: '',
        aux2: '',
      }
    }
  },

  computed: {

    colorSnackbar() {
      return this.opExitosa ? 'success' : 'error'
    },

    ...mapState('auth', ['token']),
    ...mapState('adminTipos', ['entidadesRecaudadoras']),

    formTitle() {
      return this.editedIndex === -1 ? 'Agregar Entidad' : 'Editar Entidad'
    },

  },


  watch: {
    dialog(val) {
      val || this.close()
    },
  },

  mounted() {
    this.obtenerEntidadesRecaudadoras()
  },

  methods: {
    ...mapActions('adminTipos', ['obtenerEntidadesRecaudadoras']),

    cerrarSnackbar() {
      this.snackbarOn = false,
        this.opExitosa = false
    },

    editItem(item) {
      this.editedIndex = this.entidadesRecaudadoras.indexOf(item)
      this.editedItem = Object.assign({}, item)
      this.dialog = true
    },

    close() {
      this.dialog = false
      this.$nextTick(() => {
        this.editedItem = Object.assign({}, this.defaultItem)
        this.editedIndex = -1
      })
      this.$refs.form.resetValidation()
    },

    async save() {
      if (this.editedIndex > -1) {
        await this.actualizarEntidadRecaudadora(this.editedItem)
      } else {
        await this.agregarEntidadRecaudadora(this.editedItem)
      }
      this.snackbarOn = true
      this.close()
    },

    async actualizarEntidadRecaudadora(aux) {
      try {
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        await axios.put(url.entidadesRecaudadoras.urlModificar + '/' + aux.id_banco, aux, configuracion)
        await this.obtenerEntidadesRecaudadoras()
        this.opExitosa = true
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },

    async agregarEntidadRecaudadora(aux) {
      try {
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        await axios.post(url.entidadesRecaudadoras.urlAgregar, aux, configuracion)
        await this.obtenerEntidadesRecaudadoras()
        this.opExitosa = true
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },

  }

}




