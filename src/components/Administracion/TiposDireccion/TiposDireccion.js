import { mapState, mapActions } from 'vuex'
import axios from 'axios'
import url from '../../../urls/index.js'

export default {
  name: 'tipos-direccion',
  components: {},
  props: [],
  data() {
    return {
      search: '',
      snackbarOn: false,
      opExitosa: false,
      isFormValid: false,
      reglasID: [
        value => (/^\d+$/.test(value) && value > 0 && value <= 9999) || 'Id Tipos de direccion debe ser un número entre 1 y 9999',
        value => !this.tiposDireccion.find(tipo => tipo.id_tipo_direccion == value) || `El ID ${value} ya fue utilizado`,
      ],
      reglasDescripcion: [
        value => !!value || 'El campo Descripción es requerido!'],
      headers: [
        {
          text: "Id tipo direccion",
          value: "id_tipo_direccion"
        },
        {
          text: "Descripcion",
          value: "descripcion"
        },
        {
          text: "Aux1",
          value: "aux1"
        },
        {
          text: "Aux2",
          value: "aux2"
        },
        {
          text: 'Acciones',
          value: 'actions',
          sortable: false
        }
      ],
      itemsPorPagina: 10,
      dialog: false,
      editedIndex: -1,
      editedItem: {
        id_tipo_direccion: '',
        descripcion: '',
        aux1: '',
        aux2: '',
      },
      defaultItem: {
        id_tipo_direccion: '',
        descripcion: '',
        aux1: '',
        aux2: '',
      }
    }
  },

  computed: {

    colorSnackbar() {
      return this.opExitosa ? 'success' : 'error'
    },

    ...mapState('auth', ['token']),
    ...mapState('adminTipos', ['tiposDireccion']),

    formTitle() {
      return this.editedIndex === -1 ? 'Agregar Tipo de Direccion' : 'Editar Tipo de Direccion'
    },

  },


  watch: {
    dialog(val) {
      val || this.close()
    },
  },

  mounted() {
    this.obtenerTiposDireccion()
  },

  methods: {
    ...mapActions('adminTipos', ['obtenerTiposDireccion']),

    cerrarSnackbar() {
      this.snackbarOn = false,
      this.opExitosa = false
    },

    editItem(item) {
      this.editedIndex = this.tiposDireccion.indexOf(item)
      this.editedItem = Object.assign({}, item)
      this.dialog = true
    },

    close() {
      this.dialog = false
      this.$nextTick(() => {
        this.editedItem = Object.assign({}, this.defaultItem)
        this.editedIndex = -1
      })
      this.$refs.form.resetValidation()
    },

    async save() {
      if (this.editedIndex > -1) {
        await this.actualizarTiposDireccion(this.editedItem)
      } else {
        await this.agregarTiposDireccion(this.editedItem)
      }
      this.snackbarOn = true
      this.close()
    },

    async actualizarTiposDireccion(aux) {
      try {
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        await axios.put(url.tiposDireccion.urlModificar + '/' + aux.id_tipo_direccion, aux, configuracion)
        await this.obtenerTiposDireccion()
        this.opExitosa = true
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },

    async agregarTiposDireccion(aux) {
      try {
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        await axios.post(url.tiposDireccion.urlAgregar, aux, configuracion)
        await this.obtenerTiposDireccion()
        this.opExitosa = true
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },

  }

}
