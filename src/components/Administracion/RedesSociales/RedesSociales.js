import { mapState, mapActions } from 'vuex'
import axios from 'axios'
import url from '../../../urls/index.js'

export default {
  name: 'redes-sociales',
  components: {},
  props: [],
  data() {
    return {
      search: '',
      valid: false,
      snackbarOn: false,
      opExitosa: false,
      reglasDescripcion: [
        value => !!value || 'El campo Descripción es requerido!'],
      reglasID: [
        value => (!isNaN(parseFloat(value)) && value > 0 && value <= 9999) || 'Id Red Social debe ser un número entre 1 y 9999',
        value => !this.redesSociales.find(redSocial => redSocial.id_red_social == value) || `El ID ${value} ya fue utilizado`,
      ],
      headers: [
        {
          text: "Id-Red Social",
          value: "id_red_social"
        },
        {
          text: "Descripción",
          value: "descripcion"
        },
        {
          text: 'Acciones',
          value: 'actions',
          sortable: false
        }
      ],
      itemsPorPagina: 10,
      dialog: false,
      dialogDelete: false,
      editedIndex: -1,
      editedItem: {
        id_red_social: '',
        descripcion: '',
      },
      defaultItem: {
        id_red_social: '',
        descripcion: '',
      }
    }
  },

  computed: {

    colorSnackbar() {
      return this.opExitosa ? 'success' : 'error'
    },

    ...mapState('auth', ['token']),
    ...mapState('adminTipos', ['redesSociales']),

    formTitle() {
      return this.editedIndex === -1 ? 'Agregar Red Social' : 'Editar Red Social'
    },

  },


  watch: {
    dialog(val) {
      val || this.close()
    },
  },

  mounted() {
    this.obtenerTiposRedesSociales()
  },

  methods: {
    ...mapActions('adminTipos', ['obtenerTiposRedesSociales']),

    cerrarSnackbar() {
      this.snackbarOn = false,
      this.opExitosa = false
    },

    editItem(item) {
      this.editedIndex = this.redesSociales.indexOf(item)
      this.editedItem = Object.assign({}, item)
      this.dialog = true
    },

    close() {
      this.dialog = false
      this.$nextTick(() => {
        this.editedItem = Object.assign({}, this.defaultItem)
        this.editedIndex = -1
      })
      this.$refs.form.resetValidation()
    },

    async save() {
      if (this.editedIndex > -1) {
        await this.actualizarRedSocial(this.editedItem)
      } else {
        await this.agregarRedSocial(this.editedItem)
      }
      this.snackbarOn = true
      this.close()
    },

    async actualizarRedSocial(aux) {
      try {
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        await axios.put(url.tiposRed.urlModificar + '/' + aux.id_red_social, aux, configuracion)
        await this.obtenerTiposRedesSociales()
        this.opExitosa = true
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },

    async agregarRedSocial(aux) {
      try {
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        await axios.post(url.tiposRed.urlAgregar, aux, configuracion)
        await this.obtenerTiposRedesSociales()
        this.opExitosa = true
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },

  }

}

