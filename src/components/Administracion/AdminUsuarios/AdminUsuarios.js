import { mapState, mapActions } from 'vuex'
import axios from 'axios'
import url from '../../../urls/index.js'

export default {
  name: 'tipos-pago',
  components: {},
  props: [],
  data() {
    return {
      mensaje: '',
      search: '',
      valid: false,
      snackbarOn: false,
      opExitosa: false,
      reglasID: [
        value => (!isNaN(parseFloat(value)) && value > 0 && value <= 9999) || 'Id Usuario debe ser un número entre 1 y 9999',
        value => !this.dataUsuarios.find(usuario => usuario.id_usuario == value) || `El ID ${value} ya fue utilizado`,
      ],
      reglasLegajo: [
        value => (!isNaN(parseFloat(value)) && value > 0 && value <= 9999) || 'Legajo debe ser un número entre 1 y 9999',
        value => !this.dataUsuarios.find(usuario => usuario.legajo_neotel == value) || `El Legajo ${value} ya fue utilizado`,
      ],
      reglasVisible: [
        value => (!isNaN(parseFloat(value)) && value >= 0 && value <= 1) || 'El campo Visible debe ser un número entre 0 y 1',
        value => !!value || 'El campo Visible es requerido!'
      ],
      reglasDescripcion: [
        value => !!value || 'El campo Nombre es requerido!'],
      reglasDni: [
        value => (!isNaN(parseFloat(value)) && value > 999999 && value <= 99999999) || 'DNI debe ser unicamente numérico y mínimo 7 dígitos, maximo 8 dígitos.',],
      reglasCuil: [
        value => (!isNaN(parseFloat(value)) && value > 11111111111 && value <= 99999999999) || 'CUIL debe ser unicamente numérico y tener 11 dígitos.',],
      reglasCorreo: [
        v => !!v || 'El campo correo es requerido!',
        v => /.+@.+\..+/.test(v) || 'El correo debe ser valido!',
      ],
      reglasHorario: [
        value => !!value || 'El campo es requerido!'],
      headers: [
        {
          text: "Id usuario",
          value: "id_usuario",
          align: "center"
        },
        {
          text: "Legajo",
          value: "legajo_neotel",
          align: "center"
        },
        {
          text: "Id Rol",
          value: "id_rol",
          align: "center"
        },
        {
          text: "Rol",
          value: "rol",
          align: "center"
        },
        {
          text: "Nombre Completo",
          value: "nombre_completo",
          align: "center",
          width: "200px"
        },
        {
          text: "Nro. Documento",
          value: "dni",
          align: "center"
        },
        {
          text: "Nro. Cuil",
          value: "cuil",
          align: "center"
        },
        {
          text: "Correo Electrónico",
          value: "correo",
          align: "center"
        },
        {
          text: "Hora de Ingreso",
          value: "hora_ingreso",
          align: "center"
        },
        {
          text: "Jornada Laboral",
          value: "jornada_laboral",
          align: "center"
        },
        {
          text: "Hora Break 1",
          value: "break1",
          align: "center"
        },
        {
          text: "Hora Break 2",
          value: "break2",
          align: "center"
        },
        {
          text: "Hora Break 3",
          value: "break3",
          align: "center"
        },
        {
          text: "Hora Break 4",
          value: "break4",
          align: "center"
        },
        {
          text: 'Acciones',
          value: 'actions',
          sortable: false
        },
      ],
      dataUsuarios: [],
      itemsPorPagina: 10,
      dialog: false,
      editedIndex: -1,
      editedItem: {
        id_usuario: '',
        legajo_neotel: '',
        id_rol: '',
        nombre_completo: '',
        dni: '',
        cuil: '',
        correo: '',
        hora_ingreso: '',
        jornada_laboral: '',
        break1: '',
        break2: '',
        break3: '',
        break4: '',
        id_grupo: '',
        aux1: '',
        aux2: '',
        aux3: '',
        aux4: '',
        aux5: '',
        aux6: '',
        aux7: '',
        aux8: '',
        aux9: '',
        aux10: ''

      },
      defaultItem: {
        id_usuario: '',
        legajo_neotel: '',
        id_rol: '',
        nombre_completo: '',
        dni: '',
        cuil: '',
        correo: '',
        hora_ingreso: '',
        jornada_laboral: '',
        break1: '',
        break2: '',
        break3: '',
        break4: '',
        id_grupo: '',
        aux1: '',
        aux2: '',
        aux3: '',
        aux4: '',
        aux5: '',
        aux6: '',
        aux7: '',
        aux8: '',
        aux9: '',
        aux10: ''
      },
      tableLoading: false,
      horario_salida: '',
      horarios: [
        '07:00',
        '07:15',
        '07:30',
        '07:45',
        '08:00',
        '08:15',
        '08:30',
        '08:45',
        '09:00',
        '09:15',
        '09:30',
        '09:45',
        '10:00',
        '10:15',
        '10:30',
        '10:45',
        '11:00',
        '11:15',
        '11:30',
        '11:45',
        '12:00',
        '12:15',
        '12:30',
        '12:45',
        '13:00',
        '13:15',
        '13:30',
        '13:45',
        '14:00',
        '14:15',
        '14:30',
        '14:45',
        '15:00',
        '15:15',
        '15:30',
        '15:45',
        '16:00',
        '16:15',
        '16:30',
        '16:45',
        '17:00',
        '17:15',
        '17:30',
        '17:45',
        '18:00',
        '18:15',
        '18:30',
        '18:45',
        '19:00',
        '19:15',
        '19:30',
        '19:45',
        '20:00',
        '20:15',
        '20:30',
        '20:45',
        '21:00'
      ],
      btnLoading: false,
      colorSnackbar: ''
    }

  },

  computed: {

    ...mapState('auth', ['token']),
    ...mapState('usuarios', ['usuarios']),
    ...mapState('adminTipos', ['roles', 'gruposDeTrabajo']),

    formTitle() {
      return this.editedIndex === -1 ? 'Agregar Usuario' : 'Editar Usuario'
    },

  },


  watch: {
    dialog(val) {
      val || this.close()
    },
  },

  mounted() {
    this.get()
  },

  methods: {
    ...mapActions('usuarios', ['obtenerUsuarios']),
    ...mapActions('adminTipos', ['obtenerRoles', 'obtenerGruposTrabajo']),

    async get() {
      this.tableLoading = true
      await this.obtenerUsuarios()
      await this.obtenerRoles()
      await this.obtenerGruposTrabajo()
      this.armaDataTabla()
    },

    armaDataTabla() {
      this.dataUsuarios = []
      this.usuarios.forEach(usuario => {
        let rolDesc = this.roles.find(rol => rol.id_rol == usuario.id_rol)
        usuario['rol'] = rolDesc.nombre
        this.dataUsuarios.push(usuario)
      })
      this.tableLoading = false
    },


    cerrarSnackbar() {
      this.snackbarOn = false
      this.opExitosa = false
    },

    editItem(item) {
      this.editedIndex = this.dataUsuarios.indexOf(item)
      this.editedItem = Object.assign({}, item)
      let inicio = this.editedItem.jornada_laboral.indexOf('A')
      let largo = this.editedItem.jornada_laboral.length
      this.horario_salida = this.editedItem.jornada_laboral.substring(inicio + 2, largo)
      this.dialog = true
    },

    close() {
      this.dialog = false
      this.$nextTick(() => {
        this.editedItem = Object.assign({}, this.defaultItem)
        this.editedIndex = -1
      })
      this.horario_salida = ''
      this.$refs.form.resetValidation()
    },

    async save() {
      if (this.$refs.form.validate()) {
        if (this.editedIndex > -1) {
          await this.actualizarUsuario(this.editedItem)
        } else {
          await this.agregarUsuario(this.editedItem)
        }
        this.snackbarOn = true
        this.close()
      }
    },

    async actualizarUsuario(aux) {
      this.btnLoading = true
      aux.jornada_laboral = `${aux.hora_ingreso} A ${this.horario_salida}`
      try {
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        await axios.put(url.usuarios.urlModificar + '/' + aux.id_usuario, aux, configuracion)
        await this.get()
        this.opExitosa = true
        this.mensaje = 'Operación realizada con éxito.'
        this.colorSnackbar = 'success'
      } catch (error) {
        this.colorSnackbar = 'error'
        console.log("Error GET: " + error)
      }
      this.btnLoading = false
    },

    async resetearContraseña(id) {
      try {
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        await axios.put(url.usuarios.urlModificar + '/' + id, { password: '123456' }, configuracion)
        await this.get()
        this.mensaje = 'Contraseña blanqueada exitosamente.'
        this.colorSnackbar = 'success'
        this.opExitosa = true
        this.snackbarOn = true
      } catch (error) {
        this.colorSnackbar = 'error'
        console.log("Error GET: " + error)
      }
    },

    async agregarUsuario(aux) {
      aux['password'] = '123456'
      aux.jornada_laboral = `${aux.hora_ingreso} A ${this.horario_salida}`
      aux.id_usuario = parseInt(aux.id_usuario)
      aux.legajo_neotel = parseInt(aux.legajo_neotel)
      this.btnLoading = true
      try {
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        await axios.post(url.usuarios.urlAgregar, aux, configuracion)
        await this.get()
        this.colorSnackbar = 'success'
        this.mensaje = 'Operación realizada con éxito.'
        this.opExitosa = true
      } catch (error) {
        this.colorSnackbar = 'error'
        console.log("Error GET: " + error)
      }
      this.btnLoading = false
    },

  }

}


