import { mapActions } from "vuex";
import { mapState } from "vuex";

export default {
  name: 'navbar',
  components: {},
  props: [],

  mounted() {
    this.iniciales = this.usuario.nombre.substring(0, 2).toUpperCase()
    let modo = localStorage.getItem('modo')
    if (modo == "oscuro") {
      this.switchTheme = true
    } else {
      this.switchTheme = false
    }

  },

  data() {
    return {
      opciones: [
        { texto: 'Mi Perfil', link: '/MiPerfil' },
        { texto: 'Cerrar sesión', link: '/Login' }
      ],
      iniciales: '',
      color1: "bg-primary",
      color2: "primary",
      switchTheme: false
    }
  },

  computed: {
    ...mapState("auth", ["usuario", 'switchTema']),   
  },
  methods: {
    ...mapActions("auth", ["salir", "setearTema"]),
    clickOpcion(i) {
      if (i == 0) {
        this.$router.push({ path: '/MiPerfil' })
      } else if (i == 1) {
        this.salir()
      }
    },
    cambiarModoOscuro() {
      this.$vuetify.theme.dark = !this.$vuetify.theme.dark;
      if (this.$vuetify.theme.dark) {        
        this.setearTema(true)
      } else {        
        this.setearTema(false)
      }

    }

  }
}


