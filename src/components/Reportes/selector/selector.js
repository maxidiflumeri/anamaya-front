import { mapActions } from 'vuex'
import { mapState } from 'vuex'
import JsonExcel from "vue-json-excel";
import jsPDF from 'jspdf'
import 'jspdf-autotable'

export default {
  name: 'selector',
  components: { JsonExcel },
  props: {
    campos: Object,
    url: String,
    nombreArchivo: String,
    vistaPrevia: String,
    descargaDirecta: String    
  },
  data() {
    return {
      dpRecepcionDesde: false,
      dpRecepcionHasta: false,      
      dpRecepcionOmiteDesde: false,
      dpRecepcionOmiteHasta: false,
      dpCierreDesde: false,
      dpCierreHasta: false,      
      dpCierreOmiteDesde: false,
      dpCierreOmiteHasta: false,
      btnLoading: false,
      muestraMensaje: false,
      colorMensaje: '',
      mensaje: '',
      formValid: true,
      objetoSelector: this.iniciarFormulario(),
      empresas: [],
      switch1: false,
      rules: [
        value => !!value || 'Campo Obligatorio.',
      ],
      headers: [
        { text: 'Id Situacion', value: 'id_situacion' },
        { text: 'Descripción', value: 'descripcion' },
        { text: 'Cantidad', value: 'cantidad' },
        { text: 'Deuda Histórica', value: 'deuda_historica' },
        { text: 'Pagos', value: 'pagos' },
        { text: 'Porcentaje', value: 'prom' }
      ],
      data: [],
      comboAux: [
        'Aux1',
        'Aux2',
        'Aux3',
        'Aux4',
        'Aux5',
        'Aux6',
        'Aux7',
        'Aux8',
        'Aux9',
        'Aux10',
        'Aux11',
        'Aux12',
        'Aux13',
        'Aux14',
        'Aux15',
        'Aux16',
        'Aux17',
        'Aux18',
        'Aux19',
        'Aux20',
        'Aux21',
        'Aux22',
        'Aux23',
        'Aux24',
        'Aux25',
        'Aux26',
        'Aux27',
        'Aux28',
        'Aux29',
        'Aux30',
        'Aux31',
        'Aux32',
        'Aux33',
        'Aux34',
        'Aux35',
        'Aux36',
        'Aux37',
        'Aux38',
        'Aux39',
        'Aux40',
        'Aux41',
        'Aux42',
        'Aux43',
        'Aux44',
        'Aux45',
        'Aux46',
        'Aux47',
        'Aux48',
        'Aux49',
        'Aux50'
      ],
      auxSeleccionado: '',
      eligeAux1: false,
      eligeAux2: false,
      eligeAux3: false,
      eligeAux4: false,
      eligeAux5: false,
      eligeAux6: false,
      eligeAux7: false,
      eligeAux8: false,
      eligeAux9: false,
      eligeAux10: false,
      eligeAux11: false,
      eligeAux12: false,
      eligeAux13: false,
      eligeAux14: false,
      eligeAux15: false,
      eligeAux16: false,
      eligeAux17: false,
      eligeAux18: false,
      eligeAux19: false,
      eligeAux20: false,
      eligeAux21: false,
      eligeAux22: false,
      eligeAux23: false,
      eligeAux24: false,
      eligeAux25: false,
      eligeAux26: false,
      eligeAux27: false,
      eligeAux28: false,
      eligeAux29: false,
      eligeAux30: false,
      eligeAux31: false,
      eligeAux32: false,
      eligeAux33: false,
      eligeAux34: false,
      eligeAux35: false,
      eligeAux36: false,
      eligeAux37: false,
      eligeAux38: false,
      eligeAux39: false,
      eligeAux40: false,
      eligeAux41: false,
      eligeAux42: false,
      eligeAux43: false,
      eligeAux44: false,
      eligeAux45: false,
      eligeAux46: false,
      eligeAux47: false,
      eligeAux48: false,
      eligeAux49: false,
      eligeAux50: false
    }
  },
  computed: {
    ...mapState('auth', ['token']),

  },
  mounted() {
    this.getEmpresas();

  },
  methods: {
    ...mapActions('empresas', ['obtenerEmpresas']),

    imprimirPDF(){
      const doc = new jsPDF()
      doc.setFontSize(20).setFont('arial', 'bold').text(70, 10, 'Estadistico de pagos')
      doc.setLineWidth(0.01).line(10, 15, 195, 15)
      doc.setLineWidth(0.01).line(10, 17, 195, 17)
      doc.setFontSize(14)

      doc.setFont('times', 'bold')
      doc.text(10, 30, 'Empresa: ')
      doc.setFont('times', 'normal')
      doc.text(35, 30, this.objetoSelector.id_empresa.toString())    
      
      doc.setFont('times', 'bold')
      doc.text(10, 25, 'Numero de Remesa: ')
      doc.setFont('times', 'normal')
      doc.text(55, 25, this.objetoSelector.remesa_desde.toString())  

      doc.autoTable({
        columns: [
          {
            title: 'Id Situacion',
            dataKey: 'id_situacion'
          },
          {
            title: 'Descripcion',
            dataKey: 'descripcion'
          },
          {
            title: 'Cantidad',
            dataKey: 'cantidad'
          },
          {
            title: 'Deuda Historica',
            dataKey: 'deuda_historica'
          },
          {
            title: 'Pagos',
            dataKey: 'pagos'
          },
          {
            title: 'Porcentaje',
            dataKey: 'prom'
          },
        ],
        body: this.data,
        margin: { left: 10, top: 40 }
      })

      doc.save(`estadistico_${this.objetoSelector.remesa_desde}.pdf`)

    },

    switchClick() {
      this.objetoSelector.id_empresa = 0
      this.$refs.form.resetValidation()
    },

    agregarAuxiliar() {
      if (this.auxSeleccionado == 'Aux1') {
        this.eligeAux1 = true
      } else if (this.auxSeleccionado == 'Aux2') {
        this.eligeAux2 = true
      } else if (this.auxSeleccionado == 'Aux3') {
        this.eligeAux3 = true
      } else if (this.auxSeleccionado == 'Aux4') {
        this.eligeAux4 = true
      } else if (this.auxSeleccionado == 'Aux5') {
        this.eligeAux5 = true
      } else if (this.auxSeleccionado == 'Aux6') {
        this.eligeAux6 = true
      } else if (this.auxSeleccionado == 'Aux7') {
        this.eligeAux7 = true
      } else if (this.auxSeleccionado == 'Aux8') {
        this.eligeAux8 = true
      } else if (this.auxSeleccionado == 'Aux9') {
        this.eligeAux9 = true
      } else if (this.auxSeleccionado == 'Aux10') {
        this.eligeAux10 = true
      } else if (this.auxSeleccionado == 'Aux11') {
        this.eligeAux11 = true
      } else if (this.auxSeleccionado == 'Aux12') {
        this.eligeAux12 = true
      } else if (this.auxSeleccionado == 'Aux13') {
        this.eligeAux13 = true
      } else if (this.auxSeleccionado == 'Aux14') {
        this.eligeAux14 = true
      } else if (this.auxSeleccionado == 'Aux15') {
        this.eligeAux15 = true
      } else if (this.auxSeleccionado == 'Aux16') {
        this.eligeAux16 = true
      } else if (this.auxSeleccionado == 'Aux17') {
        this.eligeAux17 = true
      } else if (this.auxSeleccionado == 'Aux18') {
        this.eligeAux18 = true
      } else if (this.auxSeleccionado == 'Aux19') {
        this.eligeAux19 = true
      } else if (this.auxSeleccionado == 'Aux20') {
        this.eligeAux20 = true
      } else if (this.auxSeleccionado == 'Aux21') {
        this.eligeAux21 = true
      } else if (this.auxSeleccionado == 'Aux22') {
        this.eligeAux22 = true
      } else if (this.auxSeleccionado == 'Aux23') {
        this.eligeAux23 = true
      } else if (this.auxSeleccionado == 'Aux24') {
        this.eligeAux24 = true
      } else if (this.auxSeleccionado == 'Aux25') {
        this.eligeAux25 = true
      } else if (this.auxSeleccionado == 'Aux26') {
        this.eligeAux26 = true
      } else if (this.auxSeleccionado == 'Aux27') {
        this.eligeAux27 = true
      } else if (this.auxSeleccionado == 'Aux28') {
        this.eligeAux28 = true
      } else if (this.auxSeleccionado == 'Aux29') {
        this.eligeAux29 = true
      } else if (this.auxSeleccionado == 'Aux30') {
        this.eligeAux30 = true
      } else if (this.auxSeleccionado == 'Aux31') {
        this.eligeAux31 = true
      } else if (this.auxSeleccionado == 'Aux32') {
        this.eligeAux32 = true
      } else if (this.auxSeleccionado == 'Aux33') {
        this.eligeAux33 = true
      } else if (this.auxSeleccionado == 'Aux34') {
        this.eligeAux34 = true
      } else if (this.auxSeleccionado == 'Aux35') {
        this.eligeAux35 = true
      } else if (this.auxSeleccionado == 'Aux36') {
        this.eligeAux36 = true
      } else if (this.auxSeleccionado == 'Aux37') {
        this.eligeAux37 = true
      } else if (this.auxSeleccionado == 'Aux38') {
        this.eligeAux38 = true
      } else if (this.auxSeleccionado == 'Aux39') {
        this.eligeAux39 = true
      } else if (this.auxSeleccionado == 'Aux40') {
        this.eligeAux40 = true
      } else if (this.auxSeleccionado == 'Aux41') {
        this.eligeAux41 = true
      } else if (this.auxSeleccionado == 'Aux42') {
        this.eligeAux42 = true
      } else if (this.auxSeleccionado == 'Aux43') {
        this.eligeAux43 = true
      } else if (this.auxSeleccionado == 'Aux44') {
        this.eligeAux44 = true
      } else if (this.auxSeleccionado == 'Aux45') {
        this.eligeAux45 = true
      } else if (this.auxSeleccionado == 'Aux46') {
        this.eligeAux46 = true
      } else if (this.auxSeleccionado == 'Aux47') {
        this.eligeAux47 = true
      } else if (this.auxSeleccionado == 'Aux48') {
        this.eligeAux48 = true
      } else if (this.auxSeleccionado == 'Aux49') {
        this.eligeAux49 = true
      } else if (this.auxSeleccionado == 'Aux50') {
        this.eligeAux50 = true
      }
      this.auxSeleccionado = ''
    },

    cancelarSeleccion() {
      this.objetoSelector = this.iniciarFormulario()
      this.switch1 = false
      this.$refs.form.resetValidation()
      this.data = []
      this.eligeAux1 = false
      this.eligeAux2 = false
      this.eligeAux3 = false
      this.eligeAux4 = false
      this.eligeAux5 = false
      this.eligeAux6 = false
      this.eligeAux7 = false
      this.eligeAux8 = false
      this.eligeAux9 = false
      this.eligeAux10 = false
      this.eligeAux11 = false
      this.eligeAux12 = false
      this.eligeAux13 = false
      this.eligeAux14 = false
      this.eligeAux15 = false
      this.eligeAux16 = false
      this.eligeAux17 = false
      this.eligeAux18 = false
      this.eligeAux19 = false
      this.eligeAux20 = false
      this.eligeAux21 = false
      this.eligeAux22 = false
      this.eligeAux23 = false
      this.eligeAux24 = false
      this.eligeAux25 = false
      this.eligeAux26 = false
      this.eligeAux27 = false
      this.eligeAux28 = false
      this.eligeAux29 = false
      this.eligeAux30 = false
      this.eligeAux31 = false
      this.eligeAux32 = false
      this.eligeAux33 = false
      this.eligeAux34 = false
      this.eligeAux35 = false
      this.eligeAux36 = false
      this.eligeAux37 = false
      this.eligeAux38 = false
      this.eligeAux39 = false
      this.eligeAux40 = false
      this.eligeAux41 = false
      this.eligeAux42 = false
      this.eligeAux43 = false
      this.eligeAux44 = false
      this.eligeAux45 = false
      this.eligeAux46 = false
      this.eligeAux47 = false
      this.eligeAux48 = false
      this.eligeAux49 = false
      this.eligeAux50 = false
    },

    guardarCambios() {
      console.log(this.objetoSelector)
    },

    async getEmpresas() {
      this.empresas = await this.obtenerEmpresas()
      this.empresas.sort(function (a, b) {
        if (a.descripcion > b.descripcion) {
          return 1
        }
        if (a.descripcion < b.descripcion) {
          return -1
        }
        return 0;
      })
    },

    async obtenerSeleccion() {      
      try {
        if(this.descargaDirecta == "SI"){
          this.btnLoading = true
          let header = { 'token': this.token }
          let configuracion = { headers: header, params: this.objetoSelector, responseType: 'arraybuffer' }
          let response = await this.axios.get(this.url, configuracion)          
          const url = URL.createObjectURL(new Blob([response.data], {
            type: 'application/vnd.ms-excel'
          }))          
          this.mostrarMensaje(`Consulta exitosa.`, "success")
          this.btnLoading = false
          const link = document.createElement('a')
          link.href = url
          link.setAttribute('download', this.nombreArchivo)
          document.body.appendChild(link)
          link.click()
        }else {
          this.btnLoading = true
          let header = { 'token': this.token }
          let configuracion = { headers: header, params: this.objetoSelector }
          let response = await this.axios.get(this.url, configuracion)
          this.data = response.data[0]
          this.mostrarMensaje(`Consulta exitosa. ${this.data.length} registros`, "success")
        }
      } catch (error) {
        console.log("Error POST: " + error)
        this.mostrarMensaje("Error al procesar la consulta.", "error")
      }
      this.btnLoading = false
    },

    mostrarMensaje(mensaje, color) {
      this.muestraMensaje = true
      this.mensaje = mensaje
      this.colorMensaje = color
    },

    iniciarFormulario() {
      return {
        id_empresa: 0,
        id_situacion_desde: 1,
        id_situacion_hasta: 99999999,
        id_gestion_desde: 1,
        id_gestion_hasta: 99999999,
        deuda_desde: 1,
        deuda_hasta: 99999999,
        remesa_desde: 1,
        remesa_hasta: 99999,
        id_situacion_omite_desde: 999999999999,
        id_situacion_omite_hasta: 999999999999,
        id_gestion_omite_desde: 999999999999,
        id_gestion_omite_hasta: 999999999999,
        deuda_omite_desde: 999999999999,
        deuda_omite_hasta: 999999999999,
        remesa_omite_desde: 999999999999,
        remesa_omite_hasta: 999999999999,
        politica_desde: 1,
        politica_hasta: 99999,
        politica_omite_desde: 99999,
        politica_omite_hasta: 99999,
        id_deudor_desde: 1,
        id_deudor_hasta: 99999999999999,
        id_deudor_omite_desde: 99999999999999,
        id_deudor_omite_hasta: 99999999999999,
        fecha_cierre_desde: '',
        fecha_cierre_hasta: '',
        fecha_cierre_omite_desde: '',
        fecha_cierre_omite_hasta: '',
        fecha_recepcion_desde: '',
        fecha_recepcion_hasta: '',
        fecha_recepcion_omite_desde: '',
        fecha_recepcion_omite_hasta: '',
        telEfectivos: false,
        celIplan: false,
        celIplanLc: false,
        net2phone: false,
        aux1_desde: '',
        aux1_hasta: '',
        aux1_omite_desde: '',
        aux1_omite_hasta: '',
        aux2_desde: '',
        aux2_hasta: '',
        aux2_omite_desde: '',
        aux2_omite_hasta: '',
        aux3_desde: '',
        aux3_hasta: '',
        aux3_omite_desde: '',
        aux3_omite_hasta: '',
        aux4_desde: '',
        aux4_hasta: '',
        aux4_omite_desde: '',
        aux4_omite_hasta: '',
        aux5_desde: '',
        aux5_hasta: '',
        aux5_omite_desde: '',
        aux5_omite_hasta: '',
        aux6_desde: '',
        aux6_hasta: '',
        aux6_omite_desde: '',
        aux6_omite_hasta: '',
        aux7_desde: '',
        aux7_hasta: '',
        aux7_omite_desde: '',
        aux7_omite_hasta: '',
        aux8_desde: '',
        aux8_hasta: '',
        aux8_omite_desde: '',
        aux8_omite_hasta: '',
        aux9_desde: '',
        aux9_hasta: '',
        aux9_omite_desde: '',
        aux9_omite_hasta: '',
        aux10_desde: '',
        aux10_hasta: '',
        aux10_omite_desde: '',
        aux10_omite_hasta: '',
        aux11_desde: '',
        aux11_hasta: '',
        aux11_omite_desde: '',
        aux11_omite_hasta: '',
        aux12_desde: '',
        aux12_hasta: '',
        aux12_omite_desde: '',
        aux12_omite_hasta: '',
        aux13_desde: '',
        aux13_hasta: '',
        aux13_omite_desde: '',
        aux13_omite_hasta: '',
        aux14_desde: '',
        aux14_hasta: '',
        aux14_omite_desde: '',
        aux14_omite_hasta: '',
        aux15_desde: '',
        aux15_hasta: '',
        aux15_omite_desde: '',
        aux15_omite_hasta: '',
        aux16_desde: '',
        aux16_hasta: '',
        aux16_omite_desde: '',
        aux16_omite_hasta: '',
        aux17_desde: '',
        aux17_hasta: '',
        aux17_omite_desde: '',
        aux17_omite_hasta: '',
        aux18_desde: '',
        aux18_hasta: '',
        aux18_omite_desde: '',
        aux18_omite_hasta: '',
        aux19_desde: '',
        aux19_hasta: '',
        aux19_omite_desde: '',
        aux19_omite_hasta: '',
        aux20_desde: '',
        aux20_hasta: '',
        aux20_omite_desde: '',
        aux20_omite_hasta: '',
        aux21_desde: '',
        aux21_hasta: '',
        aux21_omite_desde: '',
        aux21_omite_hasta: '',
        aux22_desde: '',
        aux22_hasta: '',
        aux22_omite_desde: '',
        aux22_omite_hasta: '',
        aux23_desde: '',
        aux23_hasta: '',
        aux23_omite_desde: '',
        aux23_omite_hasta: '',
        aux24_desde: '',
        aux24_hasta: '',
        aux24_omite_desde: '',
        aux24_omite_hasta: '',
        aux25_desde: '',
        aux25_hasta: '',
        aux25_omite_desde: '',
        aux25_omite_hasta: '',
        aux26_desde: '',
        aux26_hasta: '',
        aux26_omite_desde: '',
        aux26_omite_hasta: '',
        aux27_desde: '',
        aux27_hasta: '',
        aux27_omite_desde: '',
        aux27_omite_hasta: '',
        aux28_desde: '',
        aux28_hasta: '',
        aux28_omite_desde: '',
        aux28_omite_hasta: '',
        aux29_desde: '',
        aux29_hasta: '',
        aux29_omite_desde: '',
        aux29_omite_hasta: '',
        aux30_desde: '',
        aux30_hasta: '',
        aux30_omite_desde: '',
        aux30_omite_hasta: '',
        aux31_desde: '',
        aux31_hasta: '',
        aux31_omite_desde: '',
        aux31_omite_hasta: '',
        aux32_desde: '',
        aux32_hasta: '',
        aux32_omite_desde: '',
        aux32_omite_hasta: '',
        aux33_desde: '',
        aux33_hasta: '',
        aux33_omite_desde: '',
        aux33_omite_hasta: '',
        aux34_desde: '',
        aux34_hasta: '',
        aux34_omite_desde: '',
        aux34_omite_hasta: '',
        aux35_desde: '',
        aux35_hasta: '',
        aux35_omite_desde: '',
        aux35_omite_hasta: '',
        aux36_desde: '',
        aux36_hasta: '',
        aux36_omite_desde: '',
        aux36_omite_hasta: '',
        aux37_desde: '',
        aux37_hasta: '',
        aux37_omite_desde: '',
        aux37_omite_hasta: '',
        aux38_desde: '',
        aux38_hasta: '',
        aux38_omite_desde: '',
        aux38_omite_hasta: '',
        aux39_desde: '',
        aux39_hasta: '',
        aux39_omite_desde: '',
        aux39_omite_hasta: '',
        aux40_desde: '',
        aux40_hasta: '',
        aux40_omite_desde: '',
        aux40_omite_hasta: '',
        aux41_desde: '',
        aux41_hasta: '',
        aux41_omite_desde: '',
        aux41_omite_hasta: '',
        aux42_desde: '',
        aux42_hasta: '',
        aux42_omite_desde: '',
        aux42_omite_hasta: '',
        aux43_desde: '',
        aux43_hasta: '',
        aux43_omite_desde: '',
        aux43_omite_hasta: '',
        aux44_desde: '',
        aux44_hasta: '',
        aux44_omite_desde: '',
        aux44_omite_hasta: '',
        aux45_desde: '',
        aux45_hasta: '',
        aux45_omite_desde: '',
        aux45_omite_hasta: '',
        aux46_desde: '',
        aux46_hasta: '',
        aux46_omite_desde: '',
        aux46_omite_hasta: '',
        aux47_desde: '',
        aux47_hasta: '',
        aux47_omite_desde: '',
        aux47_omite_hasta: '',
        aux48_desde: '',
        aux48_hasta: '',
        aux48_omite_desde: '',
        aux48_omite_hasta: '',
        aux49_desde: '',
        aux49_hasta: '',
        aux49_omite_desde: '',
        aux49_omite_hasta: '',
        aux50_desde: '',
        aux50_hasta: '',
        aux50_omite_desde: '',
        aux50_omite_hasta: ''
      }
    }

  }
}


