import selector from '../selector/index'
import url from '../../../urls/index'

export default {
  name: 'estadistico-pagos',
  components: {selector},
  props: [],
  data () {
    return {
      campos: {
        id_situacion: 'id_situacion',
        descripcion: 'descripcion',
        cantidad: 'cantidad',
        deuda_historica: 'deuda_historica',
        pagos: 'pagos',
        Porcentaje: 'prom',
      },
      url: url.reportes.urlEstadistico

    }
  },
  computed: {

  },
  mounted () {

  },
  methods: {

  }
}


