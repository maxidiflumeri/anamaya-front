import selector from '../selector/index'
import url from '../../../urls/index'

export default {
  name: 'base-neotel',
  components: {selector},
  props: [],
  data () {
    return {
      campos: {
        telefono1: {
          field: "telefono1",
          callback: (value) => {
            return `'${value.toString()}`;
          },
        },
        telefono2: 'telefono2',
        telefono3: 'telefono3',
        telefono4: 'telefono4',
        telefono5: 'telefono5',
        telefono6: 'telefono6',
        telefono7: 'telefono7',
        telefono8: 'telefono8',
        data: 'data',
        campo1: 'campo1',
        campo2: 'campo2',
        campo3: 'campo3',
        campo4: 'campo4',
        campo5: 'campo5',
        campo6: 'campo6',
        campo7: 'campo7',
        campo8: 'campo8',
        campo9: 'campo9',
        campo10: 'campo10'
      },
      url: url.reportes.urlBaseNeotel      
    }
  },
  computed: {

  },
  mounted () {

  },
  methods: {

  }
}


