import { mapState } from "vuex"
import url from '../../urls/index.js'

export default {
  name: 'mi-perfil',
  components: {},
  props: [],
  data() {
    return {
      usuarioLocal: {},
      roles: [],
      cargando: true,
      passwordActual: '',
      passwordNueva: '',
      passwordNuevaConfirm: '',
      reglas: [
        value => !!value || 'El campo contraseña es requerido!',
        value => (value && value.length >= 8) || 'Mínimo 8 caracteres',
        value => (value && value.length <= 12) || 'Maximo 12 caracteres permitidos'                        
      ],
      dialog: false,
      muestraMensaje: false,
      mensaje: '',
      colorMensaje: '',
      show1: false,
      show2: false,
      show3: false
    }
  },
  computed: {
    ...mapState('auth', ['usuario', 'token']),
    nombreRol() {
      const rol = this.roles.find(rol => rol.id_rol == this.usuarioLocal.id_rol)
      return rol.nombre
    },
    validarAgregar(){      
      if(this.passwordActual == '' || this.passwordNueva == '' || this.passwordNuevaConfirm == ''){
        return false
      }else if(this.passwordNueva.length <= 7 || this.passwordNueva.length >= 13){
        return false
      }else if(this.passwordNueva != this.passwordNuevaConfirm){
        return false
      }else{
        return true
      }
    },
    contraseñaErronea(){
      if(this.passwordNueva != this.passwordNuevaConfirm){
        return true
      }else {
        return false
      }
    }

  },
  mounted() {
    this.obtenerUsuario()    
  },
  methods: {
    async obtenerUsuario() {
      try {
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        const data = await this.axios.get(url.usuarios.urlConsultar + '?id=' + this.usuario.id_usuario, configuracion)
        this.usuarioLocal = data.data
        this.obtenerRoles()                     
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },
    async obtenerRoles() {
      try {
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        const data = await this.axios.get(url.roles.urlConsultar, configuracion)
        this.roles = data.data
        this.cargando = false                
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },
    async cambiarContraseña() {
      try {
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        const body = {
          passwordActual: this.passwordActual,
          passwordNueva: this.passwordNueva
        }
        const response = await this.axios.put(url.usuarios.urlModificarPassword + '/' + this.usuario.id_usuario, body, configuracion)                       
        if(response.data.error == 100){
          this.cerrarDialog()
          this.mostrarMensaje('La clave actual es incorrecta.', 'error')
        }else{
          this.cerrarDialog()
          this.mostrarMensaje('Contraseña actualizada exitosamente.', 'success')
        }        
        
      } catch (error) {
        console.log(error)        
      }
    },

    PantallaPrincipal(){
      this.$router.push({ path: '/PantallaPrincipal' })
    },

    cerrarDialog(){
      this.dialog = false
      this.passwordActual = ''
      this.passwordNueva = ''
      this.passwordNuevaConfirm = ''
    },

    mostrarMensaje(mensaje, color) {
      this.muestraMensaje = true
      this.mensaje = mensaje
      this.colorMensaje = color
    },

  }
}


