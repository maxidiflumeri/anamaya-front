import { mapState } from 'vuex'

export default {

  name: 'crm-pagos',

  props: {
    id_deudor: String
  },

  data() {
    return {
      cabeceras: [
        { text: 'Nro Comprobante', value: 'nro_comprobante' },
        { text: 'Fecha Pago', value: 'fecha_pago' },
        { text: 'Fecha Aplicación', value: 'fecha_aplicacion' },
        { text: 'Fecha Rendición', value: 'fecha_rendicion' },
        { text: 'Importe Total', value: 'importe_total' },
        { text: 'Importe Aplicado', value: 'importe_aplicado' },
        { text: 'Tipo Pago', value: 'id_tipo_pago' },
        { text: 'Entidad Recaudadora', value: 'id_entidad' },
        { text: 'Fecha Registro', value: 'fecha_registro' },
        { text: 'Aux1', value: 'aux1' },
        { text: 'Aux2', value: 'aux2' },
        { text: 'Aux3', value: 'aux3' },
      ],
      headerProps: {
        sortByText: 'Ordenar por '
      },
    }
  },

  computed: {
    ...mapState('deudor', ['pagosPorIdDeudor']),
  },

}


