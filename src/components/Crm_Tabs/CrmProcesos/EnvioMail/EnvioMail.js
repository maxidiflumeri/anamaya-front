import { VueEditor } from "vue2-editor";
import { mapActions, mapState } from "vuex";

export default {
  name: "envio-mail",
  components: { VueEditor },
  props: [],
  data() {
    return {
      idllamada: 0,
      grabacion: '',
      para: "",
      asunto: "",
      texto: "",
      btnLoading: false,
      archivosAdjuntos: null,
      template: "",
    };
  },
  computed: {
    habilitarEnvio() {
      if (
        this.para.length == 0 ||
        this.asunto.length == 0 ||
        this.texto.length == 0 ||
        this.btnLoading == true
      ) {
        return true;
      } else {
        return false;
      }
    },
    ...mapState("deudor", ["deudor", "correos"]),
    ...mapState("auth", ["token"]),
    ...mapState("empresas", ["templateCorreosPorEmpresa"]),
    ...mapState('neotel', ['posicion']),
  },

  watch: {
    template() {
      let textoTemp = this.templateCorreosPorEmpresa.find(
        (temp) => temp.id_template == this.template
      );
      if (textoTemp) {

        // INICIO CONVERSION CAMPOS FORMULAS EN EL TEXTO
        let textoTemplate = textoTemp.texto;
        // BUSCO DONDE INICIA LA PRIMERA FORMULA  
        let inicio = textoTemplate.indexOf("{[");
        while (inicio >= 0) {
          // SI HAY INICIO DE LA PRIMERA FORMULA, BUSCO EN QUE POSICION TERMINA
          let fin = textoTemplate.indexOf("]}");
          // EXTRAIGO LA FORMULA COMPLETA A PARTIR DE LA POSICION DE INICIO Y FIN
          let formulaCompleta = textoTemplate.substring(inicio, fin + 2);
          // LE SACO LOS DOS PRIMEROS CARACTERES A LA FORMULA ({[)
          let formula = textoTemplate.substring(inicio + 2, fin);


          // ACA EXTRAIGO LA PARTE DEL VALOR DE LA FORMULA. EJ: PQ30-HIST (EXTRAIGO EL 30)
          var regex = /(\d+)/g;
          let formulaSplit = formula.split('-')

          let valorFormula = "";
          formulaSplit[0].match(regex).forEach((valor) => {
            if (
              valorFormula.indexOf(".") >= 0 ||
              formulaSplit[0].match(regex).length == 1
            ) {
              valorFormula = valorFormula + valor;
            } else {
              valorFormula = valorFormula + valor + ".";
            }
          });

          // ACA EXTRAIGO LA OPERACION QUE TENGO QUE HACER, SIGUIEN EL EJEMPLO DEL COMENTARIO ANTERIOR, EXTRAIGO PQ
          var regex2 = /[^\d.-]/g;
          let operacionFormula = "";
          formulaSplit[0].match(regex2).forEach((valor) => {
            operacionFormula = operacionFormula + valor;
          });

          let montoParcial = 0;
          let deudaCalculada = 0;
          switch (operacionFormula) {
            case "su":
              if (formulaSplit[1] == 'hist') {
                deudaCalculada = this.deudor.deuda_historica + parseFloat(valorFormula)
              } else if (formulaSplit[1] == 'actu') {
                deudaCalculada = this.deudor.deuda_actualizada + parseFloat(valorFormula)
              }
              break;
            case "re":
              if (formulaSplit[1] == 'hist') {
                deudaCalculada = this.deudor.deuda_historica - valorFormula
              } else if (formulaSplit[1] == 'actu') {
                deudaCalculada = this.deudor.deuda_actualizada - valorFormula
              }
              break;
            case "di":
              if (formulaSplit[1] == 'hist') {
                deudaCalculada = this.deudor.deuda_historica / valorFormula
              } else if (formulaSplit[1] == 'actu') {
                deudaCalculada = this.deudor.deuda_actualizada / valorFormula
              }
              break;
            case "mu":
              if (formulaSplit[1] == 'hist') {
                deudaCalculada = this.deudor.deuda_historica * valorFormula
              } else if (formulaSplit[1] == 'actu') {
                deudaCalculada = this.deudor.deuda_actualizada * valorFormula
              }
              break;
            case "pq":
              if (formulaSplit[1] == 'hist') {
                montoParcial = (this.deudor.deuda_historica * valorFormula) / 100;
                deudaCalculada = this.deudor.deuda_historica - montoParcial;
              } else if (formulaSplit[1] == 'actu') {
                montoParcial = (this.deudor.deuda_actualizada * valorFormula) / 100;
                deudaCalculada = this.deudor.deuda_actualizada - montoParcial;
              }
              break;
            case "pr":
              if (formulaSplit[1] == 'hist') {
                montoParcial = (this.deudor.deuda_historica * valorFormula) / 100;
                deudaCalculada = this.deudor.deuda_historica + montoParcial;
              } else if (formulaSplit[1] == 'actu') {
                montoParcial = (this.deudor.deuda_actualizada * valorFormula) / 100;
                deudaCalculada = this.deudor.deuda_actualizada + montoParcial;
              }
              break;
          }
          if (formulaSplit[1] == 'hist') {
            textoTemplate = textoTemplate.replace(
              `@deuda_historica@${formulaCompleta}`,
              this.trunc(deudaCalculada, 2)
            );
          } else if (formulaSplit[1] == 'actu') {
            textoTemplate = textoTemplate.replace(
              `@deuda_actualizada@${formulaCompleta}`,
              this.trunc(deudaCalculada, 2)
            );
          }
          inicio = textoTemplate.indexOf("{[");
        }
        // FIN CONVERSION CAMPOS FORMULAS EN EL TEXTO


        // INICIO CONVERSION CAMPOS FORMULAS EN EL ASUNTO
        let asuntoTemplate = textoTemp.asunto;
        // BUSCO DONDE INICIA LA PRIMERA FORMULA  
        inicio = asuntoTemplate.indexOf("{[");
        while (inicio >= 0) {
          // SI HAY INICIO DE LA PRIMERA FORMULA, BUSCO EN QUE POSICION TERMINA
          let fin = asuntoTemplate.indexOf("]}");
          // EXTRAIGO LA FORMULA COMPLETA A PARTIR DE LA POSICION DE INICIO Y FIN
          let formulaCompleta = asuntoTemplate.substring(inicio, fin + 2);
          // LE SACO LOS DOS PRIMEROS CARACTERES A LA FORMULA ({[)
          let formula = asuntoTemplate.substring(inicio + 2, fin);


          // ACA EXTRAIGO LA PARTE DEL VALOR DE LA FORMULA. EJ: PQ30-HIST (EXTRAIGO EL 30)
          regex = /(\d+)/g;
          let formulaSplit = formula.split('-')

          let valorFormula = "";
          formulaSplit[0].match(regex).forEach((valor) => {
            if (
              valorFormula.indexOf(".") >= 0 ||
              formulaSplit[0].match(regex).length == 1
            ) {
              valorFormula = valorFormula + valor;
            } else {
              valorFormula = valorFormula + valor + ".";
            }
          });

          // ACA EXTRAIGO LA OPERACION QUE TENGO QUE HACER, SIGUIEN EL EJEMPLO DEL COMENTARIO ANTERIOR, EXTRAIGO PQ
          regex2 = /[^\d.-]/g;
          let operacionFormula = "";
          formulaSplit[0].match(regex2).forEach((valor) => {
            operacionFormula = operacionFormula + valor;
          });

          let montoParcial = 0;
          let deudaCalculada = 0;
          switch (operacionFormula) {
            case "su":
              if (formulaSplit[1] == 'hist') {
                deudaCalculada = this.deudor.deuda_historica + parseFloat(valorFormula)
              } else if (formulaSplit[1] == 'actu') {
                deudaCalculada = this.deudor.deuda_actualizada + parseFloat(valorFormula)
              }
              break;
            case "re":
              if (formulaSplit[1] == 'hist') {
                deudaCalculada = this.deudor.deuda_historica - valorFormula
              } else if (formulaSplit[1] == 'actu') {
                deudaCalculada = this.deudor.deuda_actualizada - valorFormula
              }
              break;
            case "di":
              if (formulaSplit[1] == 'hist') {
                deudaCalculada = this.deudor.deuda_historica / valorFormula
              } else if (formulaSplit[1] == 'actu') {
                deudaCalculada = this.deudor.deuda_actualizada / valorFormula
              }
              break;
            case "mu":
              if (formulaSplit[1] == 'hist') {
                deudaCalculada = this.deudor.deuda_historica * valorFormula
              } else if (formulaSplit[1] == 'actu') {
                deudaCalculada = this.deudor.deuda_actualizada * valorFormula
              }
              break;
            case "pq":
              if (formulaSplit[1] == 'hist') {
                montoParcial = (this.deudor.deuda_historica * valorFormula) / 100;
                deudaCalculada = this.deudor.deuda_historica - montoParcial;
              } else if (formulaSplit[1] == 'actu') {
                montoParcial = (this.deudor.deuda_actualizada * valorFormula) / 100;
                deudaCalculada = this.deudor.deuda_actualizada - montoParcial;
              }
              break;
            case "pr":
              if (formulaSplit[1] == 'hist') {
                montoParcial = (this.deudor.deuda_historica * valorFormula) / 100;
                deudaCalculada = this.deudor.deuda_historica + montoParcial;
              } else if (formulaSplit[1] == 'actu') {
                montoParcial = (this.deudor.deuda_actualizada * valorFormula) / 100;
                deudaCalculada = this.deudor.deuda_actualizada + montoParcial;
              }
              break;
          }
          if (formulaSplit[1] == 'hist') {
            asuntoTemplate = asuntoTemplate.replace(
              `@deuda_historica@${formulaCompleta}`,
              this.trunc(deudaCalculada, 2)
            );
          } else if (formulaSplit[1] == 'actu') {
            asuntoTemplate = asuntoTemplate.replace(
              `@deuda_actualizada@${formulaCompleta}`,
              this.trunc(deudaCalculada, 2)
            );
          }
          inicio = asuntoTemplate.indexOf("{[");
        }

        textoTemplate = textoTemplate.replace(
          "@nro_cliente3@",
          this.deudor.nro_cliente3
        );
        textoTemplate = textoTemplate.replace(
          "@nro_cliente2@",
          this.deudor.nro_cliente2
        );
        textoTemplate = textoTemplate.replace(
          "@nro_cliente1@",
          this.deudor.nro_cliente1
        );
        textoTemplate = textoTemplate.replace(
          "@nombre_apellido@",
          this.deudor.nombre_apellido
        );
        textoTemplate = textoTemplate.replace(
          "@nombre_contacto@",
          this.deudor.nombre_contacto
        );
        textoTemplate = textoTemplate.replace(
          "@nro_doc@",
          this.deudor.nro_doc
        );
        textoTemplate = textoTemplate.replace(
          "@deuda_historica@",
          this.deudor.deuda_historica
        );
        textoTemplate = textoTemplate.replace(
          "@deuda_actualizada@",
          this.deudor.deuda_actualizada
        );

        asuntoTemplate = asuntoTemplate.replace(
          "@nro_cliente3@",
          this.deudor.nro_cliente3
        );
        asuntoTemplate = asuntoTemplate.replace(
          "@nro_cliente2@",
          this.deudor.nro_cliente2
        );
        asuntoTemplate = asuntoTemplate.replace(
          "@nro_cliente1@",
          this.deudor.nro_cliente1
        );
        asuntoTemplate = asuntoTemplate.replace(
          "@nombre_apellido@",
          this.deudor.nombre_apellido
        );
        asuntoTemplate = asuntoTemplate.replace(
          "@nombre_contacto@",
          this.deudor.nombre_contacto
        );
        asuntoTemplate = asuntoTemplate.replace(
          "@nro_doc@",
          this.deudor.nro_doc
        );
        asuntoTemplate = asuntoTemplate.replace(
          "@deuda_historica@",
          this.deudor.deuda_historica
        );
        asuntoTemplate = asuntoTemplate.replace(
          "@deuda_actualizada@",
          this.deudor.deuda_actualizada
        );
        this.texto = textoTemplate;
        this.asunto = asuntoTemplate;
      }
    },
  },

  mounted() {
    this.obtenerTemplateCorreosPorIdEmpresa(this.deudor.id_empresa);
  },
  methods: {
    ...mapActions("empresas", ["obtenerTemplateCorreosPorIdEmpresa"]),

    trunc(x, posiciones = 0) {
      var s = x.toString();
      var l = s.length;
      var decimalLength = s.indexOf(".") + 1;

      if (l - decimalLength <= posiciones) {
        return x;
      }
      // Parte decimal del número
      var isNeg = x < 0;
      var decimal = x % 1;
      var entera = isNeg ? Math.ceil(x) : Math.floor(x);
      // Parte decimal como número entero
      // Ejemplo: parte decimal = 0.77
      // decimalFormated = 0.77 * (10^posiciones)
      // si posiciones es 2 ==> 0.77 * 100
      // si posiciones es 3 ==> 0.77 * 1000
      var decimalFormated = Math.floor(
        Math.abs(decimal) * Math.pow(10, posiciones)
      );
      // Sustraemos del número original la parte decimal
      // y le sumamos la parte decimal que hemos formateado
      var finalNum =
        entera +
        (decimalFormated / Math.pow(10, posiciones)) * (isNeg ? -1 : 1);

      return finalNum;
    },

    cerrarDialog(seEnvio) {
      this.$emit("click", seEnvio);
      this.para = "";
      this.asunto = "";
      this.texto = "";
      this.btnLoading = false;
      this.archivosAdjuntos = null;
      this.template = "";
    },

    aceptarMail() {
      let textoLowerCase = this.texto.toLowerCase()
      if (textoLowerCase.indexOf('adjunto') != -1 && !this.archivosAdjuntos) {
        let resultado = confirm('Parece que te has olvidado de adjuntar un archivo. \n\nEn tu mensaje has escrito "adjunto", pero no lleva ningún archivo adjunto. ¿Quieres enviarlo igualmente?')
        if (resultado) {
          this.enviarMail()
        }
      } else {
        this.enviarMail()
      }
    },

    async enviarMail() {
      this.verificarDatosLlamada()
      this.btnLoading = true;
      const fd = new FormData();
      if (this.archivosAdjuntos) {
        this.archivosAdjuntos.forEach((adjunto) => {
          fd.append("adjuntos", adjunto);
        });
      }

      fd.append("id_llamada", this.idllamada);
      fd.append("grabacion", this.grabacion)
      fd.append("id_deudor", this.deudor.id_deudor);
      fd.append("id_politica", this.deudor.id_politica);
      fd.append("destinatario", this.para);
      fd.append("asunto", this.asunto);
      fd.append("texto", this.texto);
      try {
        let header = {
          token: this.token,
          "Content-Type": "multipart/form-data",
        };
        let configuracion = { headers: header };
        await this.axios.post("/api/servicio/enviomail", fd, configuracion);
        this.cerrarDialog(1);
      } catch (error) {
        console.log(error.response);
        this.cerrarDialog(2);
      }
    },

    verificarDatosLlamada() {
      if (this.posicion.data) {
        if (this.posicion.data.data != this.deudor.id_deudor) {
          this.idllamada = 0
          this.grabacion = ''
        } else {
          this.idllamada = this.posicion.data.idllamada
          this.grabacion = this.posicion.data.grabacion
        }
      } else {
        this.idllamada = 0
        this.grabacion = ''
      }
    },
  },
};
