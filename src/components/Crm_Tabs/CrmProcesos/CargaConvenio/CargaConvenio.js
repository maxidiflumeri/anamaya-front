import { mapActions, mapState } from "vuex"
import convenioApi from '../../../../services/Apis/BackEnd/convenio'
import moment from 'moment'

export default {
  name: 'carga-convenio',
  components: {},
  props: { facturas: Array },
  data() {
    return {
      idllamada: 0,
      grabacion: '',
      btnLoading: false,
      dataPickerConvenio: false,
      fecha_convenio: this.formatearFecha(new Date(), 0),
      dataPickerFecha1: false,
      fecha_vto1: '',
      monto_cuota1: 0,
      dataPickerFecha2: false,
      fecha_vto2: '',
      monto_cuota2: 0,
      dataPickerFecha3: false,
      fecha_vto3: '',
      monto_cuota3: 0,
      dataPickerFecha4: false,
      fecha_vto4: '',
      monto_cuota4: 0,
      dataPickerFecha5: false,
      fecha_vto5: '',
      monto_cuota5: 0,
      dataPickerFecha6: false,
      fecha_vto6: '',
      monto_cuota6: 0,
      dataPickerFecha7: false,
      fecha_vto7: '',
      monto_cuota7: 0,
      dataPickerFecha8: false,
      fecha_vto8: '',
      monto_cuota8: 0,
      dataPickerFecha9: false,
      fecha_vto9: '',
      monto_cuota9: 0,
      dataPickerFecha10: false,
      fecha_vto10: '',
      monto_cuota10: 0,
      dataPickerFecha11: false,
      fecha_vto11: '',
      monto_cuota11: 0,
      dataPickerFecha12: false,
      fecha_vto12: '',
      monto_cuota12: 0,
      cantCuotas: 1,
      idConvenioSeleccionado: '',
      convenioSeleccionado: '',
      montoTotalConvenio: 0
    }
  },
  computed: {
    ...mapState('empresas', ['tiposConveniosPorEmpresa']),
    ...mapState('deudor', ['deudor', 'deudaActualizada', 'direcciones']),
    ...mapState('neotel', ['posicion']),

    habilitarCuotas() {
      if (this.convenioSeleccionado == '') {
        return true
      } else {
        return false
      }
    },
    habilitaConvenioLibre() {
      if (this.convenioSeleccionado.es_libre == 0 || this.convenioSeleccionado == '') {
        return true
      } else {
        return false
      }
    },

    habilitarGenerarConvenio() {
      let diff = this.montoTotalConvenio - this.montoTotalCuotas()
      if (diff < -0.5 || diff > 0.5) {
        return true
      } else if (this.validaFechasMontos()) {
        return true
      } else if (this.direcciones.length == 0) {
        return true
      } else {
        return false
      }
    },
  },

  mounted() {
    console.log(this.facturas)
    this.facturas.forEach(factura => {
      let params = {
        monto: factura.importe1,
        id_tipo_actualizacion: this.deudor.id_tipo_actualizacion,
        fecha: factura.fecha_vencimiento,
        id_iva: this.deudor.id_iva,
        id_empresa: this.deudor.id_empresa,
        id_deudor: this.deudor.id_deudor
      }
      this.obtenerDeudaActualizada(params)
    });
    this.obtenerTiposConveniosPorEmpresa(this.deudor.id_empresa)
  },

  watch: {
    fecha_convenio() {
      this.calcularFechas()
    }
  },

  methods: {
    ...mapActions('empresas', ['obtenerTiposConveniosPorEmpresa']),
    ...mapActions('deudor', ['obtenerDeudaActualizada', 'limpiarDeudaActualizada', 'obtenerFacturasPorIdDeudor', 'obtenerConveniosPorIdDeudor']),

    validaFechasMontos() {
      if (this.montoTotalConvenio == 0) {
        return true
      }
      if (this.cantCuotas == 1) {
        if (this.fecha_vto1.length > 0 && new Date(this.fecha_vto1) <= new Date(this.fecha_convenio)) {
          return true
        } else if (this.monto_cuota1 == 0) {
          return true
        }
      } else if (this.cantCuotas == 2) {
        if (this.fecha_vto1.length > 0 && new Date(this.fecha_vto1) <= new Date(this.fecha_convenio)) {
          return true
        } else if (this.monto_cuota1 == 0) {
          return true
        } else if (this.fecha_vto2.length > 0 && new Date(this.fecha_vto2) <= new Date(this.fecha_vto1)) {
          return true
        } else if (this.monto_cuota2 == 0) {
          return true
        }
      } else if (this.cantCuotas == 3) {
        if (this.fecha_vto1.length > 0 && new Date(this.fecha_vto1) <= new Date(this.fecha_convenio)) {
          return true
        } else if (this.monto_cuota1 == 0) {
          return true
        } else if (this.fecha_vto2.length > 0 && new Date(this.fecha_vto2) <= new Date(this.fecha_vto1)) {
          return true
        } else if (this.monto_cuota2 == 0) {
          return true
        } else if (this.fecha_vto3.length > 0 && new Date(this.fecha_vto3) <= new Date(this.fecha_vto2)) {
          return true
        } else if (this.monto_cuota3 == 0) {
          return true
        }
      } else if (this.cantCuotas == 4) {
        if (this.fecha_vto1.length > 0 && new Date(this.fecha_vto1) <= new Date(this.fecha_convenio)) {
          return true
        } else if (this.monto_cuota1 == 0) {
          return true
        } else if (this.fecha_vto2.length > 0 && new Date(this.fecha_vto2) <= new Date(this.fecha_vto1)) {
          return true
        } else if (this.monto_cuota2 == 0) {
          return true
        } else if (this.fecha_vto3.length > 0 && new Date(this.fecha_vto3) <= new Date(this.fecha_vto2)) {
          return true
        } else if (this.monto_cuota3 == 0) {
          return true
        } else if (this.fecha_vto4.length > 0 && new Date(this.fecha_vto4) <= new Date(this.fecha_vto3)) {
          return true
        } else if (this.monto_cuota4 == 0) {
          return true
        }
      } else if (this.cantCuotas == 5) {
        if (this.fecha_vto1.length > 0 && new Date(this.fecha_vto1) <= new Date(this.fecha_convenio)) {
          return true
        } else if (this.monto_cuota1 == 0) {
          return true
        } else if (this.fecha_vto2.length > 0 && new Date(this.fecha_vto2) <= new Date(this.fecha_vto1)) {
          return true
        } else if (this.monto_cuota2 == 0) {
          return true
        } else if (this.fecha_vto3.length > 0 && new Date(this.fecha_vto3) <= new Date(this.fecha_vto2)) {
          return true
        } else if (this.monto_cuota3 == 0) {
          return true
        } else if (this.fecha_vto4.length > 0 && new Date(this.fecha_vto4) <= new Date(this.fecha_vto3)) {
          return true
        } else if (this.monto_cuota4 == 0) {
          return true
        } else if (this.fecha_vto5.length > 0 && new Date(this.fecha_vto5) <= new Date(this.fecha_vto4)) {
          return true
        } else if (this.monto_cuota5 == 0) {
          return true
        }
      } else if (this.cantCuotas == 6) {
        if (this.fecha_vto1.length > 0 && new Date(this.fecha_vto1) <= new Date(this.fecha_convenio)) {
          return true
        } else if (this.monto_cuota1 == 0) {
          return true
        } else if (this.fecha_vto2.length > 0 && new Date(this.fecha_vto2) <= new Date(this.fecha_vto1)) {
          return true
        } else if (this.monto_cuota2 == 0) {
          return true
        } else if (this.fecha_vto3.length > 0 && new Date(this.fecha_vto3) <= new Date(this.fecha_vto2)) {
          return true
        } else if (this.monto_cuota3 == 0) {
          return true
        } else if (this.fecha_vto4.length > 0 && new Date(this.fecha_vto4) <= new Date(this.fecha_vto3)) {
          return true
        } else if (this.monto_cuota4 == 0) {
          return true
        } else if (this.fecha_vto5.length > 0 && new Date(this.fecha_vto5) <= new Date(this.fecha_vto4)) {
          return true
        } else if (this.monto_cuota5 == 0) {
          return true
        } else if (this.fecha_vto6.length > 0 && new Date(this.fecha_vto6) <= new Date(this.fecha_vto5)) {
          return true
        } else if (this.monto_cuota6 == 0) {
          return true
        }
      } else if (this.cantCuotas == 7) {
        if (this.fecha_vto1.length > 0 && new Date(this.fecha_vto1) <= new Date(this.fecha_convenio)) {
          return true
        } else if (this.monto_cuota1 == 0) {
          return true
        } else if (this.fecha_vto2.length > 0 && new Date(this.fecha_vto2) <= new Date(this.fecha_vto1)) {
          return true
        } else if (this.monto_cuota2 == 0) {
          return true
        } else if (this.fecha_vto3.length > 0 && new Date(this.fecha_vto3) <= new Date(this.fecha_vto2)) {
          return true
        } else if (this.monto_cuota3 == 0) {
          return true
        } else if (this.fecha_vto4.length > 0 && new Date(this.fecha_vto4) <= new Date(this.fecha_vto3)) {
          return true
        } else if (this.monto_cuota4 == 0) {
          return true
        } else if (this.fecha_vto5.length > 0 && new Date(this.fecha_vto5) <= new Date(this.fecha_vto4)) {
          return true
        } else if (this.monto_cuota5 == 0) {
          return true
        } else if (this.fecha_vto6.length > 0 && new Date(this.fecha_vto6) <= new Date(this.fecha_vto5)) {
          return true
        } else if (this.monto_cuota6 == 0) {
          return true
        } else if (this.fecha_vto7.length > 0 && new Date(this.fecha_vto7) <= new Date(this.fecha_vto6)) {
          return true
        } else if (this.monto_cuota7 == 0) {
          return true
        }
      } else if (this.cantCuotas == 8) {
        if (this.fecha_vto1.length > 0 && new Date(this.fecha_vto1) <= new Date(this.fecha_convenio)) {
          return true
        } else if (this.monto_cuota1 == 0) {
          return true
        } else if (this.fecha_vto2.length > 0 && new Date(this.fecha_vto2) <= new Date(this.fecha_vto1)) {
          return true
        } else if (this.monto_cuota2 == 0) {
          return true
        } else if (this.fecha_vto3.length > 0 && new Date(this.fecha_vto3) <= new Date(this.fecha_vto2)) {
          return true
        } else if (this.monto_cuota3 == 0) {
          return true
        } else if (this.fecha_vto4.length > 0 && new Date(this.fecha_vto4) <= new Date(this.fecha_vto3)) {
          return true
        } else if (this.monto_cuota4 == 0) {
          return true
        } else if (this.fecha_vto5.length > 0 && new Date(this.fecha_vto5) <= new Date(this.fecha_vto4)) {
          return true
        } else if (this.monto_cuota5 == 0) {
          return true
        } else if (this.fecha_vto6.length > 0 && new Date(this.fecha_vto6) <= new Date(this.fecha_vto5)) {
          return true
        } else if (this.monto_cuota6 == 0) {
          return true
        } else if (this.fecha_vto7.length > 0 && new Date(this.fecha_vto7) <= new Date(this.fecha_vto6)) {
          return true
        } else if (this.monto_cuota7 == 0) {
          return true
        } else if (this.fecha_vto8.length > 0 && new Date(this.fecha_vto8) <= new Date(this.fecha_vto7)) {
          return true
        } else if (this.monto_cuota8 == 0) {
          return true
        }
      } else if (this.cantCuotas == 9) {
        if (this.fecha_vto1.length > 0 && new Date(this.fecha_vto1) <= new Date(this.fecha_convenio)) {
          return true
        } else if (this.monto_cuota1 == 0) {
          return true
        } else if (this.fecha_vto2.length > 0 && new Date(this.fecha_vto2) <= new Date(this.fecha_vto1)) {
          return true
        } else if (this.monto_cuota2 == 0) {
          return true
        } else if (this.fecha_vto3.length > 0 && new Date(this.fecha_vto3) <= new Date(this.fecha_vto2)) {
          return true
        } else if (this.monto_cuota3 == 0) {
          return true
        } else if (this.fecha_vto4.length > 0 && new Date(this.fecha_vto4) <= new Date(this.fecha_vto3)) {
          return true
        } else if (this.monto_cuota4 == 0) {
          return true
        } else if (this.fecha_vto5.length > 0 && new Date(this.fecha_vto5) <= new Date(this.fecha_vto4)) {
          return true
        } else if (this.monto_cuota5 == 0) {
          return true
        } else if (this.fecha_vto6.length > 0 && new Date(this.fecha_vto6) <= new Date(this.fecha_vto5)) {
          return true
        } else if (this.monto_cuota6 == 0) {
          return true
        } else if (this.fecha_vto7.length > 0 && new Date(this.fecha_vto7) <= new Date(this.fecha_vto6)) {
          return true
        } else if (this.monto_cuota7 == 0) {
          return true
        } else if (this.fecha_vto8.length > 0 && new Date(this.fecha_vto8) <= new Date(this.fecha_vto7)) {
          return true
        } else if (this.monto_cuota8 == 0) {
          return true
        } else if (this.fecha_vto9.length > 0 && new Date(this.fecha_vto9) <= new Date(this.fecha_vto8)) {
          return true
        } else if (this.monto_cuota9 == 0) {
          return true
        }
      } else if (this.cantCuotas == 10) {
        if (this.fecha_vto1.length > 0 && new Date(this.fecha_vto1) <= new Date(this.fecha_convenio)) {
          return true
        } else if (this.monto_cuota1 == 0) {
          return true
        } else if (this.fecha_vto2.length > 0 && new Date(this.fecha_vto2) <= new Date(this.fecha_vto1)) {
          return true
        } else if (this.monto_cuota2 == 0) {
          return true
        } else if (this.fecha_vto3.length > 0 && new Date(this.fecha_vto3) <= new Date(this.fecha_vto2)) {
          return true
        } else if (this.monto_cuota3 == 0) {
          return true
        } else if (this.fecha_vto4.length > 0 && new Date(this.fecha_vto4) <= new Date(this.fecha_vto3)) {
          return true
        } else if (this.monto_cuota4 == 0) {
          return true
        } else if (this.fecha_vto5.length > 0 && new Date(this.fecha_vto5) <= new Date(this.fecha_vto4)) {
          return true
        } else if (this.monto_cuota5 == 0) {
          return true
        } else if (this.fecha_vto6.length > 0 && new Date(this.fecha_vto6) <= new Date(this.fecha_vto5)) {
          return true
        } else if (this.monto_cuota6 == 0) {
          return true
        } else if (this.fecha_vto7.length > 0 && new Date(this.fecha_vto7) <= new Date(this.fecha_vto6)) {
          return true
        } else if (this.monto_cuota7 == 0) {
          return true
        } else if (this.fecha_vto8.length > 0 && new Date(this.fecha_vto8) <= new Date(this.fecha_vto7)) {
          return true
        } else if (this.monto_cuota8 == 0) {
          return true
        } else if (this.fecha_vto9.length > 0 && new Date(this.fecha_vto9) <= new Date(this.fecha_vto8)) {
          return true
        } else if (this.monto_cuota9 == 0) {
          return true
        } else if (this.fecha_vto10.length > 0 && new Date(this.fecha_vto10) <= new Date(this.fecha_vto9)) {
          return true
        } else if (this.monto_cuota10 == 0) {
          return true
        }
      } else if (this.cantCuotas == 11) {
        if (this.fecha_vto1.length > 0 && new Date(this.fecha_vto1) <= new Date(this.fecha_convenio)) {
          return true
        } else if (this.monto_cuota1 == 0) {
          return true
        } else if (this.fecha_vto2.length > 0 && new Date(this.fecha_vto2) <= new Date(this.fecha_vto1)) {
          return true
        } else if (this.monto_cuota2 == 0) {
          return true
        } else if (this.fecha_vto3.length > 0 && new Date(this.fecha_vto3) <= new Date(this.fecha_vto2)) {
          return true
        } else if (this.monto_cuota3 == 0) {
          return true
        } else if (this.fecha_vto4.length > 0 && new Date(this.fecha_vto4) <= new Date(this.fecha_vto3)) {
          return true
        } else if (this.monto_cuota4 == 0) {
          return true
        } else if (this.fecha_vto5.length > 0 && new Date(this.fecha_vto5) <= new Date(this.fecha_vto4)) {
          return true
        } else if (this.monto_cuota5 == 0) {
          return true
        } else if (this.fecha_vto6.length > 0 && new Date(this.fecha_vto6) <= new Date(this.fecha_vto5)) {
          return true
        } else if (this.monto_cuota6 == 0) {
          return true
        } else if (this.fecha_vto7.length > 0 && new Date(this.fecha_vto7) <= new Date(this.fecha_vto6)) {
          return true
        } else if (this.monto_cuota7 == 0) {
          return true
        } else if (this.fecha_vto8.length > 0 && new Date(this.fecha_vto8) <= new Date(this.fecha_vto7)) {
          return true
        } else if (this.monto_cuota8 == 0) {
          return true
        } else if (this.fecha_vto9.length > 0 && new Date(this.fecha_vto9) <= new Date(this.fecha_vto8)) {
          return true
        } else if (this.monto_cuota9 == 0) {
          return true
        } else if (this.fecha_vto10.length > 0 && new Date(this.fecha_vto10) <= new Date(this.fecha_vto9)) {
          return true
        } else if (this.monto_cuota10 == 0) {
          return true
        } else if (this.fecha_vto11.length > 0 && new Date(this.fecha_vto11) <= new Date(this.fecha_vto10)) {
          return true
        } else if (this.monto_cuota11 == 0) {
          return true
        }
      } else if (this.cantCuotas == 12) {
        if (this.fecha_vto1.length > 0 && new Date(this.fecha_vto1) <= new Date(this.fecha_convenio)) {
          return true
        } else if (this.monto_cuota1 == 0) {
          return true
        } else if (this.fecha_vto2.length > 0 && new Date(this.fecha_vto2) <= new Date(this.fecha_vto1)) {
          return true
        } else if (this.monto_cuota2 == 0) {
          return true
        } else if (this.fecha_vto3.length > 0 && new Date(this.fecha_vto3) <= new Date(this.fecha_vto2)) {
          return true
        } else if (this.monto_cuota3 == 0) {
          return true
        } else if (this.fecha_vto4.length > 0 && new Date(this.fecha_vto4) <= new Date(this.fecha_vto3)) {
          return true
        } else if (this.monto_cuota4 == 0) {
          return true
        } else if (this.fecha_vto5.length > 0 && new Date(this.fecha_vto5) <= new Date(this.fecha_vto4)) {
          return true
        } else if (this.monto_cuota5 == 0) {
          return true
        } else if (this.fecha_vto6.length > 0 && new Date(this.fecha_vto6) <= new Date(this.fecha_vto5)) {
          return true
        } else if (this.monto_cuota6 == 0) {
          return true
        } else if (this.fecha_vto7.length > 0 && new Date(this.fecha_vto7) <= new Date(this.fecha_vto6)) {
          return true
        } else if (this.monto_cuota7 == 0) {
          return true
        } else if (this.fecha_vto8.length > 0 && new Date(this.fecha_vto8) <= new Date(this.fecha_vto7)) {
          return true
        } else if (this.monto_cuota8 == 0) {
          return true
        } else if (this.fecha_vto9.length > 0 && new Date(this.fecha_vto9) <= new Date(this.fecha_vto8)) {
          return true
        } else if (this.monto_cuota9 == 0) {
          return true
        } else if (this.fecha_vto10.length > 0 && new Date(this.fecha_vto10) <= new Date(this.fecha_vto9)) {
          return true
        } else if (this.monto_cuota10 == 0) {
          return true
        } else if (this.fecha_vto11.length > 0 && new Date(this.fecha_vto11) <= new Date(this.fecha_vto10)) {
          return true
        } else if (this.monto_cuota11 == 0) {
          return true
        } else if (this.fecha_vto12.length > 0 && new Date(this.fecha_vto12) <= new Date(this.fecha_vto11)) {
          return true
        } else if (this.monto_cuota12 == 0) {
          return true
        }
      } else {
        return false
      }
    },

    montoTotalCuotas() {
      return (parseFloat(this.monto_cuota1) + parseFloat(this.monto_cuota2) + parseFloat(this.monto_cuota3) + parseFloat(this.monto_cuota4) +
        parseFloat(this.monto_cuota5) + parseFloat(this.monto_cuota6) + parseFloat(this.monto_cuota7) + parseFloat(this.monto_cuota8) +
        parseFloat(this.monto_cuota9) + parseFloat(this.monto_cuota10) + parseFloat(this.monto_cuota11) + parseFloat(this.monto_cuota12))
    },

    formatearFecha(date, dayToAdd) {
      let fechaRet = ''
      let dateOk = date.setDate(date.getDate() + dayToAdd)
      dateOk = new Date(dateOk)
      let dias = 0
      if (dateOk.getDay() == 6) {
        dias = 2
      } else if (dateOk.getDay() == 0) {
        dias = 1
      }
      let day = dateOk.getDate() + dias
      let month = dateOk.getMonth() + 1
      let year = dateOk.getFullYear()

      if (month < 10 && day < 10) {
        fechaRet = `${year}-0${month}-0${day}`
      } else if (month < 10) {
        fechaRet = `${year}-0${month}-${day}`
      } else if (day < 10) {
        fechaRet = `${year}-${month}-0${day}`
      } else {
        fechaRet = `${year}-${month}-${day}`
      }
      return fechaRet
    },

    cerrarDialog(seEnvio) {
      this.limpiarDeudaActualizada()
      this.fecha_convenio = ''
      this.idConvenioSeleccionado = ''
      this.convenioSeleccionado = ''
      this.cantCuotas = 1
      this.fecha_vto1 = ''
      this.monto_cuota1 = 0
      this.fecha_vto2 = ''
      this.monto_cuota2 = 0
      this.fecha_vto3 = ''
      this.monto_cuota3 = 0
      this.fecha_vto4 = ''
      this.monto_cuota4 = 0
      this.fecha_vto5 = ''
      this.monto_cuota5 = 0
      this.fecha_vto6 = ''
      this.monto_cuota6 = 0
      this.fecha_vto7 = ''
      this.monto_cuota7 = 0
      this.fecha_vto8 = ''
      this.monto_cuota8 = 0
      this.fecha_vto9 = ''
      this.monto_cuota9 = 0
      this.fecha_vto10 = ''
      this.monto_cuota10 = 0
      this.fecha_vto11 = ''
      this.monto_cuota11 = 0
      this.fecha_vto12 = ''
      this.monto_cuota12 = 0
      this.$emit('click', seEnvio)
    },

    async cargarConvenio() {
      this.verificarDatosLlamada()
      const nuevoPost = {
        convenioNuevo: {
          id_deudor: this.deudor.id_deudor,
          fecha: this.fecha_convenio,
          cuotas: this.cantCuotas,
          id_tipo: this.idConvenioSeleccionado,
          importe: this.montoTotalConvenio,
          id_tasa: 1,
          id_direccion: this.direcciones[0].id_direccion,
          deuda_actualizada: this.deudaActualizada.monto + this.deudaActualizada.interes,
          deuda_historica: this.deudaActualizada.monto,
          monto: this.montoTotalConvenio,
          fecha1: this.fecha_vto1.length > 0 ? this.fecha_vto1 : null,
          fecha2: this.fecha_vto2.length > 0 ? this.fecha_vto2 : null,
          fecha3: this.fecha_vto3.length > 0 ? this.fecha_vto3 : null,
          fecha4: this.fecha_vto4.length > 0 ? this.fecha_vto4 : null,
          fecha5: this.fecha_vto5.length > 0 ? this.fecha_vto5 : null,
          fecha6: this.fecha_vto6.length > 0 ? this.fecha_vto6 : null,
          fecha7: this.fecha_vto7.length > 0 ? this.fecha_vto7 : null,
          fecha8: this.fecha_vto8.length > 0 ? this.fecha_vto8 : null,
          fecha9: this.fecha_vto9.length > 0 ? this.fecha_vto9 : null,
          fecha10: this.fecha_vto10.length > 0 ? this.fecha_vto10 : null,
          fecha11: this.fecha_vto11.length > 0 ? this.fecha_vto11 : null,
          fecha12: this.fecha_vto12.length > 0 ? this.fecha_vto12 : null,
          importe1: this.monto_cuota1,
          importe2: this.monto_cuota2,
          importe3: this.monto_cuota3,
          importe4: this.monto_cuota4,
          importe5: this.monto_cuota5,
          importe6: this.monto_cuota6,
          importe7: this.monto_cuota7,
          importe8: this.monto_cuota8,
          importe9: this.monto_cuota9,
          importe10: this.monto_cuota10,
          importe11: this.monto_cuota11,
          importe12: this.monto_cuota12,
          anulado: 0,
          id_llamada: this.idllamada,
          grabacion: this.grabacion
        },
        facturas: this.facturas
      }

      this.btnLoading = true
      var response = await convenioApi.agregarConvenio(nuevoPost)
      if (response.data) {
        this.cerrarDialog(1)
        await this.obtenerFacturasPorIdDeudor(this.deudor.id_deudor)
        await this.obtenerConveniosPorIdDeudor(this.deudor.id_deudor)
      } else if (response.error) {
        this.cerrarDialog(2)
      }
      this.btnLoading = false

    },

    verificarDatosLlamada() {
      if (this.posicion.data) {
        if (this.posicion.data.data != this.deudor.id_deudor) {
          this.idllamada = 0
          this.grabacion = ''
        } else {
          this.idllamada = this.posicion.data.idllamada
          this.grabacion = this.posicion.data.grabacion
        }
      } else {
        this.idllamada = 0
        this.grabacion = ''
      }
    },

    restarCuotas() {
      if (this.cantCuotas > 1) {
        this.cantCuotas--
      }
      if (this.convenioSeleccionado != '') {
        if (this.convenioSeleccionado.es_libre == 0) {
          this.calcularCuotas()
        }
        this.calcularFechas()
      }
    },

    sumarCuotas() {
      if (this.cantCuotas < this.convenioSeleccionado.max_cuotas) {
        this.cantCuotas++
      }
      if (this.convenioSeleccionado != '') {
        if (this.convenioSeleccionado.es_libre == 0) {
          this.calcularCuotas()
        }
        this.calcularFechas()
      }

    },

    ChangeConvenioSeleccionado() {
      this.convenioSeleccionado = this.tiposConveniosPorEmpresa.find(tipoConvenio => tipoConvenio.id_tipo_convenio == this.idConvenioSeleccionado)
      if (this.convenioSeleccionado.es_libre == 0) {
        if (this.convenioSeleccionado.sobre_historico == 1) {
          this.montoTotalConvenio = this.deudaActualizada.monto
        } else {
          this.montoTotalConvenio = this.deudaActualizada.monto + this.deudaActualizada.interes
        }
        this.calcularCuotas()
      }
      this.calcularFechas()
    },

    calcularCuotas() {

      this.monto_cuota1 = 0
      this.monto_cuota2 = 0
      this.monto_cuota3 = 0
      this.monto_cuota4 = 0
      this.monto_cuota5 = 0
      this.monto_cuota6 = 0
      this.monto_cuota7 = 0
      this.monto_cuota8 = 0
      this.monto_cuota9 = 0
      this.monto_cuota10 = 0
      this.monto_cuota11 = 0
      this.monto_cuota12 = 0

      if (this.cantCuotas == 1) {
        this.monto_cuota1 = this.montoTotalConvenio
        // this.fecha_vto1 = this.formatearFecha(new Date(this.fecha_convenio), 30)
        this.fecha_vto1 = moment(this.fecha_convenio).add(30, 'days').format('YYYY-MM-DD')
      } else if (this.cantCuotas == 2) {
        this.monto_cuota1 = this.round(this.montoTotalConvenio / 2, 2)
        this.monto_cuota2 = this.round(this.montoTotalConvenio / 2, 2)

      } else if (this.cantCuotas == 3) {
        this.monto_cuota1 = this.round(this.montoTotalConvenio / 3, 2)
        this.monto_cuota2 = this.round(this.montoTotalConvenio / 3, 2)
        this.monto_cuota3 = this.round(this.montoTotalConvenio / 3, 2)

      } else if (this.cantCuotas == 4) {
        this.monto_cuota1 = this.round(this.montoTotalConvenio / 4, 2)
        this.monto_cuota2 = this.round(this.montoTotalConvenio / 4, 2)
        this.monto_cuota3 = this.round(this.montoTotalConvenio / 4, 2)
        this.monto_cuota4 = this.round(this.montoTotalConvenio / 4, 2)

      } else if (this.cantCuotas == 5) {
        this.monto_cuota1 = this.round(this.montoTotalConvenio / 5, 2)
        this.monto_cuota2 = this.round(this.montoTotalConvenio / 5, 2)
        this.monto_cuota3 = this.round(this.montoTotalConvenio / 5, 2)
        this.monto_cuota4 = this.round(this.montoTotalConvenio / 5, 2)
        this.monto_cuota5 = this.round(this.montoTotalConvenio / 5, 2)

      } else if (this.cantCuotas == 6) {
        this.monto_cuota1 = this.round(this.montoTotalConvenio / 6, 2)
        this.monto_cuota2 = this.round(this.montoTotalConvenio / 6, 2)
        this.monto_cuota3 = this.round(this.montoTotalConvenio / 6, 2)
        this.monto_cuota4 = this.round(this.montoTotalConvenio / 6, 2)
        this.monto_cuota5 = this.round(this.montoTotalConvenio / 6, 2)
        this.monto_cuota6 = this.round(this.montoTotalConvenio / 6, 2)

      } else if (this.cantCuotas == 7) {
        this.monto_cuota1 = this.round(this.montoTotalConvenio / 7, 2)
        this.monto_cuota2 = this.round(this.montoTotalConvenio / 7, 2)
        this.monto_cuota3 = this.round(this.montoTotalConvenio / 7, 2)
        this.monto_cuota4 = this.round(this.montoTotalConvenio / 7, 2)
        this.monto_cuota5 = this.round(this.montoTotalConvenio / 7, 2)
        this.monto_cuota6 = this.round(this.montoTotalConvenio / 7, 2)
        this.monto_cuota7 = this.round(this.montoTotalConvenio / 7, 2)

      } else if (this.cantCuotas == 8) {
        this.monto_cuota1 = this.round(this.montoTotalConvenio / 8, 2)
        this.monto_cuota2 = this.round(this.montoTotalConvenio / 8, 2)
        this.monto_cuota3 = this.round(this.montoTotalConvenio / 8, 2)
        this.monto_cuota4 = this.round(this.montoTotalConvenio / 8, 2)
        this.monto_cuota5 = this.round(this.montoTotalConvenio / 8, 2)
        this.monto_cuota6 = this.round(this.montoTotalConvenio / 8, 2)
        this.monto_cuota8 = this.round(this.montoTotalConvenio / 8, 2)
        this.monto_cuota7 = this.round(this.montoTotalConvenio / 8, 2)
        this.monto_cuota8 = this.round(this.montoTotalConvenio / 8, 2)

      } else if (this.cantCuotas == 9) {
        this.monto_cuota1 = this.round(this.montoTotalConvenio / 9, 2)
        this.monto_cuota2 = this.round(this.montoTotalConvenio / 9, 2)
        this.monto_cuota3 = this.round(this.montoTotalConvenio / 9, 2)
        this.monto_cuota4 = this.round(this.montoTotalConvenio / 9, 2)
        this.monto_cuota5 = this.round(this.montoTotalConvenio / 9, 2)
        this.monto_cuota6 = this.round(this.montoTotalConvenio / 9, 2)
        this.monto_cuota7 = this.round(this.montoTotalConvenio / 9, 2)
        this.monto_cuota8 = this.round(this.montoTotalConvenio / 9, 2)
        this.monto_cuota9 = this.round(this.montoTotalConvenio / 9, 2)

      } else if (this.cantCuotas == 10) {
        this.monto_cuota1 = this.round(this.montoTotalConvenio / 10, 2)
        this.monto_cuota2 = this.round(this.montoTotalConvenio / 10, 2)
        this.monto_cuota3 = this.round(this.montoTotalConvenio / 10, 2)
        this.monto_cuota4 = this.round(this.montoTotalConvenio / 10, 2)
        this.monto_cuota5 = this.round(this.montoTotalConvenio / 10, 2)
        this.monto_cuota6 = this.round(this.montoTotalConvenio / 10, 2)
        this.monto_cuota7 = this.round(this.montoTotalConvenio / 10, 2)
        this.monto_cuota8 = this.round(this.montoTotalConvenio / 10, 2)
        this.monto_cuota9 = this.round(this.montoTotalConvenio / 10, 2)
        this.monto_cuota10 = this.round(this.montoTotalConvenio / 10, 2)

      } else if (this.cantCuotas == 11) {
        this.monto_cuota1 = this.round(this.montoTotalConvenio / 11, 2)
        this.monto_cuota2 = this.round(this.montoTotalConvenio / 11, 2)
        this.monto_cuota3 = this.round(this.montoTotalConvenio / 11, 2)
        this.monto_cuota4 = this.round(this.montoTotalConvenio / 11, 2)
        this.monto_cuota5 = this.round(this.montoTotalConvenio / 11, 2)
        this.monto_cuota6 = this.round(this.montoTotalConvenio / 11, 2)
        this.monto_cuota7 = this.round(this.montoTotalConvenio / 11, 2)
        this.monto_cuota8 = this.round(this.montoTotalConvenio / 11, 2)
        this.monto_cuota9 = this.round(this.montoTotalConvenio / 11, 2)
        this.monto_cuota10 = this.round(this.montoTotalConvenio / 11, 2)
        this.monto_cuota11 = this.round(this.montoTotalConvenio / 11, 2)

      } else if (this.cantCuotas == 12) {
        this.monto_cuota1 = this.round(this.montoTotalConvenio / 12, 2)
        this.monto_cuota2 = this.round(this.montoTotalConvenio / 12, 2)
        this.monto_cuota3 = this.round(this.montoTotalConvenio / 12, 2)
        this.monto_cuota4 = this.round(this.montoTotalConvenio / 12, 2)
        this.monto_cuota5 = this.round(this.montoTotalConvenio / 12, 2)
        this.monto_cuota6 = this.round(this.montoTotalConvenio / 12, 2)
        this.monto_cuota7 = this.round(this.montoTotalConvenio / 12, 2)
        this.monto_cuota8 = this.round(this.montoTotalConvenio / 12, 2)
        this.monto_cuota9 = this.round(this.montoTotalConvenio / 12, 2)
        this.monto_cuota10 = this.round(this.montoTotalConvenio / 12, 2)
        this.monto_cuota11 = this.round(this.montoTotalConvenio / 12, 2)
        this.monto_cuota12 = this.round(this.montoTotalConvenio / 12, 2)

      }
    },

    calcularFechas() {

      this.fecha_vto1 = ''
      this.fecha_vto2 = ''
      this.fecha_vto3 = ''
      this.fecha_vto4 = ''
      this.fecha_vto5 = ''
      this.fecha_vto6 = ''
      this.fecha_vto7 = ''
      this.fecha_vto8 = ''
      this.fecha_vto9 = ''
      this.fecha_vto10 = ''
      this.fecha_vto11 = ''
      this.fecha_vto11 = ''

      if (this.cantCuotas == 1) {
        this.fecha_vto1 = moment(this.fecha_convenio).add(30, 'days').format('YYYY-MM-DD')
      } else if (this.cantCuotas == 2) {
        this.fecha_vto1 = moment(this.fecha_convenio).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto2 = moment(this.fecha_vto1).add(30, 'days').format('YYYY-MM-DD')

      } else if (this.cantCuotas == 3) {
        this.fecha_vto1 = moment(this.fecha_convenio).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto2 = moment(this.fecha_vto1).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto3 = moment(this.fecha_vto2).add(30, 'days').format('YYYY-MM-DD')

      } else if (this.cantCuotas == 4) {
        this.fecha_vto1 = moment(this.fecha_convenio).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto2 = moment(this.fecha_vto1).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto3 = moment(this.fecha_vto2).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto4 = moment(this.fecha_vto3).add(30, 'days').format('YYYY-MM-DD')

      } else if (this.cantCuotas == 5) {
        this.fecha_vto1 = moment(this.fecha_convenio).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto2 = moment(this.fecha_vto1).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto3 = moment(this.fecha_vto2).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto4 = moment(this.fecha_vto3).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto5 = moment(this.fecha_vto4).add(30, 'days').format('YYYY-MM-DD')

      } else if (this.cantCuotas == 6) {
        this.fecha_vto1 = moment(this.fecha_convenio).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto2 = moment(this.fecha_vto1).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto3 = moment(this.fecha_vto2).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto4 = moment(this.fecha_vto3).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto5 = moment(this.fecha_vto4).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto6 = moment(this.fecha_vto5).add(30, 'days').format('YYYY-MM-DD')

      } else if (this.cantCuotas == 7) {
        this.fecha_vto1 = moment(this.fecha_convenio).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto2 = moment(this.fecha_vto1).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto3 = moment(this.fecha_vto2).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto4 = moment(this.fecha_vto3).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto5 = moment(this.fecha_vto4).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto6 = moment(this.fecha_vto5).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto7 = moment(this.fecha_vto6).add(30, 'days').format('YYYY-MM-DD')

      } else if (this.cantCuotas == 8) {
        this.fecha_vto1 = moment(this.fecha_convenio).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto2 = moment(this.fecha_vto1).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto3 = moment(this.fecha_vto2).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto4 = moment(this.fecha_vto3).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto5 = moment(this.fecha_vto4).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto6 = moment(this.fecha_vto5).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto7 = moment(this.fecha_vto6).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto8 = moment(this.fecha_vto7).add(30, 'days').format('YYYY-MM-DD')

      } else if (this.cantCuotas == 9) {
        this.fecha_vto1 = moment(this.fecha_convenio).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto2 = moment(this.fecha_vto1).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto3 = moment(this.fecha_vto2).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto4 = moment(this.fecha_vto3).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto5 = moment(this.fecha_vto4).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto6 = moment(this.fecha_vto5).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto7 = moment(this.fecha_vto6).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto8 = moment(this.fecha_vto7).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto9 = moment(this.fecha_vto8).add(30, 'days').format('YYYY-MM-DD')

      } else if (this.cantCuotas == 10) {
        this.fecha_vto1 = moment(this.fecha_convenio).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto2 = moment(this.fecha_vto1).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto3 = moment(this.fecha_vto2).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto4 = moment(this.fecha_vto3).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto5 = moment(this.fecha_vto4).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto6 = moment(this.fecha_vto5).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto7 = moment(this.fecha_vto6).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto8 = moment(this.fecha_vto7).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto9 = moment(this.fecha_vto8).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto10 = moment(this.fecha_vto9).add(30, 'days').format('YYYY-MM-DD')

      } else if (this.cantCuotas == 11) {
        this.fecha_vto1 = moment(this.fecha_convenio).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto2 = moment(this.fecha_vto1).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto3 = moment(this.fecha_vto2).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto4 = moment(this.fecha_vto3).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto5 = moment(this.fecha_vto4).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto6 = moment(this.fecha_vto5).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto7 = moment(this.fecha_vto6).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto8 = moment(this.fecha_vto7).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto9 = moment(this.fecha_vto8).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto10 = moment(this.fecha_vto9).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto11 = moment(this.fecha_vto10).add(30, 'days').format('YYYY-MM-DD')

      } else if (this.cantCuotas == 12) {
        this.fecha_vto1 = moment(this.fecha_convenio).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto2 = moment(this.fecha_vto1).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto3 = moment(this.fecha_vto2).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto4 = moment(this.fecha_vto3).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto5 = moment(this.fecha_vto4).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto6 = moment(this.fecha_vto5).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto7 = moment(this.fecha_vto6).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto8 = moment(this.fecha_vto7).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto9 = moment(this.fecha_vto8).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto10 = moment(this.fecha_vto9).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto11 = moment(this.fecha_vto10).add(30, 'days').format('YYYY-MM-DD')
        this.fecha_vto12 = moment(this.fecha_vto11).add(30, 'days').format('YYYY-MM-DD')

      }

    },

    trunc(x, posiciones = 0) {
      var s = x.toString()
      var decimalLength = s.indexOf('.') + 1
      var numStr = s.substr(0, decimalLength + posiciones)
      return Number(numStr)
    },

    round(num, decimales = 2) {
      var signo = (num >= 0 ? 1 : -1);
      num = num * signo;
      if (decimales === 0) //con 0 decimales
        return signo * Math.round(num);
      // round(x * 10 ^ decimales)
      num = num.toString().split('e');
      num = Math.round(+(num[0] + 'e' + (num[1] ? (+num[1] + decimales) : decimales)));
      // x * 10 ^ (-decimales)
      num = num.toString().split('e');
      return signo * (num[0] + 'e' + (num[1] ? (+num[1] - decimales) : -decimales));
    }

  }
}


