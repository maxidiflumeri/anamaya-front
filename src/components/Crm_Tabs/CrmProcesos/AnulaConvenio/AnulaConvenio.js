import { mapActions, mapState } from 'vuex'
import convenioApi from '../../../../services/Apis/BackEnd/convenio'

export default {
  name: 'anula-convenio',
  components: {},
  props: [],
  data() {
    return {
      idllamada: 0,
      grabacion: '',
      btnLoading: false,
      conveniosActivos: [],
      cabeceras: [
        { text: 'Id Convenio', value: 'id_convenio' },
        { text: 'Fecha Convenio', value: 'fecha' },
        { text: 'Cuotas', value: 'cuotas' },
        { text: 'Importe', value: 'importe' },
        { text: 'Accion', value: 'detalle' }
      ],
      headerProps: {
        sortByText: 'Ordenar por '
      },
      dialogAnular: false,
      convenioParaAnular: {}

    }
  },
  computed: {
    ...mapState('deudor', ['conveniosPorIdDeudor', 'deudor']),
    ...mapState('neotel', ['posicion']),
  },
  mounted() {
    this.conveniosActivos = this.conveniosPorIdDeudor.filter(convenio => convenio.anulado == 0)
  },
  methods: {

    ...mapActions('deudor', ['obtenerFacturasPorIdDeudor', 'obtenerConveniosPorIdDeudor']),

    dialogAnularConvenio(item) {
      this.dialogAnular = true
      this.convenioParaAnular = item
    },

    cerrarDialgoAnularConvenio() {
      this.dialogAnular = false
      this.convenioParaAnular = {}
    },

    async anularConvenio() {
      this.btnLoading = true
      this.verificarDatosLlamada()
      var response = await convenioApi.anularConvenio(this.convenioParaAnular.id_convenio, this.deudor.id_deudor, this.idllamada, this.grabacion)
      if (response.data) {
        this.cerrarDialog(1)
        await this.obtenerFacturasPorIdDeudor(this.deudor.id_deudor)
        await this.obtenerConveniosPorIdDeudor(this.deudor.id_deudor)
      } else if (response.error) {
        this.cerrarDialog(2)
      }
      this.btnLoading = false
    },

    cerrarDialog(resultado) {
      this.$emit('click', resultado)
    },

    verificarDatosLlamada() {
      if (this.posicion.data) {
        if (this.posicion.data.data != this.deudor.id_deudor) {
          this.idllamada = 0
          this.grabacion = ''
        } else {
          this.idllamada = this.posicion.data.idllamada
          this.grabacion = this.posicion.data.grabacion
        }
      } else {
        this.idllamada = 0
        this.grabacion = ''
      }
    },

  }
}


