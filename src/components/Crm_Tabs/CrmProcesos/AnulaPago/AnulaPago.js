import { mapActions, mapState } from 'vuex'
import pagoApi from '../../../../services/Apis/BackEnd/pago'

export default {
  name: 'anula-convenio',
  components: {},
  props: [],
  data() {
    return {
      idllamada: 0,
      grabacion: '',
      btnLoading: false,
      pagosActivos: [],
      cabeceras: [
        { text: 'Id Pago', value: 'id_pago' },
        { text: 'Nro. Comprobante', value: 'nro_comprobante' },
        { text: 'Id Tipo Pago', value: 'id_tipo_pago' },
        { text: 'Fecha de Pago', value: 'fecha_pago' },
        { text: 'Fecha de Aplicación', value: 'fecha_aplicacion' },
        { text: 'Importe', value: 'importe_total' },
        { text: 'Accion', value: 'detalle' }
      ],
      headerProps: {
        sortByText: 'Ordenar por '
      },
      dialogAnular: false,
      pagoParaAnular: {}
    }
  },
  computed: {
    ...mapState('deudor', ['pagosPorIdDeudor', 'deudor']),
    ...mapState('neotel', ['posicion']),
  },
  mounted() {
    this.pagosActivos = this.pagosPorIdDeudor.filter(pago => pago.anulado == 0)
  },
  methods: {

    ...mapActions('deudor', ['obtenerPagosPorIdDeudor', 'obtenerDeudor']),

    habilitaAnulaPago(item) {
      if (item.id_tipo_pago == 1) {
        return true
      } else {
        return false
      }
    },

    dialogAnularPago(item) {
      this.dialogAnular = true
      this.pagoParaAnular = item
    },

    cerrarDialgoAnularPago() {
      this.dialogAnular = false
      this.pagoParaAnular = {}
    },

    async anularPago() {
      this.verificarDatosLlamada()
      this.btnLoading = true
      var response = await pagoApi.anularPago(this.pagoParaAnular.id_pago, this.deudor.id_situacion, this.deudor.id_deudor, this.idllamada, this.grabacion)
      if (response.data) {
        this.cerrarDialog(1)
        await this.obtenerPagosPorIdDeudor(this.deudor.id_deudor)
        await this.obtenerDeudor(this.deudor.id_deudor)
      } else if (response.error) {
        this.cerrarDialog(2)
      }
      this.btnLoading = false
    },

    verificarDatosLlamada() {
      if (this.posicion.data) {
        if (this.posicion.data.data != this.deudor.id_deudor) {
          this.idllamada = 0
          this.grabacion = ''
        } else {
          this.idllamada = this.posicion.data.idllamada
          this.grabacion = this.posicion.data.grabacion
        }
      } else {
        this.idllamada = 0
        this.grabacion = ''
      }
    },

    cerrarDialog(resultado) {
      this.$emit('click', resultado)
    }

  }
}


