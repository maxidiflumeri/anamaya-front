import EnvioMail from "./EnvioMail/index"
import PagosDeudor from "./PagosDeudor/index"
import CargaConvenios from "./CargaConvenio/index"
import AnulaConvenio from "./AnulaConvenio/index"
import AnulaPago from "./AnulaPago/index"
import CuponPago from "./CuponPago/index"
import jsPDF from 'jspdf'
import 'jspdf-autotable'
import { mapState, mapActions } from "vuex"
import urls from '../../../urls/index'
import moment from 'moment'

export default {
  name: 'crm-procesos',
  components: {
    EnvioMail,
    PagosDeudor,
    CargaConvenios,
    AnulaConvenio,
    AnulaPago,
    CuponPago
  },
  props: {
    id_deudor: String
  },
  data() {
    return {
      pepe: require('../../../assets/logo-toyota.png'),
      btnLoading: false,
      idllamada: 0,
      grabacion: '',
      codigosSituacionLocal: [],
      codigosGestionLocal: [],
      codigosOrigenLocal: [],
      codigosRespuestaLocal: [],
      codigoSituacion: null,
      codigoGestion: null,
      codigoSituacionInicial: null,
      codigoGestionInicial: null,
      dataPickerFecha: false,
      dataPickerHora: false,
      valid: false,
      dialogAgendaPagoTC: false,
      cuponesWeb: [],
      cuponesImprimir: [],
      cuponParaImprimir: null,
      facturasParaConvenios: [],
      componentKey: 0,
      dialogMail: false,
      muestraMensaje: false,
      mensaje: '',
      colorMensaje: '',
      dialogPagos: false,
      dialogPagosDeudor: false,
      dialogFacturasConvenio: false,
      dialogCargaConvenio: false,
      dialogAnulaConvenio: false,
      dialogAnulaPago: false,
      dialogCuponPago: false,
      dialogVistaCupon: false,
      dialogGestionToyota: false,
      cuponesSeleccionados: [],
      facturasSeleccionadas: [],
      seleccionSimple: false,
      agendaPago: {
        id_deudor: '',
        telefono: '',
        fecha: '',
        hora: '',
        estado: 'PENDIENTE'
      },
      gestionToyota: {
        id_deudor: '',
        id_asignacion: '',
        id_origen: '',
        id_respuesta: '',
        fecha_pago: '',
        id_entidad: '',
        id_tipo_pago: '',
        importe_pago: '',
        comentario: '',
        id_usuario: '',
        fecha_informe: ''
      },
      reglasAgenda: [
        value => !!value || 'El campo es requerido!'],
      reglasGestionToyota: [
        value => !!value || 'El campo es requerido!'],
      cabeceras: [
        { text: 'Nro Factura', value: 'nro_factura' },
        { text: 'Fecha Comprobante', value: 'fecha_comprobante' },
        { text: 'Fecha Vencimiento', value: 'fecha_vencimiento' },
        { text: 'Importe 1', value: 'importe1' },
        { text: 'Importe 2', value: 'importe2' },
        { text: 'Importe 3', value: 'importe3' },
        { text: 'Importe 4', value: 'importe4' },
        { text: 'Importe 5', value: 'importe5' },
        { text: 'Clave de Pago', value: 'clave_pago' },
        { text: 'Cant Cartas', value: 'cant_cartas' },
        { text: 'Correo', value: 'correo' }
      ],
      cabecerasCupones: [
        { text: 'Nro. Convenio', value: 'nro_convenio' },
        { text: 'Clave de Pago', value: 'clave_pago' },
        { text: 'importe', value: 'saldo' },
        { text: 'Fecha Vencimiento', value: 'fecha_vencimiento' },
        { text: 'Vista Previa', value: 'acciones' }
      ],
      cabecerasCuponesWeb: [
        { text: 'URL', value: 'url' },
        { text: 'Accion', value: 'accion' }
      ],
    }
  },

  watch: {
    gestionToyotaIdRespuesta() {
      if (this.gestionToyotaIdRespuesta == 651) {
        this.gestionToyota.id_tipo_pago = 3
      } else if (this.gestionToyotaIdRespuesta == 654) {
        this.gestionToyota.id_tipo_pago = 8
      }
    }
  },

  computed: {
    ...mapState(['deudor']),
    ...mapState('empresas', ['empresa']),
    ...mapState('deudor', ['deudor', 'facturasPorIdDeudor', 'cuponesPorIdDeudor', 'comentarios', 'direcciones', 'telefonos', 'correos', 'redes']),
    ...mapState('auth', ['token', 'usuario']),
    ...mapState('empresas', ['codigosGestion']),
    ...mapState('empresas', ['codigosSituacion']),
    ...mapState('adminTipos', ['entidadesRecaudadoras', 'tiposPago']),
    ...mapState('neotel', ['posicion']),

    gestionToyotaIdRespuesta() {
      return this.gestionToyota.id_respuesta
    },

    habilitarContinuarConvenios() {
      if (this.facturasSeleccionadas.length == 0) {
        return true
      } else if (this.direcciones.length == 0) {
        return true
      } else if (this.deudor.id_situacion >= 500 && this.deudor.id_situacion <= 600) {
        return true
      } else {
        return false
      }
    },

    estaCancelada() {
      if (this.deudor.id_situacion >= 500 && this.deudor.id_situacion <= 600) {
        return true
      } else {
        return false
      }
    },

    habilitarVistaCupon() {
      if (this.cuponesSeleccionados.length == 0) {
        return true
      } else {
        return false
      }
    },


  },
  mounted() {
    this.facturasPorIdDeudor.forEach(factura => {
      if (factura.id_convenio == null) {
        this.facturasParaConvenios.push(factura)
      }
    })
    this.cuponesWeb = this.cuponesPorIdDeudor.filter(cupon => cupon.url != null)
    this.cuponesImprimir = this.cuponesPorIdDeudor.filter(cupon => cupon.url == null)
    this.inicializarCombos()
    this.obtenerTiposPago()
    this.obtenerEntidadesRecaudadoras()
  },
  methods: {
    ...mapActions('adminTipos', ['obtenerTiposPago', 'obtenerEntidadesRecaudadoras']),
    ...mapActions('deudor', ['obtenerPagosPorIdDeudor', 'obtenerDeudor', 'obtenerComentarios']),

    forceRerender() {
      this.componentKey += 1;
    },

    habilitarBotonCuponWeb(item) {
      if (item.url == null) {
        return true
      } else if (item.url.length == 0) {
        return true
      } else {
        return false
      }
    },

    abrirDialogPagosDeudor() {
      this.dialogPagos = false
      this.dialogPagosDeudor = true
    },

    abrirDialog() {
      this.dialogMail = true
    },

    cerrarDialogMail(resultado) {
      if (resultado == 0) {
        this.dialogMail = false
      } else if (resultado == 1) {
        this.dialogMail = false
        this.muestraMensaje = true
        this.mensaje = 'Correo enviado exitosamente.'
        this.colorMensaje = 'success'
      } else if (resultado == 2) {
        this.dialogMail = false
        this.muestraMensaje = true
        this.mensaje = 'Error al enviar el correo.'
        this.colorMensaje = 'error'
      }
    },

    cerrarDialogPagosDeudor(resultado) {
      if (resultado == 0) {
        this.dialogPagosDeudor = false
      } else if (resultado == 1) {
        this.dialogPagosDeudor = false
        this.muestraMensaje = true
        this.mensaje = 'Pago registrado exitosamente.'
        this.colorMensaje = 'success'
      } else if (resultado == 2) {
        this.dialogPagosDeudor = false
        this.muestraMensaje = true
        this.mensaje = 'Error al registrar el pago.'
        this.colorMensaje = 'error'
      } else if (resultado == 3) {
        this.dialogPagosDeudor = false
        this.muestraMensaje = true
        this.mensaje = 'Error al registrar el pago porque ya existe una promesa de pago activa sin vencer.'
        this.colorMensaje = 'error'
      }
    },

    abrirCuponWeb(item) {
      window.open(item.url, '_blank')
    },

    cargaPagos() {
      this.dialogPagos = true
    },

    abrirAgendaPagoTC() {
      this.dialogAgendaPagoTC = true
    },

    cerrarDialogPagoTC() {
      this.dialogAgendaPagoTC = false
      this.agendaPago = {
        id_deudor: '',
        telefono: '',
        fecha: '',
        hora: '',
        estado: 'PENDIENTE'
      }
      this.$refs.form.resetValidation()
    },

    abrirGestionToyota() {
      this.dialogGestionToyota = true
    },

    cerrarGestionToyota() {
      this.dialogGestionToyota = false
      this.gestionToyota = {
        id_deudor: '',
        id_asignacion: '',
        id_origen: '',
        id_respuesta: '',
        fecha_pago: '',
        id_entidad: '',
        id_tipo_pago: '',
        importe_pago: '',
        comentario: '',
        id_usuario: '',
        fecha_informe: ''
      },
        this.$refs.formToy.resetValidation()
    },

    cargaConvenio() {
      this.dialogFacturasConvenio = true
    },

    abrirDialogAnularConvenio() {
      this.dialogAnulaConvenio = true
    },

    abrirDialogAnularPago() {
      this.dialogAnulaPago = true
    },

    abrirDialogCargaConvenio() {
      this.dialogFacturasConvenio = false
      this.dialogCargaConvenio = true
    },

    abrirDialogVistaCupon(item) {
      //this.forceRerender()            
      this.cuponParaImprimir = item
      this.dialogCuponPago = false
      this.dialogVistaCupon = true
    },

    cerrarDialogVistaCupon(resultado) {
      if (resultado == 0) {
        this.dialogCuponPago = true
        this.dialogVistaCupon = false
      } else if (resultado == 1) {
        this.dialogCuponPago = true
        this.dialogVistaCupon = false
        this.muestraMensaje = true
        this.mensaje = 'Cupón de pago enviado exitosamente.'
        this.colorMensaje = 'success'
      } else if (resultado == 2) {
        this.dialogCuponPago = true
        this.dialogVistaCupon = false
        this.muestraMensaje = true
        this.mensaje = 'Error al enviar el cupón de pago.'
        this.colorMensaje = 'error'
      }
    },

    abrirDialogCuponPago() {
      this.dialogCuponPago = true
    },

    cerrarDialogCuponPago() {
      this.dialogCuponPago = false
      this.cuponParaImprimir = null
    },

    cerrarDialogCargaConvenio(resultado) {
      if (resultado == 0) {
        this.dialogCargaConvenio = false
        this.facturasSeleccionadas = []
        this.forceRerender()
      } else if (resultado == 1) {
        this.dialogCargaConvenio = false
        this.facturasSeleccionadas = []
        this.forceRerender()
        this.muestraMensaje = true
        this.mensaje = 'Convenio generado exitosamente.'
        this.colorMensaje = 'success'
      } else if (resultado == 2) {
        this.dialogCargaConvenio = false
        this.facturasSeleccionadas = []
        this.forceRerender()
        this.muestraMensaje = true
        this.mensaje = 'Error al generar el convenio.'
        this.colorMensaje = 'error'
      }
    },

    cerrarDialogAnulaConvenio(resultado) {
      if (resultado == 0) {
        this.dialogAnulaConvenio = false
      } else if (resultado == 1) {
        this.dialogAnulaConvenio = false
        this.muestraMensaje = true
        this.mensaje = 'Convenio anulado exitosamente.'
        this.colorMensaje = 'success'
      } else if (resultado == 2) {
        this.dialogAnulaConvenio = false
        this.muestraMensaje = true
        this.mensaje = 'Error al anular el convenio.'
        this.colorMensaje = 'error'
      }
    },

    cerrarDialogAnulaPago(resultado) {
      if (resultado == 0) {
        this.dialogAnulaPago = false
      } else if (resultado == 1) {
        this.dialogAnulaPago = false
        this.muestraMensaje = true
        this.mensaje = 'Pago anulado exitosamente.'
        this.colorMensaje = 'success'
      } else if (resultado == 2) {
        this.dialogAnulaPago = false
        this.muestraMensaje = true
        this.mensaje = 'Error al anular el Pago.'
        this.colorMensaje = 'error'
      }
    },

    cerrarDialogFacturasConvenio() {
      this.facturasSeleccionadas = []
      this.dialogFacturasConvenio = false
    },

    async agendarPagoConTC() {
      this.agendaPago.id_deudor = this.deudor.id_deudor
      try {
        let header = { token: this.token }
        let configuracion = { headers: header }
        await this.axios.post(urls.agendasTc.urlAgregar, this.agendaPago, configuracion);
        this.cerrarDialogPagoTC()
        this.muestraMensaje = true
        this.mensaje = 'Caso agendado exitosamente.'
        this.colorMensaje = 'success'
      } catch (error) {
        this.muestraMensaje = true
        this.mensaje = 'Error al agendar el caso.'
        this.colorMensaje = 'error'
      }
    },

    imprimirReporte() {
      let telefonos1 = []
      let correos1 = []
      let redes1 = []
      if (this.telefonos.length > 0) {
        this.telefonos.forEach(tel => {
          let telNuevo = {
            id_deudor: tel.id_deudor,
            telefono: tel.telefono,
            efectivo: tel.efectivo == 0 ? 'NO' : 'SI',
            tipo_telefono: tel.tipo_telefono.descripcion
          }
          telefonos1.push(telNuevo)
        });
      }
      if (this.correos.length > 0) {
        this.correos.forEach(correo => {
          let correoNuevo = {
            id_deudor: correo.id_deudor,
            correo: correo.correo,
            efectivo: correo.efectivo == 0 ? 'NO' : 'SI',
            tipo_correo: correo.tipo_correo.descripcion
          }
          correos1.push(correoNuevo)
        });
      }
      if (this.redes.length > 0) {
        this.redes.forEach(red => {
          let redNuevo = {
            id_deudor: red.id_deudor,
            usuario: red.usuario,
            tipo_red: red.id_red_social.descripcion
          }
          redes1.push(redNuevo)
        });
      }
      const doc = new jsPDF()
      doc.setFontSize(20).setFont('arial', 'bold').text(70, 10, this.empresa.descripcion)
      doc.setLineWidth(0.01).line(10, 15, 195, 15)
      doc.setLineWidth(0.01).line(10, 17, 195, 17)
      doc.setFontSize(14)
      doc.setFont('times', 'bold')
      doc.text(10, 25, 'Numero Deudor: ')
      doc.setFont('times', 'normal')
      doc.text(48, 25, this.deudor.id_deudor.toString())

      doc.setFont('times', 'bold')
      doc.text(130, 25, 'Remesa: ')
      doc.setFont('times', 'normal')
      doc.text(150, 25, this.deudor.remesa.toString())

      doc.setFont('times', 'bold')
      doc.text(10, 30, 'Nombre: ')
      doc.setFont('times', 'normal')
      doc.text(30, 30, this.deudor.nombre_apellido)

      doc.setFont('times', 'bold')
      doc.text(130, 30, 'Nro. Cliente: ')
      doc.setFont('times', 'normal')
      doc.text(159, 30, this.deudor.nro_cliente1)

      doc.setFont('times', 'bold')
      doc.text(10, 35, 'DNI: ')
      doc.setFont('times', 'normal')
      doc.text(22, 35, this.deudor.nro_doc.toString())

      doc.setFont('times', 'bold')
      doc.text(130, 35, 'Deuda: ')
      doc.setFont('times', 'normal')
      doc.text(147, 35, this.deudor.deuda_historica.toString())

      doc.setLineWidth(0.01).line(10, 40, 195, 40)
      doc.setLineWidth(0.01).line(10, 42, 195, 42)
      doc.setFontSize(12).setFont('times', 'bold').text(10, 49, "Telefonos");

      doc.autoTable({
        columns: [
          {
            title: 'Telefono',
            dataKey: 'telefono'
          },
          {
            title: 'Tipo Telefono',
            dataKey: 'tipo_telefono'
          },
          {
            title: 'Efectivo',
            dataKey: 'efectivo'
          },
        ],
        body: telefonos1,
        margin: { left: 10, top: 50 }
      })

      doc.setFontSize(12).setFont('times', 'bold').text(10, doc.autoTable.previous.finalY + 5, "Correos Electrónicos");

      doc.autoTable({

        columns: [
          {
            title: 'Correo',
            dataKey: 'correo'
          },
          {
            title: 'Tipo Correo',
            dataKey: 'tipo_correo'
          },
          {
            title: 'Efectivo',
            dataKey: 'efectivo'
          },
        ],
        body: correos1,
        margin: { left: 10 }
      })

      doc.setFontSize(12).setFont('times', 'bold').text(10, doc.autoTable.previous.finalY + 5, "Redes Sociales");

      doc.autoTable({

        columns: [
          {
            title: 'Usuario',
            dataKey: 'usuario'
          },
          {
            title: 'Red Social',
            dataKey: 'tipo_red'
          }
        ],
        body: redes1,
        margin: { left: 10, top: doc.autoTable.previous.finalY + 50 }
      })

      doc.setFontSize(12).setFont('times', 'bold').text(10, doc.autoTable.previous.finalY + 5, "Comentarios");

      doc.autoTable({
        columns: [
          {
            title: 'Fecha',
            dataKey: 'fecha'
          },
          {
            title: 'Comentario',
            dataKey: 'comentario'
          },
        ],
        body: this.comentarios,
        margin: { left: 10, top: 45 }
      })

      doc.setFontSize(12).setFont('times', 'bold').text(10, doc.autoTable.previous.finalY + 5, "Direcciones");

      doc.autoTable({
        columns: [
          {
            title: 'Calle',
            dataKey: 'calle'
          },
          {
            title: 'Numero',
            dataKey: 'numero'
          },
          {
            title: 'Piso',
            dataKey: 'piso'
          },
          {
            title: 'Depto',
            dataKey: 'departamento'
          },
          {
            title: 'Cod. Postal',
            dataKey: 'cod_postal'
          },
          {
            title: 'Localidad',
            dataKey: 'localidad'
          },
          {
            title: 'Provincia',
            dataKey: 'provincia'
          },
        ],
        body: this.direcciones,
        margin: { left: 10, top: 45 }
      })

      doc.setFontSize(12).setFont('times', 'bold').text(10, doc.autoTable.previous.finalY + 5, "Facturas");
      doc.autoTable({
        columns: [
          {
            title: 'Nro Factura',
            dataKey: 'nro_factura'
          },
          {
            title: 'Fecha Comprobante',
            dataKey: 'fecha_comprobante'
          },
          {
            title: 'fecha Vencimiento',
            dataKey: 'fecha_vencimiento'
          },
          {
            title: 'Importe',
            dataKey: 'importe1'
          },
          {
            title: 'Importe 2',
            dataKey: 'importe2'
          },
          {
            title: 'Importe 3',
            dataKey: 'importe3'
          },
          {
            title: 'Importe 4',
            dataKey: 'importe4'
          },
          {
            title: 'Importe 5',
            dataKey: 'importe5'
          },
        ],
        body: this.facturasPorIdDeudor,
        margin: { left: 10, top: 45 }
      })

      doc.setFontSize(12).setFont('times', 'bold').text(10, doc.autoTable.previous.finalY + 5, "Pagos");

      doc.autoTable({
        columns: [
          {
            title: 'Nro Comprobante',
            dataKey: 'nro_comprobante'
          },
          {
            title: 'Fecha Pago',
            dataKey: 'fecha_pago'
          },
          {
            title: 'fecha Aplicacion',
            dataKey: 'fecha_aplicacion'
          },
          {
            title: 'fecha Rendicion',
            dataKey: 'fecha_rendicion'
          },
          {
            title: 'Importe Total',
            dataKey: 'importe_total'
          },
          {
            title: 'Tipo Pago',
            dataKey: 'id_tipo_pago'
          },
        ],
        body: this.pagosPorIdDeudor,
        margin: { left: 10, top: 45 }
      })

      doc.save(`informe_${this.deudor.id_deudor.toString()}.pdf`)

    },

    inicializarCombos() {
      this.codigoSituacion = this.codigosSituacion.find(codigo => codigo.id_situacion == this.deudor.id_situacion)
      this.codigoSituacionInicial = this.codigosSituacion.find(codigo => codigo.id_situacion == this.deudor.id_situacion)
      this.codigoGestion = this.codigosGestion.find(codigo => codigo.id_gestion == this.deudor.id_gestion)
      this.codigoGestionInicial = this.codigosGestion.find(codigo => codigo.id_gestion == this.deudor.id_gestion)
      this.codigosSituacionLocal = this.codigosSituacion.filter(codigo => codigo.visible == 1)
      this.codigosGestionLocal = this.codigosGestion.filter(codigo => codigo.visible == 1)
      this.codigosOrigenLocal = this.codigosSituacion.filter(codigo => codigo.aux1 == "1")
      this.codigosRespuestaLocal = this.codigosGestion.filter(codigo => codigo.aux1 == "1")
    },

    async cargarGestionToyota() {
      this.btnLoading = true
      this.verificarDatosLlamada()
      let nuevoPago = null
      let nuevoComentario = null
      let deudorPut = null

      if (this.gestionToyota.id_respuesta == 651) {
        nuevoPago = {
          nro_comprobante: `${this.deudor.nro_cliente1}-${this.gestionToyota.fecha_pago}-${this.gestionToyota.importe_pago}`,
          id_deudor: this.deudor.id_deudor,
          id_entidad: this.gestionToyota.id_entidad,
          id_tipo_pago: this.gestionToyota.id_tipo_pago,
          fecha_pago: this.gestionToyota.fecha_pago,
          importe_total: this.gestionToyota.importe_pago,
          fecha_registro: this.gestionToyota.fecha_pago,
          anulado: 0,
          id_llamada: this.idllamada,
          grabacion: this.grabacion
        }
      } else if (this.gestionToyota.id_respuesta == 654) {
        nuevoPago = {
          nro_comprobante: '08 - Promesa de pago',
          id_deudor: this.deudor.id_deudor,
          id_entidad: this.gestionToyota.id_entidad,
          id_tipo_pago: this.gestionToyota.id_tipo_pago,
          fecha_pago: this.gestionToyota.fecha_pago,
          importe_total: this.gestionToyota.importe_pago,
          fecha_registro: this.gestionToyota.fecha_pago,
          anulado: 0,
          id_llamada: this.idllamada,
          grabacion: this.grabacion
        }

      }
      else {
        nuevoPago = null
      }

      nuevoComentario = {
        id_usuario: this.usuario.id_usuario,
        id_deudor: this.deudor.id_deudor,
        comentario: this.gestionToyota.comentario,
        id_llamada: this.idllamada,
        grabacion: this.grabacion
      }

      if (this.codigoSituacion != this.codigoSituacionInicial || this.codigoGestion != this.codigoGestionInicial) {
        deudorPut = {
          id_situacion: this.codigoSituacion.id_situacion ? this.codigoSituacion.id_situacion : this.codigoSituacion,
          fecha_cambio_sit: moment().format('YYYY-MM-DD'),
          id_gestion: this.codigoGestion.id_gestion ? this.codigoGestion.id_gestion : this.codigoGestion,
          fecha_cambio_gest: moment().format('YYYY-MM-DD'),
          id_llamada: this.idllamada,
          grabacion: this.grabacion
        }
      } else {
        deudorPut = null
      }

      this.gestionToyota.id_deudor = this.deudor.id_deudor
      this.gestionToyota.id_usuario = this.usuario.id_usuario
      this.gestionToyota.id_asignacion = this.deudor.nro_cliente1

      let header = { token: this.token }
      let configuracion = { headers: header }

      try {
        nuevoPago ? await this.axios.post(urls.pagos.urlAgregar, nuevoPago, configuracion) : ''
        deudorPut ? await this.axios.put(urls.deudor.urlModificar + '/' + this.deudor.id_deudor, deudorPut, configuracion) : ''
        nuevoComentario ? await this.axios.post(urls.comentarios.urlAgregar, nuevoComentario, configuracion) : ''
        await this.axios.post(urls.toyotaGestion.urlAgregar, this.gestionToyota, configuracion)
        await this.obtenerDeudor(this.deudor.id_deudor)
        await this.obtenerComentarios(this.deudor.id_deudor)
        await this.obtenerPagosPorIdDeudor(this.deudor.id_deudor)
        this.cerrarGestionToyota()
        this.muestraMensaje = true
        this.mensaje = 'Gestion de Toyota agregada exitosamente.'
        this.colorMensaje = 'success'
      } catch (error) {
        this.muestraMensaje = true
        this.mensaje = 'Error al cargar la gestion de Toyota.'
        this.colorMensaje = 'error'
        console.log(error.message)
      }
      this.btnLoading = false
    },

    verificarDatosLlamada() {
      if (this.posicion.data) {
        if (this.posicion.data.data != this.deudor.id_deudor) {
          this.idllamada = 0
          this.grabacion = ''
        } else {
          this.idllamada = this.posicion.data.idllamada
          this.grabacion = this.posicion.data.grabacion
        }
      } else {
        this.idllamada = 0
        this.grabacion = ''
      }
    },

  }
}


