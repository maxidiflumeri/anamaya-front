import { mapActions, mapState } from "vuex"
import VueBarcode from 'vue-barcode'
import html2pdf from 'html2pdf.js'

// import pagoService from '../../../../services/Apis/BackEnd/pago'

export default {
  name: 'pagos-deudor',
  components: { VueBarcode },
  props: {
    cupon: Object
  },
  data() {
    return {
      idllamada: 0,
      grabacion: '',
      btnLoading: false,
      imgLogo: '',
      logoMaya: require('../../../../assets/anamayasa.jpg'),
      destinatario: '',
      dialogDestinatario: false
    }
  },
  computed: {
    ...mapState('empresas', ['empresa']),
    ...mapState('deudor', ['deudor', "correos"]),
    ...mapState('adminTipos', ['entidadesRecaudadoras', 'tiposPago']),
    ...mapState('auth', ['token']),
    ...mapState('neotel', ['posicion']),

    habilitaEnviar() {
      if (this.destinatario == '') {
        return true
      } else {
        return false
      }
    }
  },

  watch: {
  },

  mounted() {
    this.imgLogo = require(`../../../../assets/${this.empresa.logo}`)
  },
  methods: {
    ...mapActions('adminTipos', ['obtenerTiposPago', 'obtenerEntidadesRecaudadoras']),
    ...mapActions('deudor', ['obtenerPagosPorIdDeudor', 'obtenerDeudor']),

    cerrarDialogDestinatario() {
      this.dialogDestinatario = false
      this.destinatario = ''
    },

    cerrarDialog(seEnvio) {
      this.$emit('click', seEnvio)
      this.btnLoading = false
    },

    descargar() {
      var element = document.getElementById('cuponPrint');
      var opt = {
        margin: 1,
        filename: `cupón_${this.deudor.id_deudor}`,
        image: { type: 'jpeg', quality: 0.98 },
        html2canvas: { scale: 2 },
        jsPDF: { unit: 'in', format: 'letter', orientation: 'portrait' }
      };

      // New Promise-based usage:

      html2pdf().set(opt).from(element).save();
    },

    async convertPdfToBase64() {
      return new Promise((resolve) => {

        var element = document.getElementById('cuponPrint');
        var opt = {
          margin: 1,
          filename: `cupón_${this.deudor.id_deudor}`,
          image: { type: 'jpeg', quality: 0.98 },
          html2canvas: { scale: 2 },
          jsPDF: { unit: 'in', format: 'letter', orientation: 'portrait' }
        };

        html2pdf().set(opt).from(element).outputPdf().then(function (pdf) {
          resolve(btoa(pdf))

        })
      })

    },

    async enviarPorMail() {
      this.verificarDatosLlamada()
      this.btnLoading = true;
      var base64pdf = await this.convertPdfToBase64()

      let body = {
        deudor: this.deudor.id_deudor,
        id_politica: this.deudor.id_politica,
        destinatario: this.destinatario,
        fileBase64: base64pdf,
        id_llamada: this.idllamada,
        grabacion: this.grabacion
      }
      try {
        let header = {
          token: this.token
        };
        let configuracion = { headers: header };
        await this.axios.post("/api/servicio/envioCuponPdfMail", body, configuracion);
        this.cerrarDialog(1)
        this.btnLoading = false

      } catch (error) {
        this.cerrarDialog(2)
        this.btnLoading = false
      }
    },

    verificarDatosLlamada() {
      if (this.posicion.data) {
        if (this.posicion.data.data != this.deudor.id_deudor) {
          this.idllamada = 0
          this.grabacion = ''
        } else {
          this.idllamada = this.posicion.data.idllamada
          this.grabacion = this.posicion.data.grabacion
        }
      } else {
        this.idllamada = 0
        this.grabacion = ''
      }
    },
  }
}


