import { mapActions, mapState } from "vuex"
import pagoService from '../../../../services/Apis/BackEnd/pago'

export default {
  name: 'pagos-deudor',
  components: {},
  props: [],
  data() {
    return {
      idllamada: 0,
      grabacion: '',
      btnLoading: false,
      nro_cliente: '',
      deuda_historica: '',
      comprobante: '',
      fecha_pago: '',
      fecha_aplicacion: '',
      importe: '',
      dataPickerPago: false,
      dataPickerAplicacion: false,
      comentario: '',
      tipoPago: '',
      entidadRecaudadora: ''
    }
  },
  computed: {
    habilitarCarga() {
      if (this.comprobante != '' && this.fecha_pago != '' && this.fecha_aplicacion != '' && this.importe != '' &&
        this.comentario != '' && this.entidadRecaudadora != '' && this.tipoPago != '') {
        return false
      } else {
        return true
      }
    },
    ...mapState('deudor', ['deudor']),
    ...mapState('adminTipos', ['entidadesRecaudadoras', 'tiposPago']),
    ...mapState('neotel', ['posicion']),

    esPromesaDePago() {
      if (this.tipoPago == 8) {
        this.fecha_aplicacion = this.fecha_pago
        this.comprobante = "08 - Promesa de pago"
        return true
      } else {
        this.fecha_aplicacion = ""
        this.comprobante = ""
        return false
      }
    }
  },

  watch: {
    fecha_pago() {
      if (this.tipoPago == 8) {
        this.fecha_aplicacion = this.fecha_pago
      }
    }
  },
  mounted() {
    this.obtenerTiposPago()
    this.obtenerEntidadesRecaudadoras()
    this.nro_cliente = this.deudor.nro_cliente1
    this.deuda_historica = this.deudor.deuda_historica
  },
  methods: {
    ...mapActions('adminTipos', ['obtenerTiposPago', 'obtenerEntidadesRecaudadoras']),
    ...mapActions('deudor', ['obtenerPagosPorIdDeudor', 'obtenerDeudor']),

    cerrarDialog(seEnvio) {
      this.$emit('click', seEnvio)
      this.comprobante = ""
      this.fecha_pago = ""
      this.fecha_aplicacion = ""
      this.importe = ""
      this.entidadRecaudadora = ""
      this.tipoPago = ""
      this.comentario = ""
      this.tipoPago = ""
      this.btnLoading = false
    },

    async cargarPago() {

      this.verificarDatosLlamada()

      const nuevoPago = {
        nro_comprobante: this.comprobante,
        id_deudor: this.deudor.id_deudor,
        id_entidad: this.entidadRecaudadora,
        id_tipo_pago: this.tipoPago,
        fecha_pago: this.fecha_pago,
        importe_total: this.importe,
        fecha_registro: this.fecha_aplicacion,
        anulado: 0,
        id_llamada: this.idllamada,
        grabacion: this.grabacion
      }
      this.btnLoading = true
      var response = await pagoService.agregarPago(nuevoPago)
      if (response.data) {
        this.obtenerDeudor(this.deudor.id_deudor)
        this.obtenerPagosPorIdDeudor(this.deudor.id_deudor)
        this.cerrarDialog(1)
      } else if (response.error) {
        if (response.error.response.data.mensaje == "Ya existe promesa de pago") {
          this.cerrarDialog(3)
        } else {
          this.cerrarDialog(2)
        }
      }
      this.btnLoading = false
    },

    verificarDatosLlamada() {
      if (this.posicion.data) {
        if (this.posicion.data.data != this.deudor.id_deudor) {
          this.idllamada = 0
          this.grabacion = ''
        } else {
          this.idllamada = this.posicion.data.idllamada
          this.grabacion = this.posicion.data.grabacion
        }
      } else {
        this.idllamada = 0
        this.grabacion = ''
      }
    },

  }
}


