import { mapState } from 'vuex'
import TablaDirecciones from '../../TablasCrud/TablaDirecciones/index.vue'
import TablaVinculos from '../../TablasCrud/TablaVinculos/index.vue'

export default {
  name: 'crm-datos-complementarios',
  components: { TablaDirecciones, TablaVinculos },
  props: {
  },
  data() {
    return {

    }
  },
  computed: {
    ...mapState('empresas', ['empresa']),
    ...mapState('deudor', ['deudor']),
    hayAux1() {
      if (this.deudor.aux1) {
        if (this.deudor.aux1.length > 0) {
          return true
        } else {
          return false
        }
      } else {
        return false
      }
    },
    hayAux2() {
      if (this.deudor.aux2) {
        if (this.deudor.aux2.length > 0) {
          return true
        } else {
          return false
        }
      } else {
        return false
      }
    },
    hayAux3() {
      if (this.deudor.aux3) {
        if (this.deudor.aux3.length > 0) {
          return true
        } else {
          return false
        }
      } else {
        return false
      }
    },
    hayAux4() {
      if (this.deudor.aux4) {
        if (this.deudor.aux4.length > 0) {
          return true
        } else {
          return false
        }
      } else {
        return false
      }
    },
    hayAux5() {
      if (this.deudor.aux5) {
        if (this.deudor.aux5.length > 0) {
          return true
        } else {
          return false
        }
      } else {
        return false
      }
    },
    hayAux6() {
      if (this.deudor.aux6) {
        if (this.deudor.aux6.length > 0) {
          return true
        } else {
          return false
        }
      } else {
        return false
      }
    },
    hayAux7() {
      if (this.deudor.aux7) {
        if (this.deudor.aux7.length > 0) {
          return true
        } else {
          return false
        }
      } else {
        return false
      }
    },
    hayAux8() {
      if (this.deudor.aux8) {
        if (this.deudor.aux8.length > 0) {
          return true
        } else {
          return false
        }
      } else {
        return false
      }
    },    
    hayAux9() {
      if (this.deudor.aux9) {
        if (this.deudor.aux9.length > 0) {
          return true
        } else {
          return false
        }
      } else {
        return false
      }
    },
    hayAux10() {
      if (this.deudor.aux10) {
        if (this.deudor.aux10.length > 0) {
          return true
        } else {
          return false
        }
      } else {
        return false
      }
    },
    hayAux11() {
      if (this.deudor.aux11) {
        if (this.deudor.aux11.length > 0) {
          return true
        } else {
          return false
        }
      } else {
        return false
      }
    },
    hayAux12() {
      if (this.deudor.aux12) {
        if (this.deudor.aux12.length > 0) {
          return true
        } else {
          return false
        }
      } else {
        return false
      }
    },
    hayAux13() {
      if (this.deudor.aux13) {
        if (this.deudor.aux13.length > 0) {
          return true
        } else {
          return false
        }
      } else {
        return false
      }
    },
    hayAux14() {
      if (this.deudor.aux14) {
        if (this.deudor.aux14.length > 0) {
          return true
        } else {
          return false
        }
      } else {
        return false
      }
    },
    hayAux15() {
      if (this.deudor.aux15) {
        if (this.deudor.aux15.length > 0) {
          return true
        } else {
          return false
        }
      } else {
        return false
      }
    },
    hayAux16() {
      if (this.deudor.aux16) {
        if (this.deudor.aux16.length > 0) {
          return true
        } else {
          return false
        }
      } else {
        return false
      }
    },
    hayAux17() {
      if (this.deudor.aux17) {
        if (this.deudor.aux17.length > 0) {
          return true
        } else {
          return false
        }
      } else {
        return false
      }
    },
    hayAux18() {
      if (this.deudor.aux18) {
        if (this.deudor.aux18.length > 0) {
          return true
        } else {
          return false
        }
      } else {
        return false
      }
    },
    hayAux19() {
      if (this.deudor.aux19) {
        if (this.deudor.aux19.length > 0) {
          return true
        } else {
          return false
        }
      } else {
        return false
      }
    },
    hayAux20() {
      if (this.deudor.aux20) {
        if (this.deudor.aux20.length > 0) {
          return true
        } else {
          return false
        }
      } else {
        return false
      }
    },
    hayAux21() {
      if (this.deudor.aux21) {
        if (this.deudor.aux21.length > 0) {
          return true
        } else {
          return false
        }
      } else {
        return false
      }
    },
    hayAux22() {
      if (this.deudor.aux22) {
        if (this.deudor.aux22.length > 0) {
          return true
        } else {
          return false
        }
      } else {
        return false
      }
    },
    hayAux23() {
      if (this.deudor.aux23) {
        if (this.deudor.aux23.length > 0) {
          return true
        } else {
          return false
        }
      } else {
        return false
      }
    },
    hayAux24() {
      if (this.deudor.aux24) {
        if (this.deudor.aux24.length > 0) {
          return true
        } else {
          return false
        }
      } else {
        return false
      }
    },
    hayAux25() {
      if (this.deudor.aux25) {
        if (this.deudor.aux25.length > 0) {
          return true
        } else {
          return false
        }
      } else {
        return false
      }
    },
    hayAux26() {
      if (this.deudor.aux26) {
        if (this.deudor.aux26.length > 0) {
          return true
        } else {
          return false
        }
      } else {
        return false
      }
    },
    hayAux27() {
      if (this.deudor.aux27) {
        if (this.deudor.aux27.length > 0) {
          return true
        } else {
          return false
        }
      } else {
        return false
      }
    },
    hayAux28() {
      if (this.deudor.aux28) {
        if (this.deudor.aux28.length > 0) {
          return true
        } else {
          return false
        }
      } else {
        return false
      }
    },    
    hayAux29() {
      if (this.deudor.aux29) {
        if (this.deudor.aux29.length > 0) {
          return true
        } else {
          return false
        }
      } else {
        return false
      }
    },
    hayAux30() {
      if (this.deudor.aux30) {
        if (this.deudor.aux30.length > 0) {
          return true
        } else {
          return false
        }
      } else {
        return false
      }
    },
    hayAux31() {
      if (this.deudor.aux31) {
        if (this.deudor.aux31.length > 0) {
          return true
        } else {
          return false
        }
      } else {
        return false
      }
    },
    hayAux32() {
      if (this.deudor.aux32) {
        if (this.deudor.aux32.length > 0) {
          return true
        } else {
          return false
        }
      } else {
        return false
      }
    },
    hayAux33() {
      if (this.deudor.aux33) {
        if (this.deudor.aux33.length > 0) {
          return true
        } else {
          return false
        }
      } else {
        return false
      }
    },
    hayAux34() {
      if (this.deudor.aux34) {
        if (this.deudor.aux34.length > 0) {
          return true
        } else {
          return false
        }
      } else {
        return false
      }
    },
    hayAux35() {
      if (this.deudor.aux35) {
        if (this.deudor.aux35.length > 0) {
          return true
        } else {
          return false
        }
      } else {
        return false
      }
    },
    hayAux36() {
      if (this.deudor.aux36) {
        if (this.deudor.aux36.length > 0) {
          return true
        } else {
          return false
        }
      } else {
        return false
      }
    },
    hayAux37() {
      if (this.deudor.aux37) {
        if (this.deudor.aux37.length > 0) {
          return true
        } else {
          return false
        }
      } else {
        return false
      }
    },
    hayAux38() {
      if (this.deudor.aux38) {
        if (this.deudor.aux38.length > 0) {
          return true
        } else {
          return false
        }
      } else {
        return false
      }
    },
    hayAux39() {
      if (this.deudor.aux39) {
        if (this.deudor.aux39.length > 0) {
          return true
        } else {
          return false
        }
      } else {
        return false
      }
    },
    hayAux40() {
      if (this.deudor.aux40) {
        if (this.deudor.aux40.length > 0) {
          return true
        } else {
          return false
        }
      } else {
        return false
      }
    },


  },
  mounted() {

  },
  methods: {

  }
}


