import { mapState } from "vuex"

export default {
  name: 'crm-otras-cuentas',
  components: {},
  props: {
    id_deudor: String
  },
  data() {
    return {
      cabeceras: [
        { text: 'Nro deudor', value: 'id_deudor' },
        { text: 'Empresa', value: 'id_empresa' },
        { text: 'Nombre y Apellido', value: 'nombre_apellido' },
        { text: 'Nro Cliente', value: 'nro_cliente1' },
        { text: 'Nro Cliente 2', value: 'nro_cliente2' },
        { text: 'Nro Cliente 3', value: 'nro_cliente3' },
        { text: 'Nro Remesa', value: 'remesa' },
        { text: 'DNI', value: 'nro_doc' },
        { text: 'Fecha Recepción', value: 'fecha_recepcion' },
        { text: 'Fecha Cierre', value: 'fecha_cierra' },
        { text: 'Deuda Histórica', value: 'deuda_historica' },
      ],

    }
  },
  computed: {
    ...mapState('deudor', ['deudores']),
    ...mapState('empresas', ['empresas'])
  },
  mounted() {

  },
  methods: {
    mostrarDescripcionEmpresa(id_empresa) {
      let empresa = this.empresas.find(emp => emp.id_empresa == id_empresa)
      return empresa.descripcion
    },

    mostrarNroCliente(nro_cliente){
      return nro_cliente ? nro_cliente : '-'
    }
  }
}


