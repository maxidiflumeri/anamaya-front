import { mapActions, mapState } from 'vuex'

export default {
  name: 'crm-transacciones',
  components: {},
  props: {
    id_deudor: String
  },
  data() {
    return {
      cabeceras: [
        { text: 'Nro deudor', value: 'id_deudor' },
        { text: 'Fecha Inicio', value: 'fecha_inicio' },
        { text: 'Fecha Fin', value: 'fecha_fin' },
        { text: 'Tipo', value: 'id_tipo_transaccion' },
        { text: 'Detalle', value: 'detalle' },
        { text: 'Usuario', value: 'nombre_completo' },
        { text: 'ID Llamada', value: 'id_llamada' },
        { text: 'Grabacion', value: 'grabacion' },
        { text: 'Aux1', value: 'aux1' }
      ],
      dialog: false,
      detalleItem: ''
    }
  },
  computed: {    
    ...mapState('deudor', ['transacciones', 'deudor']),

  },
  mounted() {
    this.obtenerTransacciones(this.deudor.id_deudor)
  },
  methods: {
    ...mapActions('deudor', ['obtenerTransacciones']),
    getDate(datetime) {
      let date = datetime.substr(8, 2) + "/" + datetime.substr(5, 2) + "/" + datetime.substr(0, 4) + " " + datetime.substr(11, 8)      
      return date
    },
    mostrarDetalle(item){
      this.dialog = true
      this.detalleItem = item.detalle
    }
  }
}


