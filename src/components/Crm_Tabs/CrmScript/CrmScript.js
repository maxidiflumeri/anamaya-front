import { mapState } from "vuex"

export default {
  name: 'crm-script',
  components: {},
  props: {
    id_deudor: String
  },
  data () {
    return {

    }
  },
  computed: {
    ...mapState('politicas', ['politica']),
  },
  mounted () {

  },
  methods: {
    hayTexto(texto) {
      if (texto) {
        if (texto.trim().length > 0) {
          return texto.trim()
        } else {
          return "Sin datos."
        }
      } else {
        return "Sin datos"
      }
    }
  }
}


