import { mapState } from 'vuex'

export default {

  name: 'crm-convenios',

  data() {
    return {
      cabeceras: [
        { text: 'Id Convenio', value: 'id_convenio' },
        { text: 'Fecha Convenio', value: 'fecha' },
        { text: 'Cuotas', value: 'cuotas' },
        { text: 'Importe', value: 'importe' },
        {text: 'Estado', value: 'anulado'},
        {text: 'detalle', value: 'detalle'}
      ],
      headerProps: {
        sortByText: 'Ordenar por '
      },
      dialog: false,
      fecha_vto1: '',
      importe1: 0,
      fecha_vto2: '',
      importe2: 0,
      fecha_vto3: '',
      importe3: 0,
      fecha_vto4: '',
      importe4: 0,
      fecha_vto5: '',
      importe5: 0,
      fecha_vto6: '',
      importe6: 0,
      fecha_vto7: '',
      importe7: 0,
      fecha_vto8: '',
      importe8: 0,
      fecha_vto9: '',
      importe9: 0,
      fecha_vto10: '',
      importe10: 0,
      fecha_vto11: '',
      importe11: 0,
      fecha_vto12: '',
      importe12: 0,

    }
  },

  mounted() {    
  },

  computed: {
    ...mapState('deudor', ['conveniosPorIdDeudor', 'deudor']),
  },

  methods: {
    mostrarDetalle(item){
      this.dialog = true
      this.fecha_vto1 = item.fecha1
      this.importe1 = item.importe1
      this.fecha_vto2 = item.fecha2
      this.importe2 = item.importe2
      this.fecha_vto3 = item.fecha3
      this.importe3 = item.importe3
      this.fecha_vto4 = item.fecha4
      this.importe4 = item.importe4
      this.fecha_vto5 = item.fecha5
      this.importe5 = item.importe5
      this.fecha_vto6 = item.fecha6
      this.importe6 = item.importe6
      this.fecha_vto7 = item.fecha7
      this.importe7 = item.importe7
      this.fecha_vto8 = item.fecha8
      this.importe8 = item.importe8
      this.fecha_vto9 = item.fecha9
      this.importe9 = item.importe9
      this.fecha_vto10 = item.fecha10
      this.importe10 = item.importe10
      this.fecha_vto11 = item.fecha11
      this.importe11 = item.importe11
      this.fecha_vto12 = item.fecha12
      this.importe12 = item.importe12
    }
  }

}


