import { mapState } from 'vuex'

export default {

  name: 'crm-facturas',

  data() {
    return {
      cabeceras: [
        { text: 'Nro Factura', value: 'nro_factura' },
        { text: 'Fecha Comprobante', value: 'fecha_comprobante' },
        { text: 'Fecha Vencimiento', value: 'fecha_vencimiento' },
        { text: 'Id Convenio', value: 'id_convenio' },
        { text: 'Importe 1', value: 'importe1' },
        { text: 'Importe 2', value: 'importe2' },
        { text: 'Importe 3', value: 'importe3' },
        { text: 'Importe 4', value: 'importe4' },
        { text: 'Importe 5', value: 'importe5' },
        { text: 'Clave de Pago', value: 'clave_pago' },
        { text: 'Cant Cartas', value: 'cant_cartas' },
        { text: 'Correo', value: 'correo' },
      ],
      headerProps: {
        sortByText: 'Ordenar por '
      },
    }
  },

  computed: {
    ...mapState('deudor', ['facturasPorIdDeudor', 'deudor']),
  },

}


