import CrmGestion from '../CrmGestion/index';
import { mapActions } from 'vuex'
import { mapState } from 'vuex'
import deudorApi from '../../../services/Apis/BackEnd/deudor'
import toyotaAtClienteApi from '../../../services/Apis/BackEnd/toyotaAtCliente'
import neoApi from '../../../services/Apis/Neotel/NeoApi'


export default {
  name: 'crm-menu',
  components: {
    CrmGestion
  },
  props: {
    id_deudor: String
  },
  data() {
    return {
      idDeudor: this.$route.params.id_deudor,
      muestraMensaje: false,
      dialog: true,
      cargando: true,
      dialogDeudor: false,
      deudorNuevo: 0,
      mensaje: '',
      colorMensaje: '',
      posicionNueva: null,
      intervalId: '',
      hora1: '',
      dialogToyotaAtCliente: false,
      id_gestion: ''
    }
  },
  computed: {
    ...mapState('deudor', ['deudor']),
    ...mapState('empresas', ['empresa']),
    ...mapState('tiposRed', ['tiposRed']),
    ...mapState('neotel', ['posicion']),
    ...mapState('auth', ['usuario']),
    backgroundTab() {
      return (this.$vuetify.theme.dark) ? '#1E5F74' : '#48A4AC'
    },
    hayIdGestion() {
      if (this.id_gestion == '') {
        return true
      } else {
        return false
      }
    }
  },

  mounted() {
    this.get()
    //this.timerPosicionNeotel()
  },

  destroyed() {
    clearInterval(this.intervalId)
  },

  watch: {
    idDeudor() {
      this.idDeudor = this.$route.params.id_deudor
    }
  },
  methods: {
    ...mapActions('deudor', ['obtenerDeudor']),
    ...mapActions('deudor', ['obtenerComentarios']),
    ...mapActions('deudor', ['obtenerFacturasPorIdDeudor', 'obtenerPagosPorIdDeudor', 'obtenerConveniosPorIdDeudor', 'obtenerCuponesPorIdDeudor']),
    ...mapActions('empresas', ['obtenerEmpresaPorId']),
    ...mapActions('empresas', ['obtenerEmpresas']),
    ...mapActions('deudor', ['obtenerCorreos']),
    ...mapActions('deudor', ['obtenerTelefonos']),
    ...mapActions('deudor', ['obtenerRedes']),
    ...mapActions('deudor', ['obtenerTransacciones']),
    ...mapActions('politicas', ['obtenerPoliticaPorId']),
    ...mapActions('tiposRed', ['obtenerTiposRed']),
    ...mapActions('tiposTelefono', ['obtenerTiposTelefono']),
    ...mapActions('tiposDireccion', ['obtenerTiposDireccion']),
    ...mapActions('tiposCorreo', ['obtenerTiposCorreo']),
    ...mapActions('empresas', ['obtenerCodigosGestionPorIdEmpresa']),
    ...mapActions('empresas', ['obtenerCodigosSituacionPorIdEmpresa']),
    ...mapActions('empresas', ['obtenerMotivosNoPagoPorIdEmpresa']),
    ...mapActions('deudor', ['obtenerDirecciones']),
    ...mapActions('deudor', ['obtenerVinculos']),
    ...mapActions('tiposVinculo', ['obtenerTiposVinculos']),
    ...mapActions('deudor', ['obtenerDeudoresPorNroDocumento']),
    ...mapActions('neotel', ['obtenerPosicion']),

    async get() {
      try {
        await this.obtenerDeudor(this.idDeudor)
        await this.obtenerEmpresas(),
          await Promise.all([
            this.obtenerFacturasPorIdDeudor(this.idDeudor),
            this.obtenerPagosPorIdDeudor(this.idDeudor),
            this.obtenerCuponesPorIdDeudor(this.id_deudor),
            this.obtenerConveniosPorIdDeudor(this.idDeudor),
            this.obtenerComentarios(this.idDeudor),
            this.obtenerEmpresaPorId(this.deudor.id_empresa),
            this.obtenerTelefonos(this.idDeudor),
            this.obtenerCorreos(this.idDeudor),
            this.obtenerRedes(this.idDeudor),
            this.obtenerPoliticaPorId(this.deudor.id_politica),
            this.obtenerDirecciones(this.idDeudor),
            this.obtenerVinculos(this.idDeudor),
            this.obtenerTiposCorreo(),
            this.obtenerTiposRed(),
            this.obtenerTiposTelefono(),
            this.obtenerTiposDireccion(),
            this.obtenerTiposVinculos(),
            this.obtenerCodigosGestionPorIdEmpresa(this.deudor.id_empresa),
            this.obtenerCodigosSituacionPorIdEmpresa(this.deudor.id_empresa),
            this.obtenerMotivosNoPagoPorIdEmpresa(this.deudor.id_empresa),
            this.obtenerTransacciones(this.idDeudor),
            this.obtenerDeudoresPorNroDocumento(this.deudor.nro_doc)
          ])
        await this.obtenerPosicion(this.usuario.legajo_neotel)
        this.mostrarDialogGestionToyota()
      } catch (error) {
        this.mostrarMensaje("Error al procesar su solicitud. Vuelva a intentarlo mas tarde.", "error")
        console.log("ERROR DE CRM MENU:" + error)

      }
      this.cargando = false
      this.dialog = false
    },

    mostrarMensaje(mensaje, color) {
      this.muestraMensaje = true
      this.mensaje = mensaje
      this.colorMensaje = color
    },

    mostrarDialogGestionToyota() {
      if (this.deudor.id_empresa == 85 && this.usuario.id_rol>3) {
        localStorage.setItem('atClienteToyota', '0')
        this.dialogToyotaAtCliente = true
      }
    },

    async cargarAtClienteGestionToyota() {      
      var gestion = {
        id_llamada: this.posicion.data.idllamada,
        id_gestion: parseInt(this.id_gestion),
        id_deudor: this.deudor.id_deudor,
        id_usuario: this.usuario.id_usuario,
        legajo_usuario: this.usuario.legajo_neotel,
        grabacion: this.posicion.data.grabacion
      }
      
      var response = await toyotaAtClienteApi.agregarGestion(gestion)
      if (response.data) {
        localStorage.removeItem('atClienteToyota')
        this.dialogToyotaAtCliente = false
        this.mostrarMensaje("Id Gestión cargado exitosamente.", "success")
        this.id_gestion = ''
      } else if (response.error) {
        this.dialogToyotaAtCliente = false
        this.mostrarMensaje("Error al cargar el Id Gestión.", "error")
        this.id_gestion = ''
      }
    },

    async actualizarBuscar() {
      if (this.deudor.id_empresa == 85 && localStorage.getItem('atClienteToyota')) {
        this.mostrarDialogGestionToyota()
      } else {
        await this.obtenerPosicion(this.usuario.legajo_neotel)
        if (this.posicion.data) {
          this.deudorNuevo = Number(this.posicion.data.data)
          if (this.deudorNuevo > 0) {
            if (this.deudorNuevo != this.deudor.id_deudor) {
              const deuNuevo = await deudorApi.obtenerDeudor(this.deudorNuevo)
              if (deuNuevo.data.mensaje) {
                this.dialogDeudor = true
              } else {
                this.$router.replace({ path: `/CrmMenu/${this.deudorNuevo}/CrmGestion` })
                location.reload()
              }
            } else {
              this.$router.push({ path: "/buscadorDeudor" })
            }
          } else {
            this.$router.push({ path: "/buscadorDeudor" })
          }
        } else {
          if (this.usuario.rol > 3) {
            this.$router.push({ path: "/buscadorDeudor" })
          } else {
            this.mostrarMensaje("Usuario no logueado en Neotel.", "error")
          }
        }
      }

    },



    async timerPosicionNeotel() {
      this.hora1 = new Date()
      return await new Promise(() => {
        this.intervalId = setInterval(async () => {
          this.posicionNueva = await neoApi.getPosicion(this.usuario.legajo_neotel)
          if (this.posicion.data) {
            if (this.posicion.data.idllamada == this.posicionNueva.data.idllamada) {
              console.log('es la misma')
              setTimeout(null, 10000)
              let hora2 = new Date()
              //TODO: fijarse que atributo de la empresa tiene el tiempo limite de la llamada para realizar el corte de la llamada en neotel
              let diff = 0
              diff = parseInt((hora2.getTime() - this.hora1.getTime()) / 1000)
              if (diff > 20) {
                console.log('Corto el llamado porque van mas de 20 seg')
              }
            }
          } else {

            console.log('no hay posicion')
          }
        }, 5000);
      });
    },

    async agendarLlamado() {
      //TODO: ponerle parametros correctos de la posicion (CRM, USUARIO, BASE, IDCONTACTO, DATA, FECHA DE AGENDA, TELEFONO DE AGENDA)
      console.log(await neoApi.updateContact('4', '1709', '9933', '29348509', '39501862', '2021-05-19T16:00:00', '1555775452'))
    }
  }
}
