import TablaTelefonos from '../../TablasCrud/TablaTelefonos/index.vue'
import TablaRedes from '../../TablasCrud/TablaRedes/index.vue'
import TablaCorreos from '../../TablasCrud/TablaCorreos/index.vue'
import { mapState } from 'vuex'
import { mapActions } from 'vuex'
import url from '../../../urls/index.js'
import moment from 'moment'

export default {
  name: 'crm-gestion',
  components: { TablaTelefonos, TablaRedes, TablaCorreos },
  props: {
  },
  data() {
    return {
      idllamada: 0,
      grabacion: '',
      colorSelectSituacion: 'primary',
      colorSelectGestion: 'primary',
      colorSelectMotivo: 'primary',
      btnLoading: false,
      cambioSituacion: false,
      cambioGestion: false,
      cambioMotivo: false,
      dialogGuardar: false,
      codigoSituacion: null,
      codigosSituacionLocal: [],
      codigoGestion: null,
      codigosGestionLocal: [],
      motivoNoPago: null,
      motivosNoPagoLocal: [],
      muestraMensaje: false,
      colorMensaje: "",
      mensaje: "",
      nuevoComentario: "",
      dialogComentario: false,
      cabeceraTelefonos: [
        {
          text: "Telefono",
          value: "telefono"
        },
        {
          text: "Tipo",
          value: "tipo_telefono"
        },
        {
          text: "Principal",
          value: "efectivo"
        },
        {
          text: 'Acciones',
          value: 'actions',
          sortable: false
        }
      ],
      cabeceraCorreos: [
        {
          text: "Correo",
          value: "correo"
        },
        {
          text: "Tipo",
          value: "tipo_correo"
        },
        {
          text: "Principal",
          value: "efectivo"
        },
        {
          text: 'Acciones',
          value: 'actions',
          sortable: false
        }
      ],
      cabeceraRedes: [
        {
          text: "Usuario Red Social",
          value: "usuario"
        },
        {
          text: "Red",
          value: "id_red_social"
        },
        {
          text: 'Acciones',
          value: 'actions',
          sortable: false
        }
      ],
      reglas: [
        value => !!value || 'El campo es requerido!'
      ],
      comentarioOrder: false,
      valid: false
    }
  },
  computed: {

    disabledSituacion() {
      let codigoSit = this.codigosSituacion.find(codigo => codigo.id_situacion == this.deudor.id_situacion)
      if (codigoSit.bloqueante == 1) {
        this.colorSelectSituacion = 'grey'
        return true
      } else {
        return false
      }
    },

    disabledGestion() {
      let codigoGes = this.codigosGestion.find(codigo => codigo.id_gestion == this.deudor.id_gestion)
      if (codigoGes.bloqueante == 1) {
        this.colorSelectGestion = 'grey'
        return true
      } else {
        return false
      }
    },

    disabledMotivo() {
      let motivoNP = this.motivosNoPago.find(codigo => codigo.id_motivo == this.deudor.id_motivo_no_pago)
      if (motivoNP) {
        if (motivoNP.bloqueante == 1) {
          this.colorSelectMotivo = 'grey'
          return true
        } else {
          return false
        }
      }
    },

    codigoHabilitado() {
      if (this.cambioSituacion || this.cambioGestion || this.cambioMotivo) {
        return false
      } else {
        return true
      }
    },

    hayComentario() {
      if (this.nuevoComentario != "") {
        return false
      } else {
        return true
      }
    },

    listarComentarios() {
      var salida = ""
      this.comentarios.forEach(element => {
        salida = salida + this.formatearFecha(element.fecha) + " - " + element.comentario + "\n"
      });
      return salida
    },
    ...mapState('empresas', ['empresa']),
    ...mapState('deudor', ['deudor']),
    ...mapState('deudor', ['comentarios']),
    ...mapState('deudor', ['telefonos']),
    ...mapState('deudor', ['correos']),
    ...mapState('deudor', ['redes']),
    ...mapState('deudor', ['pagosPorIdDeudor']),
    ...mapState('politicas', ['politica']),
    ...mapState('empresas', ['codigosGestion']),
    ...mapState('empresas', ['codigosSituacion']),
    ...mapState('empresas', ['motivosNoPago']),
    ...mapState('auth', ['usuario']),
    ...mapState('auth', ['token']),
    ...mapState('neotel', ['posicion']),
  },

  watch: {
    codigoSituacion() {
      if (this.codigoSituacion) {
        if (!this.codigoSituacion.id_situacion && this.deudor.id_situacion != this.codigoSituacion) {
          this.cambioSituacion = true
        } else {
          this.cambioSituacion = false
        }
      }
    },
    codigoGestion() {
      if (this.codigoGestion) {
        if (!this.codigoGestion.id_gestion && this.deudor.id_gestion != this.codigoGestion) {
          this.cambioGestion = true
        } else {
          this.cambioGestion = false
        }
      }
    },
    motivoNoPago() {
      if (this.motivoNoPago) {
        if (!this.motivoNoPago.id_motivo && this.deudor.id_motivo_no_pago != this.motivoNoPago) {
          this.cambioMotivo = true
        } else {
          this.cambioMotivo = false
        }
      }
    }
  },

  mounted() {
    this.inicializarCombos()
  },
  methods: {

    ...mapActions('deudor', ['obtenerComentarios']),
    ...mapActions('deudor', ['obtenerDeudor']),

    cerrarDialogComentario() {
      this.dialogComentario = false
      this.$refs.form.resetValidation()
    },

    ordenarComentario() {
      this.comentarios.sort((a, b) => {
        if (this.comentarioOrder == false) {
          if (a.fecha < b.fecha) {
            return 1
          }
          if (a.fecha > b.fecha) {
            return -1
          }
          return 0
        } else {
          if (a.fecha > b.fecha) {
            return 1
          }
          if (a.fecha < b.fecha) {
            return -1
          }
          return 0
        }
      })
      this.comentarioOrder = !this.comentarioOrder

    },

    async guardarCambios() {
      var codsitu = ''
      var codges = ''
      var motivo = null
      var fecha_situacion = this.deudor.fecha_cambio_sit
      var fecha_gestion = this.deudor.fecha_cambio_gest
      if (this.codigoSituacion.id_situacion) {
        codsitu = this.codigoSituacion.id_situacion
      } else {
        codsitu = this.codigoSituacion
      }
      if (this.codigoGestion.id_gestion) {
        codges = this.codigoGestion.id_gestion
      } else {
        codges = this.codigoGestion
      }
      if (this.motivoNoPago != null) {
        if (this.motivoNoPago.id_motivo) {
          motivo = this.motivoNoPago.id_motivo
        } else if (this.motivoNoPago) {
          motivo = this.motivoNoPago
        }
      }

      if (this.cambioSituacion) {
        fecha_situacion = moment().format('YYYY-MM-DD')
      }
      if (this.cambioGestion) {
        fecha_gestion = moment().format('YYYY-MM-DD')
      }

      this.verificarDatosLlamada()

      var deudorPut = {
        id_situacion: codsitu,
        fecha_cambio_sit: fecha_situacion,
        id_gestion: codges,
        fecha_cambio_gest: fecha_gestion,
        id_motivo_no_pago: motivo,
        id_llamada: this.idllamada,
        grabacion: this.grabacion
      }
      try {
        this.btnLoading = true
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        await this.axios.put(url.deudor.urlModificar + '/' + this.deudor.id_deudor, deudorPut, configuracion)
        this.obtenerDeudor(this.deudor.id_deudor)
        this.mostrarMensaje("Cambios guardados exitosamente.", "success")
        this.dialogGuardar = false
        this.cambioGestion = false
        this.cambioSituacion = false
      } catch (error) {
        console.log("Error POST: " + error)
        this.mostrarMensaje("Error al guardar los cambios.", "error")
      }
      this.btnLoading = false
    },

    formatearFecha(valor) {
      let fecha = valor.substr(8, 2) + "/" + valor.substr(5, 2) + "/" + valor.substr(0, 4)
      return fecha
    },

    async agregarComentario() {
      this.verificarDatosLlamada()
      const comentario = {
        id_usuario: this.usuario.id_usuario,
        id_deudor: this.deudor.id_deudor,
        comentario: this.nuevoComentario,
        id_llamada: this.idllamada,
        grabacion: this.grabacion
      }
      try {
        this.btnLoading = true
        let header = { 'token': this.token }
        let configuracion = { headers: header }
        await this.axios.post(url.comentarios.urlAgregar, comentario, configuracion)
        this.mostrarMensaje("Comentario agregado exitosamente.", "success")
        this.dialogComentario = false
        this.nuevoComentario = ""
        this.btnLoading = false
        await this.obtenerComentarios(this.deudor.id_deudor)
      } catch (error) {
        console.log("Error POST: " + error)
        this.dialogComentario = false
        this.nuevoComentario = ""
        this.btnLoading = false
      }
    },

    verificarDatosLlamada() {
      if (this.posicion.data) {
        if (this.posicion.data.data != this.deudor.id_deudor) {
          this.idllamada = 0
          this.grabacion = ''
        } else {
          this.idllamada = this.posicion.data.idllamada
          this.grabacion = this.posicion.data.grabacion
        }
      } else {
        this.idllamada = 0
        this.grabacion = ''
      }
    },

    mostrarMensaje(mensaje, color) {
      this.muestraMensaje = true
      this.mensaje = mensaje
      this.colorMensaje = color
    },

    inicializarCombos() {
      this.codigoSituacion = this.codigosSituacion.find(codigo => codigo.id_situacion == this.deudor.id_situacion)
      this.codigoGestion = this.codigosGestion.find(codigo => codigo.id_gestion == this.deudor.id_gestion)
      this.motivoNoPago = this.motivosNoPago.find(codigo => codigo.id_motivo == this.deudor.id_motivo_no_pago)
      this.codigosSituacionLocal = this.codigosSituacion.filter(codigo => codigo.visible == 1)
      this.codigosGestionLocal = this.codigosGestion.filter(codigo => codigo.visible == 1)
      this.motivosNoPagoLocal = this.motivosNoPago.filter(codigo => codigo.visible == 1)
      this.cambioCodigo = true
    }
  }
}