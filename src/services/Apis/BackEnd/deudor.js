import url from '../../../urls/index'
import axios from 'axios'
import store from '../../../store/index'

export default {
    obtenerDeudor: async (id_deudor) => {
        var respuesta = {
            data: null,
            error: null
        }
        try {
            let header = { 'token': store.state.auth.token }
            let configuracion = { headers: header }
            const response = await axios.get(url.deudor.urlConsultar + '?id=' + id_deudor, configuracion)
            respuesta.data = response.data
        } catch (error) {
            respuesta.error = error
        }
        return respuesta
    },

    actualizaDeuda: async (monto, id_tipo_actualizacion, fecha, id_iva, id_empresa, id_deudor) => {
        var respuesta = {
            data: null,
            error: null
        }
        try {
            let header = { 'token': store.state.auth.token }
            let configuracion = { headers: header }
            const response = await axios.get(url.deudor.urlActualizaDeuda + `?monto=${monto}&id_tipo_actualizacion=${id_tipo_actualizacion}&fecha=${fecha}&id_iva=${id_iva}&id_empresa=${id_empresa}&id_deudor=${id_deudor}`, configuracion)
            respuesta.data = response.data
        } catch (error) {
            respuesta.error = error
        }
        return respuesta
    }
}