import url from '../../../urls/index'
import axios from 'axios'
import store from '../../../store/index'

export default {
    
    agregarGestion: async (gestion) => {
        var respuesta = {
            data: null,
            error: null
        }
        try {
            let header = { 'token': store.state.auth.token }
            let configuracion = { headers: header }
            let response = await axios.post(url.toyotaAtClientes.urlAgregar, gestion, configuracion)
            respuesta.data = response.data
        } catch (error) {
            respuesta.error = error
        }
        return respuesta
    },

}