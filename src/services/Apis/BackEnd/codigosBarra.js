import url from '../../../urls/index'
import axios from 'axios'
import store from '../../../store/index'

export default {
    obtenerCuponesPorDeudor: async (id_deudor) => {
        var respuesta = {
            data: null,
            error: null
        }
        try {
            let header = { 'token': store.state.auth.token }
            let configuracion = { headers: header }
            const response = await axios.get(url.codigosBarra.urlConsultar + '?id_deudor=' + id_deudor, configuracion)
            respuesta.data = response.data
        } catch (error) {
            respuesta.error = error
        }
        return respuesta
    }    
}