import url from '../../../urls/index'
import axios from 'axios'
import store from '../../../store/index'

export default {
    obtenerPagosPorIdDeudor: async (id_deudor) => {
        var respuesta = {
            data: null,
            error: null
        }
        try {
            let header = { 'token': store.state.auth.token }
            let configuracion = { headers: header }
            const response = await axios.get(url.pagos.urlConsultar + '?id_deudor=' + id_deudor, configuracion)
            respuesta.data = response.data
        } catch (error) {
            respuesta.error = error
        }
        return respuesta
    },

    agregarPago: async (pago) => {
        var respuesta = {
            data: null,
            error: null
        }
        try {
            let header = { 'token': store.state.auth.token }
            let configuracion = { headers: header }
            let response = await axios.post(url.pagos.urlAgregar, pago, configuracion)
            respuesta.data = response.data
        } catch (error) {
            respuesta.error = error
        }
        return respuesta
    },

    anularPago: async (id_pago, codigo, id_deudor, id_llamada, grabacion) => {
        var respuesta = {
            data: null,
            error: null
        }
        try {
            let header = { 'token': store.state.auth.token }
            let configuracion = { headers: header }
            let body = {
                id_llamada: id_llamada,
                grabacion: grabacion
            }
            let response = await axios.put(url.pagos.urlAnular + '/' + id_pago + '/' + codigo + '/' + id_deudor, body, configuracion)
            respuesta.data = response.data
        } catch (error) {
            respuesta.error = error
        }
        return respuesta
    },
}



