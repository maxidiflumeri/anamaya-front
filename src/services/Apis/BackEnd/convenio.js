import url from '../../../urls/index'
import axios from 'axios'
import store from '../../../store/index'

export default {
    obtenerConvenioPorDeudor: async (id_deudor) => {
        var respuesta = {
            data: null,
            error: null
        }
        try {
            let header = { 'token': store.state.auth.token }
            let configuracion = { headers: header }
            const response = await axios.get(url.convenios.urlConsultar + '?id_deudor=' + id_deudor, configuracion)
            respuesta.data = response.data
        } catch (error) {
            respuesta.error = error
        }
        return respuesta
    },

    agregarConvenio: async (convenio) => {
        var respuesta = {
            data: null,
            error: null
        }
        try {
            let header = { 'token': store.state.auth.token }
            let configuracion = { headers: header }
            const response = await axios.post(url.convenios.urlAgregar, convenio, configuracion)
            respuesta.data = response.data
        } catch (error) {
            console.log(error.response)
            respuesta.error = error
        }
        return respuesta
    },

    anularConvenio: async (id_convenio, id_deudor, id_llamada, grabacion) => {
        var respuesta = {
            data: null,
            error: null
        }
        try {
            let header = { 'token': store.state.auth.token }
            let configuracion = { headers: header }
            let body = {
                id_llamada: id_llamada,
                grabacion: grabacion
            }
            const response = await axios.put(url.convenios.urlAnular + '/' + id_convenio + '/' + id_deudor, body, configuracion)
            respuesta.data = response.data
        } catch (error) {
            respuesta.error = error
        }
        return respuesta
    }
}