import url from '../../../urls/index'
import axios from 'axios'
import store from '../../../store/index'

export default {
    obtenerTelefonosPorIdDeudor: async (id_deudor) => {
        var respuesta = {
            data: null,
            error: null
        }
        try {
            let header = { 'token': store.state.auth.token }
            let configuracion = { headers: header }
            const response = await axios.get(url.telefonos.urlConsultar + '?id_deudor=' + id_deudor, configuracion)
            respuesta.data = response.data
        } catch (error) {
            respuesta.error = error
        }
        return respuesta
    },

    agregarTelefono: async (telefono) => {
        var respuesta = {
            data: null,
            error: null
        }
        try {
            let header = { 'token': store.state.auth.token }
            let configuracion = { headers: header }
            let response = await axios.post(url.telefonos.urlAgregar, telefono, configuracion)
            respuesta.data = response.data
        } catch (error) {
            respuesta.error = error
        }
        return respuesta
    },

    modificarTelefono: async (telefonoNuevo, telefonoViejo, id_deudor) => {
        var respuesta = {
            data: null,
            error: null
        }
        try {
            let header = { 'token': store.state.auth.token }
            let configuracion = { headers: header }
            let response = await axios.put(url.telefonos.urlModificar + '/' + id_deudor + '/' + telefonoViejo, telefonoNuevo, configuracion)
            respuesta.data = response.data
        } catch (error) {
            respuesta.error = error
        }
        return respuesta
    },

    eliminarTelefono: async (telefonoDel, id_deudor, id_llamada, grabacion) => {
        var respuesta = {
            data: null,
            error: null
        }
        try {
            let header = { 'token': store.state.auth.token }
            let configuracion = {
                headers: header,
                data: {
                    id_llamada: id_llamada,
                    grabacion: grabacion
                }
            }
            let response = await axios.delete(url.telefonos.urlEliminar + '/' + id_deudor + '/' + telefonoDel, configuracion)
            respuesta.data = response.data
        } catch (error) {
            respuesta.error = error
        }
        return respuesta
    }
}



