import axios from 'axios'

export default {
    getPosicion: async (usuario) => {
        var respuesta = {
            data: null,
            error: null
        }
        try {            
            const response = await axios.get('https://www.anamayasa.com/neotel/neoapi/webservice.asmx/Posicion?USUARIO=' + usuario)
            //const response = await axios.get('http://200.5.98.203/neoapi/webservice.asmx/Posicion?USUARIO=' + usuario)
            //const response = await axios.get('http://90.0.0.205/neoapi/webservice.asmx/Posicion?USUARIO=' + usuario)
            let data = response.data
            let arrayData = data.split('|')
            arrayData.splice(0, 1)
            arrayData.splice(51, 1)
            var json = {}
            arrayData.forEach(element => {
                let corte = element.indexOf('=')
                let clave = element.substr(0, corte).toLowerCase()
                let valor = element.substr(corte + 1, element.length)
                json[clave] = valor
            });
            respuesta.data = json
        } catch (error) {
            respuesta.error = error
        }
        return respuesta
    },

    updateContact: async (crm, usuario, base, idContacto, data, fechaAgenda, telAgenda) => {
        var respuesta = {
            data: null,
            error: null
        }
        // const url = `http://200.5.98.203/neoapi/webservice.asmx/UpdateContact?CRM=${crm}&USUARIO=${usuario}&BASE=${base}&IDCONTACTO=${idContacto}&DATA=${data}
        //                 &SUBCATEGORIA=4&XML_UPDATE=&AGENDA=true&FECHA_AGENDA=${fechaAgenda}&USUARIO_AGENDA=****&TEL_AGENDA=${telAgenda}`
        const url = `https://www.anamayasa.com/neotel/neoapi/webservice.asmx/UpdateContact?CRM=${crm}&USUARIO=${usuario}&BASE=${base}&IDCONTACTO=${idContacto}&DATA=${data}
                        &SUBCATEGORIA=4&XML_UPDATE=&AGENDA=true&FECHA_AGENDA=${fechaAgenda}&USUARIO_AGENDA=****&TEL_AGENDA=${telAgenda}`
        try {
            const response = await axios.get(url)
            respuesta.data = response.data
        } catch (error) {
            respuesta.error = error
        }
        return respuesta
    },

    cortarLlamada: async (usuario) => {
        var respuesta = {
            data: null,
            error: null
        }
         try {
            // const response = await axios.get('http://200.5.98.203/neoapi/webservice.asmx/Cortar?USUARIO=' + usuario)
            const response = await axios.get('https://www.anamayasa.com/neotel/neoapi/webservice.asmx/Cortar?USUARIO=' + usuario)
            respuesta.data = response.data
        } catch (error) {
            respuesta.error = error
        }
        return respuesta        
    }
}