import url from '../../urls/index.js'
import axios from "axios"

export default {
  namespaced: true,
  state: {
    tiposVinculo: null
  },

  actions: {    

    async obtenerTiposVinculos({ commit }) {
      try {
        let header = { 'token': this.state.auth.token }
        let configuracion = { headers: header }
        const data = await axios.get(url.tiposVinculo.urlConsultar, configuracion)        
        commit('actualizarTipo', data.data)
      } catch (error) {
        console.log("Error GET: " + error)
      }    
    },
  },

  mutations: {
    actualizarTipo(state, tipos) {
      state.tiposVinculo = tipos
    }

  },

}