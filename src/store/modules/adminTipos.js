import axios from 'axios'
import url from '../../urls/index.js'

export default {

  namespaced: true,

  state: {
    red: null,
    redesSociales: [],
    tasasInteres:[],
    tiposCorreo:[],
    codigosCorreo:[],
    codigosGestion:[],
    codigosSituacion:[],
    tiposTelefono:[],
    tiposPago:[],
    tiposConvenio:[],
    tiposActualizacion: [],
    tiposTransaccion: [],
    tiposDireccion: [],
    tiposParentesco: [],
    tiposEmpresasParam: [],
    codigosTabla: [],
    motivosNoPago:[],
    listaNumeradores:[],
    entidadesRecaudadoras: [],
    formasDePago: [],
    gruposDeTrabajo: [],
    tiposDeIva: [],
    roles: [],
    templateCorreos: []
  },

  actions: {

    async obtenerTiposIva({ commit }) {
      try {
        let header = { 'token': this.state.auth.token }
        let configuracion = { headers: header }
        const data = await axios.get(url.tiposIva.urlConsultar, configuracion)
        commit('actualizarTiposIva', data.data)
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },

    async obtenerGruposTrabajo({ commit }) {
      try {
        let header = { 'token': this.state.auth.token }
        let configuracion = { headers: header }
        const data = await axios.get(url.gruposTrabajo.urlConsultar, configuracion)
        commit('actualizarGruposTrabajo', data.data)
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },

    async obtenerFormasPago({ commit }) {
      try {
        let header = { 'token': this.state.auth.token }
        let configuracion = { headers: header }
        const data = await axios.get(url.formasPago.urlConsultar, configuracion)
        commit('actualizarFormasPago', data.data)
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },

    async obtenerEntidadesRecaudadoras({ commit }) {
      try {
        let header = { 'token': this.state.auth.token }
        let configuracion = { headers: header }
        const data = await axios.get(url.entidadesRecaudadoras.urlConsultar, configuracion)
        commit('actualizarEntidadesRecaudadoras', data.data)
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },

    async obtenerNumeradores({ commit }) {
      try {
        let header = { 'token': this.state.auth.token }
        let configuracion = { headers: header }
        const data = await axios.get(url.numeradores.urlConsultar, configuracion)
        commit('actualizarNumeradores', data.data)
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },

    async obtenerMotivosNoPago({ commit }) {
      try {
        let header = { 'token': this.state.auth.token }
        let configuracion = { headers: header }
        const data = await axios.get(url.motivosNoPago.urlConsultar, configuracion)
        commit('actualizarMotivosNoPago', data.data)
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },

    async obtenerTiposRedesSociales({ commit }) {
      try {
        let header = { 'token': this.state.auth.token }
        let configuracion = { headers: header }
        const data = await axios.get(url.tiposRed.urlConsultar, configuracion)
        commit('actualizarRedesSociales', data.data)
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },

    async obtenerTiposTasasInteres({ commit }) {
      try {
        let header = { 'token': this.state.auth.token }
        let configuracion = { headers: header }
        const data = await axios.get(url.tasasInteres.urlConsultar, configuracion)
        commit('actualizarTasasInteres', data.data)
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },

    async obtenerTiposCorreo({ commit }) {
      try {
        let header = { 'token': this.state.auth.token }
        let configuracion = { headers: header }
        const data = await axios.get(url.tiposCorreo.urlConsultar, configuracion)
        commit('actualizarTiposCorreo', data.data)
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },
    
    async obtenerTiposActualizacion({ commit }) {
      try {
        let header = { 'token': this.state.auth.token }
        let configuracion = { headers: header }
        const data = await axios.get(url.tiposActualizacion.urlConsultar, configuracion)
        commit('actualizarTiposActualizacion', data.data)
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },

    async obtenerCodigosCorreo({ commit }) {
      try {
        let header = { 'token': this.state.auth.token }
        let configuracion = { headers: header }
        const data = await axios.get(url.codigosCorreo.urlConsultar, configuracion)
        commit('actualizarCodigosCorreo', data.data)
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },
    
    async obtenerTiposTransaccion({ commit }) {
      try {
        let header = { 'token': this.state.auth.token }
        let configuracion = { headers: header }
        const data = await axios.get(url.tiposTransaccion.urlConsultar, configuracion)
        commit('actualizarTiposTransaccion', data.data)
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },

    async obtenerCodigosGestion({ commit }) {
      try {
        let header = { 'token': this.state.auth.token }
        let configuracion = { headers: header }
        const data = await axios.get(url.codigosGestion.urlConsultar, configuracion)
        commit('actualizarCodigosGestion', data.data)
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },
    
    async obtenerTiposDireccion({ commit }) {
      try {
        let header = { 'token': this.state.auth.token }
        let configuracion = { headers: header }
        const data = await axios.get(url.tiposDireccion.urlConsultar, configuracion)
        commit('actualizarTiposDireccion', data.data)
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },

    async obtenerCodigosSituacion({ commit }) {
      try {
        let header = { 'token': this.state.auth.token }
        let configuracion = { headers: header }
        const data = await axios.get(url.codigosSituacion.urlConsultar, configuracion)
        commit('actualizarCodigosSituacion', data.data)
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },
    
    async obtenerTiposParentesco({ commit }) {
      try {
        let header = { 'token': this.state.auth.token }
        let configuracion = { headers: header }
        const data = await axios.get(url.tiposParentesco.urlConsultar, configuracion)
        commit('actualizarTiposParentesco', data.data)
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },

    async obtenerTiposTelefono({ commit }) {
      try {
        let header = { 'token': this.state.auth.token }
        let configuracion = { headers: header }
        const data = await axios.get(url.tiposTelefono.urlConsultar, configuracion)
        commit('actualizarTiposTelefono', data.data)
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },
    
    async obtenerTiposEmpresasParam({ commit }) {
      try {
        let header = { 'token': this.state.auth.token }
        let configuracion = { headers: header }
        const data = await axios.get(url.empresasParam.urlConsultar, configuracion)
        commit('actualizarEmpresasParam', data.data)
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },

    async obtenerTiposPago({ commit }) {
      try {
        let header = { 'token': this.state.auth.token }
        let configuracion = { headers: header }
        const data = await axios.get(url.tiposPago.urlConsultar, configuracion)
        commit('actualizarTiposPago', data.data)
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },
    
    async obtenerCodigosTabla({ commit }) {
      try {
        let header = { 'token': this.state.auth.token }
        let configuracion = { headers: header }
        const data = await axios.get(url.codigosTabla.urlConsultar, configuracion)
        commit('actualizarCodigosTabla', data.data)
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },

    async obtenerTiposConvenio({ commit }) {
      try {
        let header = { 'token': this.state.auth.token }
        let configuracion = { headers: header }
        const data = await axios.get(url.tiposConvenio.urlConsultar, configuracion)
        commit('actualizarTiposConvenio', data.data)
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },

    async obtenerTemplateCorreos({ commit }) {
      try {
        let header = { 'token': this.state.auth.token }
        let configuracion = { headers: header }
        const data = await axios.get(url.templateCorreos.urlConsultar, configuracion)
        commit('actualizarTemplateCorreos', data.data)
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },

    async obtenerRoles({ commit }) {
      try {
        let header = { 'token': this.state.auth.token }
        let configuracion = { headers: header }
        const data = await axios.get(url.roles.urlConsultar, configuracion)
        commit('actualizarRoles', data.data)
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },
  },

  mutations: {
    actualizarRedesSociales(state, tipos) {
      state.redesSociales = tipos
    },

    actualizarTasasInteres(state, tipos) {
      state.tasasInteres = tipos
    },

    actualizarTiposCorreo(state, tipos) {
      state.tiposCorreo = tipos
    },

    actualizarCodigosCorreo(state, tipos) {
      state.codigosCorreo = tipos
    },

    actualizarCodigosGestion(state, tipos) {
      state.codigosGestion = tipos
    },

    actualizarCodigosSituacion(state, tipos) {
      state.codigosSituacion = tipos
    },

    actualizarTiposTelefono(state, tipos) {
      state.tiposTelefono = tipos
    },
    actualizarMotivosNoPago(state, motivos) {
      state.motivosNoPago = motivos
    },

    actualizarNumeradores(state, tipos) {
      state.listaNumeradores = tipos
    },

    actualizarEntidadesRecaudadoras(state, tipos) {
      state.entidadesRecaudadoras = tipos
    },

    actualizarFormasPago(state, tipos) {
      state.formasDePago = tipos
    },

    actualizarGruposTrabajo(state, tipos) {
      state.gruposDeTrabajo = tipos
    },

    actualizarTiposIva(state, tipos) {
      state.tiposDeIva = tipos
    },

    actualizarTiposPago(state, tipos) {
      state.tiposPago = tipos
    },

    actualizarTiposConvenio(state, tipos) {
      state.tiposConvenio = tipos
    },

    actualizarTiposActualizacion(state, tipos) {
      state.tiposActualizacion = tipos
    },

    actualizarTiposTransaccion(state, tipos) {
      state.tiposTransaccion = tipos
    },

    actualizarTiposDireccion(state, tipos) {
      state.tiposDireccion = tipos
    },

    actualizarTiposParentesco(state, tipos) {
      state.tiposParentesco = tipos
    },

    actualizarEmpresasParam(state, tipos) {
      state.tiposEmpresasParam = tipos
    },

    actualizarCodigosTabla(state, tipos) {
      state.codigosTabla = tipos
    },
    actualizarTemplateCorreos(state, templates) {
      state.templateCorreos = templates
    },
    actualizarRoles(state, roles) {
      state.roles = roles
    },
  },

}