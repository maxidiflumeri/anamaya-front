import url from '../../urls/index.js'
import axios from "axios"

export default {
  namespaced: true,
  state: {
  },

  actions: {
    async buscarCorreosPorCorreo(context, correo) {
      try {
        let header = { 'token': this.state.auth.token }
        let configuracion = { headers: header }
        let correosAux = {}
        try {
          const response = await axios.get(url.correos.urlConsultar + '?correo=' + correo, configuracion)
          correosAux = response.data
        } catch (error) {
          console.log(error)
        }
        //console.log(correosAux)
        return correosAux
      }
      catch (error) {
        console.log(error)
        if (error) {
          this.errorLogin = true
        }
      }
    },
  },

  mutations: {

  },

}