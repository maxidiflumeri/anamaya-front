import neotelApi from '../../services/Apis/Neotel/NeoApi'

export default {
    namespaced: true,
    state: {
        posicion: {
            data: null,
            error: null
        },
    },

    actions: {
        async obtenerPosicion({ commit }, usuario) {
            const response = await neotelApi.getPosicion(usuario)               
            commit('actualizarPosicion', response)            
        },
    },

    mutations: {
        actualizarPosicion(state, posicion) {
            state.posicion = posicion
        }
    },

}