import url from '../../urls/index.js'
import axios from "axios"

export default {
    namespaced: true,
    state: {
        primerDeudor: 0

    },

    actions: {
        async buscarDeudorPorId(context, params) {
            try {
                let header = { 'token':  this.state.auth.token }
                let configuracion = { headers: header }
                let deudorAux = {}
                try {
                    let response = null
                    if(params.empresa){
                        response = await axios.get(url.deudores.urlConsultar + '?id_empresa=' + params.empresa + "&id_deudor=" + params.input, configuracion)
                    }
                    else{
                        response = await axios.get(url.deudores.urlConsultar + '?id=' +params.input, configuracion)
                    }
                    
                    deudorAux = response.data
                } catch (error) {
                    console.log(error)
                }
                //console.log(deudorAux)
                return deudorAux
            }
            catch (error) {
                console.log(error)
                if (error) {
                    this.errorLogin = true
                }
            }
        },

        async buscarDeudorPorNroCliente(context, params) {
            try {
                let header = { 'token': this.state.auth.token }
                let configuracion = { headers: header }
                let deudorAux = {}
                try {
                    let response = null
                    if(params.empresa){
                        response = await axios.get(url.deudores.urlConsultar + '?id_empresa='+ params.empresa +'&nro_cliente=' + params.input, configuracion)
                    }
                    else{
                        response = await axios.get(url.deudores.urlConsultar + '?nro_cliente=' + params.input, configuracion)
                    }
                    deudorAux = response.data
                } catch (error) {
                    console.log(error)
                }
                return deudorAux
            }
            catch (error) {
                console.log(error)
                if (error) {
                    this.errorLogin = true
                }
            }
        },

        async buscarDeudorPorNroDocumento(context, params) {
            try {
                let header = { 'token': this.state.auth.token }
                let configuracion = { headers: header }
                let deudorAux = {}
                try {
                    let response = null
                    if(params.empresa){
                        response = await axios.get(url.deudores.urlConsultar + '?id_empresa='+ params.empresa +'&nro_documento=' + params.input, configuracion)
                    }
                    else{
                        response = await axios.get(url.deudores.urlConsultar + '?nro_documento=' + params.input, configuracion)
                    }
                    deudorAux = response.data
                } catch (error) {
                    console.log(error)
                }
                return deudorAux
            }
            catch (error) {
                console.log(error)
                if (error) {
                    this.errorLogin = true
                }
            }
        },

        async buscarDeudorPorNombre(context, params) {
            try {
                let header = { 'token': this.state.auth.token }
                let configuracion = { headers: header }
                let deudorAux = {}
                try {
                    let response = null
                    if(params.empresa){
                        response = await axios.get(url.deudores.urlConsultar + '?id_empresa='+ params.empresa +'&nombre=' + params.input, configuracion)
                    }
                    else{
                        response = await axios.get(url.deudores.urlConsultar + '?nombre=' + params.input, configuracion)
                    }
                    deudorAux = response.data
                } catch (error) {
                    console.log(error)
                }
                return deudorAux
            }
            catch (error) {
                console.log(error)
                if (error) {
                    this.errorLogin = true
                }
            }
        },
        async obtenerPrimerDeudor({commit}) {
            try {
                let header = { 'token': this.state.auth.token }
                let configuracion = { headers: header }
                const data = await axios.get(url.deudores.urlConsultar + '?primer_deudor=1', configuracion)                
                commit('setPrimerDeudor',data.data[0].id_deudor)         
            } catch (error) {
                console.log("Error GET: " + error)
            }
        }
    },

    mutations: {
        setPrimerDeudor(state, primerDeudor){
            state.primerDeudor = primerDeudor
        }

    },

}