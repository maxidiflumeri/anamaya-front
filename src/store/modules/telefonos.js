import url from '../../urls/index.js'
import axios from "axios"

export default {
  namespaced: true,
  state: {
  },

  actions: {
    async buscarTelefonosPorNro(context, idTelefono) {
      try {
        let header = { 'token': this.state.auth.token }
        let configuracion = { headers: header }
        let telefonosAux = {}
        try {
          const response = await axios.get(url.telefonos.urlConsultar + '?telefono=' + idTelefono, configuracion)
          telefonosAux = response.data
        } catch (error) {
          console.log(error)
        }
        //console.log(telefonosAux)
        return telefonosAux
      }
      catch (error) {
        console.log(error)
        if (error) {
          this.errorLogin = true
        }
      }
    },
  },

  mutations: {

  },

}