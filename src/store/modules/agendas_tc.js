import url from '../../urls/index.js'
import axios from "axios"

export default {
  namespaced: true,
  state: {
    agendasTc: null
  },

  actions: {    

    async obtenerAgendasTc({ commit }) {
      try {
        let header = { 'token': this.state.auth.token }
        let configuracion = { headers: header }
        const data = await axios.get(url.agendasTc.urlConsultar, configuracion)
        commit('actualizarAgenda', data.data)
      } catch (error) {
        console.log("Error GET: " + error)
      }    
    },
  },

  mutations: {
    actualizarAgenda(state, agendas) {
      state.agendasTc = agendas
    }

  },

}