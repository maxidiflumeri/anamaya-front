import url from '../../urls/index.js'
import axios from "axios"

export default {
  namespaced: true,
  state: {
    empresa: null,
    codigosGestion: [],
    codigosSituacion: [],
    motivosNoPago: [],
    empresas: [],
    templateCorreosPorEmpresa: [],
    tiposConveniosPorEmpresa: []
  },

  actions: {
    async obtenerEmpresas({commit}) {
      try {

        let header = { 'token': this.state.auth.token }
        let configuracion = { headers: header }
        let empresasArray = []
        try {
          const response = await axios.get(url.empresas.urlConsultar, configuracion)
          commit('actualizarEmpresas', response.data)
          empresasArray = response.data
          let empresas = empresasArray.map(emp => {
            let aux = {
              descripcion: emp.descripcion,
              idEmp: emp.id_empresa
            };
            return aux;
          })
          empresas.sort()

          return empresas
        } catch (error) {
          console.log(error)
        }
      }
      catch (error) {
        console.log(error)
        if (error) {
          this.errorLogin = true
        }
      }
    },

    async obtenerEmpresaPorId({ commit }, id_empresa) {
      try {
        let header = { 'token': this.state.auth.token }
        let configuracion = { headers: header }
        const data = await axios.get(url.empresas.urlConsultar + '?id=' + id_empresa, configuracion)
        commit('actualizarEmpresa', data.data[0])
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },

    async obtenerCodigosGestionPorIdEmpresa({ commit }, id_empresa) {
      try {
        let header = { 'token': this.state.auth.token }
        let configuracion = { headers: header }
        const data = await axios.get(url.codigosGestion.urlConsultar + '?id_empresa=' + id_empresa, configuracion)
        commit('actualizarCodigosGestion', data.data)
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },

    async obtenerCodigosSituacionPorIdEmpresa({ commit }, id_empresa) {
      try {
        let header = { 'token': this.state.auth.token }
        let configuracion = { headers: header }
        const data = await axios.get(url.codigosSituacion.urlConsultar + '?id_empresa=' + id_empresa, configuracion)                
        commit('actualizarCodigosSituacion', data.data)
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },

    async obtenerMotivosNoPagoPorIdEmpresa({ commit }, id_empresa) {
      try {
        let header = { 'token': this.state.auth.token }
        let configuracion = { headers: header }
        const data = await axios.get(url.motivosNoPago.urlConsultar + '?id_empresa=' + id_empresa, configuracion)
        commit('actualizarMotivosNoPago', data.data)
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },

    async obtenerTemplateCorreosPorIdEmpresa({ commit }, id_empresa) {
      try {
        let header = { 'token': this.state.auth.token }
        let configuracion = { headers: header }
        const data = await axios.get(url.templateCorreos.urlConsultar + '?id_empresa=' + id_empresa, configuracion)
        commit('actualizartemplateCorreosPorEmpresa', data.data)
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },

    async obtenerTiposConveniosPorEmpresa({ commit }, id_empresa) {
      try {
        let header = { 'token': this.state.auth.token }
        let configuracion = { headers: header }
        const data = await axios.get(url.tiposConvenio.urlConsultar + '?id_empresa=' + id_empresa, configuracion)
        commit('actualizarTiposConvenioPorEmpresa', data.data)
      } catch (error) {
        console.log("Error GET: " + error)
      }
    },
  },

  mutations: {
    actualizarEmpresa(state, empresa) {
      state.empresa = empresa
    },
    actualizarCodigosGestion(state, codigos){
      state.codigosGestion = codigos
    },
    actualizarCodigosSituacion(state, codigos){
      state.codigosSituacion = codigos
    },
    actualizarMotivosNoPago(state, motivos){
      state.motivosNoPago = motivos
    },
    actualizarEmpresas(state, empresas) {
      state.empresas = empresas
    },
    actualizartemplateCorreosPorEmpresa(state, templates){
      state.templateCorreosPorEmpresa = templates
    },
    actualizarTiposConvenioPorEmpresa(state, tiposConvenio){
      state.tiposConveniosPorEmpresa = tiposConvenio
    }
  },

}