import url from "../../urls/index.js"
import axios from 'axios'

export default {

    namespaced: true,

    state: {
        errorStatus: false,
        loaderStatus: false,
        grupos: [],
        itemsGrupos: []

    },

    actions: {

        async obtenerGrupos({ commit }) {
            try {
                let header = { 'token': this.state.auth.token }
                let configuracion = { headers: header }                
                commit('SET_LOADER_STATUS', true)
                const response = await axios.get(url.gruposProcesos.urlConsultar, configuracion)
                commit('SET_GRUPOS', response.data)                
            }
            catch (error) {
                console.log(error.message)     
                commit('SET_ERROR_STATUS', true)           
            }
            commit('SET_LOADER_STATUS', false)
        },

        async obtenerItemsGrupos({ commit }) {
            try {
                let header = { 'token': this.state.auth.token }
                let configuracion = { headers: header }                
                commit('SET_LOADER_STATUS', true)
                const response = await axios.get(url.itemsProcesos.urlConsultar, configuracion)
                commit('SET_ITEMS_GRUPOS', response.data)                               
            }
            catch (error) {                
                console.log(error.message)                
                commit('SET_ERROR_STATUS', true)
            }
            commit('SET_LOADER_STATUS', false)
        },

        cerrarError({commit}){
            commit('SET_ERROR_STATUS', false)         
        }



    },

    mutations: {

        SET_LOADER_STATUS(state, newStatus) {
            state.loaderStatus = newStatus
        },
        SET_ERROR_STATUS(state, errorStatus) {
            state.errorStatus = errorStatus
        },
        SET_GRUPOS(state, gruposArray) {
            state.grupos = gruposArray.map((grupo) => {                   
                let aux = {
                    idGrupo: grupo.id_grupo,
                    nombreGrupo: grupo.nombre_grupo,
                    descripcion: grupo.descripcion,
                    idRol: grupo.id_rol,
                    path: grupo.path,
                    imagen: require(`../../assets/icons-ppal/${grupo.foto}`)
                }
                return aux
            })
        },
        SET_ITEMS_GRUPOS(state, items) {
            state.itemsGrupos = items.map( item => {
                let aux = {
                    idItem: item.id_item,
                    idGrupo: item.id_grupo,
                    nombreItem: item.nombre_item,
                    descripcion: item.descripcion,
                    idRol: item.id_rol,
                    path: item.path
                }
                return aux
            })
        }

    },

    getters: {

        getImg: state => (index, atributo) => {
            return state.grupos[index][`${atributo}`]
        }, 
        
        getGrupos: state => {
            return state.grupos
        }

    } 

}