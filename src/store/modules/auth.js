import decode from 'jwt-decode'
import router from '../../routers.js'

export default {
    namespaced: true,
    state: {
        token: null,
        usuario: null,
        switchTema: false
    },

    actions: {
        guardarToken({ commit }, token) {
            commit('setToken', token)
            commit('setUsuario', decode(token)),
                localStorage.setItem('token', token)
        },

        autoLogin({ commit }) {
            let token = localStorage.getItem('token')
            if (token) {
                commit('setToken', token)
                commit('setUsuario', decode(token))
                //router.push({ name: 'PantallaPrincipal' })
            }
            else {
                router.push({ name: 'Login' })
            }
        },
        salir({ commit }) {
            commit('setToken', null)
            commit('setUsuario', null)
            localStorage.removeItem('token')
            router.push({ name: 'Login' })
        },
        setearTema({ commit }, estado) { 
            if(estado){
                localStorage.setItem('modo', 'oscuro')           
            }else{
                localStorage.setItem('modo', 'claro')
            }
            commit('setTema', estado)            
        }

    },

    mutations: {
        setToken(state, token) {
            state.token = token
        },
        setUsuario(state, usuario) {
            state.usuario = usuario
        },
        setTema(state, estado){
            state.switchTema = estado
        }
    },

}