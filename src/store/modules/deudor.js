import axios from 'axios'
import url from '../../urls/index.js'
import telefonoApi from '../../services/Apis/BackEnd/telefono'
import deudorApi from '../../services/Apis/BackEnd/deudor'
import convenioApi from '../../services/Apis/BackEnd/convenio'
import cuponesApi from '../../services/Apis/BackEnd/codigosBarra'


export default {
    namespaced: true,
    state: {
        deudor: null,
        comentarios: null,
        transacciones: null,
        telefonos: null,
        redes: null,
        correos: null,
        politica: null,
        direcciones: null,
        vinculos: null,
        facturasPorIdDeudor: [],
        conveniosPorIdDeudor: [],
        pagosPorIdDeudor: [],
        deudores: null,
        deudaActualizada: {
            monto: 0,
            interes: 0
        },
        cuponesPorIdDeudor: []
    },

    actions: {
        async obtenerCuponesPorIdDeudor({commit}, id_deudor) {
            const response = await cuponesApi.obtenerCuponesPorDeudor(id_deudor)
            if(response.data){
                commit('actualizarCupones', response.data)
            }else if(response.error){
                console.log(response.error.response)
            }

        },
        async obtenerFacturasPorIdDeudor({commit}, id_deudor) {
            try {
                let header = { 'token': this.state.auth.token }
                let configuracion = { headers: header }
                const data = await axios.get(url.facturas.urlConsultar + '?id_deudor=' + id_deudor, configuracion)
                commit('cargarFacturasPorIdDeudor', data.data)
            } catch (error) {
                console.log("Error GET: " + error)
            }
        },
        async obtenerPagosPorIdDeudor({commit}, id_deudor) {
            try {
                let header = { 'token': this.state.auth.token }
                let configuracion = { headers: header }
                const data = await axios.get(url.pagos.urlConsultar + '?id_deudor=' + id_deudor, configuracion)
                commit('cargarPagosPorIdDeudor', data.data)
            } catch (error) {
                console.log("Error GET: " + error)
            }
        },
        async obtenerDeudor({ commit }, id_deudor) {
            try {
                let header = { 'token': this.state.auth.token }
                let configuracion = { headers: header }
                const data = await axios.get(url.deudor.urlConsultar + '?id=' + id_deudor, configuracion)
                commit('actualizarDeudor', data.data)
            } catch (error) {
                console.log("Error GET: " + error)
            }
        },
        async obtenerComentarios({ commit }, id_deudor) {
            try {
                let header = { 'token': this.state.auth.token }
                let configuracion = { headers: header }
                const data = await axios.get(url.comentarios.urlConsultar + '?id_deudor=' + id_deudor, configuracion)
                commit('actualizarComentarios', data.data)
            } catch (error) {
                console.log("Error GET: " + error)
            }
        },
        async obtenerTelefonos({ commit }, id_deudor) {
            const response = await telefonoApi.obtenerTelefonosPorIdDeudor(id_deudor)
            if(response.data){
                commit('actualizarTelefonos', response.data)
            }else if(response.error){
                console.log(response.error.response)
            }
        },
        async obtenerRedes({ commit }, id_deudor) {
            try {
                let header = { 'token': this.state.auth.token }
                let configuracion = { headers: header }
                const data = await axios.get(url.redesSociales.urlConsultar + '?id_deudor=' + id_deudor, configuracion)
                commit('actualizarRedes', data.data)
            } catch (error) {
                console.log("Error GET: " + error)
            }
        },
        async obtenerCorreos({ commit }, id_deudor) {
            try {
                let header = { 'token': this.state.auth.token }
                let configuracion = { headers: header }
                const data = await axios.get(url.correos.urlConsultar + '?id_deudor=' + id_deudor, configuracion)
                commit('actualizarCorreos', data.data)
            } catch (error) {
                console.log("Error GET: " + error)
            }
        },
        async obtenerDirecciones({ commit }, id_deudor) {
            try {
                let header = { 'token': this.state.auth.token }
                let configuracion = { headers: header }
                const data = await axios.get(url.direcciones.urlConsultar + '?id_deudor=' + id_deudor, configuracion)
                commit('actualizarDirecciones', data.data)
            } catch (error) {
                console.log("Error GET: " + error)
            }
        },
        async obtenerDeudoresPorNroDocumento({ commit }, nro_doc) {
            try {
                let header = { 'token': this.state.auth.token }
                let configuracion = { headers: header }
                const data = await axios.get(url.deudor.urlConsultar + '?nro_documento=' + nro_doc, configuracion)
                commit('actualizarDeudores', data.data)
            } catch (error) {
                console.log("Error GET: " + error)
            }
        },
        async obtenerVinculos({ commit }, id_deudor) {
            try {
                let header = { 'token': this.state.auth.token }
                let configuracion = { headers: header }
                const data = await axios.get(url.vinculos.urlConsultar + '?id_deudor=' + id_deudor, configuracion)
                commit('actualizarVinculos', data.data)
            }catch (error) {
                console.log("Error GET: " + error)
            }
        },

        async obtenerTransacciones({ commit }, id_deudor) {
            try {
                let header = { 'token': this.state.auth.token }
                let configuracion = { headers: header }
                const data = await axios.get(url.transacciones.urlConsultar + '?id_deudor=' + id_deudor, configuracion)                
                commit('actualizarTransacciones', data.data)
            } catch (error) {
                console.log("Error GET: " + error)
            }
        },

        async obtenerDeudaActualizada({ commit },params) {
            const response = await deudorApi.actualizaDeuda(params.monto, params.id_tipo_actualizacion, params.fecha, params.id_iva, params.id_empresa, params.id_deudor)
            if(response.data){
                commit('actualizarDeuda', response.data)
            }else if(response.error){
                console.log(response.error.response)
            }
        },

        async obtenerConveniosPorIdDeudor({ commit }, id_deudor) {
            const response = await convenioApi.obtenerConvenioPorDeudor(id_deudor)
            if(response.data){
                commit('cargarConveniosPorIdDeudor', response.data)
            }else if(response.error){
                console.log(response.error.response)
            }
        },

        async limpiarDeudaActualizada({commit}){
            commit('limpiarDeudaActualizada', {monto: 0, interes: 0})
        }
    },

    mutations: {
        actualizarDeudor(state, deudor) {
            state.deudor = deudor
        },
        actualizarComentarios(state, comentarios) {
            state.comentarios = comentarios
        },
        actualizarTelefonos(state, telefonos) {
            state.telefonos = telefonos
        },
        actualizarCupones(state, cupones){
            state.cuponesPorIdDeudor = cupones
        },
        actualizarRedes(state, redes) {
            state.redes = redes
        },
        actualizarCorreos(state, correos) {
            state.correos = correos
        },
        actualizarDirecciones(state, direcciones) {
            state.direcciones = direcciones
        },
        actualizarVinculos(state, vinculos) {
            state.vinculos = vinculos
        }, 
        cargarFacturasPorIdDeudor(state, facturas) {
            state.facturasPorIdDeudor = facturas
        },
        cargarPagosPorIdDeudor(state, pagos) {
            state.pagosPorIdDeudor = pagos
        },        
        actualizarTransacciones(state, transacciones) {
            state.transacciones = transacciones
        },
        actualizarDeudores(state, deudores){
            state.deudores = deudores
        },
        actualizarDeuda(state, deudaActualizada){
            state.deudaActualizada.monto = state.deudaActualizada.monto + deudaActualizada.monto
            state.deudaActualizada.interes = state.deudaActualizada.interes + deudaActualizada.interes
        },
        limpiarDeudaActualizada(state, deudaActualizada){
            state.deudaActualizada = deudaActualizada
        },
        cargarConveniosPorIdDeudor(state, convenios) {
            state.conveniosPorIdDeudor = convenios
        },
    },

}