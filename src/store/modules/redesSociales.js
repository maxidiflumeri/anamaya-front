import url from '../../urls/index.js'
import axios from "axios"

export default {
  namespaced: true,
  state: {
  },

  actions: {
    async buscarRedesPorUsuario(context, usuario) {
      try {
        let header = { 'token': this.state.auth.token }
        let configuracion = { headers: header }
        let redesAux = {}
        try {
          const response = await axios.get(url.redesSociales.urlConsultar + '?usuario=' + usuario, configuracion)
          redesAux = response.data
        } catch (error) {
          console.log(error)
        }
        //console.log(redesAux)
        return redesAux
      }
      catch (error) {
        console.log(error)
        if (error) {
          this.errorLogin = true
        }
      }
    },
  },

  mutations: {

  },

}