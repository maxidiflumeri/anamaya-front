import url from '../../urls/index.js'
import axios from "axios"

export default {
  namespaced: true,
  state: {
    usuarios: null
  },

  actions: {    

    async obtenerUsuarios({ commit }) {
      try {
        let header = { 'token': this.state.auth.token }
        let configuracion = { headers: header }
        const data = await axios.get(url.usuarios.urlConsultar, configuracion)
        commit('actualizarUsuarios', data.data)
      } catch (error) {
        console.log("Error GET: " + error)
      }    
    },
  },

  mutations: {
    actualizarUsuarios(state, usuarios) {
      state.usuarios = usuarios
    }

  },

}