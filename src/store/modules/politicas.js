import url from '../../urls/index.js'
import axios from "axios"

export default {
  namespaced: true,
  state: {
    politica: null,
    politicas: null
  },

  actions: {    

    async obtenerPoliticas({ commit },) {
      try {
        let header = { 'token': this.state.auth.token }
        let configuracion = { headers: header }
        const data = await axios.get(url.politicas.urlConsultar, configuracion)
        commit('actualizarPoliticas', data.data)
      } catch (error) {
        console.log("Error GET: " + error)
      }    
    },

    async obtenerPoliticaPorId({ commit }, id_politica) {
      try {
        let header = { 'token': this.state.auth.token }
        let configuracion = { headers: header }
        const data = await axios.get(url.politicas.urlConsultar + '?id=' + id_politica, configuracion)
        commit('actualizarPolitica', data.data[0])
      } catch (error) {
        console.log("Error GET: " + error)
      }    
    },
  },

  mutations: {
    actualizarPolitica(state, politica) {
      state.politica = politica
    },
    actualizarPoliticas(state, politicas) {
      state.politicas = politicas
    },

  },

}