import url from '../../urls/index.js'
import axios from "axios"

export default {
  namespaced: true,
  state: {
    tiposDireccion: null
  },

  actions: {    

    async obtenerTiposDireccion({ commit }) {
      try {
        let header = { 'token': this.state.auth.token }
        let configuracion = { headers: header }
        const data = await axios.get(url.tiposDireccion.urlConsultar, configuracion)
        commit('actualizarTipo', data.data)
      } catch (error) {
        console.log("Error GET: " + error)
      }    
    },
  },

  mutations: {
    actualizarTipo(state, tipos) {
      state.tiposDireccion = tipos
    }

  },

}