import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

import operaciones from "./modules/operaciones.js"
import auth from "./modules/auth.js"
import deudores from "./modules/deudores.js"
import empresas from "./modules/empresas.js"
import telefonos from "./modules/telefonos.js"
import correos from "./modules/correos.js"
import redesSociales from "./modules/redesSociales.js"
import deudor from "./modules/deudor.js"
import politicas from "./modules/politicas.js"
import tiposCorreo from "./modules/tiposCorreo.js"
import tiposTelefono from "./modules/tiposTelefono.js"
import tiposRed from "./modules/tiposRed.js"
import tiposDireccion from "./modules/tiposDireccion.js"
import tiposVinculo from "./modules/tiposVinculo.js"
import adminTipos from "./modules/adminTipos.js"
import neotel from "./modules/neotel.js"
import agendasTc from "./modules/agendas_tc"
import usuarios from "./modules/usuarios"

export default new Vuex.Store({
  state: {},  
  mutations: {},
  actions: {},
  modules: { operaciones, auth, deudores, empresas, deudor, politicas, tiposCorreo, tiposTelefono, tiposRed, 
    telefonos, correos, redesSociales, adminTipos, tiposDireccion, tiposVinculo, neotel, agendasTc, usuarios}
});
