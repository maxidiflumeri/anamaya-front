const urlAgregar = 'api/tiposRedesSociales/agregar'
const urlEliminar = 'api/tiposRedesSociales/eliminar'
const urlModificar = 'api/tiposRedesSociales/modificar'
const urlConsultar = 'api/tiposRedesSociales/consultar'


export default{
    urlAgregar,
    urlEliminar,
    urlModificar,
    urlConsultar
}