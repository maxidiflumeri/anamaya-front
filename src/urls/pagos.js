const urlConsultar = 'api/pagos/consultar'
const urlAgregar = 'api/pagos/agregar'
const urlAnular = 'api/pagos/anular'

export default {
    urlConsultar,
    urlAgregar,
    urlAnular
}