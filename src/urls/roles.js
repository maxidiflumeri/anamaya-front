const urlAgregar = 'api/roles/agregar'
const urlEliminar = 'api/roles/eliminar'
const urlModificar = 'api/roles/modificar'
const urlConsultar = 'api/roles/consultar'


export default{
    urlAgregar,
    urlEliminar,
    urlModificar,
    urlConsultar
}