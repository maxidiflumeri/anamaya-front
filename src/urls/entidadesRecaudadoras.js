const urlAgregar = 'api/entidadesRecaudadoras/agregar'
const urlEliminar = 'api/entidadesRecaudadoras/eliminar'
const urlModificar = 'api/entidadesRecaudadoras/modificar'
const urlConsultar = 'api/entidadesRecaudadoras/consultar'


export default{
    urlAgregar,
    urlEliminar,
    urlModificar,
    urlConsultar
}