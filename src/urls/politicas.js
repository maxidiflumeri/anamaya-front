const urlAgregar = 'api/politicas/agregar'
const urlEliminar = 'api/politicas/eliminar'
const urlModificar = 'api/politicas/modificar'
const urlConsultar = 'api/politicas/consultar'


export default{
    urlAgregar,
    urlEliminar,
    urlModificar,
    urlConsultar
}