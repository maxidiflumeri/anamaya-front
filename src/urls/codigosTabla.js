const urlAgregar = 'api/codigosTabla/agregar'
const urlEliminar = 'api/codigosTabla/eliminar'
const urlModificar = 'api/codigosTabla/modificar'
const urlConsultar = 'api/codigosTabla/consultar'


export default{
    urlAgregar,
    urlEliminar,
    urlModificar,
    urlConsultar
}