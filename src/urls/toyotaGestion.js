const urlConsultar = 'api/toyotaGestion/consultar'
const urlAgregar = 'api/toyotaGestion/agregar'
const urlModificar = 'api/toyotaGestion/modificar'
const urlEliminar = 'api/toyotaGestion/eliminar'

export default {
    urlConsultar,
    urlAgregar,
    urlModificar,
    urlEliminar
}