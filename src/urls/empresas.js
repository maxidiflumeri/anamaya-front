const urlAgregar = 'api/empresas/agregar'
const urlEliminar = 'api/empresas/eliminar'
const urlModificar = 'api/empresas/modificar'
const urlConsultar = 'api/empresas/consultar'


export default{
    urlAgregar,
    urlEliminar,
    urlModificar,
    urlConsultar
}