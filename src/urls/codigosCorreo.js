const urlAgregar = 'api/codigosCorreo/agregar'
const urlEliminar = 'api/codigosCorreo/eliminar'
const urlModificar = 'api/codigosCorreo/modificar'
const urlConsultar = 'api/codigosCorreo/consultar'


export default{
    urlAgregar,
    urlEliminar,
    urlModificar,
    urlConsultar
}