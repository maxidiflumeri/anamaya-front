const urlConsultar = 'api/templateCorreos/consultar'
const urlAgregar = 'api/templateCorreos/agregar'
const urlModificar = 'api/templateCorreos/modificar'
const urlEliminar = 'api/templateCorreos/eliminar'

export default {
    urlConsultar,
    urlAgregar,
    urlModificar,
    urlEliminar
}