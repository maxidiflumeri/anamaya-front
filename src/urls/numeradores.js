const urlAgregar = 'api/numeradores/agregar'
const urlEliminar = 'api/numeradores/eliminar'
const urlModificar = 'api/numeradores/modificar'
const urlConsultar = 'api/numeradores/consultar'


export default{
    urlAgregar,
    urlEliminar,
    urlModificar,
    urlConsultar
}