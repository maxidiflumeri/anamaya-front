const urlAgregar = 'api/deudores/agregar'
const urlEliminar = 'api/deudores/eliminar'
const urlModificar = 'api/deudores/modificar'
const urlConsultar = 'api/deudores/consultar'


export default{
    urlAgregar,
    urlEliminar,
    urlModificar,
    urlConsultar
}