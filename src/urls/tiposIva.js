const urlAgregar = 'api/tiposIva/agregar'
const urlEliminar = 'api/tiposIva/eliminar'
const urlModificar = 'api/tiposIva/modificar'
const urlConsultar = 'api/tiposIva/consultar'

export default {
    urlAgregar,
    urlEliminar,
    urlModificar,
    urlConsultar
}