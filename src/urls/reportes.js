const urlBaseNeotel = 'api/procesos/baseDiscador'
const urlEstadistico = 'api/procesos/estadistico'
const urlArchivoUsoMultiple = 'api/procesos/archivoUsoMultiples'

export default {
    urlBaseNeotel,
    urlEstadistico,
    urlArchivoUsoMultiple
}