const urlAgregar = 'api/tiposPago/agregar'
const urlEliminar = 'api/tiposPago/eliminar'
const urlModificar = 'api/tiposPago/modificar'
const urlConsultar = 'api/tiposPago/consultar'


export default{
    urlAgregar,
    urlEliminar,
    urlModificar,
    urlConsultar
}