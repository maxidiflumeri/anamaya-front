const urlAgregar = 'api/tasasInteres/agregar'
const urlEliminar = 'api/tasasInteres/eliminar'
const urlModificar = 'api/tasasInteres/modificar'
const urlConsultar = 'api/tasasInteres/consultar'


export default{
    urlAgregar,
    urlEliminar,
    urlModificar,
    urlConsultar
}