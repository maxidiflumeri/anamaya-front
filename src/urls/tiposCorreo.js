const urlAgregar = 'api/tiposCorreo/agregar'
const urlEliminar = 'api/tiposCorreo/eliminar'
const urlModificar = 'api/tiposCorreo/modificar'
const urlConsultar = 'api/tiposCorreo/consultar'


export default{
    urlAgregar,
    urlEliminar,
    urlModificar,
    urlConsultar
}