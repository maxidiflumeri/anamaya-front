const urlAgregar = 'api/gruposTrabajo/agregar'
const urlEliminar = 'api/gruposTrabajo/eliminar'
const urlModificar = 'api/gruposTrabajo/modificar'
const urlConsultar = 'api/gruposTrabajo/consultar'

export default {
    urlAgregar,
    urlEliminar,
    urlModificar,
    urlConsultar
}