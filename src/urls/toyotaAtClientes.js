const urlConsultar = 'api/toyotaAtClientes/consultar'
const urlAgregar = 'api/toyotaAtClientes/agregar'
const urlModificar = 'api/toyotaAtClientes/modificar'
const urlEliminar = 'api/toyotaAtClientes/eliminar'

export default {
    urlConsultar,
    urlAgregar,
    urlModificar,
    urlEliminar
}