const urlAgregar = 'api/tiposActualizacion/agregar'
const urlEliminar = 'api/tiposActualizacion/eliminar'
const urlModificar = 'api/tiposActualizacion/modificar'
const urlConsultar = 'api/tiposActualizacion/consultar'


export default{
    urlAgregar,
    urlEliminar,
    urlModificar,
    urlConsultar
}