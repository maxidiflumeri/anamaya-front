const urlConsultar = 'api/direcciones/consultar'
const urlAgregar = 'api/direcciones/agregar'
const urlModificar = 'api/direcciones/modificar'
const urlEliminar = 'api/direcciones/eliminar'

export default {
    urlConsultar,
    urlAgregar,
    urlModificar,
    urlEliminar
}