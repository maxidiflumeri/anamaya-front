const urlAgregar = 'api/motivosnopago/agregar'
const urlEliminar = 'api/motivosnopago/eliminar'
const urlModificar = 'api/motivosnopago/modificar'
const urlConsultar = 'api/motivosnopago/consultar'


export default{
    urlAgregar,
    urlEliminar,
    urlModificar,
    urlConsultar
}