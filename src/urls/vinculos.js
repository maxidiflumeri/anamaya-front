const urlConsultar = 'api/vinculos/consultar'
const urlAgregar = 'api/vinculos/agregar'
const urlModificar = 'api/vinculos/modificar'
const urlEliminar = 'api/vinculos/eliminar'

export default {
    urlConsultar,
    urlAgregar,
    urlModificar,
    urlEliminar
}