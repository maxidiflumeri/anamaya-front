const urlAgregar = 'api/formasPago/agregar'
const urlEliminar = 'api/formasPago/eliminar'
const urlModificar = 'api/formasPago/modificar'
const urlConsultar = 'api/formasPago/consultar'


export default{
    urlAgregar,
    urlEliminar,
    urlModificar,
    urlConsultar
}