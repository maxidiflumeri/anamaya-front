const urlAgregar = 'api/tiposTelefonos/agregar'
const urlEliminar = 'api/tiposTelefonos/eliminar'
const urlModificar = 'api/tiposTelefonos/modificar'
const urlConsultar = 'api/tiposTelefonos/consultar'


export default{
    urlAgregar,
    urlEliminar,
    urlModificar,
    urlConsultar
}