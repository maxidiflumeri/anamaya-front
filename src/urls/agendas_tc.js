const urlAgregar = 'api/agendasTc/agregar'
const urlEliminar = 'api/agendasTc/eliminar'
const urlModificar = 'api/agendasTc/modificar'
const urlConsultar = 'api/agendasTc/consultar'


export default{
    urlAgregar,
    urlEliminar,
    urlModificar,
    urlConsultar
}