const urlConsultar = 'api/comentarios/consultar'
const urlAgregar = 'api/comentarios/agregar'

export default {
    urlConsultar,
    urlAgregar
}