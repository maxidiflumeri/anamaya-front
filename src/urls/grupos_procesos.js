const urlAgregar = 'api/gruposProcesos/agregar'
const urlEliminar = 'api/gruposProcesos/eliminar'
const urlModificar = 'api/gruposProcesos/modificar'
const urlConsultar = 'api/gruposProcesos/consultar'

export default {
    urlAgregar,
    urlEliminar,
    urlModificar,
    urlConsultar
}