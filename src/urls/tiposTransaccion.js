const urlAgregar = 'api/tiposTransaccion/agregar'
const urlEliminar = 'api/tiposTransaccion/eliminar'
const urlModificar = 'api/tiposTransaccion/modificar'
const urlConsultar = 'api/tiposTransaccion/consultar'


export default{
    urlAgregar,
    urlEliminar,
    urlModificar,
    urlConsultar
}