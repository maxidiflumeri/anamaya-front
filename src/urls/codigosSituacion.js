const urlAgregar = 'api/codigossituacion/agregar'
const urlEliminar = 'api/codigossituacion/eliminar'
const urlModificar = 'api/codigossituacion/modificar'
const urlConsultar = 'api/codigossituacion/consultar'


export default{
    urlAgregar,
    urlEliminar,
    urlModificar,
    urlConsultar
}