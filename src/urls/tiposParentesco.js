const urlAgregar = 'api/tiposParentesco/agregar'
const urlEliminar = 'api/tiposParentesco/eliminar'
const urlModificar = 'api/tiposParentesco/modificar'
const urlConsultar = 'api/tiposParentesco/consultar'


export default{
    urlAgregar,
    urlEliminar,
    urlModificar,
    urlConsultar
}