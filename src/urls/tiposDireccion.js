const urlAgregar = 'api/tiposDireccion/agregar'
const urlEliminar = 'api/tiposDireccion/eliminar'
const urlModificar = 'api/tiposDireccion/modificar'
const urlConsultar = 'api/tiposDireccion/consultar'


export default{
    urlAgregar,
    urlEliminar,
    urlModificar,
    urlConsultar
}