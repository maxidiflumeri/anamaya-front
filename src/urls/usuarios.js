const urlLogin = 'api/usuarios/login'
const urlAgregar = 'api/usuarios/agregar'
const urlEliminar = 'api/usuarios/eliminar'
const urlModificar = 'api/usuarios/modificar'
const urlConsultar = 'api/usuarios/consultar'
const urlModificarPassword = 'api/usuarios/modificarPassword'

export default{
    urlLogin,
    urlAgregar,
    urlEliminar,
    urlModificar,
    urlConsultar,
    urlModificarPassword
}