const urlConsultar = 'api/telefonos/consultar'
const urlAgregar = 'api/telefonos/agregar'
const urlModificar = 'api/telefonos/modificar'
const urlEliminar = 'api/telefonos/eliminar'

export default {
    urlConsultar,
    urlAgregar,
    urlModificar,
    urlEliminar
}