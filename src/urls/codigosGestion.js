const urlAgregar = 'api/codigosgestion/agregar'
const urlEliminar = 'api/codigosgestion/eliminar'
const urlModificar = 'api/codigosgestion/modificar'
const urlConsultar = 'api/codigosgestion/consultar'


export default{
    urlAgregar,
    urlEliminar,
    urlModificar,
    urlConsultar
}