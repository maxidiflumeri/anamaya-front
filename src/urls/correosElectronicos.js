const urlConsultar = 'api/correos/consultar'
const urlAgregar = 'api/correos/agregar'
const urlModificar = 'api/correos/modificar'
const urlEliminar = 'api/correos/eliminar'

export default {
    urlConsultar,
    urlAgregar,
    urlModificar,
    urlEliminar
}