const urlConsultar = 'api/deudores/consultar'
const urlModificar = 'api/deudores/modificar'
const urlActualizaDeuda = 'api/deudores/actualizaDeuda'

export default{    
    urlConsultar,
    urlModificar,
    urlActualizaDeuda
}