const urlAgregar = 'api/itemsProcesos/agregar'
const urlEliminar = 'api/itemsProcesos/eliminar'
const urlModificar = 'api/itemsProcesos/modificar'
const urlConsultar = 'api/itemsProcesos/consultar'

export default {
    urlAgregar,
    urlEliminar,
    urlModificar,
    urlConsultar
}