const urlConsultar = 'api/transacciones/consultar'
const urlAgregar = 'api/transacciones/agregar'

export default {
    urlConsultar,
    urlAgregar
}