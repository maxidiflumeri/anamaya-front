import usuarios from "./usuarios"
import empresas from "./empresas"
import deudores from "./deudores"
import comentarios from "./comentarios"
import correos from "./correosElectronicos"
import politicas from "./politicas"
import tiposCorreo from "./tiposCorreo"
import tiposTelefono from "./tiposTelefono"
import tiposRed from "./tiposRed"
import codigosGestion from "./codigosGestion"
import codigosSituacion from "./codigosSituacion"
import motivosNoPago from "./motivosNoPago"
import empresasParam from "./empresasParam"
import telefonos from "./telefonos"
import redesSociales from "./redesSociales"
import deudor from "./deudor.js"
import gruposProcesos from "./grupos_procesos"
import itemsProcesos from "./items_procesos"
import roles from "./roles"
import direcciones from "./direcciones"
import vinculos from "./vinculos"
import tiposDireccion from "./tiposDireccion"
import tiposVinculo from "./tiposVinculo"
import facturas from "./facturas"
import pagos from "./pagos"
import transacciones from "./transacciones"
import tasasInteres from "./tasasInteres"
import codigosCorreo from "./codigosCorreo"
import tiposPago from "./tiposPago"
import tiposConvenio from "./tiposConvenio"
import tiposActualizacion from "./tiposActualizacion"
import tiposTransaccion from "./tiposTransaccion"
import tiposParentesco from "./tiposParentesco"
import codigosTabla from "./codigosTabla"
import numeradores from "./numeradores"
import entidadesRecaudadoras from "./entidadesRecaudadoras"
import formasPago from "./formasDePago"
import gruposTrabajo from "./gruposTrabajo"
import tiposIva from "./tiposIva"   
import templateCorreos from './templateCorreos'
import convenios from './convenios'
import codigosBarra from './codigosBarra'
import reportes from './reportes'
import agendasTc from './agendas_tc'
import toyotaAtClientes from './toyotaAtClientes'
import toyotaGestion from "./toyotaGestion"


export default {
    usuarios,
    empresas,
    deudores,    
    correos,
    redesSociales,
    deudor,
    comentarios,
    telefonos,    
    politicas,
    tiposCorreo,
    tiposTelefono,
    tiposRed,
    codigosGestion,
    codigosSituacion,
    motivosNoPago,
    empresasParam,
    gruposProcesos,
    itemsProcesos,
    roles,
    vinculos,
    direcciones,
    tiposDireccion,
    tiposVinculo,
    facturas,
    pagos,    
    transacciones,
    tasasInteres,
    codigosCorreo,
    tiposPago,
    tiposConvenio,
    tiposActualizacion,
    tiposTransaccion,
    tiposParentesco,
    codigosTabla,
    numeradores,
    entidadesRecaudadoras,
    formasPago,
    gruposTrabajo,
    tiposIva,
    templateCorreos,
    convenios,
    codigosBarra,
    reportes,
    agendasTc,
    toyotaAtClientes,
    toyotaGestion
}