const urlAgregar = 'api/tiposConvenio/agregar'
const urlEliminar = 'api/tiposConvenio/eliminar'
const urlModificar = 'api/tiposConvenio/modificar'
const urlConsultar = 'api/tiposConvenio/consultar'


export default{
    urlAgregar,
    urlEliminar,
    urlModificar,
    urlConsultar
}