const urlAgregar = 'api/convenios/agregar'
const urlEliminar = 'api/convenios/eliminar'
const urlModificar = 'api/convenios/modificar'
const urlConsultar = 'api/convenios/consultar'
const urlAnular = 'api/convenios/anular'


export default{
    urlAgregar,
    urlEliminar,
    urlModificar,
    urlConsultar,
    urlAnular
}