const urlAgregar = 'api/empresasparam/agregar'
const urlEliminar = 'api/empresasparam/eliminar'
const urlModificar = 'api/empresasparam/modificar'
const urlConsultar = 'api/empresasparam/consultar'


export default{
    urlAgregar,
    urlEliminar,
    urlModificar,
    urlConsultar
}