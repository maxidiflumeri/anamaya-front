const urlAgregar = 'api/codigosBarra/agregar'
const urlEliminar = 'api/codigosBarra/eliminar'
const urlModificar = 'api/codigosBarra/modificar'
const urlConsultar = 'api/codigosBarra/consultar'


export default{
    urlAgregar,
    urlEliminar,
    urlModificar,
    urlConsultar
}