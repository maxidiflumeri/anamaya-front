import Vue from 'vue'
import VueMeta from 'vue-meta'
import VueRouter from 'vue-router'
import store from './store/index'

import Login from './components/Login/index.vue'
import Navbar from './components/Navbar/index.vue'
import Footer from './components/Footer/index.vue'
import PantallaPrincipal from './components/PantallaPpal/ContenedorPantallaPpal/index.vue'
import SegundoFiltro from './components/SegundoFiltro/index.vue'
import CrmMenu from "./components/Crm_Tabs/CrmMenu/index.vue"
import CrmGestion from "./components/Crm_Tabs/CrmGestion/index.vue"
import CrmOtrasCuentas from "./components/Crm_Tabs/CrmOtrasCuentas/index.vue"
import CrmFacturas from "./components/Crm_Tabs/CrmFacturas/index.vue"
import CrmPagos from "./components/Crm_Tabs/CrmPagos/index.vue"
import CrmConvenios from "./components/Crm_Tabs/CrmConvenios/index.vue"
import CrmDatosComplementarios from "./components/Crm_Tabs/CrmDatosComplementarios/index.vue"
import CrmProcesos from "./components/Crm_Tabs/CrmProcesos/index.vue"
import CrmPoliticas from "./components/Crm_Tabs/CrmPoliticas/index.vue"
import CrmScript from "./components/Crm_Tabs/CrmScript/index.vue"
import CrmTransacciones from "./components/Crm_Tabs/CrmTransacciones/index.vue"
import BuscadorDeudor from './components/BuscadorDeudor/index.vue'
import MiPerfil from './components/MiPerfil/index.vue'
import AdmCodigosCorreo from './components/Administracion/CodigosCorreo/index.vue'
import AdmCodigosDeGestion from './components/Administracion/CodigoDeGestion/index.vue'
import AdmCodigosDeSituacion from './components/Administracion/CodigoDeSituacion/index.vue'
import AdmCodigosTabla from './components/Administracion/CodigosTabla/index.vue'
import AdmEmpresasParam from './components/Administracion/EmpresasParam/index.vue'
import AdmEntidadesRecaudadoras from './components/Administracion/EntidadesRecaudadoras/index.vue'
import AdmFormasDePago from './components/Administracion/FormasDePago/index.vue'
import AdmGruposDeTrabajo from './components/Administracion/GruposDeTrabajo/index.vue'
import AdmMotivoNoPago from './components/Administracion/MotivoNoPago/index.vue'
import AdmNumeradores from './components/Administracion/Numeradores/index.vue'
import AdmRedesSociales from './components/Administracion/RedesSociales/index.vue'
import AdmTasasInteres from './components/Administracion/TasasInteres/index.vue'
import AdmTiposActualizacion from './components/Administracion/TiposActualizacion/index.vue'
import AdmTiposConvenio from './components/Administracion/TiposConvenio/index.vue'
import AdmTiposCorreo from './components/Administracion/TiposCorreo/index.vue'
import AdmTiposDireccion from './components/Administracion/TiposDireccion/index.vue'
import AdmTiposIva from './components/Administracion/TiposIva/index.vue'
import AdmTiposPago from './components/Administracion/TiposPago/index.vue'
import AdmTiposParentesco from './components/Administracion/TiposParentesco/index.vue'
import AdmTiposTelefono from './components/Administracion/TiposTelefono/index.vue'
import AdmTiposTransaccion from './components/Administracion/TiposTransaccion/index.vue'
import AdmTemplateCorreos from './components/Administracion/TemplateCorreos/index.vue'
import RepBaseNeotel from './components/Reportes/baseNeotel/index.vue'
import RepEstadisticoPagos from './components/Reportes/estadisticoPagos/index.vue'
import RepArchivoUsoMultiple from './components/Reportes/archivoUsoMultiple/index.vue'
import SupLogsReportes from './components/Supervision/logsReportes/index.vue'
import AdmPoliticas from './components/Administracion/PoliticasParam/index.vue'
import PagosConTarjeta from './components/PagosConTarjeta/index.vue'
import AdminUsuarios from './components/Administracion/AdminUsuarios/index.vue'


Vue.use(VueRouter)
Vue.use(VueMeta)


const routes = [
    {
        path: '/',
        redirect: '/PantallaPrincipal',
        beforeEnter(to, from, next) {
            store.dispatch('auth/autoLogin')
            if (store.state.auth.usuario) {
                if (store.state.auth.usuario.rol >= 0) {
                    next()
                } else {
                    alert('Usuario no autorizado para acceder a la ruta')
                }
            } else {
                this.$router.push({path: '/Login'})
            }
        }
    },
    {
        path: '/Login',
        name: 'Login',
        component: Login
    },
    {
        path: '/Navbar',
        component: Navbar,
        beforeEnter(to, from, next) {
            store.dispatch('auth/autoLogin')
            if (store.state.auth.usuario) {
                if (store.state.auth.usuario.rol >= 0) {
                    next()
                } else {
                    alert('Usuario no autorizado para acceder a la ruta')
                }
            } else {
                this.$router.push({path: '/Login'})
            }
        }
    },
    {
        path: '/Footer',
        component: Footer,
        beforeEnter(to, from, next) {
            store.dispatch('auth/autoLogin')
            if (store.state.auth.usuario) {
                if (store.state.auth.usuario.rol >= 0) {
                    next()
                } else {
                    alert('Usuario no autorizado para acceder a la ruta')
                }
            } else {
                this.$router.push({path: '/Login'})
            }
        }
    }, 
    {
        path: '/MiPerfil',
        component: MiPerfil,
        beforeEnter(to, from, next) {
            store.dispatch('auth/autoLogin')
            if (store.state.auth.usuario && store.state.auth.usuario.rol >= 0) {
                next()
            } else {
                alert('Usuario no autorizado para acceder a la ruta')
            }
        }
    }, 
    {
        path: '/PantallaPrincipal',
        name: "PantallaPrincipal",
        component: PantallaPrincipal,
        beforeEnter(to, from, next) {
            store.dispatch('auth/autoLogin')
            if (store.state.auth.usuario) {
                if (store.state.auth.usuario.rol >= 0) {
                    next()
                } else {
                    alert('Usuario no autorizado para acceder a la ruta')
                }
            } else {
                this.$router.push({path: '/Login'})
            }
        }
    }, 
    {
        path: '/SegundoFiltro/:idGrupo/',
        name: "SegundoFiltro",
        component: SegundoFiltro,
        props: true,
        beforeEnter(to, from, next) {
            store.dispatch('auth/autoLogin')
            if (store.state.auth.usuario) {
                if (store.state.auth.usuario.rol >= 0) {
                    next()
                } else {
                    alert('Usuario no autorizado para acceder a la ruta')
                }
            } else {
                this.$router.push({path: '/Login'})
            }
        }
    }, 
    {
        path: "/CrmMenu/:id_deudor",
        component: CrmMenu,
        props: true,
        beforeEnter(to, from, next) {
            store.dispatch('auth/autoLogin')
            if (store.state.auth.usuario) {
                if (store.state.auth.usuario.rol >= 0) {
                    next()
                } else {
                    alert('Usuario no autorizado para acceder a la ruta')
                }
            } else {
                this.$router.push({path: '/Login'})
            }
        },

        children: [
            {
                path: "CrmGestion",
                component: CrmGestion,
                name: 'crmGestion',
                beforeEnter(to, from, next) {
                    store.dispatch('auth/autoLogin')
                    if (store.state.auth.usuario) {
                        if (store.state.auth.usuario.rol >= 1) {
                            next()
                        } else {
                            alert('Usuario no autorizado para acceder a la ruta')
                        }
                    } else {
                        this.$router.push({path: '/Login'})
                    }
                }
            },
            {
                path: "CrmOtrasCuentas",
                component: CrmOtrasCuentas,
                beforeEnter(to, from, next) {
                    store.dispatch('auth/autoLogin')
                    if (store.state.auth.usuario) {
                        if (store.state.auth.usuario.rol >= 1) {
                            next()
                        } else {
                            alert('Usuario no autorizado para acceder a la ruta')
                        }
                    } else {
                        this.$router.push({path: '/Login'})
                    }
                }
            },
            {
                path: "CrmFacturas",
                component: CrmFacturas,
                beforeEnter(to, from, next) {
                    store.dispatch('auth/autoLogin')
                    if (store.state.auth.usuario) {
                        if (store.state.auth.usuario.rol >= 1) {
                            next()
                        } else {
                            alert('Usuario no autorizado para acceder a la ruta')
                        }
                    } else {
                        this.$router.push({path: '/Login'})
                    }
                }
            },
            {
                path: "CrmPagos",
                component: CrmPagos,
                beforeEnter(to, from, next) {
                    store.dispatch('auth/autoLogin')
                    if (store.state.auth.usuario) {
                        if (store.state.auth.usuario.rol >= 1) {
                            next()
                        } else {
                            alert('Usuario no autorizado para acceder a la ruta')
                        }
                    } else {
                        this.$router.push({path: '/Login'})
                    }
                }
            }, 
            {
                path: "CrmConvenios",
                component: CrmConvenios,
                beforeEnter(to, from, next) {
                    store.dispatch('auth/autoLogin')
                    if (store.state.auth.usuario) {
                        if (store.state.auth.usuario.rol >= 1) {
                            next()
                        } else {
                            alert('Usuario no autorizado para acceder a la ruta')
                        }
                    } else {
                        this.$router.push({path: '/Login'})
                    }
                }
            }, 
            {
                path: "CrmDatosComplementarios",
                component: CrmDatosComplementarios,
                beforeEnter(to, from, next) {
                    store.dispatch('auth/autoLogin')
                    if (store.state.auth.usuario) {
                        if (store.state.auth.usuario.rol >= 1) {
                            next()
                        } else {
                            alert('Usuario no autorizado para acceder a la ruta')
                        }
                    } else {
                        this.$router.push({path: '/Login'})
                    }
                }
            }, 
            {
                path: "CrmProcesos",
                component: CrmProcesos,
                beforeEnter(to, from, next) {
                    store.dispatch('auth/autoLogin')
                    if (store.state.auth.usuario) {
                        if (store.state.auth.usuario.rol >= 1) {
                            next()
                        } else {
                            alert('Usuario no autorizado para acceder a la ruta')
                        }
                    } else {
                        this.$router.push({path: '/Login'})
                    }
                }
            }, 
            {
                path: "CrmPoliticas",
                component: CrmPoliticas,
                beforeEnter(to, from, next) {
                    store.dispatch('auth/autoLogin')
                    if (store.state.auth.usuario) {
                        if (store.state.auth.usuario.rol >= 1) {
                            next()
                        } else {
                            alert('Usuario no autorizado para acceder a la ruta')
                        }
                    } else {
                        this.$router.push({path: '/Login'})
                    }
                }
            }, 
            {
                path: "CrmScript",
                component: CrmScript,
                beforeEnter(to, from, next) {
                    store.dispatch('auth/autoLogin')
                    if (store.state.auth.usuario) {
                        if (store.state.auth.usuario.rol >= 1) {
                            next()
                        } else {
                            alert('Usuario no autorizado para acceder a la ruta')
                        }
                    } else {
                        this.$router.push({path: '/Login'})
                    }
                }
            }, 
            {
                path: "CrmTransacciones",
                component: CrmTransacciones,
                beforeEnter(to, from, next) {
                    store.dispatch('auth/autoLogin')
                    if (store.state.auth.usuario) {
                        if (store.state.auth.usuario.rol >= 1) {
                            next()
                        } else {
                            alert('Usuario no autorizado para acceder a la ruta')
                        }
                    } else {
                        this.$router.push({path: '/Login'})
                    }
                }
            }
        ]
    }, 
    {
        path: '/BuscadorDeudor',
        name: "BuscadorDeudor",
        component: BuscadorDeudor,
        beforeEnter(to, from, next) {
            store.dispatch('auth/autoLogin')
            if (store.state.auth.usuario) {
                if (store.state.auth.usuario.rol >= 0) {
                    next()
                } else {
                    alert('Usuario no autorizado para acceder a la ruta')
                }
            } else {
                this.$router.push({path: '/Login'})
            }
        }
    }, 
    {
        path: "/AdmCodigosDeGestion",
        name: "codigosDeGestion",
        beforeEnter(to, from, next) {
            store.dispatch('auth/autoLogin')
            if (store.state.auth.usuario) {
                if (store.state.auth.usuario.rol >= 5) {
                    next()
                } else {
                    alert('Usuario no autorizado para acceder a la ruta')
                }
            } else {
                this.$router.push({path: '/Login'})
            }
        },
        component: AdmCodigosDeGestion
    }, 
    {
        path: "/AdmCodigosCorreo",
        name: "codigosCorreo",
        component: AdmCodigosCorreo,
        beforeEnter(to, from, next) {
            store.dispatch('auth/autoLogin')
            if (store.state.auth.usuario) {
                if (store.state.auth.usuario.rol >= 5) {
                    next()
                } else {
                    alert('Usuario no autorizado para acceder a la ruta')
                }
            } else {
                this.$router.push({path: '/Login'})
            }
        }
    }, 
    {
        path: "/AdmCodigosDeSituacion",
        name: "codigosDeSituacion",
        component: AdmCodigosDeSituacion,
        beforeEnter(to, from, next) {
            store.dispatch('auth/autoLogin')
            if (store.state.auth.usuario) {
                if (store.state.auth.usuario.rol >= 5) {
                    next()
                } else {
                    alert('Usuario no autorizado para acceder a la ruta')
                }
            } else {
                this.$router.push({path: '/Login'})
            }
        }
    }, 
    {
        path: "/AdmCodigosTabla",
        name: "codigosTabla",
        component: AdmCodigosTabla,
        beforeEnter(to, from, next) {
            store.dispatch('auth/autoLogin')
            if (store.state.auth.usuario) {
                if (store.state.auth.usuario.rol >= 5) {
                    next()
                } else {
                    alert('Usuario no autorizado para acceder a la ruta')
                }
            } else {
                this.$router.push({path: '/Login'})
            }
        }
    }, 
    {
        path: "/AdmEmpresasParam",
        name: "empresasParam",
        component: AdmEmpresasParam,
        beforeEnter(to, from, next) {
            store.dispatch('auth/autoLogin')
            if (store.state.auth.usuario) {
                if (store.state.auth.usuario.rol >= 5) {
                    next()
                } else {
                    alert('Usuario no autorizado para acceder a la ruta')
                }
            } else {
                this.$router.push({path: '/Login'})
            }
        }
    }, 
    {
        path: "/AdmEntidadesRecaudadoras",
        name: "entidadesRecaudadoras",
        component: AdmEntidadesRecaudadoras,
        beforeEnter(to, from, next) {
            store.dispatch('auth/autoLogin')
            if (store.state.auth.usuario) {
                if (store.state.auth.usuario.rol >= 5) {
                    next()
                } else {
                    alert('Usuario no autorizado para acceder a la ruta')
                }
            } else {
                this.$router.push({path: '/Login'})
            }
        }
    }, 
    {
        path: "/AdmFormasDePago",
        name: "formasDePago",
        component: AdmFormasDePago,
        beforeEnter(to, from, next) {
            store.dispatch('auth/autoLogin')
            if (store.state.auth.usuario) {
                if (store.state.auth.usuario.rol >= 5) {
                    next()
                } else {
                    alert('Usuario no autorizado para acceder a la ruta')
                }
            } else {
                this.$router.push({path: '/Login'})
            }
        }
    }, 
    {
        path: "/AdmGruposDeTrabajo",
        name: "gruposDeTrabajo",
        component: AdmGruposDeTrabajo,
        beforeEnter(to, from, next) {
            store.dispatch('auth/autoLogin')
            if (store.state.auth.usuario) {
                if (store.state.auth.usuario.rol >= 5) {
                    next()
                } else {
                    alert('Usuario no autorizado para acceder a la ruta')
                }
            } else {
                this.$router.push({path: '/Login'})
            }
        }
    }, 
    {
        path: "/AdmMotivoNoPago",
        name: "motivosDeNoPago",
        component: AdmMotivoNoPago,
        beforeEnter(to, from, next) {
            store.dispatch('auth/autoLogin')
            if (store.state.auth.usuario) {
                if (store.state.auth.usuario.rol >= 5) {
                    next()
                } else {
                    alert('Usuario no autorizado para acceder a la ruta')
                }
            } else {
                this.$router.push({path: '/Login'})
            }
        }
    }, 
    {
        path: "/AdmNumeradores",
        name: "numeradores",
        component: AdmNumeradores,
        beforeEnter(to, from, next) {
            store.dispatch('auth/autoLogin')
            if (store.state.auth.usuario) {
                if (store.state.auth.usuario.rol >= 5) {
                    next()
                } else {
                    alert('Usuario no autorizado para acceder a la ruta')
                }
            } else {
                this.$router.push({path: '/Login'})
            }
        }
    }, 
    {
        path: "/AdmRedesSociales",
        name: "redesSociales",
        component: AdmRedesSociales,
        beforeEnter(to, from, next) {
            store.dispatch('auth/autoLogin')
            if (store.state.auth.usuario) {
                if (store.state.auth.usuario.rol >= 5) {
                    next()
                } else {
                    alert('Usuario no autorizado para acceder a la ruta')
                }
            } else {
                this.$router.push({path: '/Login'})
            }
        }
    }, 
    {
        path: "/AdmTiposActualizacion",
        name: "tiposActualizacion",
        component: AdmTiposActualizacion,
        beforeEnter(to, from, next) {
            store.dispatch('auth/autoLogin')
            if (store.state.auth.usuario) {
                if (store.state.auth.usuario.rol >= 5) {
                    next()
                } else {
                    alert('Usuario no autorizado para acceder a la ruta')
                }
            } else {
                this.$router.push({path: '/Login'})
            }
        }
    }, 
    {
        path: "/AdmTasasInteres",
        name: "tasasInteres",
        component: AdmTasasInteres,
        beforeEnter(to, from, next) {
            store.dispatch('auth/autoLogin')
            if (store.state.auth.usuario) {
                if (store.state.auth.usuario.rol >= 5) {
                    next()
                } else {
                    alert('Usuario no autorizado para acceder a la ruta')
                }
            } else {
                this.$router.push({path: '/Login'})
            }
        }
    }, 
    {
        path: "/AdmTiposConvenio",
        name: "tiposConvenio",
        component: AdmTiposConvenio,
        beforeEnter(to, from, next) {
            store.dispatch('auth/autoLogin')
            if (store.state.auth.usuario) {
                if (store.state.auth.usuario.rol >= 5) {
                    next()
                } else {
                    alert('Usuario no autorizado para acceder a la ruta')
                }
            } else {
                this.$router.push({path: '/Login'})
            }
        }
    }, 
    {
        path: "/AdmTiposCorreo",
        name: "tiposCorreo",
        component: AdmTiposCorreo,
        beforeEnter(to, from, next) {
            store.dispatch('auth/autoLogin')
            if (store.state.auth.usuario) {
                if (store.state.auth.usuario.rol >= 5) {
                    next()
                } else {
                    alert('Usuario no autorizado para acceder a la ruta')
                }
            } else {
                this.$router.push({path: '/Login'})
            }
        }
    }, 
    {
        path: "/AdmTiposDireccion",
        name: "tiposDireccion",
        component: AdmTiposDireccion,
        beforeEnter(to, from, next) {
            store.dispatch('auth/autoLogin')
            if (store.state.auth.usuario) {
                if (store.state.auth.usuario.rol >= 5) {
                    next()
                } else {
                    alert('Usuario no autorizado para acceder a la ruta')
                }
            } else {
                this.$router.push({path: '/Login'})
            }
        }
    }, 
    {
        path: "/AdmTiposIva",
        name: "tiposIva",
        component: AdmTiposIva,
        beforeEnter(to, from, next) {
            store.dispatch('auth/autoLogin')
            if (store.state.auth.usuario) {
                if (store.state.auth.usuario.rol >= 5) {
                    next()
                } else {
                    alert('Usuario no autorizado para acceder a la ruta')
                }
            } else {
                this.$router.push({path: '/Login'})
            }
        }
    }, 
    {
        path: "/AdmTiposPago",
        name: "tiposPago",
        component: AdmTiposPago,
        beforeEnter(to, from, next) {
            store.dispatch('auth/autoLogin')
            if (store.state.auth.usuario) {
                if (store.state.auth.usuario.rol >= 5) {
                    next()
                } else {
                    alert('Usuario no autorizado para acceder a la ruta')
                }
            } else {
                this.$router.push({path: '/Login'})
            }
        }
    }, 
    {
        path: "/AdmTiposParentesco",
        name: "tiposParentesco",
        component: AdmTiposParentesco,
        beforeEnter(to, from, next) {
            store.dispatch('auth/autoLogin')
            if (store.state.auth.usuario) {
                if (store.state.auth.usuario.rol >= 5) {
                    next()
                } else {
                    alert('Usuario no autorizado para acceder a la ruta')
                }
            } else {
                this.$router.push({path: '/Login'})
            }
        }
    }, 
    {
        path: "/AdmTiposTelefono",
        name: "tiposTelefono",
        component: AdmTiposTelefono,
        beforeEnter(to, from, next) {
            store.dispatch('auth/autoLogin')
            if (store.state.auth.usuario) {
                if (store.state.auth.usuario.rol >= 5) {
                    next()
                } else {
                    alert('Usuario no autorizado para acceder a la ruta')
                }
            } else {
                this.$router.push({path: '/Login'})
            }
        }
    }, 
    {
        path: "/AdmTiposTransaccion",
        name: "tiposTransaccion",
        component: AdmTiposTransaccion,
        beforeEnter(to, from, next) {
            store.dispatch('auth/autoLogin')
            if (store.state.auth.usuario) {
                if (store.state.auth.usuario.rol >= 5) {
                    next()
                } else {
                    alert('Usuario no autorizado para acceder a la ruta')
                }
            } else {
                this.$router.push({path: '/Login'})
            }
        }
    },
    {
        path: "/TemplateCorreos",
        name: "templateCorreos",
        component: AdmTemplateCorreos,
        beforeEnter(to, from, next) {
            store.dispatch('auth/autoLogin')
            if (store.state.auth.usuario) {
                if (store.state.auth.usuario.rol >= 3) {
                    next()
                } else {
                    alert('Usuario no autorizado para acceder a la ruta')
                }
            } else {
                this.$router.push({path: '/Login'})
            }
        }
    },
    {
        path: "/AdmPoliticas",
        name: "politicasParam",
        component: AdmPoliticas,
        beforeEnter(to, from, next) {
            store.dispatch('auth/autoLogin')
            if (store.state.auth.usuario) {
                if (store.state.auth.usuario.rol >= 3) {
                    next()
                } else {
                    alert('Usuario no autorizado para acceder a la ruta')
                }
            } else {
                this.$router.push({path: '/Login'})
            }
        }
    },
    {
        path: "/AdmUsuarios",
        name: "adminUsuarios",
        component: AdminUsuarios,
        beforeEnter(to, from, next) {
            store.dispatch('auth/autoLogin')
            if (store.state.auth.usuario) {
                if (store.state.auth.usuario.rol >= 5) {
                    next()
                } else {
                    alert('Usuario no autorizado para acceder a la ruta')
                }
            } else {
                this.$router.push({path: '/Login'})
            }
        }
    },
    {
        path: "/BaseNeotel",
        name: "baseNeotel",
        component: RepBaseNeotel,
        beforeEnter(to, from, next) {
            store.dispatch('auth/autoLogin')
            if (store.state.auth.usuario) {
                if (store.state.auth.usuario.rol >= 5) {
                    next()
                } else {
                    alert('Usuario no autorizado para acceder a la ruta')
                }
            } else {
                this.$router.push({path: '/Login'})
            }
        }
    },
    {
        path: "/EstadisticoPagos",
        name: "estadisticoPagos",
        component: RepEstadisticoPagos,
        beforeEnter(to, from, next) {
            store.dispatch('auth/autoLogin')
            if (store.state.auth.usuario) {
                if (store.state.auth.usuario.rol >= 5) {
                    next()
                } else {
                    alert('Usuario no autorizado para acceder a la ruta')
                }
            } else {
                this.$router.push({path: '/Login'})
            }
        }
    },
    {
        path: "/ArchivoUsoMultiple",
        name: "archivoUsoMultiple",
        component: RepArchivoUsoMultiple,
        beforeEnter(to, from, next) {
            store.dispatch('auth/autoLogin')
            if (store.state.auth.usuario) {
                if (store.state.auth.usuario.rol >= 5) {
                    next()
                } else {
                    alert('Usuario no autorizado para acceder a la ruta')
                }
            } else {
                this.$router.push({path: '/Login'})
            }
        }
    },
    {
        path: "/logsReportes",
        name: "logsReportes",
        component: SupLogsReportes,
        beforeEnter(to, from, next) {
            store.dispatch('auth/autoLogin')
            if (store.state.auth.usuario) {
                if (store.state.auth.usuario.rol >= 5) {
                    next()
                } else {
                    alert('Usuario no autorizado para acceder a la ruta')
                }
            } else {
                this.$router.push({path: '/Login'})
            }
        }
    },
    {
        path: "/pagosConTarjeta",
        name: "PagosConTarjeta",
        component: PagosConTarjeta,
        beforeEnter(to, from, next) {
            store.dispatch('auth/autoLogin')
            if (store.state.auth.usuario) {
                if (store.state.auth.usuario.rol >= 1) {
                    next()
                } else {
                    alert('Usuario no autorizado para acceder a la ruta')
                }
            } else {
                this.$router.push({path: '/Login'})
            }
        }
    },
]

const router = new VueRouter({mode: 'history', routes})

export default router
